################### ABOVE ALL ###################

If you have any trouble installing and playing this game, or if you just want to
contact the development team, send a mail (no need to be formal, and you can send it in French) to 
**ens-info-genielog-2015@lists.crans.org**

################### JAVA 8 ###################

This project requires java 1.8.
To check if you have it, you can enter in a terminal:
```java -version```

**To install Java 8 on Debian:**
 * Enter in a terminal:
 ```sudo nano /etc/apt/source.list```
 * Add at the end of the file :
  ```deb http://http.debian.net/debian jessie-backports main```
 * Ctrl-O
 * Enter
 * Ctrl-X
 * Enter in a terminal:
 ```sudo apt-get update```
  * Enter in a terminal:
  ```sudo apt-get -t jessie-backports install openjdk-8```

**To install Java 8 on Ubuntu 14.10 and later:**
 * Just enter in a terminal:
```sudo apt-get install openjdk-8```

**To install Java 8 on Ubuntu 14.04:**
 * Follow the instructions of: http://ubuntuhandbook.org/index.php/2015/01/install-openjdk-8-ubuntu-14-04-12-04-lts/


################### INSTALLATION AND REQUIRED FEATURES ###################

**To compile:**

    cd project folder
    make

**To run:**
* The graphical version:


    make play

* The terminal version:


    make play-term

**To compile and run:**
* The graphical version:


    make buildplay

* The terminal version:


    make buildplay-term

**To launch the tests :**

    make test

################### HOW TO PLAY ###################

1)Launch the game.

2)On the main panel, you may :
  * single player : starts the game in a single player mode
  * multi player :  enables to create/join a multi player mode
  * configuration : you may configure the keys used to play the game
  * IGP : stands for "in game programming" : you may configure your own A.I. for your game allies
  * exit : leaves the game

3)Default commands:

  **Graphical version:**
  * Move left    : left_arrow
  * Move right   : right_arrow
  * Move up      : up_arrow
  * Move down    : down_arrow
  * Attack       : a
  * Ability1     : z
  * Ability2     : e
  * Ability3     : r
  * Back to main : escape

  **Terminal version:**
  * Move left    : q
  * Move right   : d
  * Move up      : z
  * Move down    : s
  * Attack       : o
  * Ability1     : k
  * Ability2     : l
  * Ability3     : m
  * Back to main : escape

################### HOW TO GENERATE DOCUMENTATION ###################

To document the project, you need the package *doxygen*.
It will create a HTML documentation from the code.
To have inheritance graphs, please install *graphivz*.

To access the documentation, double-click on the *doc.html* file at the root of the project that
 will be generated/updated by running the commands below.

**English version :**
* With graphviz :


    make doc-html

* Without graphivz :


    make doc-html2

**French version :**
* With graphviz :


    make doc-html-fr

* Without graphivz :


    make doc-html2-fr

################### GOAL OF THE GAME ###################

Kill the monsters and reach the stairs !

################### DEVELOPMENT TEAM ###################

This game was created between september 2015 and january 2016.
It was designed in a software engineering course of M1 at ENS Cachan, France.
The following people were the developers :
* Arrighi Emmanuel
* Babonnaud William
* Beauseigneur Clément
* Begel Myriam
* Bogdan Ursu
* Cohen Louis
* Chouasne-Guillon Gwendoline
* Delcros Lucas
* Dupriez Thomas
* Garnier Remy
* Ghyselen Alexis
* Guerquin Arnaud
* Hilaire Mathieu
* Hocquet Guillaume
* Huot Mathieu
* Lopez Théodore
* Ramusat Yann
* Ringeade Clément
* Toussaint Etienne
* Thomas Colin
* Ursu Bogdan
