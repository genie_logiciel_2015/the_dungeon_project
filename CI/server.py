#!/usr/bin/python3

import common
import http.server
import subprocess
import sys,os,pwd,grp
import argparse

def ssh(host, command):
    cossh = subprocess.Popen(["ssh", "%s" % host, command], shell=False, stdout=subprocess.PIPE)
    print(cossh.stdout.readlines())


def run():
    PORT = 8080
    server_address = ("", PORT)

    server = http.server.HTTPServer
    handler = http.server.SimpleHTTPRequestHandler
    print("Serveur actif sur le port :", PORT)

    httpd = server(server_address, handler)
    while True:
        httpd.handle_request()
        ssh("01.dptinfo.ens-cachan.fr", "cd projet_genielog/the_dungeon_project/; git pull")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-fg", help="run the server in foreground", action="store_true")
    args = parser.parse_args()
    if args.fg:
        run()
        exit()
    # do the UNIX double-fork magic, see Stevens' "Advanced
    # Programming in the UNIX Environment" for details (ISBN 0201563177)
    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            sys.exit(0)
    except OSError as e:
        print("fork #1 failed: %d (%s)" % (e.errno, e.strerror),file=sys.stderr)
        sys.exit(1)

    # decouple from parent environment
    os.chdir("/")   #don't prevent unmounting....
    os.setsid()
    os.umask(0)

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent, print eventual PID before
            #print "Daemon PID %d" % pid
            open(common.PIDFILE, 'w').write("%d" % pid)
            sys.exit(0)
    except OSError as e:
        print("fork #2 failed: %d (%s)" % (e.errno, e.strerror),file=sys.stderr)
        sys.exit(1)

    #main loop
    run()
