#!/usr/bin/python3

# Import smtplib for the actual sending function
import smtplib, argparse

# Import the email modules we'll need
from email.mime.text import MIMEText

parser = argparse.ArgumentParser()
parser.add_argument("mail", help="mail of the receiver")
parser.add_argument("filename", help="file containing the corp of the mail")
args = parser.parse_args()

with open(args.filename) as fp:
    # Create a text/plain message
    msg = MIMEText(fp.read())

msg['From'] = 'the_dungeon_project@ci.org'
msg['To'] = args.mail
msg['Reply-To'] = 'ens-info-genielog-2015@lists.crans.org'
msg['Subject'] = 'Continuous Integration of the the_dungeon_project : Error'


# Send the message via our own SMTP server.
s = smtplib.SMTP('smtp.crans.org')
s.send_message(msg)
s.quit()
