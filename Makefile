# This is where you specify the necessary source files

# Program packages and files
#   - The packages should be the path inside your src directory. eg: package1 package2/package3

PACKAGEDIRS = ${shell find . -iname '*.java' | sed 's/\.\/\|[^/]*.java//g' | sort -g | uniq}

TESTPACKAGE = ${shell find . -name *Tests.java | sed 's/.\/src\/\|.java//g' | sed 's/\//./g'}

PARSER = src/ingame_programming/Script.jj

# Java compiler
JAVAC = javac
JVM = 1.8

# Directory for compiled binaries
# - trailing slash is important!
BIN = bin/

# Directory of source files
# - trailing slash is important!
SRC = src/

# The lib directory
LIB = lib/


# Java compiler flags
JAVAFLAGS = -g -d $(BIN) -cp $(SRC):$(LIB)* -target $(JVM) -source $(JVM)
WARNINGFLAGS = -Xlint:all,-unchecked,-serial

# Creating a .class file
COMPILE = $(JAVAC) $(JAVAFLAGS)

PACKAGEFILES = $(subst $(SRC), $(EMPTY), $(foreach DIR, $(PACKAGEDIRS), $(wildcard $(DIR)*.java)))
ALL_FILES = $(PACKAGEFILES)

# One of these should be the "main" class listed in Runfile
CLASS_FILES = $(subst $(SRC), $(BIN), $(ALL_FILES:.java=.class))

# Image directory subpath
IMG = assets/

IMAGEDIRS = ${shell cd $(SRC)$(IMG); ls -d */ | sed 's/\/\///g'}

# Other image files (assuming every image file is an PNG one)
IMAGEFILES = $(wildcard $(SRC)$(IMG)*.png $(SRC)$(IMG)*.gif $(SRC)$(IMG)*.txt)

# The first target is the one that is executed when you invoke
# "make".

.PHONY: all clean cleandoc hack genpars

all : hack genpars $(addprefix $(BIN), $(CLASS_FILES))
	@cp -r $(addprefix $(SRC)$(IMG), $(IMAGEDIRS)) $(IMAGEFILES) $(BIN)$(IMG)
	@cp -r $(LIB) $(BIN)

# The line describing the action starts with <TAB>
$(BIN)%.class : $(SRC)%.java
	@echo $<
	$(COMPILE) $<


# Hack pour bin/
hack :
	@mkdir -p bin/log;

buildplay: all
	cd bin/; java -cp $(LIB)*:. graphics.guiSkeleton.Igniter_Main

buildplay-term: all
	cd bin/; java -cp $(LIB)*:. graphics.termSkeleton.Igniter_Main_Term

# Generate parser
genpars: $(PARSER)
	@cd `dirname $<` ;find . -name *.jj | while read i; do java -cp lib/javacc-6.1.2.jar javacc $$i; done

test : all
	cd $(BIN); java -cp lib/*:. org.junit.runner.JUnitCore $(TESTPACKAGE)

clean :
	rm -rf $(BIN)

cleandoc:
	rm -rf doc.html
	rm -rf docs

#To generate the documentation
doc-html-fr :
	doxygen Doxyfiles-htmlFR
	ln -s -i docs/html/index.html doc.html

doc-html :
	doxygen Doxyfiles-html
	ln -s -i docs/html/index.html doc.html

doc-html2 :
	doxygen Doxyfiles-html-no-graphivz
	ln -s -i docs/html/index.html doc.html

doc-html2-fr:
	doxygen Doxyfiles-html-no-graphivzFR
	ln -s -i docs/html/index.html doc.html

play:
	cd bin/; java -cp $(LIB)*:. graphics.guiSkeleton.Igniter_Main

play-term:
	cd bin/; java -cp $(LIB)*:. graphics.termSkeleton.Igniter_Main_Term
