package gameloop;

import artificial_intelligence.AIControler;
import core.relayer.Relayer;
import core.relayer.Relayers;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.guiSkeleton.GraphicsMaster;
import logging.Logging;
import network.inner_shell.ClientConnection;
import network.inner_shell.LocalConnection;
import network.inner_shell.NetworkConnection;

/**
 * Created by dupriez on 12/11/15.
 * Some changes made by Guerquin on 17/12/15
 *
 * This class's purpose is to start the gameLoop with the initialized network connection.
 * and then to stop the game if a connection problem appears
 * 
 * @author Dupriez Thomas
 * @author Guerquin Arnaud
 * @author bogdanbear
 *
 */
public class GameStarter {

	
	static public void startGameSinglePlayer()
	{
		NetworkConnection[] localConnections= LocalConnection.createNew();
		NetworkConnection serverConnection = localConnections[0];
		NetworkConnection clientConnection = localConnections[1];
		LocalGameLoop.getInstance().setNetworkConnection(clientConnection);


		//we are not allowing the LocalGameLoop to function here, commented the next line
		//LocalGameLoop.getInstance().startGame(GameType.SINGLEPLAYER);
		//but we start the thread nonetheless
		if (!LocalGameLoop.getInstance().startedBefore())
			LocalGameLoop.getInstance().start();
		//this is start and not run like in the DummyServer, because here we are dealing with a thread
		ServerLoop dummyServerLoop=new ServerLoop(serverConnection);
		dummyServerLoop.start();
		//let the loop run now!
		LocalGameLoop.getInstance().startGame();
	}
	
	
	static public void startGameMultiplayerPlayer(String ip, int port)
	{
		NetworkConnection networkConnection=new ClientConnection(ip,port);
		LocalGameLoop.getInstance().setNetworkConnection(networkConnection);
		//Symmetric behavior, the same as with singlePlayerStartProcedure
		if (!LocalGameLoop.getInstance().startedBefore())
			LocalGameLoop.getInstance().start();
		//let the loop run now!
		LocalGameLoop.getInstance().startGame();
	}



	static public void leaveGame(/*definitely!*/ )
	{
		Logging.getInstance().getLogger().info("Leaving the game");
        AIControler.killAll();
		Relayers.killAll();
		LocalGameLoop.getInstance().getNetworkConnection().initiateConnectionEnd();
		LocalGameLoop.getInstance().pauseGame();
	}
	static public void initiateBrokenConnectionMeasures()
	{
		leaveGame();
		GraphicsMaster.getInstance().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOST_CONNECTION);
	}

	/**
	 * To be called by the gui when the player wants to launch a game
	 * @return the relayer of the instance the player is going to command
	 */
	static public Relayer startGameBackup() {
		return LocalGameLoop.getInstance().getFollowedRelayer();
	}
}
