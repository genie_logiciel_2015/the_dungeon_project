package gameloop;
import java.util.Objects;
import core.event.Event;


public class TimeEvent implements Comparable<TimeEvent>{
	
	private final Event event; // Event to be done
	private final int time; // Frame at which the event will take place
	
	
	/*
	 * QueueEvent are the elements composing the PriorityQueue in the GameLoop
	 * They are received from "network" (or otherwise in case of local game) 
	 * 
	 */
	
	public TimeEvent(Event event, int time){
		this.event = Objects.requireNonNull(event);
		this.time = time;
	}
	
	
	Event getEvent()
	{
		return event;
	}
	
	int getTime(){
		return time;
	}
	
	//CompareTo rend l'element Comparable pour la PriorityQueue
	@Override
	public int compareTo(TimeEvent e2)
	{
		return this.time - e2.time;
	}
	
	
}
