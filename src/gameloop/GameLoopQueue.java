package gameloop;

import java.util.concurrent.PriorityBlockingQueue;

import core.event.Event;

/**
 * Encapsulate the GameLoop's queue to dissimulate its type and some of its method.
 * @author Guerquin Arnaud
 *
 */
public class GameLoopQueue {

    final PriorityBlockingQueue<TimeEvent> queue = new PriorityBlockingQueue<>(100, TimeEvent::compareTo);

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	public TimeEvent poll() {
		return queue.poll();
	}

	public TimeEvent peek() {
		return queue.peek();
	}

	public void addEvent(Event event, int frame) {
		queue.add(new TimeEvent(event, frame));
		
	}
}
