package gameloop;

import artificial_intelligence.AIControler;
import core.event.Event;
import core.event.MapInit;
import core.gamestate.*;
import core.relayer.Relayers;
import logging.Logging;
import map_generation.map.Map;
import map_generation.map.MapGeneration;
import map_generation.map.MapSize;
import network.helpers.EndEvent;
import network.inner_shell.NetworkConnection;
import network.inner_shell.ServerConnection;
import network.outer_shell.Lounge;

/**
 * This might also be a LocalServerLoop, it is just a thread object that cannot and should not be reused
 * This is not a singleton anymore
 * @author Guerquin Arnaud
 * @author Ursu Bodgan
 */
public class ServerLoop extends Thread {
    private NetworkConnection networkConnection;
    public final static long frame = 33;

    private GameLoopQueue queue=new GameLoopQueue();

    private Map nextMap;

    public ServerLoop(NetworkConnection serverConnection) {
        this.networkConnection=serverConnection;
    }

	public synchronized NetworkConnection getNetworkConnection()
	{
		return networkConnection;
	}
	private static ServerLoop ourInstance = new ServerLoop();
	public static ServerLoop getInstance() {
		return ourInstance;
	}
	private ServerLoop() {
	}
    int cframe = 0;
    private GameContent load(){
    	Map newMap= MapGeneration.mapGeneration(MapSize.SMALL);
    	nextMap= MapGeneration.mapGeneration(MapSize.SMALL);
    	int playerNumber;
    	if (networkConnection instanceof ServerConnection)
    		playerNumber= Lounge.MAX_PLAYERS;
    	else
    		playerNumber=1;
        GameContent gameContent=null;
        try{
        gameContent=new GameContent(newMap,playerNumber);
        }catch(InterruptedException e){
            throw new IllegalStateException(e.getMessage());
        }
        if (networkConnection instanceof ServerConnection) {
    		ServerConnection serverNetworkConnection =(ServerConnection) networkConnection;
    		for(int i=0;i<playerNumber;i++){
    			Event initEvent = new MapInit(gameContent, gameContent.getPlayer(i));
    			serverNetworkConnection.sendEventToSpecificClient(initEvent, i);
    		}
    	}
    	else {
    		//we are dealing with a LocalConnection
    		Event initEvent = new MapInit(gameContent, gameContent.getPlayer(0));
    		networkConnection.sendEvent(initEvent);
    	}
    	return gameContent;
    }

    @Override
    public void run()
    {
    	GameContent gameContent = load();

    	long ltime = System.currentTimeMillis(); //lastTime, time executed
    	long ctime = System.currentTimeMillis(); //currentTime, real time of the program
    	boolean cont = true; //when the GameLoop stops
    	AIControler.startAll();
    	while (cont)
    	{
    		/*Add all event received to the queue*/
    		for(Event event=(Event)networkConnection.receiveEvent(false);event!=null;event=(Event)networkConnection.receiveEvent(false)){
    			queue.addEvent(event, cframe);
    			if (event instanceof EndEvent) {
    				Logging.getInstance().getLogger().info("Just received an EndEvent");
    				//this is so bad because if this thread sleeps for too long it's a disaster
    				cont=false;
					networkConnection.initiateConnectionEnd();
    			}
    		}
    		if(cont == false)
    			break;
    		ctime = System.currentTimeMillis();
    		if(ctime - ltime > frame)
    		{
                try {
					executeFrame(gameContent);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
    			ltime += frame;
    			cframe++;
    		}
    		try {
    			Thread.sleep(frame-System.currentTimeMillis()%frame);
    		} catch (InterruptedException e) {
    		}
    	}
		Logging.getInstance().getLogger().info("DummyServerThread is dying now"+Thread.currentThread().getId());
    	AIControler.killAll();
    	Relayers.killAll();

    }

    
    /*
     * Execute all the Event of the queue that are associated with the current frame
     * (Effectively, it executes all the Event of the queue)
     * */
   void executeFrame(GameContent gameContent) throws InterruptedException {
    	while (!(queue.isEmpty()) && queue.peek().getTime() == cframe) {
    		Event event = queue.poll().getEvent();
    		Event toBeSentToClientsEvent = event.apply(gameContent);
    		networkConnection.sendEvent(toBeSentToClientsEvent);
    	}
    	/*Once per frame, check the stairs and check the trigger*/
    	gameContent.checkTrigger(networkConnection);
    	if(gameContent.checkStairs(nextMap,networkConnection))
    		nextMap=MapGeneration.mapGeneration(MapSize.MEDIUM);
    }


    public int getCurrentFrame(){
    	return cframe;
    }
}
