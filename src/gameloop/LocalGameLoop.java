package gameloop;

import core.event.Event;
import core.event.MapChangeEvent;
import core.event.MapInit;
import core.event.ToClientDeathEvent;
import core.gamestate.Entity;
import core.gamestate.Character;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import core.relayer.Relayers;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import logging.Logging;
import network.helpers.EndEvent;
import network.helpers.NetworkObject;
import network.inner_shell.NetworkConnection;


import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 * Created by bogdanbear on 22/11/2015.
 Singleton class for the LocalGameLoop
 */
public class LocalGameLoop extends Thread{
    private NetworkConnection networkConnection;
    private GameContent gameContent;
    //this is the counter which gives us the ID of the game
    private int gameCounterID=0;

    private Semaphore relayerSemaphore = new Semaphore(1, true);
    private Logger LOGGER = Logging.getInstance().getLogger();

    private Relayer followedRelayer;
    private boolean isPlaying=false;

    public Relayer getFollowedRelayer() {
        try {
            relayerSemaphore.acquire();
            LOGGER.info("Acquired relayer "+ followedRelayer+ " by "+currentThread().getId());
            return followedRelayer;
        } catch (InterruptedException e) {
            LOGGER.warning("Problem with acquiring relayer...");
        }finally {
            //finally is almost always executed, unless an Interrupted exception or a System.exit appeared
            LOGGER.info("Relayer is "+followedRelayer);
            relayerSemaphore.release();
            LOGGER.info("Released relayer  by "+currentThread().getId());
        }

        return followedRelayer;
    }

    public synchronized void setFollowedRelayer(Relayer followedRelayer) {
        this.followedRelayer = followedRelayer;
    }
    public synchronized void startGame()
    {
        try {
            relayerSemaphore.acquire();
        } catch (InterruptedException e) {
            LOGGER.info("Semaphore acquired!");
        }
        LOGGER.info("Game Flags Set to True");
        isPlaying=true;
        gameCounterID++;
    }

    public synchronized void setNetworkConnection(NetworkConnection networkConnection)
    {
        this.networkConnection=networkConnection;
    }

    /**
     * We are recycling this thread between plays, this means that we need a way of knowing when to call start()
     * and when not to call it
     * @return
     */
    public synchronized boolean startedBefore()
    {
        LOGGER.info("Run Count "+gameCounterID);
        if (gameCounterID>0)
            return true;
        return false;
    }

    public synchronized void setStartedBefore()
    {
        gameCounterID=1;
    }


    /**
     * pauses the execution of the gameLoop
     */
    public synchronized void pauseGame()
    {
        isPlaying=false;
        //do not release here
        //relayerSemaphore.release();
        LOGGER.info("Released relayer");
    }


    /**
     * a separate thread which will verify periodically whether the network connection is still alive
     * wakes up every 2 seconds, thus it is not a performance issue
     * it would have been nice to not have this running when we are in Single Player
     */
    private Runnable BrokenConnectionVerifier=new Runnable() {
        @Override
        public void run() {
            while (true) {

                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (LocalGameLoop.getInstance().getIsPlaying()) {
                    //System.out.println("Broken Connection verifier!");
                    if (getNetworkConnection().isBroken())
                        GameStarter.initiateBrokenConnectionMeasures();
                }

            }
        }
    };

    public synchronized boolean getIsPlaying()
    {
        return isPlaying;
    }

    //default visibility for the getter, should be inaccessible to entities etc.
    synchronized NetworkConnection getNetworkConnection()
    {
        return networkConnection;
    }
    public synchronized void setContent(GameContent gameContent1)
    {
        gameContent=gameContent1;
    }
    public synchronized GameContent getContent()
    {
        return gameContent;
    }
    private static LocalGameLoop ourInstance = new LocalGameLoop();
    public static LocalGameLoop getInstance() {
        return ourInstance;
    }
    private LocalGameLoop() {
    }

    @Override
    public void run() {
        //start the thread which verifies if the connection is still alive
        (new Thread(BrokenConnectionVerifier)).start();
        LOGGER.info("LocalGameLoop Thread started+"+currentThread().getId());
        while (true) {
            //System.out.println("DummyLocalGameLoopThread, game is "+isPlaying);
            if (getIsPlaying()) {
                //initially this is false!
                //setShouldRPC(false);
                LOGGER.info("DummyLocalGameLoopThread,"+currentThread().getId()+ "game is "+isPlaying);
                Event eventToReceive = null;
                LOGGER.info("The game is on!");
                while (!((eventToReceive = (Event) getNetworkConnection().receiveEvent(true)) instanceof EndEvent)) {
                    //System.out.println("LocalGameLoop Thread: We have received an event!applying...+"+eventToReceive);
                    try {
                        eventToReceive.apply(gameContent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (eventToReceive instanceof MapInit) {
                        GameContent gc = LocalGameLoop.getInstance().getContent();
                        Entity e = ((MapInit) eventToReceive).getFollowedDummyEntity();
                        if(!(e instanceof Character)){
                        	throw new RuntimeException("Player isn't a Character");
                        }
                        Relayer firstEntityRelayer = Relayers.addFollowedRelayer((Character) e);
                        setFollowedRelayer(firstEntityRelayer);
                        relayerSemaphore.release();
                    }
                    if(eventToReceive instanceof MapChangeEvent){
                        GraphicsMasterAbstraction.getInstance().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_WIN);
                    }
                    if (eventToReceive instanceof ToClientDeathEvent && ((ToClientDeathEvent) eventToReceive).getEntityID()==followedRelayer.getCharacter().getID()) {

                        GraphicsMasterAbstraction.getInstance().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_OVER);
                    }
                }
                if (eventToReceive instanceof EndEvent)
                    LOGGER.info("End Event Received!");
            }
            else {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Entities, relayers and objects related to the GameContent should not have a direct reference to the network connection
     * this is why I insert here a sendEvent method
     * it is inefficient to test whether the connection is alive here
     */
    public void sendEvent(NetworkObject event)
    {
        getNetworkConnection().sendEvent(event);
    }

    /**
     * for compatibility reasons when changing visibility of getNetworkConnection(),
     * we need to implement the following function
     * **/
    public boolean networkConnectionIsNull()
    {
        return(getNetworkConnection()==null);
    }


}
