package assets;

/**
 * Created by dupriez on 08/11/15.
 *
 * Provides a fixed point in the assets folder, used when loading sprites from this folder
 *
 */
public class
		UsedForLoadingSprites {
	private static UsedForLoadingSprites ourInstance = new UsedForLoadingSprites();

	public static UsedForLoadingSprites getInstance() {
		return ourInstance;
	}

	private UsedForLoadingSprites() {
	}
}
