package network.helpers;

import core.event.Event;
import core.gamestate.GameContent;


/**
 * Created by bogdanbear on 29/10/2015.
 * just an event used for testing purposes in the junit tests
 */

public class TestEvent implements Event{
    private String testMessage;
    public TestEvent(String string)
    {
        setTestMessage(string);
    }
    @Override
	public boolean execute(GameContent gameContent) {
        return true;
    }

	@Override
	public Event resolve(GameContent gameContent) {
		return this;
	}

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }
}
