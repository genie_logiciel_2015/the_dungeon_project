package network.helpers;

import core.event.Event;
import core.gamestate.GameContent;

/**
 * Created by bogdanbear on 11/11/2015.
 * Event used for orderly connection closure
 */
public class EndEvent implements Event{
    @Override
    public boolean execute(GameContent gameContent) {
        return false;
    }

    @Override
    public Event resolve(GameContent gameContent) {
        return null;
    }
}
