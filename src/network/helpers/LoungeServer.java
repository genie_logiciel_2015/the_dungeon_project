package network.helpers;

import gameloop.ServerLoop;
import logging.Logging;
import network.inner_shell.ServerConnection;
import network.outer_shell.Lounge;


/**
 * Created by bogdanbear on 20/11/2015.
 * a wrapper of the ServerConnection which can be run by a thread and which allows thread synchronization
 */
public class LoungeServer extends Waitable implements Runnable{
    private Lounge lounge;
    private int numberOfPlayers;
    public LoungeServer(Lounge lounge, int numberOfPlayers)
    {
        this.lounge=lounge;
        this.numberOfPlayers=numberOfPlayers;
    }
    @Override
    public void run()
    {
        ServerConnection serverConnection=new ServerConnection(lounge.getPort(),lounge.MAX_PLAYERS,0,this);
        Logging.getInstance().getLogger().info("Server started second phase, initialization done");

        ServerLoop dummyServerLoop=new ServerLoop(serverConnection);
        dummyServerLoop.start();

    }

}
