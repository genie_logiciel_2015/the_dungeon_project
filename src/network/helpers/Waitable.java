package network.helpers;

/**
 * Created by bogdanbear on 06/01/2016.
 * this abstract class is for objects which can be used for thread synchronization using notify and wait
 * since these require that the object monitor is acquired, we have to use synchronized methods
 */
public abstract class Waitable {
    /**
     * we need this wrapper because notify must be called inside a synchronized method
     * to do - implement this and waitOnIt() as an interface.
     */
    public synchronized void notifyOnIt()
    {
        notify();
    }

    /**
     * just wait on this object till notified by another thread
     */
    public synchronized void waitOnIt()
    {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
