package network.helpers;

import logging.Logging;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Created by bogdanbear on 11/12/2015.
 * this interface is one of the main classes of the networks package
 * every object we want to send (in game) should implement this interface
 */
public interface NetworkObject extends Serializable {
    Logger LOGGER = Logging.getInstance().getLogger();
}
