package network.outer_shell.server;

import logging.Logging;
import network.helpers.LoungeServer;
import network.outer_shell.Lounge;
import network.outer_shell.RegisterInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * Created by bogdanbear on 02/01/2016.
 * This class provides implementations for the methods defined in the RegisterInterfaces - this code will
 * be run by the client on the server
 * note on sunchronized methods - does not prevent simultaneous remote calls for two JVM (two clients)
 but it works for one. (the pair client-server)
 */


public class Registrar extends UnicastRemoteObject implements RegisterInterface {
    private HashMap<Long, Lounge> ongoing;
    private HashMap<Long, Lounge> waiting;

    private HashSet<Long> identifiers;
    private HashSet<Integer> portIdentifiers;

    private Logger LOGGER = Logging.getInstance().getLogger();

    public Registrar() throws RemoteException {
        waiting = new HashMap<Long, Lounge>();
        ongoing = new HashMap<Long, Lounge>();
        identifiers = new HashSet<Long>();
        portIdentifiers = new HashSet<Integer>();
    }
    public synchronized long generateId()
    {
        long gen = ((long) (Math.random() * Long.MAX_VALUE));
        while (identifiers.contains(gen))
            gen++;
        return gen;
    }

    public synchronized int generatePort()
    {
        //generate random port between 1024 and 65535;
        int gen = 1024+((int) (Math.random() * 64512));
        while (portIdentifiers.contains(gen))
            gen++;
        return gen;
    }

    /**
     * This is a method used for testing purposes
     * @throws RemoteException
     */
    @Override
    public synchronized void test() throws RemoteException {
        Logging.getInstance().getLogger().info("A client has invoked the test method");
    }

    /**
     * called by a remote client who wants to create a lounge on the server
     * @param name is the name of the lounge
     * @return returns an object which contains all the parameters of the lounge
     * @throws RemoteException
     */
    @Override
    public synchronized Lounge createLounge(String name) throws RemoteException {
        LOGGER.info("RPCServer: Somebody called createLounge method");
        long genID = generateId();
        identifiers.add(genID);
        int genPort=generatePort();
        portIdentifiers.add(genPort);
        Lounge lounge=new Lounge(genID,name,genPort);
        waiting.put(genID, lounge);
        //instantiate a loungeServer on the serverside and then put it into motion
        LoungeServer loungeServer=new LoungeServer(lounge,lounge.MAX_PLAYERS);
        (new Thread(loungeServer)).start();
        loungeServer.waitOnIt();


        //after obtaining the genID the host will immediately launch joinGame
        return lounge;
    }

    /**
     * Method called by a client who wants to retrieve the list of all lounges
     * @return an arraylist containing all the lounges for which the game has not started yet
     * @throws RemoteException
     */

    @Override
    public synchronized ArrayList<Lounge> getWaiting() throws RemoteException {
        ArrayList<Lounge> temp = new ArrayList<Lounge>();
        for (Lounge lounge : waiting.values()) {
            temp.add(lounge);
        }
        LOGGER.info("Registrar: Someone asked for waiting list, which is "+temp.toString());
        return temp;
    }

    /**
     * method through which a client joins a lounge, simple implementation
     * @param id the id of the lounge we wish to join
     * @return a boolean value representing whether joining the lounge was successull or not
     * @throws RemoteException
     */
    @Override
    public synchronized boolean joinLounge(long id) throws RemoteException {
        LOGGER.info("someone is joining game "+id);
        if (!waiting.containsKey(id))
            return false;
        else {
            Lounge targetLounge = waiting.get(id);
            boolean result=targetLounge.addPlayer();
            if (result==true && targetLounge.isFull())
            {
                waiting.remove(id);
                ongoing.put(id, targetLounge);
            }
            return result;
        }
    }
}
