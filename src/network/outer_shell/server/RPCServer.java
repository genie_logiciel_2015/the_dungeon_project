package network.outer_shell.server;

import logging.Logging;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Logger;

/**
 * Created by bogdanbear on 02/01/2016.
 * Main class for the RPC server which ensure lounge management
 * Creates a registry at port 1099 and exports method declarations, definitions are provided by the Registrar class
 */
public class RPCServer {
    public static int scale=25;
    private static Logger LOGGER = Logging.getInstance().getLogger();
    public static void startServer()
    {
        try {
            LocateRegistry.createRegistry(1099);
            Naming.rebind("Registrar", new Registrar());
            LOGGER.info("RPCServer: Server is ready.");
        } catch (RemoteException |MalformedURLException e) {
            LOGGER.warning("RPCServer: Server failed: " + e);
        }
    }
    public static void main(String[] arguments)
    {
        startServer();
    }
}

