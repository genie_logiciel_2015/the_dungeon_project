package network.outer_shell;


import logging.Logging;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by bogdanbear on 04/01/2016.
 * This class contains methods which perform the actual RMI calls to the server
 */
public class RPCClient {
    private static RegisterInterface registrar;
    private static String ip="127.0.0.1";
    private static int port=1099;
    private static Logger LOGGER = Logging.getInstance().getLogger();
    //private static ArrayList<Lounge> LoungeList=new ArrayList<Lounge>();

    /**
     * Create a lounge on the remote server
     * @param loungeName
     * @return a Lounge object which contains all the parameters that define a lounge
     */
    public static Lounge createLounge(String loungeName) throws RemoteException, MalformedURLException, NotBoundException {
        long id=0;
        RegisterInterface registrar = (RegisterInterface) Naming.lookup("//" + ip + ":" + port + "/Registrar");
        Lounge resultLounge=registrar.createLounge(loungeName);
        resultLounge.addPlayer();
        return resultLounge;
    }

    /**
     * this method has been used for testing to check that connections are made correctly
     */
    public static void connect() throws RemoteException, NotBoundException, MalformedURLException {
        LOGGER.info("RPCClient: Start test to //"+ip+"  "+port);
        registrar = (RegisterInterface) Naming.lookup("//"+ip+":"+port+"/Registrar");
        LOGGER.info("RPCClient: Lookup finished, begin testing");
        registrar.test();
        LOGGER.info("RPCClient: Success");
    }

    /**
     * Method on the RMI client side for joining a lounge.
     * @param id - the id of the lounge we wish to join
     * @return a boolean representing whether the operation has been accepted by the server and we are allowed to join
     * the game or not
     */
    public static boolean joinLounge(long id) throws RemoteException, MalformedURLException, NotBoundException {
        RegisterInterface registrar=(RegisterInterface) Naming.lookup("//"+ip+":"+port+"/Registrar");
        boolean lever=registrar.joinLounge(id);
        return lever;
    }

    /**
     * method used to grab the lounge list from the server, will be used by the gui to display available rooms
     * @return an arraylist of all the lounges which are waiting for new players
     */
    public static ArrayList<Lounge> getLoungeList() throws RemoteException, NotBoundException, MalformedURLException {
        registrar = (RegisterInterface) Naming.lookup("//"+ip+":"+port+"/Registrar");
        ArrayList<Lounge> allLounges=registrar.getWaiting();
        //LoungeList=allLounges;
        return allLounges;
    }

    public static String getIp() { return ip; }

    public static void setIp(String ip) {
        RPCClient.ip = ip;
        LOGGER.info("RCP IP : "+RPCClient.getIp());
    }

//    /**
//     * @return the last known lounges list, without making a RPC call
//     */
//    public static ArrayList<Lounge> getLoungeListWithoutRPC() {
//        return LoungeList;
//    }
}
