package network.outer_shell;

import java.io.Serializable;

/**
 * Created by bogdanbear on 03/01/2016.
 * Class which contains all the meta-parameters of a game,
 * like port, id, number of free places etc.
 */
public class Lounge implements Serializable {
    public static final int MAX_PLAYERS=3;
    private int occupiedSpots=0;
    private String name;
    private long loungeID;
    private int port;
    public Lounge(long loungeID, String name, int port)
    {
        this.setLoungeID(loungeID);
        this.name=name;
        this.port=port;
    }

    public long getLoungeID() {
        return loungeID;
    }

    public void setLoungeID(long loungeID) {
        this.loungeID = loungeID;
    }
    public int  numberFreeSpots()
    {
        return MAX_PLAYERS-occupiedSpots;
    }

    /**
     * add a new player to the lounge by increasing player counter
     * @return a boolean representing whether the operation has been executed or not
     */
    public boolean addPlayer()
    {
        if (occupiedSpots<MAX_PLAYERS)
            occupiedSpots++;
        else return false;
        return true;
    }

    public boolean isFull()
    {
        return (occupiedSpots==MAX_PLAYERS);
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }
}
