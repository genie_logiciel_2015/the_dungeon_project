package network.outer_shell;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by bogdanbear on 02/01/2016.
 * interface which provides the definitions of the methods which may be called by an rpc client
 */

public interface RegisterInterface extends Remote{
    public void test() throws RemoteException;
    public Lounge createLounge(String name) throws RemoteException;
    public ArrayList<Lounge> getWaiting()throws RemoteException;
    public boolean joinLounge(long id) throws RemoteException;
}
