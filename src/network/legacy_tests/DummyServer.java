package network.legacy_tests;

import gameloop.ServerLoop;
import logging.Logging;
import network.inner_shell.ServerConnection;


/**
 * Created by bogdanbear on 20/11/2015.
 */
public class DummyServer {
    public static void main(String[] args)
    {
        ServerConnection serverConnection=new ServerConnection(8888,2,0,null);
        Logging.getInstance().getLogger().info("Server started second phase, initialization done");

        ServerLoop dummyServerLoop=new ServerLoop(serverConnection);
        dummyServerLoop.start();
    }
}
