package network.legacy_tests;

import core.event.Event;
import logging.Logging;
import network.helpers.TestEvent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by bogdanbear on 28/10/2015.
 */

//A client that surely works
public class DumbClient {
    static private ObjectOutputStream outputStream;
    static private ObjectInputStream inputStream;
    public static void main(String [] args) throws IOException, ClassNotFoundException {
        String  serverAddress = "127.0.0.1";
        int PORT = 8888;
        Socket socket = null;
        try
        {
            socket = new Socket(serverAddress , PORT);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            //UnitTest sending
            Event tester=new TestEvent("Just a echero simple test!");
            outputStream.writeObject(tester);

            Event tester2=(Event)inputStream.readObject();
            TestEvent tester3=(TestEvent)tester2;
            System.out.println(tester3.getTestMessage());

        }
        catch (UnknownHostException e) {
            Logging.getInstance().getLogger().warning("Unable to locate server");
        }
        finally
        {
            if (outputStream!=null)
                outputStream.close();
            if(inputStream!=null)
                inputStream.close();
            if (socket != null)
                socket.close();
        }
    }
}
