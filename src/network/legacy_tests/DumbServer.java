package network.legacy_tests;

import core.event.Event;
import logging.Logging;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;


/**
 * Created by bogdanbear on 28/10/2015.
 * A Dumb Server that surely works
 */
public class DumbServer {
    public static final int PORT=8888;
    static ServerSocket serverSocket=null;
    static Logger LOGGER = Logging.getInstance().getLogger();
    public static void main(String[] littleStrings) throws IOException{
        try{
            serverSocket=new ServerSocket(PORT);
            while(true)
            {
                LOGGER.info("Waiting for clients...");
                Socket socket=serverSocket.accept();
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                try {
                    while (true) {
                        LOGGER.info("Talking to a dear client!");
                        Event event = (Event) inputStream.readObject();
                        LOGGER.info("We have read an event");
                        LOGGER.info(event.getClass().toString());
                        outputStream.writeObject(event);
                        LOGGER.info("We have written an event");
                    }
                }finally
                {
                    socket.close();
                }
            }
        } catch (ClassNotFoundException e) {
            LOGGER.warning("Problem with package inclusions in package network:class Server");
        } finally {
            serverSocket.close();
        }
    }
}
