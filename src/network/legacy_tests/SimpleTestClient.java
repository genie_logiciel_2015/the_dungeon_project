package network.legacy_tests;

import network.helpers.TestEvent;
import network.inner_shell.ClientConnection;

import java.io.*;

/**
 * Created by bogdanbear on 20/11/2015.
 * A client that uses ClientConnections to send Events to the server
 */
public class SimpleTestClient {
    public static void main(String [] args) throws IOException {
        ClientConnection clientConnection=new ClientConnection("127.0.0.1",8888);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //System.out.println("Type client name");
        //String name = bufferedReader.readLine();

        while(true)
        {
            /*
            while (clientConnection.available())
            {
                TestEvent event=(TestEvent)clientConnection.receiveEvent();
                System.out.println(event.testMessage);
            }
            */

            System.out.println("Your turn to write!");
            String string= bufferedReader.readLine();
            TestEvent testEvent=new TestEvent(string);
            clientConnection.sendEvent(testEvent);


            TestEvent event=null;
            while ((event=(TestEvent)clientConnection.receiveEvent(false))!=null)
            {
                System.out.println(event.getTestMessage());
            }

        }
    }
}
