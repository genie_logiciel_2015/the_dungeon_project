package network.legacy_tests;

import logging.Logging;
import network.helpers.NetworkObject;
import network.inner_shell.ServerConnection;

import java.util.logging.Logger;

/**
 * Created by bogdanbear on 20/11/2015.
 * this tests the serverside part of a NetworkConnection, the ServerConnection
 * SimpleTestClients should connect to it
 * also need to perform tests in which the server initiates the connection end
 */
public class SimpleTestServer {
    public static void main(String[] args)
    {
        //For the constructor, 8888 is the port and 2 is the number of expected clients
        ServerConnection serverConnection=new ServerConnection(8888,2,0,null);
        Logger LOGGER = Logging.getInstance().getLogger();
        while (true)
        {
            LOGGER.info("Server started second phase");
            //just read what was sent by clients and send it back
            NetworkObject event=serverConnection.receiveEvent(true);
            LOGGER.info("I have received the event: "/*+event.toString()*/);
            serverConnection.sendEvent(event);
        }
    }
}
