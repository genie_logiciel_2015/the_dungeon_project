package network.legacy_tests;

import network.helpers.TestEvent;
import network.inner_shell.ClientConnection;

import java.io.*;
/**
 * Created by bogdanbear on 30/11/2015.
 * Testing that the connection ends correctly - this will be made using JUnit testing in the future
 * when "end" is typed into the console, the client calls the InitiateConnectionEnd method
 * works by first starting SimpleTestServer
 */
public class ConnectionEndTestClient {
    public static void main(String [] args) throws IOException {
        ClientConnection clientConnection = new ClientConnection("127.0.0.1", 8888);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //System.out.println("Type client name");
        //String name = bufferedReader.readLine();

        while (true) {


            System.out.println("Your turn to write!");
            String string = bufferedReader.readLine();
            if (string!=null)
                if (string.equals("end")) {
                    clientConnection.initiateConnectionEnd();
                    break;
                }
            TestEvent testEvent = new TestEvent(string);
            clientConnection.sendEvent(testEvent);


            TestEvent event = null;
            while ((event = (TestEvent) clientConnection.receiveEvent(false)) != null) {
                System.out.println(event.getTestMessage());
            }


        }
    }
}