package network.inner_shell;


import logging.Logging;
import network.helpers.EndEvent;
import network.helpers.NetworkObject;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Logger;

/**
 * Thread which receive incoming data on the client.
 * Created by bogdanbear on 10/11/2015.
 * Still to be implemented - throwing exceptions when the connection is not open
 */
public class ClientInputThread extends Thread {
    private Socket socket = null;
    private NetworkConnection parent;
    private boolean serverside;
    private static Logger LOGGER = Logging.getInstance().getLogger();
    private int ID;

    /**
     * Initialization of the class.
     * @param socket socket of the connection
     * @param networkConnection instance of NetworkConnection, here a ClientConnection
     */
    public ClientInputThread(Socket socket, NetworkConnection networkConnection,boolean serverside,int ID) {
        this.socket = socket;
        parent=networkConnection;
        this.serverside=serverside;
        this.ID=ID;
    }

    /**
     * Receive input data.
     * Here are really received the new objects.
     * They are put to the end of the queue of received events.
     * The queue is specific for the client.
     * Finally close the socket.
     */
    @Override
    public void run() {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            while (true) {
                NetworkObject command = (NetworkObject) inputStream.readObject();
                //need to push event into the Event queue, but only if it is not an EndEvent
                if (command instanceof EndEvent) {
                    socket.close();
                    LOGGER.info("ClientInputThread ending its life naturally");
                    break;
                }
                parent.addIncomingEvent(command);
            }


        } catch (SocketException e1) {
            //this is here because of server threads, as of now the connection closure is asymmetric
            LOGGER.info("A socket has been closed on the other end, corresponding thread is useless and ends its life");

            //Append an end event! This will only have an effect on the client side, since on the server one if you receive an endevent
            //the thread stops executing
            if (serverside==false) {
                EndEvent endEvent = new EndEvent();
                parent.addIncomingEvent(endEvent);
                //we are allowed to do the following on the client side
                parent.setBroken(true);
            }
        } catch (EOFException e2){
            //this is here because of the client thread, see above
            LOGGER.info("A socket has been closed on the other end, corresponding thread is useless and ends its life");
            EndEvent endEvent=new EndEvent();
            parent.addIncomingEvent(endEvent);
            if (!serverside)
                parent.setBroken(true);
        } catch(ClassNotFoundException e1){
            //this is needed in order to correctly transmit objects through the network - both server and client must contain class definitions
            LOGGER.warning("Problem with package inclusions");
        } catch(Exception e2){
            LOGGER.severe(e2.toString());
        }

        finally
        {
            LOGGER.info("ClientInputThread dying");
            try {
                socket.close();
            } catch (IOException e1) {
                LOGGER.warning("Socket cannot be closed in thread "+ this.getId());
            }
        }
    }
}
