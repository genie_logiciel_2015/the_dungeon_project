package network.inner_shell;


import logging.Logging;
import network.helpers.NetworkObject;
import network.helpers.TestEvent;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Logger;

/**
 * Abstract class for all types of connection.
 * Created by bogdanbear on 20/11/2015.
 */
@SuppressWarnings("unchecked")
public abstract class NetworkConnection {
    protected BlockingDeque<NetworkObject> incomingEvents=new LinkedBlockingDeque<>();
    protected static Logger LOGGER = Logging.getInstance().getLogger();
    private boolean broken=false;

    /**
     * Function for testing whether an object is Serializable or not
     * @param event the argument must be a networkobject
     * @return a boolean representing whether our object is truly serializable or not
     */
    public boolean notSerializable(NetworkObject event)
    {
        try {
            new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(event);
        } catch (IOException e) {
            //Our Object is not Serializable!
            LOGGER.severe(e.toString());
            return true;
        }
        return false;
    }
    /**
     * Abstract method to close the connection properly.
     */
    public abstract void initiateConnectionEnd();

    /**
     * Abstract method to send an Event.
     * Can be from the server to the client or from the client to the server
     * @param event Event to send
     */
    public abstract void sendEvent(NetworkObject event);

    /**
     * Add incoming event to the queue.
     * Incoming events are in a queue to treat them. This method is a link between the reception of data and treatment.
     * A new event which is received is put to the end of incomingEvents
     * Only for use by ClientInputThreads, visibility is package.
     * For the moment, the behavior is identical between ServerConnection and ClientConnection
     * @param event event received to add at the end of the queue
     */
    synchronized void addIncomingEvent(NetworkObject event)
    {
        try {
            incomingEvents.putLast(event);
        } catch (InterruptedException e) {
            LOGGER.warning(e.toString());
        }
    }

    /**LOGGER.info(e);
     * Receive new event.
     * Only read and return the first event of incomingEvents
     * This is a blocking call, we can modify the blocking properties if necessary
     * @param blocking true for a blocking call to the queue
     * @return a new event or null if there is no new event
     */
    //making this method synchronized would be a grave error, since incoming events might induce a block inside this method
    @SuppressWarnings("unchecked")
    public NetworkObject receiveEvent(boolean blocking) {
        if (!blocking) {
            try {
                return incomingEvents.removeFirst();
            } catch (NoSuchElementException e) {
                //this means that there are no events in the queue, we return null
                return null;
            }
        }
        try {
            return incomingEvents.takeFirst();
        } catch (InterruptedException e) {
            LOGGER.warning(e.toString());
            return null;
        }
    }

    /**
     * tests whether a thread encountered a problem and updated the flag of the connection as broken
     * actively tests if the connection is not broken now
     * @return a boolean representing whether the connection is broken or not
     */
    public boolean isBroken() {
        //attempt to send a test event first
        //must not be sunchronized because broken might need to be updated when we attempt to send a test event
        NetworkObject testEvent = new TestEvent("this is just to test if no exceptions are being thrown");
        sendEvent(testEvent);
        //broken might have been updated after this.
        return broken;
    }

    /**
     * like isBroken(), but does not do an active test and just looks at whether a thread encountered a problem
     * this could be an alternative to using isBroken and the timer
     * by calling this every time before sending or receiving an event
     * @return a boolean with the value of broken
     */
    public boolean isSoftBroken(){
        return broken;
    }

    //package visibility
    synchronized void setBroken(boolean broken) {
        this.broken = broken;
    }
}

