package network.inner_shell;

import network.helpers.EndEvent;
import network.helpers.NetworkObject;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectOutputStream;
import java.net.Socket;
/**
 * Instance of the Network Connection for the client part.
 * Created by bogdanbear on 28/10/2015.
 */
public class ClientConnection extends NetworkConnection{
    private int PORT;
    private Socket socket;
    private ObjectOutputStream outputStream;

    /**
     * Establish the connection with the client.
     * A new socket is open and a new thread is launch for the input of the client.
     * the exception thrown here will most likely be a UnknownHostException rather than a simple IOException
     * @param serverAddress IP address for the connection to the client
     * @param port port for the connection to the client
     */
    public ClientConnection(String serverAddress, int port) {
        try {
            PORT = port;
            socket = new Socket(serverAddress, PORT);
            //inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            //the id of this thread is not important, since it is on the client side.
            ClientInputThread clientInputThread = new ClientInputThread(socket, this,false,0);
            clientInputThread.start();
            LOGGER.info("ClientConnection initialized");
        }
        catch (IOException e)
        {
            setBroken(true);
            LOGGER.warning(e.toString());
        }
    }

    /**
     * Send an event to the server.
     * The event is written on the socket.
     * Exceptions are not implemented per se, they are included into isBroken(), which can be tested before trying to read
     * or write(but that is not efficient)
     * @param e the event to send; e has to be serializable and not empty
     */
    @Override
    public synchronized void sendEvent(NetworkObject e)
    {
        if (e==null)
            LOGGER.warning("Please do not attempt to send null events!");
        else {
            if (notSerializable(e))
                throw new AssertionError("networks: Please don't try to use unserializable objects");
            try {
                outputStream.writeObject(e);
            } catch (IOException e1) {
                if (e1 instanceof NotSerializableException)
                    LOGGER.severe("Not SerializableObject");
                setBroken(true);
                LOGGER.warning(e1.toString());
            }
        }
    }
    /**
     * Close properly the connection of the client.
     */
    @Override
    public void initiateConnectionEnd()
    {
        LOGGER.info("network:Client has called initaiteConnectionEnd!");
        EndEvent endEvent=new EndEvent();
        sendEvent(endEvent);
        try {
            //we need to flush so that we ensure that the server receives the EndEvent
            socket.getOutputStream().flush();
        } catch (IOException e) {
            LOGGER.warning(e.toString());
        }
    }


}
