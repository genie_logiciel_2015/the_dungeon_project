package network.inner_shell;

import network.helpers.EndEvent;
import network.helpers.LoungeServer;
import network.helpers.NetworkObject;
import network.helpers.Waitable;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Instance of NetworkConnection for the server.
 * Created by bogdanbear on 28/10/2015.
 */
public class ServerConnection extends NetworkConnection{
    static ServerSocket serverSocket=null;
    /**
     * toNotify Object is used to notify a thread that the ServerConnection constructor is ready to accept clients
     */
    private Waitable toNotify;
    private boolean stillRunning=true;
    private boolean disableLateJoining=true;
    /**
     * this parameter gives the maximum number of clients allowed to join late
     */
    private int maxNumberOfLateClients;

    //this blocking deque stores all the events received from the clients, in a consistent order
    private BlockingDeque<NetworkObject> incomingEvents=new LinkedBlockingDeque<>();
    //a list of outgoing client threads
    private ArrayList<ClientOutputThread> outputThreads=new ArrayList<>();

    /**
     * Add an new client to the list of clients.
     * This list is used to send event to all clients.
     * Private method, for use only by the constructor.
     * @param thread Thread which identifies new client.
     */
    synchronized void addClientOutputThread(ClientOutputThread thread)
    {
        outputThreads.add(thread);
    }
    public synchronized boolean isStillRunning() {
        return stillRunning;
    }
    public synchronized void setStillRunning(boolean stillRunning) {
        this.stillRunning = stillRunning;
    }
    public int getMaxNumberOfLateClients() {
        return maxNumberOfLateClients;
    }

    /**
     * the following class was part of an attempt to allow client to join after the game has started
     * the network part works fine, in order to finish this implementation we also need to modify the mapinit system
     * slightly, in the sense that we need to add MapRequest events
     */
    private class ServerListeningThread extends Thread{
        private ServerConnection parent;
        private ServerSocket listeningSocket;
        public ServerListeningThread(ServerConnection parent, ServerSocket serverSocket){
            this.parent=parent;
            this.listeningSocket=serverSocket;
        }
        @Override
        public void run()
        {
            LOGGER.info("LateJoining thread is online!");
            int numberOfLateClients=0;
            while (isStillRunning()&&numberOfLateClients<parent.getMaxNumberOfLateClients())
            {
                Socket socket= null;
                try {
                    socket = listeningSocket.accept();
                    ClientInputThread clientInputThread=new ClientInputThread(socket,parent,true,incomingEvents.size());
                    ClientOutputThread clientOutputThread=new ClientOutputThread(socket,parent,outputThreads.size());
                    addClientOutputThread(clientOutputThread);
                    //create new thread and start it
                    clientOutputThread.start();
                    clientInputThread.start();
                    //increase number of clients
                    numberOfLateClients++;
                } catch (IOException e) {
                    LOGGER.warning(e.toString());
                }
            }
            LOGGER.info("LateJoining thread dies peacefully");
        }
    }

    /**
     * Initialize the class and instantiate all connections.
     * Create 2 threads for each client :
     *  - clientInputThread to receive events from the client (with a queue)
     *  - clientOutputThread to send events to the client (with a queue)
     * It waits for all clients before to end.
     * @param PORT port to communicate for the server
     * @param numberOfClients
     * @param toNotify is an object that will be notified when execution is complete, or null
     */
    public ServerConnection(int PORT, int numberOfClients, int maxNumberOfLateClients, Waitable toNotify){
        if (numberOfClients>0)
            disableLateJoining=false;
        this.maxNumberOfLateClients=maxNumberOfLateClients;
        try {
            serverSocket=new ServerSocket(PORT);
            LOGGER.info("Waiting for clients ...");
            int nr=0;
            while(nr<numberOfClients)
            {
                if (toNotify!=null)
                    toNotify.notifyOnIt();
                Socket socket=serverSocket.accept();
                ClientInputThread clientInputThread=new ClientInputThread(socket,this,true,incomingEvents.size());
                ClientOutputThread clientOutputThread=new ClientOutputThread(socket,this,outputThreads.size());

                addClientOutputThread(clientOutputThread);
                //create new thread and start it
                clientOutputThread.start();
                clientInputThread.start();
                LOGGER.info(nr+" clients in");
                nr++;
            }
            if (disableLateJoining==false)
            {
                ServerListeningThread serverListeningThread=new ServerListeningThread(this, serverSocket);
                serverListeningThread.start();
            }
        } catch (IOException e) {
            setBroken(true);
            LOGGER.warning(e.toString());
        }
    }

    /**
     * Send an event to all clients.
     * In extreme circumstances, if the outgoing queues become so full that they reach MaxINT number of elements,
     * this method will block - later I will make it throw an exception
     * For all clients, it add the event to the end of the outgoing queue of the client
     */
    @Override
    public synchronized void sendEvent(NetworkObject event) {
        if (event==null)
            LOGGER.warning("Please do not attempt to send null events!");
        else {
            if (notSerializable(event))
                throw new AssertionError("networks: Please don't try to use unserializable objects");
            try {
                for (int i = 0; i < outputThreads.size(); i++)
                    outputThreads.get(i).getOutgoingQueue().putLast(event);
            } catch (InterruptedException e) {
                LOGGER.warning(e.toString());
            }

        }
    }

    /**
     * Send a special event for the end of the game.
     * Method used for sending a special event which tells all the clients that the game is over
     */
    @Override
    public synchronized void initiateConnectionEnd()
    {
        LOGGER.info("network:Server has called initaiteConnectionEnd!");
        try {
            //first we stop listening to incoming clients, if we did that in the first place
            setStillRunning(false);
            EndEvent endEvent=new EndEvent();
            for (int i=0;i<outputThreads.size();i++)
                outputThreads.get(i).getOutgoingQueue().putLast(endEvent);
        } catch (InterruptedException e) {
            LOGGER.warning(e.toString());
        }
    }

    /**
     * Send an event to only one client.
     * Very powerful method, use with great caution since it might compromise the integrity of all the GameStates
     * because everybody has to get the GameState updated
     * Still to be implemented - throwing exceptions when the connection is not open
     */
    public synchronized void sendEventToSpecificClient(NetworkObject event, int id)
    {
        if (event==null)
            LOGGER.warning("Please do not attempt to send null events!");
        else {
            if (id >= outputThreads.size())
                LOGGER.severe("Bad Client Id "+id+" - client does not exist");
            else
                try {
                    outputThreads.get(id).getOutgoingQueue().putLast(event);
                } catch (InterruptedException e) {
                    LOGGER.warning(e.toString());
                }
        }
    }

    /**
     * maybe this will not be needed
     * return whether there exists at least a thread that is alive
     * @return a boolean representing whether the connection is broken or not
     * package visibility
     */
    @Override
    public boolean isBroken()
    {
        boolean broken=true;
        for (Thread thread:outputThreads)
            if (thread.isAlive())
                broken=false;
        return broken;
    }
}
