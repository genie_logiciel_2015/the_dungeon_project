package network.inner_shell;


import logging.Logging;
import network.helpers.NetworkObject;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Logger;

/**
 * This is the thread for a unique client where events are send by the server.
 * Every client has a ClientOutputThread with a queue and a socket.
 * It sends all the objects of the queue throw the socket
 *
 * Created by bogdanbear on 10/11/2015.
 */
public class ClientOutputThread extends Thread{
    private BlockingDeque<NetworkObject> outgoingEvents=new LinkedBlockingDeque<>();
    private Socket socket = null;
    private Logger LOGGER = Logging.getInstance().getLogger();
    private int ID;
    private NetworkConnection parent;
    //private ServerConnection parent;

    /**
     * @param serverConnection
     */
    public ClientOutputThread(Socket socket, ServerConnection serverConnection,int ID) {
        this.socket = socket;
        this.ID=ID;
        this.parent=serverConnection;
    }

    /**
     * Get the waiting queue of outgoing events.
     * With this, the server can add events to this queue
     * @return the queue of outgoing events
     */
    //method which will be used only by the ServerConnection, visibility is package
    BlockingDeque<NetworkObject> getOutgoingQueue()
    {
        return outgoingEvents;
    }

    /**
     * Write the objects from the queue on the socket.
     * It takes the first event in the queue so the older and send it to the client via the socket.
     */
    @Override
    public void run() {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            LOGGER.info("ClientOutputThread running");
            try {
                while (true)
                {
                    //retrieves and removes first element from the outgoing queue, blocking if necessary
                    NetworkObject event=outgoingEvents.takeFirst();
                    outputStream.writeObject(event);
                }
            } catch (InterruptedException e) {
                LOGGER.severe("Problem with interrupted exception in network:ClientOutputThreadClass");
            }

        } catch (IOException e1) {
            LOGGER.warning("ClientInputThread - IOException");
            //parent.setBroken(true); cannot do this because we do not know whether this is a client of server thread
        } finally {
            LOGGER.info("ClientOutputThread dying");
            try {
                socket.close();
            } catch (IOException e1) {
                LOGGER.warning("Socket cannot be closed in thread " + this.getId());
            }
        }
    }
}
