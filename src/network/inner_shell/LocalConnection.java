package network.inner_shell;

import network.helpers.EndEvent;
import network.helpers.NetworkObject;

import java.util.NoSuchElementException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by bogdanbear on 21/11/2015.
 * This is the class we are going to use for transparency over the network
 * incomingEvents will be used as the single event queue;
 * receive event behaves in the exact same way
 * In the next version we are going to remove sendEventOnServer() and receiveEventOnServer()
 * since they are not architecturally sound.
 */
@SuppressWarnings("unchecked")
public class LocalConnection extends NetworkConnection {
    protected BlockingDeque<NetworkObject> outgoingEvents;

    public LocalConnection(BlockingDeque<NetworkObject> incomingEvents, BlockingDeque<NetworkObject> outgoingEvents) {
        this.outgoingEvents = outgoingEvents;
        this.incomingEvents = incomingEvents;
    }

    /**
     * useless for now, can remain empty - the garbage collector will eliminate the LocalConnection if it is not in use anymore
     * it is not an optimization to clear the queues explicitly due to the behavior of the garbage collector
     */
    @Override
    public void initiateConnectionEnd() {
        LOGGER.info("network:LocalConnection has called initaiteConnectionEnd!");
        //serverIncomingEvents.clear();
        //incomingEvents.clear();
        EndEvent endEvent=new EndEvent();
        //sendEvent(endEvent); Cannot do that anymore because of typing
        try {
            outgoingEvents.putLast(endEvent);
        } catch (InterruptedException e) {
            LOGGER.warning("endEvent not send");
        }
    }

    @Override
    public void sendEvent(NetworkObject event) {
        if (event == null)
            LOGGER.warning("Please do not attempt to send null events!");
        else
        {
            if (notSerializable(event))
                throw new AssertionError("networks: Please don't try to use unserializable objects"+event);

            try {
                outgoingEvents.putLast(event);
            } catch (InterruptedException e) {
                LOGGER.warning(e.toString());
            }
        }
    }

    /**
     * receiveEvent for the client part
     * @param blocking true if the queue is in blocking mode
     * @return a new event
     */
    @Override
    public NetworkObject receiveEvent(boolean blocking) {
        if (!blocking) {
            try {
                return incomingEvents.removeFirst();
            } catch (NoSuchElementException e) {
                //this means that there are no events in the queue, we return null
                return null;
            }
        }
        try {
            return incomingEvents.takeFirst();
        } catch (InterruptedException e) {
            LOGGER.warning(e.toString());
            return null;
        }
    }

    /**
     * Initialization of the connection for single player.
     * Will create two objects LocalConnection : one for the server part and one for the client part.
     * @return the two LocalConnection, the first is the server one and the second is the client one.
     */
    public static NetworkConnection[] createNew() {
        LocalConnection[] bothConnections = new LocalConnection[2];
        BlockingDeque<NetworkObject> incomingForServer = new LinkedBlockingDeque<>();
        BlockingDeque<NetworkObject> incomingForClient = new LinkedBlockingDeque<>();
        LocalConnection serverConnection = new LocalConnection(incomingForServer, incomingForClient);
        LocalConnection clientConnection = new LocalConnection(incomingForClient, incomingForServer);
        bothConnections[0] = serverConnection;
        bothConnections[1] = clientConnection;
        return bothConnections;
    }
}