package network.junit_tests;


import network.inner_shell.LocalConnection;
import network.inner_shell.NetworkConnection;
import network.helpers.NetworkObject;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * Class for JUnits Test for the class LocalConnection.
 * Created by mymi on 19/12/15.
 * To begin, we use LocalConnection.createNew() to create a server and a client.
 * Then we test sendEvent on the server side and receiveEvent on the client side.
 * The second function will do the opposite.
 * Finally, we will test initiateConnectionEnd (TODO)
 */
@SuppressWarnings("unchecked")
public class LocalConnectionTest{
    NetworkConnection[] localConnections= LocalConnection.createNew();
    private NetworkConnection serverConnection = localConnections[0];
    private NetworkConnection clientConnection = localConnections[1];
    private NetworkObject event = new ObjectForTest();

    @Test
    public void testSendEventForServer(){
        serverConnection.sendEvent(event);
        NetworkObject receivedEvent = clientConnection.receiveEvent(false);
        assertEquals("Network : the object send by the server side is not received by the client side.",event,receivedEvent);
    }

    @Test
    public void testSendEventForClient(){
        clientConnection.sendEvent(event);
        NetworkObject receivedEvent = serverConnection.receiveEvent(false);
        assertEquals("Network : the object send by the client side is not received by the server side.",event,receivedEvent);
    }

    @Test
    public void testInitiateConnectionEnd(){}
}
