package network.junit_tests;

import network.inner_shell.ClientConnection;
import org.junit.*;
import org.junit.runners.MethodSorters;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import static org.junit.Assert.assertEquals;

/**
 * Class for JUnits test for the class ClientConnection.
 * Created by mymi on 19/12/15.
 * To begin we open the connection between a new socket and a ClientConnection.
 * Then we test the sendEvent function. The event received by our fake server must be thus send.
 * After that we test the initiateConnectionEnd (TOFINISH)
 * Finally we close the socket.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientConnectionTest{
    private static int PORT = 8888;
    private static ServerSocket serverSocket;
    private static ClientConnection client = new ClientConnection("127.0.0.1",8888);
    private static ObjectInputStream inputStream;
    private static ObjectOutputStream outputStream;
    private static Socket socket;
    ObjectForTest event = new ObjectForTest("Test OK");


    /**
     * Create a Server to communicate with the Client Connection.
     */
    @BeforeClass
    public static void createServer() {
        try {
            serverSocket = new ServerSocket(PORT);
            client = new ClientConnection("127.0.0.1", 8888);
            socket = serverSocket.accept();
            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Will use the method sendEvent of the client and will check that the send event is the same as the received one.
     */
    @Test
    public void test1SendEvent(){
        client.sendEvent(event);
        try {
            ObjectForTest receivedEvent = (ObjectForTest) inputStream.readObject();
            assertEquals("Network : ClientConnection.sendEvent doesn't send well the object",event.str,receivedEvent.str);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * The fake server send a OjectForTest and the client has to reveive the same.
     */
    @Test
    public void test1ReceiveEvent(){
        try {
            outputStream.writeObject(event);
            ObjectForTest receivedEvent = (ObjectForTest)client.receiveEvent(true);
            assertEquals("Network ServerConnection.receiveEvent failed",event.str,receivedEvent.str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Will test the function initiateConnectionEnd. (TODO)
     */
    @Test
    public void test2InitiateConnectionEnd(){
        //client.initiateConnectionEnd();
        //assertTrue("Network : ClientConnection.initiateConnectionEnd doesn't end the connection",socket.isClosed());
    }

    /**
     * Close the socket because initiateConnectionEnd is not applied.
     */
    @AfterClass
    public static void closeConnection(){
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
