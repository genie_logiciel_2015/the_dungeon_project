package network.junit_tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * TestSuite for the network package.
 * Will launch automatically all the JUnits tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ClientConnectionTest.class,
        ServerConnectionTest.class,
        LocalConnectionTest.class
})
public class NetworkTests{}