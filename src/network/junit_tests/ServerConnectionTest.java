package network.junit_tests;


import network.inner_shell.ServerConnection;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.model.TestTimedOutException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static org.junit.Assert.assertEquals;

/**
 * Class for JUnits test for the class ServerConnection.
 * Created by mymi on 20/12/15.
 */
@SuppressWarnings("unchecked")
public class ServerConnectionTest{
    private static ServerConnection server;
    private ObjectForTest e = new ObjectForTest("Server test ok");
    private static Client client1;
    private static Client client2;

    /**
     * Private class for the server to have a Thread.
     */
    private static class Server extends Thread{
        private int PORT;
        public Server(int port){
            PORT = port;
        }

        public void run(){
            server = new ServerConnection(PORT,2,0,null);
            notify();
        }

        public synchronized void waitOnIt()
        {
            try {
                wait();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Private class to have a thread for the client.
     */
    private static class Client extends Thread{
        Socket socketClient;
        ObjectInputStream inputStream;
        ObjectOutputStream outputStream;
        int PORT;

        public Client(int port){
            PORT = port;
        }

        public void run(){
            try {
                socketClient = new Socket("127.0.0.1", PORT);
                inputStream = new ObjectInputStream(socketClient.getInputStream());
                outputStream = new ObjectOutputStream(socketClient.getOutputStream());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        public ObjectInputStream getInputStream(){
            return inputStream;
        }

        public ObjectOutputStream getOutputStream(){
            return outputStream;
        }

        public void endConnection(){
            try{
                socketClient.close();
            } catch (IOException e1){
                e1.printStackTrace();
            }
        }

    }

    /**
     * At the beginning of the test, we establish all connections.
     * 2 clients for a ServerConnection.
     * It need some time to establish all connections that's why there is a sleep.
     */
    @BeforeClass
    public static void connecting() {
        Server connect = new Server(7777);
        connect.start();
        client1 = new Client(7777);
        client2 = new Client(7777);
        client1.start();
        client2.start();
        connect.waitOnIt();

    }
    /**
     * After all tests, we closed the sockets.
     */
    @AfterClass
    public static void interruption(){
        client1.endConnection();
        client2.endConnection();
        server.initiateConnectionEnd();
    }

    /**
     * Test for the ServerConnection.sendEvent method.
     * Both client has to read the same object so the same str of the ObjectForTest.
     */
    @Test
    public void testSendEvents(){
        try{
            //System.out.println(server);
            server.sendEvent(e);
            ObjectForTest event1 = (ObjectForTest)client1.getInputStream().readObject();
            ObjectForTest event2 = (ObjectForTest)client2.getInputStream().readObject();
            assertEquals("Network ServerConnection.sendEvent failed", e.str, event1.str);
            assertEquals("Network ServerConnection.sendEvent failed", e.str, event2.str);
        } catch (IOException | ClassNotFoundException e1){
            e1.printStackTrace();
        }
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //TODO this test breaks the makefile.
    /**
     * Is testing that client1 with index 0 receive the object send and that client2 doesn't.
     * We consider that the object is not received with a timeout of 1s.
     *
     * **/
    @Test(timeout = 1000)
    public void testSendEventToSpecificClient(){
        try{
            thrown.expect(TestTimedOutException.class);
            server.sendEventToSpecificClient(e,0);
            ObjectForTest event1 = (ObjectForTest)client1.getInputStream().readObject();
            assertEquals("Network ServerConnection.sendEventToSpecificClient failed", e.str, event1.str);
            ObjectForTest event2 = (ObjectForTest)client2.getInputStream().readObject();
        } catch (IOException |ClassNotFoundException e1){
            e1.printStackTrace();
        }
    }

    /**
     * A client send a ObjectForTest and the server has to receive the same.
     */
    @Test
    public void testReceiveEvent(){
        try {
            client1.getOutputStream().writeObject(e);
            ObjectForTest event = (ObjectForTest)server.receiveEvent(true);
            assertEquals("Network ServerConnection.receiveEvent failed",e.str,event.str);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
