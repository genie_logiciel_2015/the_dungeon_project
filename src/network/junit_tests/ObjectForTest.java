package network.junit_tests;

import network.helpers.NetworkObject;

/**
 * Fake NetworkObject which do nothing.
 * Created by mymi on 19/12/15.
 * To test sendEvent methods we need a simple object so we create this one which is clearly empty,
 * just an implementation of NetworkObject
 */
public class ObjectForTest implements NetworkObject {
    public String str="Hello";

    public ObjectForTest(){}
    public ObjectForTest(String str){
        this.str = str;
    }
}
