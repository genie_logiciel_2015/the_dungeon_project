package core.zone;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to simulate an empty Zone.
 * @author Guerquin Arnaud
 *
 */
public class EmptyZone implements Zone {

	private static final long serialVersionUID = 1L;
	private static final EmptyZone empty=new EmptyZone();
	
	private EmptyZone(){} 
	
	public static EmptyZone getEmptyZone(){
		return empty;
	}
	
	/**
	 * Tell if the two zone intersect.
	 * @param zone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	@Override
	public boolean intersect(Zone zone){
		return false;
	}
	
	@Override
	public boolean isPointinZone(int x, int y) {
		return false;
	}

	@Override
	public Zone translate(Translation translation) {
		return this;
	}

	@Override
	public Zone buildTranslationConvexe(Translation translation) {
		return this;
	}

	@Override
	public Zone changeCenter(Point center) {
		return this;
	}

	@Override
	public Zone clone() {
		return this;
	}

	@Override
	public List<Point> getBorderPoint(Direction direction) {
		return new ArrayList<>();
	}
	
	@Override
	public Zone requireNonEmpty(){
		throw new IllegalArgumentException("Require non empty Zone");
	}

	@Override
	public Point getTopLeft(){
		throw new IllegalStateException("Asked topLeft corner of a EmptyZone.");
	}

	@Override
	public Point farthest(Direction direction) {
		throw new IllegalStateException("Asked farthest point of a EmptyZone.");
	}

	@Override
	public boolean intersect(Rectangle rectangle) {
		return false;
	}

	@Override
	public boolean intersect(Circle circle) {
		return false;
	}

	@Override
	public boolean intersect(ZoneUnion union) {
		return false;
	}

	@Override
	public int distance(Direction direction) {
		return 0;
	}

	@Override
	public boolean strictIntersect(Zone zone) {
		return false;
	}

	@Override
	public boolean strictIntersect(Rectangle rectangle) {
		return false;
	}

	@Override
	public boolean strictIntersect(Circle circle) {
		return false;
	}

	@Override
	public boolean strictIntersect(ZoneUnion union) {
		return false;
	}

	@Override
	public boolean isPointonBorder(int x, int y) {
		return false;
	}

}
