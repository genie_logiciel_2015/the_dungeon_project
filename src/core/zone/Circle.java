package core.zone;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import java.util.function.UnaryOperator;

/**
 * This class is used to define Zone represented as circles. 
 * @author Guerquin Arnaud
 */
public class Circle implements Zone {

	private static final long serialVersionUID = 1L;
	/**
	 * The circle's center
	 */
	private Point center;
	/**
	 * The circle's radius 
	 */
	private int radius;
	

	/**
	 * Creates a circle.
	 * @param radius the circle's radius.
	 */
	public Circle(int radius){
		this(Point.initPoint,radius);
	}
	
	/**
	 * Creates a circle.
	 * @param radius the circle's radius.
	 */
	public Circle(Point point,int radius){
		this.center=Objects.requireNonNull(point);
		assert(radius>0);
		this.radius=radius;
	}
	
	
	@Override
	public boolean isPointinZone(int x, int y) {
		return Math.pow(x-center.getPosX(),2) + Math.pow(y-center.getPosY(),2)<=radius*radius;
	}

	@Override
	public boolean isPointonBorder(int x, int y) {
		return Math.pow(x-center.getPosX(),2) + Math.pow(y-center.getPosY(),2)<radius*radius;
	}
	
	
	/**
	 * Tell if the two zone intersect.
	 * @param zone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	@Override
	public boolean intersect(Zone zone){
		return zone.intersect(this);
	}

	@Override
	public boolean intersect(Circle circle) {
		return Math.pow(Math.abs(circle.center.getPosX()-center.getPosX()),2)+Math.pow(Math.abs(circle.center.getPosY()-center.getPosY()),2)<=Math.pow(circle.radius+radius,2);
	}


	@Override
	public boolean strictIntersect(Circle circle) {
		return Math.pow(Math.abs(circle.center.getPosX()-center.getPosX()),2)+Math.pow(Math.abs(circle.center.getPosY()-center.getPosY()),2)<Math.pow(circle.radius+radius,2);
	}
	
	@Override
	public boolean intersect(Rectangle rectangle){
		int newX=Point.limits(center.getPosX(), rectangle.center.getPosX()-rectangle.halfLength, rectangle.center.getPosX()+rectangle.halfLength);
		int newY=Point.limits(center.getPosY(), rectangle.center.getPosY()-rectangle.halfWidth, rectangle.center.getPosY()+rectangle.halfWidth);
		return isPointinZone(newX,newY);
	}
	
	@Override
	public boolean strictIntersect(Rectangle rectangle) {
		int newX=Point.limits(center.getPosX(), rectangle.center.getPosX()-rectangle.halfLength, rectangle.center.getPosX()+rectangle.halfLength);
		int newY=Point.limits(center.getPosY(), rectangle.center.getPosY()-rectangle.halfWidth, rectangle.center.getPosY()+rectangle.halfWidth);
		return isPointinZone(newX,newY) && (!isPointonBorder(newX,newY) || !rectangle.isPointonBorder(newX,newY));
	}


	@Override
	public Zone translate(Translation translation) {
		center.translate(translation);
		return this;
	}

	@Override
	public Circle clone(){

		return new Circle(center.clone(),radius);
	}
	
	@Override
	public Zone buildTranslationConvexe(Translation translation) {
		Circle z1,z2;
		Rectangle z3;
		z1=this.clone();
		z1.translate(translation);
		z2=this.clone();
		z3=inBetweenRectangle(z1,z2);
		return new ZoneUnion(z1,new ZoneUnion(z2,z3));
	}

	private static Rectangle inBetweenRectangle(Circle z1,Circle z2){
		Circle tmp;
		if(z1.center.getPosX()>z2.center.getPosX() || z1.center.getPosY()>z2.center.getPosY()){
			tmp=z1;
			z1=z2;
			z2=tmp;
		}
		int distX=Point.distanceX(z1.center,z2.center),distY=Point.distanceY(z1.center, z2.center);
		return new Rectangle(Point.construct(z1.center.getPosX()+distX/2,z1.center.getPosY()+distY/2),Math.max(distX, z1.radius),Math.max(distY, z1.radius));
	}

	@Override
	public Zone changeCenter(Point center) {
		this.center=center;
		return this;
	}

	private List<Point> getBorderPoint(Direction direction,List<Point> list) {
		switch(direction){
		case LEFT:
			return getBorderTile(Point.construct(center.posX,center.posY-radius),Point.construct(center.posX-radius,center.posY)
					,Point.construct(center.posX+radius,center.posY),x->x*Point.TileScale<radius,
					x->Point.construct(x.posX-Point.TileScale,x.posY),x->Point.construct(x.posX+Point.TileScale,x.posY)
					,x->x.posY+=1,list);
		case UP:
			return getBorderTile(Point.construct(center.posX-radius,center.posY),Point.construct(center.posX,center.posY-radius)
					,Point.construct(center.posX,center.posY+radius),x->x*Point.TileScale<radius,
					x->Point.construct(x.posX,x.posY-Point.TileScale),x->Point.construct(x.posX,x.posY+Point.TileScale)
					,x->x.posX+=1,list);
		case DOWN:
			return getBorderTile(Point.construct(center.posX+radius,center.posY),Point.construct(center.posX,center.posY-radius)
					,Point.construct(center.posX,center.posY+radius),x->x*Point.TileScale<radius,
					x->Point.construct(x.posX,x.posY-Point.TileScale),x->Point.construct(x.posX,x.posY+Point.TileScale)
					,x->x.posX-=1,list);
		case RIGHT:
			return getBorderTile(Point.construct(center.posX,center.posY+radius),Point.construct(center.posX-radius,center.posY)
					,Point.construct(center.posX+radius,center.posY),x->x*Point.TileScale<radius,
					x->Point.construct(x.posX-Point.TileScale,x.posY),x->Point.construct(x.posX+Point.TileScale,x.posY)
					,x->x.posY-=1,list);
		case NONE:
			return list;
		default:
			throw new IllegalStateException("Unknown direction");
		}
	}
	
	@Override
	public List<Point> getBorderPoint(Direction direction) {
		if(direction.composed()){
			List<Point> list=new ArrayList<>();
			getBorderPoint(direction.first(),list);
			return getBorderPoint(direction.second(),list);
		}
		return getBorderPoint(direction,new ArrayList<Point>());
	}
	
	private void getProjection(Point p,Consumer<Point> consumer){
		int tmp;
		Point p2=p,res=p;
		while(0<(tmp=distanceToCircle(p2))){
			res.posX=p2.posX;
			res.posY=p2.posY;
			consumer.accept(p2);
		}
		if(tmp==0){
			p.posX=p2.posX;
			p.posY=p2.posY;
		}
		p.posX=res.posX;
		p.posY=res.posY;
	}
	
	private int distanceToCircle(Point p) {
		double tmp=Math.pow(p.posX-center.getPosX(),2) + Math.pow(p.posY-center.getPosY(),2);
		if(tmp-radius*radius>0)
			return 1;
		if(tmp-radius*radius==0)
			return 0;
		return -1;
	}

	private List<Point> getBorderTile(Point middle,Point min,Point max,IntPredicate predicate,UnaryOperator<Point> unary,UnaryOperator<Point> unary2,Consumer<Point> consumer,List<Point> list){
		Point point=middle;
		Point point2=Point.construct(middle.posX,middle.posY);
		list.add(middle);
		list.add(min);
		list.add(max);
		for(int i=1;predicate.test(i);i++){
			point=unary.apply(point);
			point2=unary2.apply(point2);
			list.add(point);
			list.add(point2);
		}
		list.forEach(x->this.getProjection(x, consumer));
		return list;
	}
	
	@Override
	public String toString() {
		return "Circle : "+ center.toString() + " Radius:" + radius ;
	}
	

	public static void main(String[] args) {
		Circle c=new Circle(Point.construct(3,3),3);
		System.out.println(c.getBorderPoint(Direction.LEFT));
		System.out.println(c.getBorderPoint(Direction.RIGHT));
		System.out.println(c.getBorderPoint(Direction.DOWN));
		System.out.println(c.getBorderPoint(Direction.UP));
	}

	@Override
	public Point getTopLeft() {
		return Point.construct(center.getPosX()-radius, center.getPosY()-radius);
	}

	//Added for displaying collisionBox - Thomas
	public int getRadius() {
		return radius;
	}

	@Override
	public Point farthest(Direction direction) {
			return Point.construct(center.posX + direction.posX(radius),center.posY + direction.posY(radius));
	}

	@Override
	public boolean intersect(ZoneUnion union) {
		return union.intersect(this);
	}

	@Override
	public int distance(Direction direction) {
		return radius;
	}

	@Override
	public boolean strictIntersect(Zone zone) {
		// TODO Auto-generated method stub
		return zone.strictIntersect(this);
	}


	@Override
	public boolean strictIntersect(ZoneUnion union) {
		return union.strictIntersect(this);
	}

}
