package core.zone;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * This test class contains UnitTest for the Rectangle class.
 * @author Guerquin Arnaud
 * @comments Gwendoline Chouasne
 *
 */
public class RectangleTest {
	
	
	@Test
	public static void constructor(){
		/*vérifie que les rectangles sont construits correctement*/
		Rectangle r1=new Rectangle(1,2);
		Rectangle r2=new Rectangle(Point.construct(2, 2),2,1);
		assertTrue(r1.halfLength==1 && r1.halfWidth==2);
		assertTrue(r1.center.posX==0 && r1.center.posY==0);
		assertTrue(r2.halfLength==2 && r2.halfWidth==1);
		assertTrue(r2.center.posX==2 && r2.center.posY==2);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public static void constructorNonNegativeLenght(){
		/*vérifie que l'on ne peut pas construire un rectangle à longueur négative*/
		@SuppressWarnings("unused")
		Rectangle r1=new Rectangle(-1,1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public static void constructorNonNegativeWidth(){
		/*vérifie que l'on ne peut pas construire un rectangle à largeur négative*/
		@SuppressWarnings("unused")
		Rectangle r1=new Rectangle(1,-1);
	}
	
	@Test(expected=NullPointerException.class)
	public static void constructorRequireNonNull(){
		/*vérifie que l'on ne peut pas construire un rectangle en donnant un centre non défini*/
		@SuppressWarnings("unused")
		Rectangle r1=new Rectangle(null,1,1);
	}
	
	@Test
	public static void isPointInZoneTest(){
		/*vérifie si on détecte correctement les points qui se trouvent dans un rectangle*/
		Rectangle r1=new Rectangle(1,2);
		assertTrue(r1.isPointinZone(0, 0));
		assertTrue(r1.isPointinZone(1, 2));
		assertTrue(r1.isPointinZone(-1, -2));
		assertTrue(r1.isPointinZone(-1, -1));
		assertTrue(r1.isPointinZone(r1.getTopLeft()));
		assertFalse(r1.isPointinZone(10, 2));
	}
	
	@Test
	public static void farthest(){
		/*teste la fonction Rectangle.farthest (quel est le point le plus loin du centre dans une direction)*/
		Rectangle r1=new Rectangle(10,10);
		assertTrue(r1.farthest(Direction.UP).posX==-10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.UP)));
		assertTrue(r1.farthest(Direction.DOWN).posX==10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.DOWN)));
		assertTrue(r1.farthest(Direction.LEFT).posY==-10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.LEFT)));
		assertTrue(r1.farthest(Direction.RIGHT).posY==10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.RIGHT)));
		assertTrue(r1.farthest(Direction.NONE).posX==0 && r1.farthest(Direction.NONE).posY==0);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.NONE)));
		assertTrue(r1.farthest(Direction.LEFTUP).posX==-10 && r1.farthest(Direction.LEFTUP).posY==-10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.LEFTUP)));
		assertTrue(r1.farthest(Direction.LEFTDOWN).posX==10 && r1.farthest(Direction.LEFTDOWN).posY==-10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.LEFTDOWN)));
		assertTrue(r1.farthest(Direction.RIGHTUP).posX==-10 && r1.farthest(Direction.RIGHTUP).posY==10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.RIGHTUP)));
		assertTrue(r1.farthest(Direction.RIGHTDOWN).posX==10 && r1.farthest(Direction.RIGHTDOWN).posY==10);
		assertTrue(r1.isPointinZone(r1.farthest(Direction.RIGHTDOWN)));
	}
	
	@Test
	public static void intersect(){
		/* vérifie la fonction Rectangle.intersect (qui teste si deux rectangles s'intersectent)*/
		Rectangle r1=new Rectangle(1,2);
		Rectangle r2=new Rectangle(Point.construct(2, 2),2,1);
		Rectangle r3=new Rectangle(Point.construct(20, 20),2,1);
		assertTrue(r1.intersect(r1));
		assertTrue(r1.intersect(r2) && r2.intersect(r1));
		assertFalse(r1.intersect(r3) || r3.intersect(r1));
	}
}
