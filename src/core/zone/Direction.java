package core.zone;

import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

/**
 * This enum is used to define all different direction an entity can move to.
 * @author Guerquin Arnaud
 *
 */
public enum Direction {
	RIGHT,UP,LEFT,DOWN, NONE,RIGHTUP,RIGHTDOWN,LEFTDOWN,LEFTUP;
	
	private static Direction arrays[]={NONE,DOWN,LEFT,LEFTDOWN,UP,NONE,LEFTUP,LEFT,RIGHT,RIGHTDOWN,NONE,DOWN,RIGHTUP,RIGHT,UP,NONE};
	
	private int bit;
	private boolean composed;
	private Direction first;
	private Direction second;
	private IntUnaryOperator orientationX;
	private IntUnaryOperator orientationY;
	private IntBinaryOperator farthestX;
	private IntBinaryOperator farthestY;
	private IntBinaryOperator tileChangeNumberX;
	private IntBinaryOperator tileChangeNumberY;
	private IntBinaryOperator maxWithoutChangeX;
	private IntBinaryOperator maxWithoutChangeY;
	private Direction inverse;
	
	static{
		NONE.inverse=NONE;
		LEFT.inverse=RIGHT;
		RIGHT.inverse=LEFT;
		DOWN.inverse=UP;
		UP.inverse=DOWN;
		LEFTUP.inverse=RIGHTDOWN;
		LEFTDOWN.inverse=RIGHTUP;
		RIGHTUP.inverse=LEFTDOWN;
		RIGHTDOWN.inverse=LEFTUP;
		
		
		LEFT.bit=2;
		RIGHT.bit=8;
		UP.bit=4;
		DOWN.bit=1;
		NONE.bit=0;
		RIGHTUP.bit=RIGHT.bit|UP.bit;
		RIGHTDOWN.bit=RIGHT.bit|DOWN.bit;
		LEFTUP.bit=LEFT.bit|UP.bit;
		LEFTDOWN.bit=LEFT.bit|DOWN.bit;
		
		RIGHT.composed=LEFT.composed=DOWN.composed=UP.composed=false;
		RIGHTUP.composed=LEFTUP.composed=RIGHTDOWN.composed=LEFTDOWN.composed=true;
		
		LEFT.first=LEFTUP.first=LEFTDOWN.first=LEFT;
		RIGHT.first=RIGHTUP.first=RIGHTDOWN.first=RIGHT;
		DOWN.first=RIGHTDOWN.second=LEFTDOWN.second=DOWN;
		UP.first=RIGHTUP.second=LEFTUP.second=UP;
		LEFT.second=Direction.RIGHT.second=DOWN.second=UP.second=NONE;


		NONE.orientationX=NONE.orientationY=(x->0);
		
		LEFT.orientationX=NONE.orientationX;
		LEFT.orientationY=(x->-x);
		RIGHT.orientationX=NONE.orientationX;
		RIGHT.orientationY=(x->x);
		UP.orientationX=(x->-x);
		UP.orientationY=NONE.orientationY;
		DOWN.orientationX=(x->x);
		DOWN.orientationY=NONE.orientationY;
		LEFTDOWN.orientationX=DOWN.orientationX;
		LEFTDOWN.orientationY=LEFT.orientationY;
		RIGHTDOWN.orientationX=DOWN.orientationX;
		RIGHTDOWN.orientationY=RIGHT.orientationY;
		LEFTUP.orientationX=UP.orientationX;
		LEFTUP.orientationY=LEFT.orientationY;
		RIGHTUP.orientationX=UP.orientationX;
		RIGHTUP.orientationY=RIGHT.orientationY;

		NONE.farthestY=NONE.farthestX=((x,y)->x);
		
		LEFT.farthestX=NONE.farthestX;
		LEFT.farthestY=Math::min;
		RIGHT.farthestX=NONE.farthestX;
		RIGHT.farthestY=Math::max;
		UP.farthestX=Math::min;
		UP.farthestY=NONE.farthestY;
		DOWN.farthestX=Math::max;
		DOWN.farthestY=NONE.farthestY;
		LEFTDOWN.farthestX=DOWN.farthestX;
		LEFTDOWN.farthestY=LEFT.farthestY;
		RIGHTDOWN.farthestX=DOWN.farthestX;
		RIGHTDOWN.farthestY=RIGHT.farthestY;
		LEFTUP.farthestX=UP.farthestX;
		LEFTUP.farthestY=LEFT.farthestY;
		RIGHTUP.farthestX=UP.farthestX;
		RIGHTUP.farthestY=RIGHT.farthestY;
		

		NONE.tileChangeNumberX=NONE.tileChangeNumberY=((x,y)->0);
		LEFT.tileChangeNumberX=NONE.tileChangeNumberX;
		LEFT.tileChangeNumberY=((pos,incr)-> -incr/Point.TileScale +((pos%Point.TileScale+incr%Point.TileScale)<0?1:0));
		RIGHT.tileChangeNumberX=NONE.tileChangeNumberX;
		RIGHT.tileChangeNumberY=((pos,incr)-> incr/Point.TileScale + ((pos%Point.TileScale+incr%Point.TileScale)>=Point.TileScale?1:0));
		UP.tileChangeNumberX=((pos,incr)-> -incr/Point.TileScale +((pos%Point.TileScale+incr%Point.TileScale)<0?1:0));
		UP.tileChangeNumberY=NONE.tileChangeNumberY;
		DOWN.tileChangeNumberX=((pos,incr)->incr/Point.TileScale + ((pos%Point.TileScale+incr%Point.TileScale)>=Point.TileScale?1:0));
		DOWN.tileChangeNumberY=NONE.tileChangeNumberY;
		LEFTDOWN.tileChangeNumberX=DOWN.tileChangeNumberX;
		LEFTDOWN.tileChangeNumberY=LEFT.tileChangeNumberY;
		RIGHTDOWN.tileChangeNumberX=DOWN.tileChangeNumberX;
		RIGHTDOWN.tileChangeNumberY=RIGHT.tileChangeNumberY;
		LEFTUP.tileChangeNumberX=UP.tileChangeNumberX;
		LEFTUP.tileChangeNumberY=LEFT.tileChangeNumberY;
		RIGHTUP.tileChangeNumberX=UP.tileChangeNumberX;
		RIGHTUP.tileChangeNumberY=RIGHT.tileChangeNumberY;
		
		NONE.maxWithoutChangeX=NONE.maxWithoutChangeY=((x,y)->0);
		LEFT.maxWithoutChangeX=NONE.maxWithoutChangeX;
		LEFT.maxWithoutChangeY=((pos,incr)->Math.max(incr,-pos%Point.TileScale));
		RIGHT.maxWithoutChangeX=NONE.maxWithoutChangeX;
		RIGHT.maxWithoutChangeY=((pos,incr)->Math.min(incr,Point.TileScale-pos%Point.TileScale-1));
	    UP.maxWithoutChangeX=((pos,incr)->Math.max(incr,-pos%Point.TileScale));
		UP.maxWithoutChangeY=NONE.maxWithoutChangeY;
		DOWN.maxWithoutChangeX=((pos,incr)->Math.min(incr,Point.TileScale-pos%Point.TileScale-1));
		DOWN.maxWithoutChangeY=NONE.maxWithoutChangeY;
		LEFTDOWN.maxWithoutChangeX=DOWN.maxWithoutChangeX;
		LEFTDOWN.maxWithoutChangeY=LEFT.maxWithoutChangeY;
		RIGHTDOWN.maxWithoutChangeX=DOWN.maxWithoutChangeX;
		RIGHTDOWN.maxWithoutChangeY=RIGHT.maxWithoutChangeY;
		LEFTUP.maxWithoutChangeX=UP.maxWithoutChangeX;
		LEFTUP.maxWithoutChangeY=LEFT.maxWithoutChangeY;
		RIGHTUP.maxWithoutChangeX=UP.maxWithoutChangeX;
		RIGHTUP.maxWithoutChangeY=RIGHT.maxWithoutChangeY;
		
	}
	public Direction add(Direction dir) {
		return arrays[this.bit|dir.bit];
	}
	
	public Direction remove(Direction dir){
		return arrays[this.bit&(~dir.bit)];
	}

	public boolean composed() {
		return composed;
	}

	public Direction first(){
		return first;
	}
	public Direction second() {
		return second;
	}
	
	public int posX(int value){
		return this.orientationX.applyAsInt(value);
	}
	public int posY(int value){
		return this.orientationY.applyAsInt(value);
	}

	public int binOpX(int x1, int x2) {
		return farthestX.applyAsInt(x1,x2);
	}
	

	public int binOpY(int y1, int y2) {
		return farthestY.applyAsInt(y1,y2);
	}
	
	public int tileChangeNumberX(int x, int incr){
		return tileChangeNumberX.applyAsInt(x,incr);
	}
	
	public int tileChangeNumberY(int y, int incr){
		return tileChangeNumberY.applyAsInt(y,incr);
	}
	
	public int maxWithoutChangeX(int x,int incr){
		return posX(maxWithoutChangeX.applyAsInt(x,incr));
	}
	
	public int maxWithoutChangeY(int y,int incr){
		return posY(maxWithoutChangeY.applyAsInt(y,incr));
		
	}

	public Direction getInverse() {
		return inverse;
	}
}
