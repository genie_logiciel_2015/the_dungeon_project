package core.zone;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * This class is used to define Zone represented as rectangles or squares.
 * @author Guerquin Arnaud
 *
 */
public class Rectangle implements Zone {
	private static final long serialVersionUID = 1L;
	/**
	 * The rectangle's center
	 */
	Point center;
	/**
	 * Half the rectangle's length (X axis) otherwise
	 */
	int halfLength;
	/**
	 * Half the rectangle's width (Y axis)
	 */
	int halfWidth;
	
	
	/**
	 * Creates a rectangle. Gives it (0,0) as center.
	 * @param halfLength half the rectangle length (X axis).
	 * @param halfWidth half the rectangle width (Y axis).
	 */
	public Rectangle(int halfLength,int halfWidth){
		this(Point.initPoint,halfLength,halfWidth);
	}
	
	/**
	 * 
	 * @param point the Rectangle's diagonal center.
	 * @param halfLength half the rectangle length (X axis).
	 * @param halfWidth half the rectangle width (Y axis).
	 */
	public Rectangle(Point point, int halfLength, int halfWidth) {
		this.center=Objects.requireNonNull(point);
		if(halfLength<=0)
			throw new IllegalArgumentException();
		if(halfWidth<=0)
			throw new IllegalArgumentException();
		this.halfLength=halfLength;
		this.halfWidth=halfWidth;
	}

	@Override
	public boolean isPointinZone(int x, int y) {
		return Point.between(x,center.getPosX()-halfLength,center.getPosX()+halfLength) && Point.between(y,center.getPosY()-halfWidth,center.getPosY()+halfWidth);
	}
	
	
	/**
	 * Tell if the two zone intersect.
	 * @param zone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	@Override
	public boolean intersect(Zone zone){
		return zone.intersect(this);
	}


	@Override
	public boolean strictIntersect(Zone zone) {
		return zone.strictIntersect(this);
	}
	
	@Override
	public boolean intersect(Rectangle rectangle) {
		return Math.abs(rectangle.center.getPosX()-center.getPosX()) <= rectangle.halfLength +halfLength && Math.abs(rectangle.center.getPosY()-center.getPosY()) <= rectangle.halfWidth +halfWidth;
	}
	
	@Override
	public boolean strictIntersect(Rectangle rectangle) {
		return Math.abs(rectangle.center.getPosX()-center.getPosX()) < rectangle.halfLength +halfLength && Math.abs(rectangle.center.getPosY()-center.getPosY()) < rectangle.halfWidth +halfWidth;
	}
	
	
	@Override
	public boolean intersect(Circle circle){
		return circle.intersect(this);
	}


	@Override
	public boolean strictIntersect(Circle circle) {
		return circle.strictIntersect(this);
	}

	@Override
	public Zone translate(Translation translation) {
		center.translate(translation);
		return this;
	}
	
	@Override
	public Rectangle clone(){
		return new Rectangle(center.clone(),halfLength,halfWidth);
	}

	@Override
	public Zone buildTranslationConvexe(Translation translation) {
		Rectangle z1,z2,z3;
		z1=this.clone();
		z1.translate(translation);
		z2=this.clone();
		z3=inBetweenRectangle(z1,z2);
		return new ZoneUnion(z1,new ZoneUnion(z2,z3));
	}

	private static Rectangle inBetweenRectangle(Rectangle r1,Rectangle r2){
		Rectangle result;
		if(r1.center.getPosX()>r2.center.getPosX() || r1.center.getPosY()>r2.center.getPosY()){
			result=r1;
			r1=r2;
			r2=result;
		}
		int distX=Point.distanceX(r1.center,r2.center)/2,distY=Point.distanceY(r1.center, r2.center)/2;
		return new Rectangle(Point.construct(r1.center.getPosX()+distX,r1.center.getPosY()+distY),Math.max(distX, r1.halfLength),Math.max(distY, r1.halfWidth));
	}

	@Override
	public Zone changeCenter(Point center) {
		this.center=center;
		return this;
	}

	
	private List<Point> getBorderPoint(Direction direction,List<Point> list) {
		switch(direction){
		case UP:
			return getSideTile(()->Point.construct(center.posX-halfLength,center.posY+halfWidth),()->Point.construct(center.posX-halfLength,center.posY-halfWidth)
					,x->x*Point.TileScale<2*halfWidth,x->Point.construct(x.posX,x.posY-Point.TileScale),list);
		case DOWN:
			return getSideTile(()->Point.construct(center.posX+halfLength,center.posY+halfWidth),()->Point.construct(center.posX+halfLength,center.posY-halfWidth)
					,x->x*Point.TileScale<2*halfWidth,x->Point.construct(x.posX,x.posY-Point.TileScale),list);
		case RIGHT:
			return getSideTile(()->Point.construct(center.posX+halfLength,center.posY+halfWidth),()->Point.construct(center.posX-halfLength,center.posY+halfWidth)
					,x->x*Point.TileScale<2*halfLength,x->Point.construct(x.posX-Point.TileScale,x.posY),list);
		case LEFT:
			return getSideTile(()->Point.construct(center.posX+halfLength,center.posY-halfWidth),()->Point.construct(center.posX-halfLength,center.posY-halfWidth)
					,x->x*Point.TileScale<2*halfLength,x->Point.construct(x.posX-Point.TileScale,x.posY),list);
		case NONE:
			return list;
		default:
				throw new IllegalStateException("Unknown direction");
		}
	}
	
	@Override
	public List<Point> getBorderPoint(Direction direction) {
		if(direction.composed()){
			List<Point> list=new ArrayList<>();
			getBorderPoint(direction.first(),list);
			return getBorderPoint(direction.second(),list);
		}
		return getBorderPoint(direction,new ArrayList<Point>());
	}
	
	private static List<Point> getSideTile(Supplier<Point> supplier1,Supplier<Point> supplier2,IntPredicate predicate,UnaryOperator<Point> unary,List<Point> list){
		Point point=supplier1.get();
		list.add(supplier2.get());
		list.add(point);
		for(int i=1;predicate.test(i);i++){
			point=unary.apply(point);
			list.add(point);
		}
		return list;
	}
	
	@Override
	public String toString() {
		return "Rectangle: "+ center.toString() + "HalfLength :" + halfLength + " HalfWidth:" + halfWidth;
	}

	@Override
	public Point getTopLeft() {
		return Point.construct(center.getPosX()-halfLength, center.getPosY()-halfWidth);
	}

	//Added for displaying collisionBox - Thomas
	public int getWidth() {return 2*halfWidth;}
	public int getHeight() {return 2*halfLength;}

	@Override
	public Point farthest(Direction direction) {
		switch(direction){
		case NONE:
			return center.clone();
		case LEFT:
		case DOWN:
		case LEFTDOWN:
			return Point.construct(center.posX + Direction.LEFTDOWN.posX(halfLength),center.posY + Direction.LEFTDOWN.posY(halfWidth));
		case UP:
		case RIGHT:
		case RIGHTUP:
			return Point.construct(center.posX + Direction.RIGHTUP.posX(halfLength),center.posY + Direction.RIGHTUP.posY(halfWidth));
		case LEFTUP:
			return Point.construct(center.posX + Direction.LEFTUP.posX(halfLength),center.posY + Direction.LEFTUP.posY(halfWidth));
		case RIGHTDOWN:
			return Point.construct(center.posX + Direction.RIGHTDOWN.posX(halfLength),center.posY + Direction.RIGHTDOWN.posY(halfWidth));
		default:
			throw new IllegalStateException("Unknown direction");
		}
	}
	
	@Override
	public boolean intersect(ZoneUnion union) {
		return union.intersect(this);
	}
	
	@Override
	public boolean strictIntersect(ZoneUnion union) {
		return union.strictIntersect(this);
	}

	@Override
	public int distance(Direction direction) {
		return Math.abs(direction.posX(halfLength))+Math.abs(direction.posY(halfWidth));
	}

	@Override
	public boolean isPointonBorder(int x, int y) {
		return (x==center.posX+halfLength||x==center.posX-halfLength)&&
				(x==center.posY+halfWidth||x==center.posY-halfWidth)
				&& isPointinZone(x,y);
	}
}
