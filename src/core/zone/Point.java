package core.zone;

import java.io.Serializable;

import map_generation.map.Map;
import map_generation.map.MapPoint;
import map_generation.tiles.TilePropertyVector;

/**
 * This class is used to specify what is a point on the GameState.
 * The X axis increases when you go down.
 * The Y axis increases when you go right.
 * To give a concrete example :
 * (-1,-1)  (-1, 0)  (-1, 1)
 * ( 0,-1)  ( 0, 0)  ( 0, 1)
 * ( 1,-1)  ( 1, 0)  ( 1, 1)
 * @author Guerquin Arnaud
 *
 */
public class Point implements Cloneable, Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * The point X axis coordinate
	 */
	int posX;
	/**
	 * The point Y axis coordinate
	 */
	int posY;
	public final static int TileScale= 32; // Scale between the entity coordinate and tile coordinate
	
	/**
	 * This point is used as a default for Zone. 
	 */
	public static final Point initPoint=new Point(0,0);
	
	/**
	 * Constructs the point with coordinate (posX,posY).
	 * @param posX the new point's X axis coordinate.
	 * @param posY the new point's Y axis coordinate.
	 */
	private Point(int posX,int posY){
		this.posX=posX;
		this.posY=posY;
	}
	
	public static Point construct(int posX,int posY){
		return new Point(posX,posY);
	}
	
	public static Point constructFromMap(int posX,int posY){
		return new Point(posX*Point.TileScale,posY*Point.TileScale);
	}
	
	/**
	 * Returns true if value is between inf and sup (included).
	 * @param value the value to check
	 * @param inf the minimum value. Must be smaller than inf.
	 * @param sup the maximum value. Must be greater than inf.
	 * @return true if value is between inf and sup.
	 */
	public static boolean between(int value,int inf,int sup){
		assert(inf<=sup);
		return value>=inf && value <=sup;
	}

	/**
	 * Returns value if it is between inf and sup. Returns inf if value is smaller than inf. Returns sup if value is greater than sup.
	 * @param value the value to check
	 * @param inf the minimum value. Must be smaller than sup.
	 * @param sup the maximum value. Must be greater than inf.
	 * @return inf if value<inf, sup if value>sup and value otherwise.
	 */
	public static int limits (int value,int inf,int sup){
		assert(inf<sup);
		if(between(value,inf,sup))
			return value;
		if(value<inf)
			return inf;
		return sup;
	}

	/**
	 * Returns the Point's X axis coordinate.
	 * @return the Point's X axis coordinate.
	 */
	public int getPosX() {
		return posX;
	}
	
	/**
	 * Changes the Point's X axis coordinate.
	 * @param posX the Point's new X axis coordinate.
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}

	/**
	 * Returns the Point's Y axis coordinate.
	 * @return the Point's Y axis coordinate.
	 */
	public int getPosY(){
		return posY;
	}
	/**
	 * Changes the Point's Y axis coordinate.
	 * @param posY the Point's new Y axis coordinate.
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

	/**
	 * Translates the point. Return this.
	 * @param translation the translation to apply.
	 * @return this
	 */
	public Point translate(Translation translation) {
		this.posX+=translation.posX;
		this.posY+=translation.posY;
		return this;
	}

	/**
	 * Returns the X axis distance between A and B
	 * @param A
	 * @param B
	 * @return the X axis distance between A and B
	 */
	public static int distanceX(Point A,Point B) {
		return B.posX-A.posX;
	}
	
	/**
	 * Returns the Y axis distance between A and B
	 * @param A
	 * @param B
	 * @return the Y axis distance between A and B
	 */
	public static int distanceY(Point A,Point B) {
		return B.posY-A.posY;
	}
	
	/**
	 * Checks if a point with the given TilePropertyVector is valid on the given Map.
	 * @param map the map used to check.
	 * @param tpv the TilePropertyVector used to check.
	 * @return true if it is possible. false otherwise.
	 */
	boolean canExist(Map map,TilePropertyVector tpv){
		MapPoint mapPoint=toMapPoint();
		return map.getTileAt(mapPoint).getTilePropertyVector().isIncluded(tpv);
	}

	/**
	 * Checks if a point with the given TilePropertyVector is valid on the given Map. Throws an exception if it isn't valid
	 * @param map the map used to check.
	 * @param tpv the TilePropertyVector used to check.
	 */
	public void hasToExist(Map map, TilePropertyVector tpv) {
		if(!canExist(map,tpv))
			throw new IllegalStateException("You had to catch me...");
	}
	
	/* This four next function are used as lambda */
	static boolean leftest(Point p,Point q){
		return p.posY<=q.posY;
	}
	
	static boolean rightest(Point p,Point q){
		return p.posY>=q.posY;
	}
	
	static boolean topest(Point p,Point q){
		return p.posX<=q.posX;
	}
	
	static boolean bottomest(Point p,Point q){
		return p.posX>=q.posX;
	}
	
	@Override
	public String toString(){
		return "("+posX + "," + posY + ')'; 
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof Point))
			return false;
		Point p=(Point) o;
		return p.posX==posX && p.posY==posY;
	}
	
	@Override
	public int hashCode(){
		return posX;
	}
	
	@Override
	public Point clone(){
		return new Point(posX,posY);
	}

	public void change(Point p) {
		posX=p.posX;
		posY=p.posY;
	}
	
	public MapPoint toMapPoint() {
		return new MapPoint(posX/Point.TileScale,posY/Point.TileScale);
	}
	


	public static Point topLeftOrder(Point A, Point B) {
		return new Point(Math.min(A.posX, B.posX),Math.min(A.posY, B.posY));
	}

	public static Point farthest(Direction direction, Point A, Point B) {
		return new Point(direction.binOpX(A.posX,B.posX),direction.binOpY(A.posY,B.posY));
	}
}
