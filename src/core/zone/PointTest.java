package core.zone;

import static org.junit.Assert.*;

import org.junit.Test;

import map_generation.map.Map;
import map_generation.map.MapGeneration;
import map_generation.map.SurfacesMapGeneration;
import map_generation.tiles.TilePropertyVector;
import map_generation.tiles.TilePropertyVector.TileProperty;

/**
 * This test class contains UnitTest for the Point class.
 * @author Guerquin Arnaud
 * @comments Gwendoline Chouasne
 *
 */
public class PointTest {

	@Test
	public static void leftest(){
		/* vérifie que le point (0,0) est à gauche de (1,0) */
		Point p =Point.construct(0,0);
		assertTrue(Point.leftest(p, Point.construct(1, 0)));
	}

	@Test
	public static void translationInverse(){
		/*vérifie qu'en translatant un point vers la gauche (de 1) puis vers la droite (de 1) on retourne bien au point de départ*/
		Point p =Point.construct(0,0);
		p.translate(Translation.construct(Direction.LEFT, 1)).translate(Translation.construct(Direction.RIGHT,1));
		assertTrue(0== p.posX && 0==p.posY);
	}

	@Test
	public static void collision(){
		/*construit une nouvelle salle pour vérifier la propriété de collision des murs*/
		Map map=MapGeneration.simpleRoom(12);
		TilePropertyVector tpv=new TilePropertyVector().addProperty(TileProperty.SOLID);
		assertFalse(Point.constructFromMap(11, 11).canExist(map,tpv));
		assertTrue(Point.constructFromMap(5, 5).canExist(map,tpv));
		assertFalse(Point.constructFromMap(10, 6).canExist(map,tpv));
	}
}
