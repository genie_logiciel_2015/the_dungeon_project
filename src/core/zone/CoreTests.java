
package core.zone;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * TestSuite for the core package.
 * Will launch automatically all the JUnits tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        PointTest.class,
        RectangleTest.class,
        TranslationTest.class
})
public class CoreTests{}
