package core.zone;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * This class contains UnitTest for the Translation class.
 * @author Guerquin Arnaud
 * @comments Gwendoline Chouasne
 */
public class TranslationTest {

	@Test(expected=IllegalArgumentException.class)
	public static void constructNoneNotEmpty(){
		@SuppressWarnings("unused")
		Translation none=Translation.construct(Direction.NONE,1);
	}

	@Test(expected=IllegalArgumentException.class)
	public static void constructNegative(){
		@SuppressWarnings("unused")
		Translation negative=Translation.construct(Direction.LEFT,-100);
	}


	@Test
	public static void inverse(){
		/* fait toutes les translations possibles et vérifie si les positions des opposés correspondent*/
		Translation left=Translation.construct(Direction.LEFT, 1);
		Translation right=Translation.construct(Direction.RIGHT, 1);
		Translation down=Translation.construct(Direction.DOWN, 1);
		Translation up=Translation.construct(Direction.UP, 1);
		Translation leftup=Translation.construct(Direction.LEFTUP, 1);
		Translation leftdown=Translation.construct(Direction.LEFTDOWN, 1);
		Translation rightup=Translation.construct(Direction.RIGHTUP, 1);
		Translation rightdown=Translation.construct(Direction.RIGHTDOWN, 1);
		assertTrue(left.posX==-right.posX && left.posY==-right.posY);
		assertTrue(up.posX==-down.posX && up.posY==-down.posY);
		assertTrue(leftup.posX==-rightdown.posX && leftup.posY==-rightdown.posY);
		assertTrue(rightup.posX==-leftdown.posX && rightup.posY==-leftdown.posY);
	}

	@Test
	public static void axisPosition(){
		/* fait toutes les translations possibles et vérifie si les positions obtenues sont les bonnes*/
		Translation left=Translation.construct(Direction.LEFT, 1);
		Translation right=Translation.construct(Direction.RIGHT, 1);
		Translation down=Translation.construct(Direction.DOWN, 1);
		Translation up=Translation.construct(Direction.UP, 1);
		Translation leftup=Translation.construct(Direction.LEFTUP, 1);
		Translation leftdown=Translation.construct(Direction.LEFTDOWN, 1);
		Translation rightup=Translation.construct(Direction.RIGHTUP, 1);
		Translation rightdown=Translation.construct(Direction.RIGHTDOWN, 1);
		assertTrue(up.posX==-1 && up.posY==0);
		assertTrue(down.posX==1 && down.posY==0);
		assertTrue(left.posX==0 && left.posY==-1);
		assertTrue(right.posX==0 && right.posY==1);
		assertTrue(1==rightdown.posX && 1==rightdown.posY);
		assertTrue(1==leftdown.posX && -1==leftdown.posY);
		assertTrue(-1==rightup.posX && 1==rightup.posY);
		assertTrue(-1==leftup.posX && -1==leftup.posY);
	}

	@Test
	public static void direction(){
		/*vérifie que la direction est bien modifiée lors d'une translation*/
		Translation left=Translation.construct(Direction.LEFT, 1);
		Translation right=Translation.construct(Direction.RIGHT, 1);
		Translation down=Translation.construct(Direction.DOWN, 1);
		Translation up=Translation.construct(Direction.UP, 1);
		Translation leftup=Translation.construct(Direction.LEFTUP, 1);
		Translation leftdown=Translation.construct(Direction.LEFTDOWN, 1);
		Translation rightup=Translation.construct(Direction.RIGHTUP, 1);
		Translation rightdown=Translation.construct(Direction.RIGHTDOWN, 1);
		assertTrue(Direction.UP==up.direction);
		assertTrue(Direction.DOWN==down.direction);
		assertTrue(Direction.LEFT==left.direction);
		assertTrue(Direction.RIGHT==right.direction);
		assertTrue(Direction.LEFTUP==leftup.direction);
		assertTrue(Direction.LEFTDOWN==leftdown.direction);
		assertTrue(Direction.RIGHTUP==rightup.direction);
		assertTrue(Direction.RIGHTDOWN==rightdown.direction);

	}

	@Test
	public static void unitAxisPosition(){
		/*vérifie que la longueur d'un translation dont on a besoin pour changer de tile est bien de la longueur d'une tile*/
		Translation left=Translation.getUnary(Direction.LEFT);
		Translation right=Translation.getUnary(Direction.RIGHT);
		Translation down=Translation.getUnary(Direction.DOWN);
		Translation up=Translation.getUnary(Direction.UP);
		Translation leftup=Translation.getUnary(Direction.LEFTUP);
		Translation leftdown=Translation.getUnary(Direction.LEFTDOWN);
		Translation rightup=Translation.getUnary(Direction.RIGHTUP);
		Translation rightdown=Translation.getUnary(Direction.RIGHTDOWN);
		assertTrue(-Point.TileScale==up.posX && 0==up.posY);
		assertTrue(Point.TileScale==down.posX && 0==down.posY);
		assertTrue(0==left.posX && -Point.TileScale==left.posY);
		assertTrue(0==right.posX && Point.TileScale== right.posY);
		assertTrue(Point.TileScale==rightdown.posX && Point.TileScale==rightdown.posY);
		assertTrue(Point.TileScale==leftdown.posX && -Point.TileScale==leftdown.posY);
		assertTrue(-Point.TileScale==rightup.posX && Point.TileScale==rightup.posY);
		assertTrue(-Point.TileScale==leftup.posX && -Point.TileScale==leftup.posY);
	}

	@Test
	public static void unitInverse(){
		/*vérifie que la longueur d'un translation dont on a besoin pour changer de tile est bien l'opposée de chaque direction opposée*/
		Translation left=Translation.getUnary(Direction.LEFT);
		Translation right=Translation.getUnary(Direction.RIGHT);
		Translation down=Translation.getUnary(Direction.DOWN);
		Translation up=Translation.getUnary(Direction.UP);
		Translation leftup=Translation.getUnary(Direction.LEFTUP);
		Translation leftdown=Translation.getUnary(Direction.LEFTDOWN);
		Translation rightup=Translation.getUnary(Direction.RIGHTUP);
		Translation rightdown=Translation.getUnary(Direction.RIGHTDOWN);
		assertTrue(left.posX==-right.posX && left.posY==-right.posY);
		assertTrue(up.posX==-down.posX && up.posY==-down.posY);
		assertTrue(leftup.posX==-rightdown.posX && leftup.posY==-rightdown.posY);
		assertTrue(rightup.posX==-leftdown.posX && rightup.posY==-leftdown.posY);
	}

	@Test
	/* */
	public static void getOrientedUnary(){
		for(Direction dir: Direction.values()){
			if(dir!=Direction.NONE){
				Translation small=Translation.construct(dir, 1);
				Translation big=Translation.construct(dir, 2*Point.TileScale);
				assertTrue(small.getOrientedUnary()==small);
				assertTrue(big.getOrientedUnary()==Translation.getUnary(dir));
			}
		}
	}
}
