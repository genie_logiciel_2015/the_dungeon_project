package core.zone;

import java.io.Serializable;

/**
 * This class is used to define Point's coordinate change (for moving an entity...).
 * @author Guerquin Arnaud
 *
 */
public class Translation implements Cloneable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The translation's X axis coordinate change.
	 */
	int posX;
	/**
	 * The translation's Y axis coordinate change.
	 */
	int posY;
	
	Direction direction;
	

	static final double SQRT2=1.414213562;
	/**
	 * Those four vector are vector used to define the minimum translation needed in each direction to change the Tile a point is on.
	 */
	private static final Translation unary[]= new Translation[Direction.values().length];
	
	static{
		unary[Direction.NONE.ordinal()]=new Translation(0,0,Direction.NONE);
		unary[Direction.LEFT.ordinal()]=new Translation(Point.TileScale,Direction.LEFT);
		unary[Direction.RIGHT.ordinal()]=new Translation(Point.TileScale,Direction.RIGHT);
		unary[Direction.DOWN.ordinal()]=new Translation(Point.TileScale,Direction.DOWN);
		unary[Direction.UP.ordinal()]=new Translation(Point.TileScale,Direction.UP);
		unary[Direction.LEFTUP.ordinal()]=new Translation(Point.TileScale,Point.TileScale,Direction.LEFTUP);
		unary[Direction.RIGHTUP.ordinal()]=new Translation(Point.TileScale,Point.TileScale,Direction.RIGHTUP);
		unary[Direction.LEFTDOWN.ordinal()]=new Translation(Point.TileScale,Point.TileScale,Direction.LEFTDOWN);
		unary[Direction.RIGHTDOWN.ordinal()]=new Translation(Point.TileScale,Point.TileScale,Direction.RIGHTDOWN);
	}
	
	/**
	 * Constructs a translation.
	 * @param posX the translation X coordinate change.
	 * @param posY the translation Y coordinate change.
	 */
	private Translation(int x,int y,Direction direction){
		this.direction=direction;
		this.posX=direction.posX(x);
		this.posY=direction.posY(y);
	}
	
	/**
	 * Constructs a translation.
	 * @param posX the translation X coordinate change.
	 * @param posY the translation Y coordinate change.
	 */
	private Translation(int value,Direction direction){
		this.direction=direction;
		this.posX=direction.posX(value);
		this.posY=direction.posY(value);
	}
	
	/**
	 * Constructs a translation in the given direction and the given lenght.
	 * @param direction the Translation's direction.
	 * @param lenght the Translation's lenght.
	 * @return
	 */
	public static Translation construct(Direction direction, int lenght) {
		if(Direction.NONE==direction && lenght!=0)
			throw new IllegalArgumentException("None direction can only be specified with 0");
		if(lenght<0)
			throw new IllegalArgumentException("A direction can't have negative lenght.");
		if(direction.composed()){
			lenght=(lenght==0?0:(int)Math.max(1, lenght/SQRT2));
		}
		return new Translation(lenght,direction);
	}
	
	/**
	 * Returns the unaryTileTranslation with the same direction as this translation.
	 * @return the unaryTileTranslation with the same direction this translation.
	 */
	public Translation getOrientedUnary() {
		if(Point.between(posX,-Point.TileScale,Point.TileScale) && Point.between(posY,-Point.TileScale,Point.TileScale))
			return this;
		return unary[direction.ordinal()];
	}
	
	/**
	 * Returns the Translation's direction.
	 * @return the translation's direction.
	 */
	public Direction getDirection(){
		return direction;
	}
	/**
	 * Returns the greater translation smaller than this that does not make the point with coordinates (x,y) change tile.
	 * @return the greater translation smaller than this that does not make the point with coordinates (x,y) change tile.
	 */
	public Translation getLeftoversTranslation(int x,int y){
		return new Translation(direction.maxWithoutChangeX(x, posX),direction.maxWithoutChangeY(y, posY),direction);
	}
	
	/**
	 * Returns the number of time this translation makes the point (x,y) change coordinate.
	 * @param x the point's X axis coordinate.
	 * @param y the point's Y axis coordinate.
	 * @return
	 */
	public int tileChangeNumber(int x,int y){
		return Math.max(direction.tileChangeNumberX(x, posX),direction.tileChangeNumberY(y, posY));
	}
	
	static public Translation getUnary(Direction dir){
		return unary[dir.ordinal()];
	}

}
