package core.zone;

import java.util.List;
import java.util.Objects;

/**
 * This class is used to define Zone represented as the Union of two Zones.
 * @author Guerquin Arnaud
 *
 */
public class ZoneUnion implements Zone {
	private static final long serialVersionUID = 1L;
	private Zone first;
	private Zone second;
	
	/**
	 * Constructs an Union of the two given Zone.
	 * @param first the first Zone to fuse.
	 * @param second the second Zone to fuse.
	 */
	public ZoneUnion(Zone first,Zone second) {
		this.first=Objects.requireNonNull(first);
		this.second=Objects.requireNonNull(second);
	}
	
	@Override
	public boolean isPointinZone(int x, int y) {
		return first.isPointinZone(x,y)||second.isPointinZone(x, y);
	}
	
	@Override
	public boolean intersect(Zone zone){
		return first.intersect(zone) && second.intersect(zone);
	}

	@Override
	public Zone translate(Translation translation) {
		first.translate(translation);
		second.translate(translation);
		return this;
	}

	@Override
	public Zone buildTranslationConvexe(Translation translation) {
		return new ZoneUnion(first.buildTranslationConvexe(translation),second.buildTranslationConvexe(translation));
	}

	@Override
	public Zone changeCenter(Point center) {
		return this;
	}
	
	@Override
	public ZoneUnion clone(){
		return new ZoneUnion(first.clone(),second.clone());
	}

	@Override
	public List<Point> getBorderPoint(Direction direction) {
		List<Point> result=first.getBorderPoint(direction);
		result.addAll(second.getBorderPoint(direction));
		return result;
	}

	@Override
	public Point getTopLeft() {
		return Point.topLeftOrder(first.getTopLeft(),second.getTopLeft());
	}

	@Override
	public Point farthest(Direction direction) {
		return Point.farthest(direction,first.farthest(direction),second.farthest(direction));
	}

	@Override
	public boolean intersect(Rectangle rectangle) {
		return first.intersect(rectangle) && second.intersect(rectangle);
	}

	@Override
	public boolean intersect(Circle circle) {
		return first.intersect(circle) && second.intersect(circle);
	}

	@Override
	public boolean intersect(ZoneUnion union) {
		return first.intersect(union) && second.intersect(union);
	}

	@Override
	public int distance(Direction direction) {
		return Math.max(first.distance(direction),second.distance(direction));
	}

	@Override
	public boolean strictIntersect(Zone zone) {
		return first.strictIntersect(zone)||second.strictIntersect(zone);
	}

	@Override
	public boolean strictIntersect(Rectangle rectangle) {
		return first.strictIntersect(rectangle)||second.strictIntersect(rectangle);
	}

	@Override
	public boolean strictIntersect(Circle circle) {
		return first.strictIntersect(circle)||second.strictIntersect(circle);
	}

	@Override
	public boolean strictIntersect(ZoneUnion union) {
		return first.strictIntersect(union)||second.strictIntersect(union);
	}

	@Override
	public boolean isPointonBorder(int x, int y) {
		return (first.isPointonBorder(x,y)&&!second.isPointinZone(x, y))
				||(second.isPointonBorder(x,y)&&!first.isPointinZone(x, y))
				||(first.isPointonBorder(x, y)&&second.isPointonBorder(x, y));
	}

}
