package core.zone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import core.gamestate.SpeciesArray;

/**
 * This interface is used to define a part of a map (in Point coordinate). 
 * @author Guerquin Arnaud
 *
 */
public interface Zone extends Cloneable, Serializable  {

	static final long serialVersionUID = 1L;
	/**
	 * Tell if the point with coordinate (x,y) is in the zone.
	 * @param x the point's abscissa.
	 * @param y the point's ordinate.
	 * @return True if the point is in the zone.
	 */
	boolean isPointinZone(int x,int y);
	

	/**
	 * Tell if the point with coordinate (x,y) is on the border of the zone.
	 * @param x the point's abscissa.
	 * @param y the point's ordinate.
	 * @return True if the point is on the border of the zone.
	 */
	boolean isPointonBorder(int x,int y);

	/**
	 * Tell if the point p is in the zone.
	 * @param p the point to check.
	 * @return True if the point is in the zone.
	 */
	default boolean isPointinZone(Point p){
		return isPointinZone(p.getPosX(), p.getPosY());
	}
	/**
	 * Tell if the point p is on the border the zone.
	 * @param p the point to check.
	 * @return True if the point is on the border of the zone.
	 */
	default boolean isPointonBorder(Point p){
		return isPointonBorder(p.getPosX(), p.getPosY());
	}
	
	/**
	 * Tell if the two zone intersect.
	 * @param zone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean intersect(Zone zone);
	
	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean intersect(Rectangle rectangle);
	
	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean intersect(Circle circle);
	

	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean intersect(ZoneUnion union);
	
	
	/**
	 * Tell if the two zone intersect.
	 * @param emptyZone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	default public boolean intersect(EmptyZone emptyZone){
		return false;
	}
	

	/**
	 * Tell if the two zone intersect.
	 * @param zone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean strictIntersect(Zone zone);
	
	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean strictIntersect(Rectangle rectangle);
	
	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean strictIntersect(Circle circle);
	

	/**
	 * Tell if the two zone intersect.
	 * @param rectangle the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	public boolean strictIntersect(ZoneUnion union);
	
	
	/**
	 * Tell if the two zone intersect.
	 * @param emptyZone the other zone to check.
	 * @return true is they intersect. false otherwise.
	 */
	default public boolean strictIntersect(EmptyZone emptyZone){
		return false;
	}

	/**
	 * Translates the zone's center.
	 * @param translation the translation to apply.
	 * @return this
	 */
	public Zone translate(Translation translation);
	
	public Zone buildTranslationConvexe(Translation translation);

	/**
	 * Changes the zone's center.
	 * @param center the new zone's center.
	 * @return this.
	 */
	Zone changeCenter(Point center);
	
	public Zone clone();
	/**
	 * Returns a copy of this but with the given center.
	 * @param center the copy's new center.
	 * @return a copy of this but with a new center.
	 */
	default Zone clone(Point center){
		Zone zone=clone();
		zone.changeCenter(center);
		return zone;
	}

	/**
	 * Returns a list of point on the Zone's border facing direction. 
	 * The point are sufficiently spaced to be on different Map Tile.
	 * @param direction the direction the given Zone's border is facing.
	 * @return A list of point on the Zone's border facing direction.
	 */
	List<Point> getBorderPoint(Direction direction);

	/**
	 * Checks that the given zone is not an EmptyZone. Throw an IllegalArgumentException if it is.
	 * @return this
	 */
	default Zone requireNonEmpty(){
		return this;
	}

	/**
	 * Return the topleft point of the zone
	 * @return the topleft point of the zone
	 */
	Point getTopLeft();

	/**
	 * Return the farthest point in the zone in this direction
	 * @param direction the direction to use
	 * @return the farthest point in the zone from this direction
	 */
	Point farthest(Direction direction);

	
	/**
	 * Project the zone using the given projection
	 * @param projection the projection  used
	 * @return this
	 */
	default public Zone project(Translation projection) {
		return this.clone().translate(projection);
	}

	/** 
	 * return the distance between the center of the zone and the farthest point in the given direction
	 * @param direction
	 * @return the distance between the center of the zone and the farthest point in the given direction
	 */
	int distance(Direction direction);

	/**
	 * Read a zone from a file
	 * @param br the file where the zone is read
	 * @return a new zone
	 * @throws IOException
	 */
	static Zone readFromFile(BufferedReader br) throws IOException{
			char c=br.readLine().charAt(0);
			SpeciesArray.lineError++;
			switch(c){
			case 'R':
				return readRectangleFromFile(br);
			case 'C':
				return readCircleFromFile(br);
			case 'U':
				return readZoneUnionFromFile(br);
			case 'E':
				return EmptyZone.getEmptyZone();
			default:
				SpeciesArray.lineError--;
				throw new IOException("Bad formated Zone.\n");
			}
	}


	static Rectangle readRectangleFromFile(BufferedReader br) throws IOException{
		int halfLenght=0,halfWidth=0;
		halfLenght=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		halfWidth=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		return new Rectangle(halfLenght,halfWidth);
	}
	
	static Circle readCircleFromFile(BufferedReader br)throws IOException{
		int radius=0;
			radius=Integer.parseInt(br.readLine());
			SpeciesArray.lineError++;
		return new Circle(radius);
	}
	
	static ZoneUnion readZoneUnionFromFile(BufferedReader br) throws IOException{
		Zone first,second;
		first=readFromFile(br);
		second=readFromFile(br);
		return new ZoneUnion(first,second);
	}
}
