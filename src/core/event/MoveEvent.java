package core.event;


import core.gamestate.Action;
import core.gamestate.Entity;
import core.gamestate.EntityNotFoundExeption;
import core.gamestate.GameContent;
import core.zone.Point;
import core.zone.Translation;
import gameloop.LocalGameLoop;

/**
 * This event is executed when an entity wants to move.
 * @author Guerquin Arnaud
 *
 */
public class MoveEvent implements ClientEvent,ServerEvent {

	private static final long serialVersionUID = 1L;
	private int targetID;
	private Translation translation;

	public MoveEvent(int targetID,Translation translation){
		this.targetID=targetID;
		this.translation=translation;
	}

	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException{
		Entity target = null;
		try {
			target = gameContent.getGameState().getEntity(targetID);
		Point p =target.expectedMove(translation,gameContent.getGameState(),gameContent.getMap());
		if(!target.isCenter(p)){
			target.setCenter(p);
			target.addAction(Action.WALK);
		}
		else if(target.getSpeciesName().equals("FrostBolt") || target.getSpeciesName().equals("FireBoltLP")){
			Event event = new AbilityEvent(targetID, 0);
			LocalGameLoop.getInstance().sendEvent(event);
		}
		return true;
		} catch (EntityNotFoundExeption entityNotFoundExeption) {
			return true;
		}
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return this;
	}

}
