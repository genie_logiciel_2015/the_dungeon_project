package core.event;

import core.gamestate.GameContent;


/**
 * This event does nothing and fails.
 * @author Guerquin Arnaud
 *
 */
public class FailedEvent implements ClientEvent,ServerEvent{
	
	private static final long serialVersionUID = 1L;
	static final public Event failed=new FailedEvent();
	
	private FailedEvent(){}

	@Override
	public boolean execute(GameContent gameContent) {
		return false;
	}


	@Override
	public Event resolve(GameContent gameContent) {
		return this;
	}
}
