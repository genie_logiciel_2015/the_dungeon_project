package core.event;


import core.gamestate.GameContent;

/**
 * This event does nothing and successes.
 * @author Guerquin Arnaud
 *
 */
public class EmptyEvent implements ClientEvent,ServerEvent {

	private static final long serialVersionUID = 1L;
	public static final EmptyEvent empty=new EmptyEvent();
	
	
	public EmptyEvent(){
	}
	
	@Override
	public boolean execute(GameContent gameContent) {
		return true;
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return this;
	}

}
