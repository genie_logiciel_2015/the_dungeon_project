package core.event;

import core.gamestate.GameContent;

/**
 * Event received by Client when an entity die.
 * @author Begel Myriam
 * Just remove it from the GameState.
 */
public class ToClientDeathEvent implements ClientEvent {
    private int entityID;

    public ToClientDeathEvent(int entityID){
        this.entityID = entityID;
    }

    public int getEntityID(){
        return entityID;
    }

    private static final long serialVersionUID = 1L;
    @Override
    public boolean execute(GameContent gameContent) throws InterruptedException {
        return gameContent.getGameState().removeEntity(entityID);
    }

    @Override
    public Event resolve(GameContent gameContent) {
        return null;
    }

}
