package core.event;

import core.abilities.*;
import core.abilities.effects.*;
import core.gamestate.Entity;
import core.gamestate.EntityNotFoundExeption;
import core.gamestate.GameContent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dupriez on 19/12/15.
 */
public class AbilityEvent implements ClientEvent,ServerEvent {

	private static final long serialVersionUID = 1L;
	private int potentialCasterCharacterID;
	private int abilityNb;

	/**
	 *
	 * @param potentialCasterCharacterID the ID of the Character trying to cast one of his Abilities
	 * @param abilityNb	the position of the Ability potentially casted in the AbilityList of the potentialCasterCharacter
	 */
	public AbilityEvent(int potentialCasterCharacterID, int abilityNb){
		this.potentialCasterCharacterID = potentialCasterCharacterID;
		this.abilityNb = abilityNb;
	}

	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException {
		Entity potentialCaster = null;
		try {
			potentialCaster = gameContent.getGameState().getEntity(potentialCasterCharacterID);
			Ability abilityPotentiallyCasted = potentialCaster.getAbilityList().get(abilityNb);

			List<EffectDescriptor> effectDescriptorList = abilityPotentiallyCasted.cast();

			if (effectDescriptorList != null) {
				//Ability successfully cast
				//TODO: Remove later in the game the Action we add in the following line
				potentialCaster.addAction(abilityPotentiallyCasted.getAction());
				for (EffectDescriptor effectDescriptor : effectDescriptorList) {
					//Get an Effect corresponding to the effectDescriptor thanks to EffectFactory
					Effect e = EffectFactory.getInstance().getEffectFromEffectDescriptorAndCasterID(effectDescriptor, potentialCasterCharacterID);
					//Apply the Effect
					e.effect(new ArrayList<>(Arrays.asList(potentialCasterCharacterID)), gameContent, potentialCasterCharacterID);
				}
			}
			else {
				//Ability not cast
			}
			return true;
		} catch (EntityNotFoundExeption entityNotFoundExeption) {
				return false;
		}
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return this;
	}
}
