package core.event;


import core.gamestate.GameContent;
import network.helpers.NetworkObject;

/**
 * This interface is used to define the message transmitted between the Relayer,
 * the GameState and the GameLoop.
 * @author Guerquin Arnaud
 *
 */
public interface Event extends NetworkObject{

	static final long serialVersionUID = 1L;
	public static final int infinity = -1;

	boolean execute(GameContent gameContent) throws InterruptedException;

	Event resolve(GameContent gameContent);
	/**
	 * Applies the event on the given gameContent.
	 * @param gameContent the gameContent on which the event will be applied.
	 * @return If modification to the gameState occured, it returns an Event whose apply method will perform the same modification
	 * @throws InterruptedException
	 */
	default public Event apply(GameContent gameContent) throws InterruptedException{
		if(execute(gameContent))
			return resolve(gameContent);
		return FailedEvent.failed;
	}



}
