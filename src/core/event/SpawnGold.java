package core.event;

import core.gamestate.*;
import core.gamestate.Character;

/**
 * Created by hilaire on 06/01/16.
 *
 * this event is used when some Gold is created
 */
public class SpawnGold  implements ClientEvent,ServerEvent {

    private static final long serialVersionUID = 1L;
    private int targetID;

    public SpawnGold(int targetID){
        this.targetID=targetID;
    }

    @Override
    public boolean execute(GameContent gameContent) throws InterruptedException {
        try {
        	gameContent.getGameState().getEntity(targetID);

                Character charac = null;
                try {
                    charac = gameContent.getGameState().getCharacter(targetID);
                } catch (EntityNotFoundExeption entityNotFoundExeption) {
                    return false;
                }
                Entity entity = SpeciesArray.create(charac.getX(), charac.getY(), 0, "Gold", "Gold");
                entity.setOrientation(charac.getOrientation());
                entity.setSpeed(0);
                entity.setOwned_character(targetID);
                gameContent.getGameState().addEntity(entity);
                gameContent.addTriggerToCheck(entity);
            return true;

        } catch (EntityNotFoundExeption entityNotFoundExeption) {
            return true;
            }
        }

    @Override
    public Event resolve(GameContent gameContent) {
        return this;
    }

}