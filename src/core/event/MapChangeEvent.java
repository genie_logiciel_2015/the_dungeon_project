package core.event;

import core.gamestate.GameContent;
import map_generation.map.Map;

/*
 * This Event notify the client that the map has changed
 * by Theodore LOPEZ and Arnaud Guerquin
 * */

public class MapChangeEvent implements Event {
	
	private static final long serialVersionUID = 1L;
	Map map;

	public MapChangeEvent(Map newMap) {
		map=newMap;
	}

	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException {
		gameContent.recreate(map,false);
		return true;
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return EmptyEvent.empty;
	}

}
