package core.event;

/**
 * Only used to add some security between event. Those event can be sent by the client to the server.
 * @author Guerquin Arnaud
 *
 */
public interface ServerEvent extends Event {

}
