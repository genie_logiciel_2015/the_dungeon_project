package core.event;

/**
 * Only used to add some security between event. Those event can be sent by the server to the client.
 * @author Guerquin Arnaud
 *
 */
public interface ClientEvent extends Event {

}
