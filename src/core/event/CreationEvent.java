package core.event;

import core.gamestate.GameContent;


/**
 * This event is executed when an entity is created.
 * @author Guerquin Arnaud
 *
 */
public class CreationEvent implements ClientEvent {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean execute(GameContent gameContent) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Event resolve(GameContent gameContent) {
		// TODO Auto-generated method stub
		return null;
	}

}
