package core.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import core.abilities.effects.Effect;
import core.gamestate.Character;
import core.gamestate.GameContent;
import core.gamestate.Trigger;

public class TriggerEvent implements ClientEvent {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	transient private Trigger trigger;
	private int casterID;

	public TriggerEvent(Trigger trigger, int casterID) {
		this.trigger=Objects.requireNonNull(trigger);
		this.casterID=casterID;
	}

	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException {
		List<Character> list=gameContent.getGameState().getCharacterWhoseHitboxIntersectsWith(trigger.getEffectZone());
		list.removeIf(e->e.getID()==casterID);
		if(!list.isEmpty()){
			ArrayList<Integer> targets=new ArrayList<>(list.size());
			for(Character c:list){
				targets.add(c.getID());
			}
			for(Effect effect:trigger.getEffectList()){
				effect.effect(targets, gameContent, casterID);
			}
		}
		return true;
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return EmptyEvent.empty;
	}

}
