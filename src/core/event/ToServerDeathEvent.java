package core.event;

import artificial_intelligence.AIControler;
import core.gamestate.GameContent;

/**
 * This event is executed by Server when an entity dies.
 * @author Begel Myriam
 * We kill the AI if it is one (if not, the method just do nothing).
 * In resolve, we send to the client almost the same Event: AI is not on ClientSide
 * so we don't kill AI.
 */
public class ToServerDeathEvent implements ServerEvent {
	private int entityID;

	public ToServerDeathEvent(int entityID){
		this.entityID = entityID;
	}

	private static final long serialVersionUID = 1L;
	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException {
		AIControler.killOne(entityID);

		return gameContent.getGameState().removeEntity(entityID);

	}

	@Override
	public Event resolve(GameContent gameContent) {
		return new ToClientDeathEvent(entityID);
	}

}
