package core.event;

import core.gamestate.Entity;
import core.gamestate.EntityNotFoundExeption;
import core.gamestate.GameContent;
import core.gamestate.Trigger;

public class AddedTriggerEvent implements ClientEvent{
	private static final long serialVersionUID = 1L;
	int targetID;
	Trigger trigger;

	@Override
	public boolean execute(GameContent gameContent) throws InterruptedException {
		Entity e= null;
		try {
			e = gameContent.getGameState().getEntity(targetID);
		} catch (EntityNotFoundExeption entityNotFoundExeption) {
			entityNotFoundExeption.printStackTrace();
			return false;
		}
		if(e.addTriggers(trigger)){
			gameContent.addTriggerToCheck(e);
		}
		return true;
	}

	@Override
	public Event resolve(GameContent gameContent) {
		return EmptyEvent.empty;
	}

}
