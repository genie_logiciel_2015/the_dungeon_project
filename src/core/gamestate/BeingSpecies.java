package core.gamestate;

import java.io.BufferedReader;
import java.io.IOException;
import core.zone.Zone;

/**
 * This clss is used to construct being from a "skeleton" found in the species.txt file. 
 * @author Guerquin Arnaud
 *
 */
public class BeingSpecies extends EntitySpecies{
	/**
	 * The hitbox all created being will obtain
	 */
	final Zone hitbox;
	/**
	 * The damageTypeVector all created being will obtain
	 */
	final DamageTypeVector dtv;
	/**
	 * The maxHP value all created being will obtain
	 */
	final int baseHP;
	/**
	 * the defense all created being will obtain.
	 */
	final int baseDef;
	/**
	 * the mental all created being will obtain.
	 */
	final int baseMental;

	/**
	 * Construct a beingSpecies using the data in the given BufferReader
	 * @param br the BufferReader where data is read
	 * @throws IOException if can't read from the BufferReader.
	 */
	BeingSpecies(BufferedReader br) throws IOException{
		super(br);
		br.readLine();
		SpeciesArray.lineError++;
		hitbox=Zone.readFromFile(br);
		br.readLine();
		SpeciesArray.lineError++;
		dtv=DamageTypeVector.readFromFile(br);
		br.readLine();
		SpeciesArray.lineError++;
		baseHP=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		baseDef=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		baseMental=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
	}

	/*
	 * Create a new Being using the parameters and the information stored in the species.
	 * */
	@Override
	public Being create(int posX, int posY, int id, int owner, String name) {
		return new Being(name,this.name,posX, posY, id,owner, collisionBox, tpv, visibility, entityDisplayerType,triggers,
			baseHP, baseDef, baseMental, dtv, hitbox,abilityList);
	}

}
