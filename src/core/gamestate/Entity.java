package core.gamestate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import core.abilities.Ability;
import core.abilities.effects.Effect;
import core.event.TriggerEvent;
import core.zone.Direction;
import core.zone.Point;
import core.zone.Translation;
import core.zone.Zone;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayer;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerProvider;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import map_generation.map.Map;
import map_generation.tiles.TilePropertyVector;
import network.inner_shell.NetworkConnection;

/**
 * This interface is used to define one object in the GameState.
 * @author Guerquin Arnaud
 *
 */
public class Entity implements Serializable{
	private static final long serialVersionUID = 1L;
	public static final int DefaultOwner=0;


	/**
	 * The character's name.
	 */
	private String name;
	/**
	 * The character's species's name.
	 */
	private String speciesName;
	/**
	 * The entity's center
	 */
	private Point center;
	/**
	 * The current direction the entity is going.
	 */
	private Direction direction=Direction.DOWN;
	/**
	 * The entity's orientation
	 */
	private int orientation=0;
	/**
	 * the entity's ID
	 */
	private int ID;
	/**
	 * the entity's owner
	 */
	private int owner;
	/**
	 * the entity's faction
	 */
	private int faction=0;
	/**
	 * the entity's visibility (the zone of the map it is able to see)
	 */
	private Zone visibility;
	/**
	 * The entity's collision box (the zone it intersect the map)
	 */
	private Zone collisionBox;
	/**
	 * the entity's typePropertyVector (all the typeProperty the entity can be on)
	 */
	private TilePropertyVector tpv;
	/**
	 * the entity's speed
	 */
	private int speed=5;
	/**
	 * the list of all Action the entity is currently doing
	 */
	private ActionVector actionVector=new ActionVector();
	/**
	 * The list of all trigger of the given entity.
	 */
	private ArrayList<Trigger> triggers=new ArrayList<>();
	//TODO the one who added all of this, document it.
	/**
	 * The id of the possible entity's owner (character)
	 */
	private int owned_character = -1;

	//The following 3 fields are used by the method getEntityDisplayer
	private EntityDisplayerType entityDisplayerType;
	private boolean hasAnEntityDisplayer = false;
	private EntityDisplayer entityDisplayer;
	protected ArrayList<Ability> abilityList;

	/**
	 * Construct an entity
	 * @param name the entity's name
	 * @param speciesName the entity's species name
	 * @param posX the entity's X position
	 * @param posY the entity's Y position
	 * @param ID the entity's ID
	 * @param owner the entity's owner
	 * @param collisionBox the entity's collision box
	 * @param tpv the entity's TypePropertyVector
	 * @param visibility the entity's visibility
	 * @param type the entity's displayer type
	 * @param triggers the entity's triggers
	 * @param abilityList the entity's abilityList
	 */
	Entity(String name,String speciesName,int posX,int posY,int ID,int owner,Zone collisionBox,
			TilePropertyVector tpv,Zone visibility,EntityDisplayerType type,List<TriggerDescriptor> triggers,ArrayList<Ability> abilityList){
		this.speciesName=Objects.requireNonNull(speciesName);
		this.name=name;
		center=Point.construct(posX,posY);
		this.ID=ID;
		this.owner=owner;
		this.visibility=visibility;
		this.collisionBox=collisionBox.clone(center);
		this.tpv=tpv.clone();
		this.entityDisplayerType = type;
		Objects.requireNonNull(triggers);
		for(TriggerDescriptor td : triggers){
			this.triggers.add(Trigger.construct(this, td));
		}
		this.abilityList = new ArrayList<>(abilityList);
	}

	/**
	 * Called by the gui when it has to display this entity.
	 * NOTE: When an entity is created, it only contains an entityDisplayerType, and not an actual entityDisplayer.
	 * It is the first call of the gui to this method that creates an entityDisplayer of the right type and bind it to this entity.
	 * @return: the entityDisplayer bound to this entity
	 */
	public EntityDisplayer getEntityDisplayer() {
		if(! hasAnEntityDisplayer) {
			entityDisplayer = EntityDisplayerProvider.createEntityDisplayer(entityDisplayerType, this);
			hasAnEntityDisplayer = true;
		}
		return entityDisplayer;
	}

	/**
	 * Returns the character's name.
	 * @return the character's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the character's species
	 * @return the character's species
	 */
	public String getSpeciesName(){
		return speciesName;
	}

	/**
	 * Returns the entity's center X position.
	 * @return the entity's center X position.
	 */
	public int getX(){
		return center.getPosX();
	}

	/**
	 * Changes the entity's center X position.
	 * @param X the entity's center new X position.
	 */
	void setX(int X){
		center.setPosX(X);
	}
	/**
	 * Returns the entity's center Y position.
	 * @return the entity's center Y position.
	 */
	public int getY(){
		return center.getPosY();
	}

	/**
	 * Changes the entity's center Y position.
	 * @param Y the entity's center new Y position.
	 */
	void setY(int Y){
		center.setPosY(Y);
	}

	/**
	 * Changes the entity's center X and Y position.
	 * @param X the entity's center new X position.
	 * @param Y the entity's center new Y position.
	 */
	void setXY(int X,int Y){
		center.setPosX(X);
		center.setPosY(Y);
	}

	/**
	 * Returns true if p is the center of the entity.
	 * @param p the point to compare tp.
	 * @return true if the given point is the entity's center.
	 */
	public boolean isCenter(Point p) {
		return center.equals(p);
	}

	/**
	 * Changes the center of the entity.
	 * @param center the entity's new center.
	 */
	public void setCenter(Point center) {
		this.center.change(center);
	}


	Point getCenter() {
		return center;
	}

	/**
	 * Returns the entity's orientation.
	 * @return the entity's orientation.
	 */
	public int getOrientation(){
		return orientation;
	}

	/**
	 * Changes the entity's orientation.
	 * @param orientation the entity's new orientation.
	 */
	public void setOrientation(int orientation){
		this.orientation=orientation%360;
	}

	/**
	 * Return the entity's direction (the direction in which it is moving)
	 * @return
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * Returns the entity's ID.
	 * @return the entity's ID.
	 */
	public int getID(){
		return ID;
	}

	/**
	 * Returns the entity's Relayer's ID.
	 * @return the entity's Relayer's ID.
	 */
	public int getOwner() {
		return owner;
	}

	/**
	 * Changes the entity's Relayer's ID.
	 * @param owner the entity's new Relayer's ID.
	 */
	public void setOwner(int owner){
		this.owner=owner;
	}

	public ArrayList<Trigger> getTriggers(){
		return triggers;
	}

	/**
	 * Add one trigger to the entity. Returns true if the entity didn't have any trigger yet
	 * @param trigger the trigger to add
	 * @return return true if the entity didn't have any trigger before
	 */
	public boolean addTriggers(Trigger trigger){
		triggers.add(trigger);
		return triggers.size()==1;
	}

	/**
	 * Remove the given trigger.
	 * @param triggerID the ID of the trigger to remove
	 * @return true if the entity doesn't have any trigger anymore
	 */
	public boolean removeTriggers(int triggerID){
		triggers.removeIf(x->x.id==triggerID);
		return triggers.size()==0;
	}

	/**
	 * Returns true if the entity is partially in the Zone zone.
	 * @param zone the zone the entity's position is compared to.
	 * @return true if the entity is partially in the given zone.
	 */
	public boolean isIn(Zone zone) {
		return collisionBox.intersect(zone);
	}

	/**
	 * Returns a copy of the entity's visibility.
	 * @return a copy of the entity's visibility.
	 */
	public Zone getVisibility() {
		return visibility.clone();
	}

	/**
	 * Returns the point where the entity's center is after trying to apply the given translation.
	 * Do not change the entity's center.
	 * @param translation the translation to apply.
	 * @param gamestate the GameState where the entity is stored.
	 * @param map the map the entity is exploring.
	 * @return the entity's center position if we apply the translation.
	 */
	public Point expectedMove(Translation translation, GameState gamestate, Map map) {
		List<Point> list=collisionBox.getBorderPoint(translation.getDirection());
		Translation unary=translation.getOrientedUnary();
		Point result=center.clone();
		Point farthest=collisionBox.farthest(translation.getDirection()).clone();
		int count=translation.tileChangeNumber(farthest.getPosX(), farthest.getPosY());
		for(int i=0;i<count
				&& farthest.getPosX()+i*Point.TileScale<map.getHeight()*Point.TileScale
				&& farthest.getPosY()+i*Point.TileScale<map.getWidth()*Point.TileScale
				;i++){
			list.forEach(x->x.translate(unary));
			try{
				list.forEach(x->x.hasToExist(map,tpv));
			}catch(Exception e){
				break;
			}
			result.translate(unary);
		}
		result.translate(translation.getLeftoversTranslation(farthest.getPosX(),farthest.getPosY()));
		//TODO change for when sprite will be able to use this direction
		direction=translation.getDirection().first();
		actionVector.addAction(Action.WALK);
		return result;
	}

	/**
	 * Return the entity's speed
	 * @return the entity's speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * Change the entity's speed
	 * @param speed the new speed
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public String toString() {
		return "Entity. Center :" +center.toString() + " Zone :" + collisionBox.toString();
	}

	/**
	 * Return the top left point of the entity
	 * @return the top left point of the entity
	 */
	public Point getTopLeft() {
		return collisionBox.getTopLeft();
	}

	/**
	 * Add an action to the entity
	 * @param action the action to add
	 */
	public void addAction(Action action) {
		actionVector.addAction(action);
	}

	/**
	 * Remove an action from the entity
	 * @param action the action to remove
	 */
	public void removeAction(Action action){
		actionVector.removeAction(action);
	}

	/**
	 * Return true if the entity has the given action
	 * @param action the action to check
	 * @return true if the entity has the given action
	 */
	public boolean hasAction(Action action){
		return actionVector.hasAction(action);
	}

	/**
	 * get the entity's faction
	 * @return the entity's faction
	 */
	public int getFaction() {
		return faction;
	}

	/**
	 * set the entity's faction to the given parameter
	 * @param faction the new entity's faction
	 */
	public void setFaction(int faction) {
		this.faction = faction;
	}

	/**
	 * return the entity's collision box
	 * @return the entity's collision box
	 */
	public Zone getCollisionBox() {
		return collisionBox;
	}

	/**
	 * @param gameState
	 * @throws InterruptedException
	 */
	boolean hackGamestate(GameState gameState) throws InterruptedException {
		return gameState.addEntityBis(this);
	}

	/**
	 * Check all trigger of this entity and apply them if they should
	 * @param gameContent the gameContent the trigger should be applied on.
	 * @param networkConnection the networkConnection where event will be send.
	 * @throws InterruptedException
	 */
	public void checkTrigger(GameContent gameContent, NetworkConnection networkConnection) throws InterruptedException {
		for(Trigger trigger:triggers){
			List<Character> list=gameContent.getGameState().getCharacterWhoseHitboxIntersectsWith(trigger.getEffectZone());
			list.removeIf(e->e.getID()==ID);
			if(!list.isEmpty()){
				ArrayList<Integer> targets=new ArrayList<>(list.size());
				for(Character c:list){
					targets.add(c.getID());
				}
				list.clear();
				networkConnection.sendEvent(new TriggerEvent(trigger,ID));
				for(Effect effect:trigger.getEffectList()){
					effect.effect(targets, gameContent, ID);
				}
				targets.clear();
			}
		}

	}

	/**
	 * Return the list of ability of the entity.
	 * @return the list of ability of the entity
	 */
	public List<Ability> getAbilityList() {
		return abilityList;
	}


	/**
	 * Getters
	 * @return
	 */
	public int getOwned_character() {
		return owned_character;
	}

	/**
	 * Setters
	 * @param owned_character
	 */
	public void setOwned_character(int owned_character) {
		this.owned_character = owned_character;
	}
}