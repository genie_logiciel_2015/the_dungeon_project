package core.gamestate;

import java.util.Arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;

/**
 * This class is used to store all type of damage a Being is sensible to.
 * @author Guerquin Arnaud
 *
 */
public class DamageTypeVector implements Serializable{
	private static final long serialVersionUID = 1L;
	private static int arraySize=DamageType.values().length/8+1;
	
	private char damageVectorArray[];
	
	/**
	 * Construct a new {@link DamageTypeVector} with no damageVector.
	 */
	public DamageTypeVector(){
		damageVectorArray=new char[arraySize];
	}
	
	/* Used by the clone function */
	private DamageTypeVector(char[] damageVectorArray) {
		this.damageVectorArray=damageVectorArray;
	}

	/**
	 * Adds a new DamageType to this.
	 * @param damageType the damageVector to add.
	 * @return this.
	 */
	public DamageTypeVector addDamageType(DamageType damageType){
		int tmp=damageType.ordinal()>>>3; /* /8 */
		damageVectorArray[tmp]|=1<<(damageType.ordinal()&7);
		return this;
	}
	
	
	/**
	 * Removes the damageVector to this if it possesses it.
	 * @param damageType the damageVector to remove.
	 * @return this.
	 */
	public DamageTypeVector removeDamageType(DamageType damageType){
		int tmp=damageType.ordinal()>>>3; /* /8 */
		damageVectorArray[tmp]&=(~(1<<(damageType.ordinal()&7)));
		return this;
	}
	
	/**
	 * Return true if it has the given DamageType
	 * @param damageType the DamageType to check
	 * @return true if the damage type is 
	 */
	public boolean hasType(DamageType damageType){
		return 0!=(damageVectorArray[damageType.ordinal()>>>3]&1<<(damageType.ordinal()&7));
	}
	
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<arraySize;i++){
			for(int j=0;j<8;j++){
				if(0!=((1<<j)&damageVectorArray[i])){
					sb.append(DamageType.values()[j+(i<<3)]+" ");
				}
			}
		}
		return sb.toString();
	}

	@Override
	public DamageTypeVector clone() {
		return new DamageTypeVector(Arrays.copyOf(damageVectorArray, arraySize));
	}

	/**
	 * Add all property to the DamageTypeVector.
	 * @return this
	 */
	public DamageTypeVector addAll() {
		for(int i=0;i<arraySize;i++)
			damageVectorArray[i]=255;
		return this;
	}
	
	/**
	 * Read a DamageTypeVector from a file
	 * @param br the BufferedReader
	 * @return a new DamageTypeVector with the information from the file
	 * @throws IOException if the BufferedRead can't be read.
	 */
	public static DamageTypeVector readFromFile(BufferedReader br) throws IOException {
		DamageTypeVector dtv=new DamageTypeVector();
		int propertyCount=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		for(int i=0;i<propertyCount;i++){
			dtv.addDamageType(DamageType.valueOf(br.readLine()));
			SpeciesArray.lineError++;
		}
		return dtv;
	}
}
