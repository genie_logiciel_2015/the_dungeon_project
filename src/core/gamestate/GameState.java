package core.gamestate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import core.zone.Zone;

/**
 * This class is used to define the list of all things on the map excepted the physical map itself.
 * @author Guerquin Arnaud
 *
 */
public class GameState implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * The list of entities in the gamestate.
	 */
	private ArrayList<Being> beings=new ArrayList<>();
	private ArrayList<Character> characters=new ArrayList<>();
	private ArrayList<Entity> entities=new ArrayList<>();
	private Semaphore sem=new Semaphore(1);

	/**
	 * Constructs a new empty gamestate.
	 */
	public GameState(){}

	/**
	 * Construct a new gamestate with the given list of entities.
	 * @param entities the list of entities the gamestate starts with.
	 * @throws InterruptedException
	 */
	public GameState(List<Entity> entities) throws InterruptedException{
		sem.acquire();
		for(Entity entity : entities)
			addEntity(entity);
		sem.release();
	}
	/**
	 * Removes all the entities in the given zone.
	 * @param zone : the zone where entities must be removed.
	 * @return true if it worked.
	 * @throws InterruptedException
	 */
	boolean removeZone(Zone zone) throws InterruptedException{
		sem.acquire();
		if(beings.removeIf(x->x.isIn(zone))){
			sem.release();
			return true;
		}
		if(characters.removeIf(x->x.isIn(zone))){
			sem.release();
			return true;
		}
		sem.release();
		return false;
	}

	/**
	 * Adds an Entity in the gameState.
	 * @param entity : the entity to add. Will throw an exception if the given entity isn't a known subclass
	 * @return true if it was added, false otherwise.
	 * @throws InterruptedException
	 */
	public boolean addEntity(Entity entity) throws InterruptedException {
		return entity.hackGamestate(this);
	}

	public boolean addEntityBis(Entity entity) throws InterruptedException{
		sem.acquire();
		boolean bool=entities.add(entity);
		sem.release();
		return bool;
	}

	/**
	 * Adds a being in the gameState.
	 * @param being: the being to add.
	 * @return true if it was added, false otherwise.
	 * @throws InterruptedException
	 */
	public boolean addEntity(Being being) throws InterruptedException{
		sem.acquire();
		boolean bool=beings.add(being);
		sem.release();
		return bool;
	}

	/**
	 * Adds a character in the gameState.
	 * @param character: the character to add.
	 * @return true if it was added, false otherwise.
	 * @throws InterruptedException
	 */
	public boolean addEntity(Character character) throws InterruptedException{
		sem.acquire();
		boolean bool= characters.add(character);
		sem.release();
		return bool;
	}

	/**
	 * Removes the entity with ID id in the GameState.
	 * @param id the entity to remove.
	 * @return true if it was successfully removed. false otherwise.
	 */
	public boolean removeEntity(int id) throws InterruptedException{
		sem.acquire();
		if(entities.removeIf(x->x.getID()==id)){
			sem.release();
			return true;
		}
		if(beings.removeIf(x->x.getID()==id)){
			sem.release();
			return true;
		}
		if(characters.removeIf(x->x.getID()==id)) {
			sem.release();
			return true;
		}
		sem.release();
		return false;
	}

	/**
	 * Removes the given entity from the gameState.
	 * @param entity the entity to remove.
	 * @return true if it was successfully removed. false otherwise.
	 */
	public boolean removeEntity(Entity entity)  throws InterruptedException{
		sem.acquire();
		if(entities.remove(entity)){
			sem.release();
			return true;
		}
		if(beings.remove(entity)){
			sem.release();
			return true;
		}
		if(characters.remove(entity)){
			sem.release();
			return true;
		}
		sem.release();
		return false;
	}


	/**
	 * Returns the entity which has the given ID.
	 * @param id the entity's ID.
	 * @return the Entity with the given id or null if there's none.
	 */
	public Entity getEntity(int id) throws EntityNotFoundExeption,InterruptedException{
		sem.acquire();
		for(Entity e : entities){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		for(Entity e : beings){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		for(Entity e : characters){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		sem.release();
		throw new EntityNotFoundExeption();
	}

	/**
	 * Returns the being which has the given ID.
	 * @param id the being's ID.
	 * @return the Being with the given id or null if there's none.
	 */
	public Being getBeing(int id) throws EntityNotFoundExeption,InterruptedException{
		sem.acquire();
		for(Being e : beings){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		for(Being e : characters){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		sem.release();
		throw new EntityNotFoundExeption();
	}

	/**
	 * Returns the character who has the given ID.
	 * @param id the character's ID.
	 * @return the character with the given id or null if there's none.
	 * @throws InterruptedException
	 */
	public Character getCharacter(int id) throws EntityNotFoundExeption, InterruptedException{
		sem.acquire();
		for(Character e : characters){
			if(e.getID() == id){
				sem.release();
				return e;
			}
		}
		throw new EntityNotFoundExeption();
	}

	/**
	 * Returns a list of all entities in the given zone.
	 * @param zone the zone where are located the entities
	 * @return a list of entities
	 * @throws InterruptedException
	 */
	List<Entity> getEntities(Zone zone) throws InterruptedException{
		sem.acquire();
		ArrayList<Entity> entities= new ArrayList<>();
		entities.addAll(beings);
		entities.addAll(characters);
		List<Entity> list=entities.stream().filter(x->x.isIn(zone)).collect(Collectors.toList());
		sem.release();
		return list;
	}

	/**
	 * Returns a list of all characters in the given zone.
	 * @param zone the zone where are located the characters
	 * @return a list of character
	 * @throws InterruptedException
	 */
	public List<Character> getCharacters(Zone zone) throws InterruptedException{
		sem.acquire();
		List<Character> list=characters.stream().filter(x->x.isIn(zone)).collect(Collectors.toList());
		sem.release();
		return list;
	}

	/**
	 * Returns a list of all characters whose hitbox intersect a given zone
	 * @param zone
	 * @return
	 * @throws InterruptedException
	 */
	public List<Character> getCharacterWhoseHitboxIntersectsWith(Zone zone) throws InterruptedException {
		sem.acquire();
		List<Character> list=characters.stream().filter(x->x.getHitbox().intersect(zone)).collect(Collectors.toList());
		sem.release();
		return list;
	}

	/**
	 * Returns a list of all entities that the given entity can see.
	 * @param entity the entity whose visibility is used
	 * @return a list of entities
	 * @throws InterruptedException
	 */
	List<Entity> getEntities (Entity entity) throws InterruptedException{
		return getEntities(entity.getVisibility());
	}

	/**
	 * Returns all entities in the GameState.
	 * @return all entities in the GameState.
	 */
	public ArrayList<Entity> getAllEntities() {
		ArrayList<Entity> entities= new ArrayList<>();
		entities.addAll(this.entities);
		entities.addAll(beings);
		entities.addAll(characters);
		return entities;
	}
	/**
	 * Returns all beings in the GameState.
	 * @return all beings in the GameState.
	 */
	public ArrayList<Being> getAllBeings() {
		ArrayList<Being> entities= new ArrayList<>();
		entities.addAll(beings);
		entities.addAll(characters);
		return entities;
	}
	/**
	 * Returns all beings that are not a subclass of being in the GameState.
	 * @return all beings that are not a subclass of being in the GameState.
	 * @throws InterruptedException
	 */
	public ArrayList<Being> getOnlyBeings() throws InterruptedException {
		sem.acquire();
		ArrayList<Being> entities= new ArrayList<>();
		entities.addAll(beings);
		sem.release();
		return entities;
	}
	/**
	 * Returns all characters in the GameState.
	 * @return all characters in the GameState.
	 * @throws InterruptedException
	 */
	public ArrayList<Character> getAllCharacters() throws InterruptedException {
		sem.acquire();
		ArrayList<Character> entities= new ArrayList<>();
		entities.addAll(characters);
		sem.release();
		return entities;
	}

	/**
	 * Returns all characters that are not a subclass of character in the GameState.
	 * @return all characters that are not a subclass of character in the GameState.
	 * @throws InterruptedException
	 */
	public ArrayList<Character> getOnlyCharacters() throws InterruptedException {
		return getAllCharacters();
	}
}