package core.gamestate;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import core.abilities.Ability;
import core.abilities.AbilityFactory;
import core.abilities.AbilityKey;
import core.zone.Zone;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import map_generation.tiles.TilePropertyVector;
import map_generation.tiles.TileType;

/**
 * This class is used as a skeleton to create entity
 * @author Guerquin Arnaud
 *
 */
public class EntitySpecies{

	String name;
	TilePropertyVector tpv;
	Zone collisionBox;
	Zone visibility;
	EntityDisplayerType entityDisplayerType;
	ArrayList<TriggerDescriptor> triggers=new ArrayList<>();
	final ArrayList<Ability> abilityList;


	/**
	 * Construct a new Entity species with the information in the species.txt file.
	 * @param br the file where data is read
	 * @throws IOException if the file can't be read
	 */
	EntitySpecies(BufferedReader br) throws IOException{
		br.readLine();
		SpeciesArray.lineError++;
		name=br.readLine();
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		tpv=TilePropertyVector.readFromFile(br);
		br.readLine();
		SpeciesArray.lineError++;
		collisionBox=Zone.readFromFile(br);
		br.readLine();
		SpeciesArray.lineError++;
		visibility=Zone.readFromFile(br);
		br.readLine();
		SpeciesArray.lineError++;
		entityDisplayerType=EntityDisplayerType.valueOf(br.readLine());
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		int triggerCount=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		for(int i=0;i<triggerCount;i++){
			triggers.add(TriggerDescriptor.readFromFile(br));
		}
		br.readLine();
		SpeciesArray.lineError++;
		int abilityCount=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		abilityList=new ArrayList<>(abilityCount);
		for(int i=0;i<abilityCount;i++){
			abilityList.add(AbilityFactory.getInstance().generateAbilityFromKey(AbilityKey.valueOf(br.readLine())));
			SpeciesArray.lineError++;
		}
		
	}

	/**
	 * Return the species' EntityDisplayerType
	 * @return the species' EntityDisplayerType
	 */
	public EntityDisplayerType getEntityDisplayerType() {
		return entityDisplayerType;
	}

	/**
	 * Return the species's name
	 * @return the species's name
	 */
	public String getName(){
		return name;
	}

	/**
	 * Create a new entity using the parameters and the information stored in the species.
	 * @param posX the entity's X position
	 * @param posY the entity's Y position
	 * @param id the entity's id
	 * @param owner the entity's owner
	 * @param name the entity's name
	 * @return a new Entity instantiated with the given parameters.
	 */
	public Entity create(int posX, int posY, int id, int owner, String name) {
		return new Entity(name,this.name,posX, posY, id,owner, collisionBox, tpv, visibility, entityDisplayerType,triggers,abilityList);
	}

	//TODO change this
	public boolean canSpawn(TileType tile) {
		switch (tile){
			case WALL : return false;
			case TORCH : return false;
			case WATER : return name.equals("Ponyta");
			default : return true;
		}
	}

}
