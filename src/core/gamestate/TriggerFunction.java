package core.gamestate;

import java.util.function.Function;
import java.util.function.Supplier;

import core.zone.Zone;

/**
 * This class is used to define all type of function the triggerDescriptor will be able to use.
 * @author Guerquin Arnaud
 *
 */
public enum TriggerFunction {
	DEFAULT_TRIGGER;
	
	static{
		DEFAULT_TRIGGER.mapping=x->x::getCollisionBox;
	}
	public Function<Entity,Supplier<Zone>> mapping;
}
