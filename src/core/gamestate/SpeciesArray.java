package core.gamestate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;

import map_generation.tiles.TileType;
import assets.UsedForLoadingSprites;

/**
 * This class is used to store all species in the game. It load it at the start from the file species.txt.
 * @author Guerquin Arnaud
 *
 */
public class SpeciesArray {
	/**
	 * An hashmap of all species
	 */
	static final HashMap<String,EntitySpecies> array=new HashMap<>();
	
	static int id=0;
	
	/**
	 * Used for debug when reading the file.
	 */
	static public int lineError=0;
	
	/**
	 * Initialise all species from the file species.txt
	 */
	static{
		URL url=UsedForLoadingSprites.getInstance().getClass().getResource("./species/species.txt");
		if(url==null)
			throw new IllegalStateException("Species loading file assets/species/species.txt not found");
		try(BufferedReader br= new BufferedReader(
		        new InputStreamReader(url.openStream()))){
			br.readLine();
			br.readLine();
			lineError=3;
			int speciesCount=Integer.parseInt(br.readLine());
			array.clear();
			lineError++;
			for(int i=0;i<speciesCount;i++){
				br.readLine();
				br.readLine();
				lineError+=2;
				EntitySpecies createdSpecies=Objects.requireNonNull(readFromFile(br));
				array.put(createdSpecies.getName(),createdSpecies);
			}
		}catch(IOException e){
			errorMessage(e.getMessage());
			throw new IllegalStateException("Should not access this line");
		}
	}
	
	/**
	 * Read a species in the given BufferReader
	 * @param br the BufferReader where the species will be read.
	 * @return a new species
	 * @throws IOException if the BufferReader can't be read
	 */
	private static EntitySpecies readFromFile(BufferedReader br) throws IOException{
		char c=br.readLine().charAt(0);
		lineError++;
		switch(c){
		case 'E':
			return new EntitySpecies(br);
		case 'B':
			return new BeingSpecies(br);
		case 'C':
			return new CharacterSpecies(br);
		default:
			lineError--;
			SpeciesArray.errorMessage("Wrong species type");
			throw new IllegalStateException("This line should be unreacheable");
		}
	}

	/**
	 * Create a new entity using the specified species
	 * @param posX the entity's initial X position
	 * @param posY the entity's initial Y position
	 * @param owner the entity's owner
	 * @param speciesName the entity's species
	 * @param name the entity's name
	 * @return a new Entity constructed using the parameters
	 */
	public static Entity create(int posX, int posY, int owner, String speciesName, String name) {
		EntitySpecies species=array.get(speciesName);
		if(species==null)
			throw new IllegalArgumentException("Asked to create a non existant species");
		return array.get(speciesName).create(posX,posY,++id,owner,name);
	}
	
	/**
	 * Create a new Character using the specified CharacterSpecies
	 * @param posX the character's initial X position
	 * @param posY the character's initial Y position
	 * @param owner the character's owner
	 * @param speciesName the entity's species
	 * @param name the character's name
	 * @return a new Character constructed using the parameters
	 */
	public static Character createCharacter(int posX, int posY, int owner, String speciesName, String name) {
		EntitySpecies species=array.get(speciesName);
		if(species==null)
			throw new IllegalArgumentException("Asked to create a non existant species");
		if(species instanceof CharacterSpecies)
			return ((CharacterSpecies)species).create(posX,posY,++id,owner,name);
		throw new IllegalStateException("Asked to create a character with a non character species");
	}
	
	/**
	 * Testing purpose
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(array);
	}
	/**
	 * Beware, throw an exception with the given message !
	 * @param message
	 */
	private static void errorMessage(String message) {
		throw new IllegalStateException("Can't initialise species." + System.lineSeparator()+
				"Line"+lineError + message);
	}
	
	/**
	 * Return true if the species can spawn on the given tile
	 * @param tile the tile to check
	 * @param speciesName the species to check
	 * @return true if the species can spawn on the given tile.
	 */
	 public static boolean canSpawn(TileType tile,String speciesName){
		 EntitySpecies species=array.get(speciesName);
		 return species!=null && species.canSpawn(tile);
	 }
}
