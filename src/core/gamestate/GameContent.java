package core.gamestate;

import core.zone.Point;
import logging.Logging;
import map_generation.map.Map;
import network.inner_shell.NetworkConnection;
import core.event.MapChangeEvent;
import core.relayer.*;
import artificial_intelligence.AIControler;
import artificial_intelligence.AIEntities.EnumBehaviourType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;


/**
 * Just a class to contain a Map and a GameState
 *
 * Possible improvement: design a readonly interface for it, to give to the gui to ensure she isn't able to modify it.
 *
 * @author Dupriez Thomas
 * @author Guerquin Arnaud
 *
 */
public class GameContent implements Serializable{

	private static final long serialVersionUID = 1L;
	private Map map;
	private GameState gameState;
	private ArrayList<Character> players;
	private ArrayList<Entity> triggerEntities=new ArrayList<>();
	private Semaphore sem = new Semaphore(1);

	public GameContent(Map map,int playersNumber) throws InterruptedException {
		/*
		 * Generate a new gamecontent, 
		 * creating the relayers associated to the entities already on the map
		 * adding player and IA at start position
		 */
		this.map=Objects.requireNonNull(map);
		this.gameState=new GameState();
		players=new ArrayList<>();
		if(map.getEntities()!=null) {
			for(Entity e:map.getEntities()){
				gameState.addEntity(e);
				if(e instanceof Character){
					Relayer dmr = Relayers.addNewRelayer((Character) e);
					String name = dmr.getCharacter().getSpeciesName();
					if(name.equals("Ronflex")){
						AIControler.add(this,dmr, EnumBehaviourType.Boss);
					}
					else{
						AIControler.add(this,dmr, EnumBehaviourType.Straight);
					}
				}
			}
		}
		int posX=(map.getPositionPlayerStart().getI())*Point.TileScale;
		int posY=(map.getPositionPlayerStart().getJ())*Point.TileScale;
		for(int i=0;i<playersNumber;i++){
			Character c = SpeciesArray.createCharacter(posX, posY, 0, "Mentali", "mentali"+(i+1));
			c.setFaction(1);
			gameState.addEntity(c);
			players.add(c);

			Character follower = SpeciesArray.createCharacter(posX, posY, 5000, "Ponyta", "ponyta"+(i+1));
			follower.setFaction(1);
			gameState.addEntity(follower);
			Relayer followerRelayer = Relayers.addNewRelayer(follower);
			AIControler.add(this, followerRelayer, EnumBehaviourType.Follower);
		}
		Logging.getInstance().getLogger().info("GameContent class: One gameContent created by"+Thread.currentThread().getId());
		Logging.getInstance().getLogger().info("GameContent class: One gameContent created by"+Thread.currentThread().getId());

	}

	public Map getMap() {
		return map;
	}
	public GameState getGameState() {
		return gameState;
	}

	public void setMap(Map map) {
		this.map = Objects.requireNonNull(map);
	}

	public void setGameState(GameState gameState) {
		this.gameState = Objects.requireNonNull(gameState);
	}

	public List<Entity> getEntityList() {
		return gameState.getAllEntities();
	}
	public List<Being> getOnlyBeings() throws InterruptedException {
		return gameState.getOnlyBeings();
	}
	public List<Being> getAllBeings() {
		return gameState.getAllBeings();
	}

	public ArrayList<Character> getOnlyCharacters() throws InterruptedException {
		return gameState.getOnlyCharacters();
	}
	public ArrayList<Character> getAllCharacters() throws InterruptedException {
		return gameState.getAllCharacters();
	}
	public Character getCharacter(int id) throws EntityNotFoundExeption, InterruptedException {
		return gameState.getCharacter(id);
	}

	public Entity getPlayer(int i) {
		return players.get(i);
	}

	public ArrayList<Character> getAllPlayers() {
		return players;
	}

	/*
	 * Check if the entity has been triggered
	 * */
	
	public void checkTrigger(NetworkConnection networkConnection) throws InterruptedException {
		sem.acquire();
		for(Entity e:triggerEntities){
			e.checkTrigger(this,networkConnection);
		}
		sem.release();
	}

	/*
	 * add the entity to the triggerEntities that must be checked for collision with other entities at every frame
	 * */
	public void addTriggerToCheck(Entity e) throws InterruptedException
	{
		sem.acquire();
		triggerEntities.add(e);
		sem.release();
	}

	public void removeTriggerToCheck(Entity e) throws InterruptedException
	{
		sem.acquire();
		triggerEntities.remove(e);
		sem.release();
	}
	
	/*
	 * Check if a player is on the stairs,
	 * */

	public boolean checkStairs(Map newMap,NetworkConnection networkConnection) throws InterruptedException {
		for(Character player:players){
			if(player.isOnStairs(map)){
				recreate(map,true);
				networkConnection.sendEvent(new MapChangeEvent(newMap));
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Not working, create a new floor,
	 * the issue is that after the new floor is created, there is problem for telling it to the GamePanel
	 * */

	public void recreate(Map map,boolean server) throws InterruptedException {
		this.map=Objects.requireNonNull(map);
		this.gameState=new GameState();
		AIControler.killAll();
		sem.acquire();
		triggerEntities.clear();
		sem.release();
		if(!server)
			Relayers.resetRelayers();
		if(map.getEntities()!=null) {
			for(Entity e:map.getEntities()){
				gameState.addEntity(e);
				if(e instanceof Character){
					Relayer dmr = Relayers.addNewRelayer((Character) e);
					AIControler.add(this,dmr, EnumBehaviourType.Straight);
				}
			}
		}
		int posX=(map.getPositionPlayerStart().getI())*Point.TileScale;
		int posY=(map.getPositionPlayerStart().getJ())*Point.TileScale;
		for(Character player: players){
			player.setXY(posX, posY);
		}
		Logging.getInstance().getLogger().info("GameContent class: One gameContent created by"+Thread.currentThread().getId());
		Logging.getInstance().getLogger().info("GameContent class: One gameContent created by"+Thread.currentThread().getId());

	}


}
