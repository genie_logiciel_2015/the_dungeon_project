package core.gamestate;

/**
 * This enum is used to define all type of damage dealed in the game.
 * @author Guerquin Arnaud
 *
 */
public enum DamageType {
	NEUTRAL,FIRE,ICE,THUNDER,WATER,LIGHT,DARK,OPEN
}
