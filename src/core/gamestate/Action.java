package core.gamestate;

/**
 * This enum is used to define all type of action that an entity can do.
 * @author Guerquin Arnaud
 *
 */
public enum Action {
	WALK,
	ATTACK,
	HIT,
	NONE
}
