package core.gamestate;

import core.abilities.Ability;
import core.zone.Zone;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import map_generation.map.Map;
import map_generation.tiles.Tile;
import map_generation.tiles.TilePropertyVector;
import map_generation.tiles.TileType;

import java.util.ArrayList;
import java.util.List;


/**
 * This class represents a Being possessing characteristics. Can either be a player or a monster.
 * @author Guerquin Arnaud
 *
 */
public final class Character extends Being {

	private static final long serialVersionUID = 1L;
	/**
	 * The character's attack value. Used to inflict physical damage. Must be positive.
	 */
	private int atk;
	/**
	 * The character's intelligence value. Used to inflict magical damage. Must be positive.
	 */
	private int intel;

	private int gold;

	/**
	 * ????
	 */
	private ArrayList<String> buffList = new ArrayList<>();

	/**
	 * Construct a new character
	 * @param name the character's name
	 * @param speciesName the character's species name
	 * @param posX the character's X position
	 * @param posY the character's Y position
	 * @param ID the charater's ID
	 * @param owner the character's owner
	 * @param collisionBox the character's collisionBox
	 * @param tpv the character's TilePropertyVector
	 * @param visibility the charcter's visibility
	 * @param entityDisplayerType the character's ENtityDisplayerType
	 * @param triggers the character's triggers
	 * @param HP the character's HP
	 * @param def the character's defense
	 * @param mental the character's mental
	 * @param dtv the character's damagePropertyVector
	 * @param hitbox the character's hitbox
	 * @param atk the character's attack
	 * @param intel the character's intel
	 * @param abilityList the character's abilityList
	 */
	Character(String name,String speciesName,int posX,int posY,int ID,int owner,Zone collisionBox,TilePropertyVector tpv,Zone visibility
			,EntityDisplayerType entityDisplayerType,List<TriggerDescriptor> triggers,
			int HP,int def,int mental,DamageTypeVector dtv,Zone hitbox,
			int atk,int intel,ArrayList<Ability> abilityList){
		super(name,speciesName,posX, posY, ID,owner, collisionBox, tpv, visibility, entityDisplayerType,triggers,
				HP, def, mental, dtv, hitbox,abilityList);
		this.atk=atk;
		this.intel=intel;
	}

	/**
	 * Returns the character's attack value.
	 * @return the character's attack value.
	 */
	public int getAtk() {
		return atk;
	}

	/**
	 * Returns the character's intelligence value.
	 * @return the character's intelligence value.
	 */
	public int getIntel() {
		return intel;
	}


	/**
	 * Returns the character's gold value.
	 * @return the character's gold value.
	 */
	public int getGold() {
		return gold;
	}

	/**
	 * Set the character's gold value.
	 * @set the character's gold value.
	 */
	public void setGold( int gold2)  {
		this.gold = gold2;
	}

	/**
	 * ????
	 * @return
	 */
	public ArrayList<String> getBuffList() {
		return buffList;
	}

	@Override
	boolean hackGamestate(GameState gameState) throws InterruptedException {
		return gameState.addEntity(this);
	}

	public boolean isOnStairs(Map map) {
		Tile t=map.getTileAt(getCenter().toMapPoint());
		if(t.getType()==TileType.STAIRS)
			return true;
		return false;
	}
}