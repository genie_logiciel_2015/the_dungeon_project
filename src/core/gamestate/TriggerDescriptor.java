package core.gamestate;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import core.abilities.effects.EffectDescriptor;

/**
 * This class is used to instantiate trigger.
 * @author Guerquin Arnaud
 *
 */
public class TriggerDescriptor {
	/**
	 * the function used to get a trigger's mapping function from the caster
	 */
	TriggerFunction mapping;
	/**
	 * The list of effect the trigger should havd
	 */
	ArrayList<EffectDescriptor> effects;

	/**
	 * Construct a new TriggerDescriptor
	 * @param function the mapping function
	 * @param effects the list of effects
	 */
	public TriggerDescriptor(TriggerFunction function,ArrayList<EffectDescriptor> effects){
		mapping=Objects.requireNonNull(function);
		this.effects=Objects.requireNonNull(effects);
	}

	/**
	 * Read a triggerDescriptor from the given BufferedReader
	 * @param br the BufferedReader where the triggerDescriptor will be read
	 * @return a new TriggerDescriptor
	 * @throws IOException if the file can't be read
	 */
	static TriggerDescriptor readFromFile(BufferedReader br) throws IOException{
		br.readLine();
		SpeciesArray.lineError++;
		TriggerFunction function=TriggerFunction.valueOf(br.readLine());
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		ArrayList<EffectDescriptor> effects=new ArrayList<>();
		int count=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		for(int i=0;i<count;i++){
			effects.add(EffectDescriptor.readFromFile(br));
		}
		return new TriggerDescriptor(function,effects);
	}
}
