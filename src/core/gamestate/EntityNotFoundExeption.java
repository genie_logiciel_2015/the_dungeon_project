package core.gamestate;

/**
 * This class represents the exception being sent whenever we try to get an entity throw gamestate which is not in:
 * -Entity might be dead now
 * -You are looking in a too specific subset, (ie : character).
 * Created by etouss on 04/01/2016.
 */
public class EntityNotFoundExeption extends Exception {
	private static final long serialVersionUID = 1L;

}
