package core.gamestate;

import java.io.Serializable;
import java.util.Arrays;

/**
 * This class is used to store all action an entity is currently doing.
 * @author Guerquin Arnaud
 *
 */
public class ActionVector implements Serializable {
	private static int arraySize=Action.values().length/8+1;

	private static final long serialVersionUID = 1L;
	private char propertyArray[];
	
	/**
	 * Construct a new {@link ActionVector} with no property.
	 */
	public ActionVector(){
		propertyArray=new char[arraySize];
	}
	
	/* Used by the clone function */
	private ActionVector(char[] propertyArray) {
		this.propertyArray=propertyArray;
	}

	/**
	 * Adds a new Action to this.
	 * @return this.
	 */
	public ActionVector addAction(Action action){
		int tmp=action.ordinal()>>>3; /* /8 */
		propertyArray[tmp]|=1<<(action.ordinal()&7);
		return this;
	}
	
	
	/**
	 * Removes the property to this if it possesses it.
	 * @return this.
	 */
	public ActionVector removeAction(Action action){
		int tmp=action.ordinal()>>>3; /* /8 */
		propertyArray[tmp]&=(~(1<<(action.ordinal()&7)));
		return this;
	}
	
	public boolean hasAction(Action action){
		return 0!=(propertyArray[action.ordinal()>>>3]&1<<(action.ordinal()&7));
	}
	
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<arraySize;i++){
			for(int j=0;j<8;j++){
				if(0!=((1<<j)&propertyArray[i])){
					sb.append(Action.values()[j+(i<<3)]+" ");
				}
			}
		}
		return sb.toString();
	}

	@Override
	public ActionVector clone() {
		return new ActionVector(Arrays.copyOf(propertyArray, arraySize));
	}
}
