package core.gamestate;

import java.io.BufferedReader;
import java.io.IOException;
/**
 * This class is used to construct character from a "skeleton" found in the species.txt file. 
 * @author Guerquin Arnaud
 *
 */
public class CharacterSpecies extends BeingSpecies{
	final int baseAtk;
	final int baseIntel;

	/**
	 * Construct a characterSpecies using the data in the given BufferReader
	 * @param br the BufferReader where data is read
	 * @throws IOException if can't read from the BufferReader.
	 */
	CharacterSpecies(BufferedReader br) throws IOException {
		super(br);
		br.readLine();
		SpeciesArray.lineError++;
		baseAtk=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		br.readLine();
		SpeciesArray.lineError++;
		baseIntel=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
	}
	
	/**
	 * Create a new character using the parameters and the information stored in the species.
	 */
	
	@Override
	public Character create(int posX, int posY, int id, int owner, String name) {
		return new Character(name,this.name,posX, posY, id,owner, collisionBox, tpv, visibility, entityDisplayerType,triggers,
			baseHP, baseDef, baseMental, dtv, hitbox,
			baseAtk, baseIntel, abilityList);
	}
}
