package core.gamestate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import core.abilities.Ability;
import core.event.Event;
import core.event.SpawnGold;
import core.event.ToServerDeathEvent;
import core.zone.Zone;
import gameloop.LocalGameLoop;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import logging.Logging;
import map_generation.tiles.TilePropertyVector;

/**
 * This class represents an Entity able to be hit and die.
 * @author Guerquin Arnaud
 *
 */
public class Being extends Entity {

	private static final long serialVersionUID = 1L;
	/**
	 * The Being's current HP. Can't be negative.
	 */
	private int HP;

	/**
	 * The Being's max HP. maxHP must always be greater or equal than HP.
	 */
	private int maxHP;
	/**
	 * The character's defense value. Used to protect against physical damage. Must be positive.
	 */
	private int def;
	/**
	 * The character's mental value. Used to protect against magical damage. Must be positive.
	 */
	private int mental;
	/**
	 * The Being's Hitbox.
	 */
	private Zone hitbox;

	private DamageTypeVector dtv;

	/**
	 * Being's constructor
	 * @param name the Being's name.
	 * @param speciesName the name of the species of the Being.
	 * @param posX the Being's center X position. Must be in Point Coordinate.
	 * @param posY the Being's center Y position. Must be in Point Coordinate.
	 * @param ID the Being's ID.
	 * @param owner the Being's Relayer ID.
	 * @param collisionBox the being's collisionBox
	 * @param tpv the being's tilePropertyVector
	 * @param visibility the being's visibility
	 * @param type the being's sprite
	 * @param triggers the list of all triggers of the being.
	 * @param HP the being's HP
     * @param def the being's def
     * @param mental the being's mental
     * @param dtv the being's damagePropertyVector
     * @param hitbox the being's hitbox.
     */
	public Being(String name,String speciesName,int posX,int posY,int ID,int owner,Zone collisionBox,TilePropertyVector tpv,Zone visibility,EntityDisplayerType type,List<TriggerDescriptor> triggers,
			int HP,int def,int mental,DamageTypeVector dtv,Zone hitbox,ArrayList<Ability> abilityList) {
		super(name,speciesName,posX, posY, ID,owner, collisionBox, tpv, visibility, type,triggers,abilityList);
		this.HP= HP;
		this.maxHP=HP;
		this.def=def;
		this.mental=mental;
		this.dtv=Objects.requireNonNull(dtv).clone();
		this.hitbox=Objects.requireNonNull(hitbox).clone(getCenter());
	}

	/**
	 * Returns the Being's current HP
	 * @return the Being's current HP
	 */
	public int getHP(){
		return HP;
	}

	/**
	 * Returns the Being's maxHP
	 * @return Returns the Being's maxHP
	 */
	public int getMaxHP() {return maxHP;}


	/**
	 * Returns the character's defense value.
	 * @return the character's defense value.
	 */
	public int getDef() {
		return def;
	}

	/**
	 * Returns the character's mental value.
	 * @return the character's mental value.
	 */
	public int getMental() {
		return mental;
	}

	/**
	 * Returns the Being's current Exp
	 * @return the Being's current Exp
	 */
	//TODO : implement exp
	public int getExp() {
		return 50;
	}

	/**
	 * Returns the Being's maxExp
	 * @return Returns the Being's maxExp
	 */
	//TODO : implement maxExp
	public int getMaxExp() {
		return 100;
	}

	/**
	 * The being's hitbox.
	 * @return the beings hitbox
	 */
	public Zone getHitbox() {
		return hitbox.clone();
	}

	/**
	 * Applies the given damage to the Being. Return a boolean meaning if this is dead or not.
	 * @param damage the damage to apply. A negative number will heal.
	 * @return true if this is dead. false otherwise.
	 */
	public boolean takeDamage(int damage){
		if(damage>0) {
			addAction(Action.HIT);
		}
		HP=Math.min(maxHP, Math.max(HP-damage, 0));
		if (HP==0){
			Event event = new ToServerDeathEvent(this.getID());
			Logger LOGGER = Logging.getInstance().getLogger();
			if (LocalGameLoop.getInstance().networkConnectionIsNull()){
				LOGGER.severe("NetworkConnection null, Being"+this.getID()+" can't die");
			} else {
				LOGGER.info("Death of "+this.getID()+" is send");
				LocalGameLoop.getInstance().sendEvent(event);

			}
			Event event2 = new SpawnGold(this.getID());
			if (LocalGameLoop.getInstance().networkConnectionIsNull()){
				LOGGER.severe("NetworkConnection null, GOld"+this.getID()+" can't be created");
			} else {
				LOGGER.info("Gold creation by " + this.getID() + " is send");
				LocalGameLoop.getInstance().sendEvent(event2);
			}

		}
		return isDead();
	}

	/**
	 * Return true is the being is sensible to the given damage
	 * @param damage the damageType used to check sensibility
	 * @return true if the being is sensible
	 */
	public boolean isSensible(DamageType damage){
		return dtv.hasType(damage);
	}

	/**
	 * Tells if the current being is dead.
	 * @return true if this is dead. false otherwise.
	 */
	public boolean isDead(){
		return HP==0;
	}


	@Override
	boolean hackGamestate(GameState gameState) throws InterruptedException {
		return gameState.addEntity(this);
	}
}