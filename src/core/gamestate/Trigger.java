package core.gamestate;

import java.io.Serializable;
import java.util.ArrayList;

import core.abilities.effects.Effect;
import core.abilities.effects.EffectDescriptor;
import core.abilities.effects.EffectFactory;
import core.zone.Zone;

/**
 * This class is used to represents effects checked at the beginning of each frame by the gameloop
 * @author Guerquin Arnaud
 *
 */
public class Trigger implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * The trigger's effect area
	 */
	private TriggerFunction mappingFunction;

	/**
	 * the trigger's list of effect
	 */
	private ArrayList<EffectDescriptor> effectList;

	/**
	 * the ID of the entity possessing the trigger.
	 */
	private Entity entity;
	/**
	 * the trigger's id
	 */
	int id;

	/**
	 * Construct a new trigger
	 * @param mappingFunction the trigger's mapping function
	 * @param effectList the trigger's list of effect.
	 */
	public Trigger(TriggerFunction mappingFunction, ArrayList<EffectDescriptor> effectList,Entity entity) {
		this.mappingFunction=mappingFunction;
		this.effectList=effectList;
		this.entity=entity;
		id=0;
	}

	/**
	 * Change the trigger's id
	 * @param id the new id
	 */
	public void setID(int id){
		this.id=id;
	}

	/**
	 * Constuct a new trigger
	 * @param entity the trigger's caster
	 * @param triggerDescriptor the triggerDescriptor used to instantiate the trigger.
	 * @return a new trigger.
	 */
	static public Trigger construct(Entity entity,TriggerDescriptor triggerDescriptor){
		return new Trigger(triggerDescriptor.mapping,triggerDescriptor.effects,entity);
	}

	/**
	 * return the trigger's effectZone
	 * @return the trigger's effectZone
	 */
	public Zone getEffectZone(){
		return mappingFunction.mapping.apply(entity).get();
	}

	/**
	 * Return the trigger's effect list
	 * @return the trigger's effect list
	 */
	public ArrayList<Effect> getEffectList(){
		ArrayList<Effect> effects=new ArrayList<>();
		for(EffectDescriptor ed: effectList){
			effects.add(EffectFactory.getInstance().getEffectFromEffectDescriptorAndCasterID(ed, entity.getID()));
		}
		return effects;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Trigger))
			return false;
		Trigger t=(Trigger)obj;
		return t.id==id;
	}

	@Override
	public int hashCode() {
		return id;
	}
}
