package core.relayer;

import core.event.AbilityEvent;
import core.event.Event;
import core.event.MoveEvent;
import core.gamestate.Action;
import core.gamestate.Character;
import core.zone.Direction;
import core.zone.Translation;
import gameloop.LocalGameLoop;
import gameloop.ServerLoop;

import java.util.Objects;

/**
 * This abstract class is what was previously known as the DecisionMaker.
 * It takes the input from the GUI / AI and transmit it to the GameLoop as Event.
 * @author Guerquin Arnaud
 *
 *It knows extends RelayerEntity
 *
 */

public class Relayer extends RelayerEntity{

	
	
	private Character character;

	private boolean hasTriedToCastAbility = false;
	/**
	 * the chracter's current ability
	 */
	private int currentAbility=0;

	/**
	 * Create a new relayer for the entity on the given gameContent.
	 * @param ID the relayer's ID
	 * @param entity the entity the relayer is using
	 * @param gameContent the gameContent the entity is on.
	 */
	public Relayer(int ID,Character character){
		this.ID=ID;
		this.character=(Character)Objects.requireNonNull(character);
	}

	private void applyMove() {
		if(direction==Direction.NONE){
			stopMove();
			return;
		}
		Event event = new MoveEvent(character.getID(), Translation.construct(direction,character.getSpeed()));
		LocalGameLoop.getInstance().sendEvent(event);
	}

	private void stopMove(){
		character.removeAction(Action.WALK);
	}

	public void tryToCastAbility(int abilityNb) {
		assert abilityNb >= 0;
		if (abilityNb >= getCharacter().getAbilityList().size()) {
			throw new RuntimeException("in Relayer: method tryToCastAbility called with an ability number not corresponding to any ability of the underlying character");
		}
		currentAbility = abilityNb;
		hasTriedToCastAbility = true;
	}

	/**
	 * Send an ability event.
	 */
	private void applyAbility() {
		Event event = new AbilityEvent(character.getID(), currentAbility);
		LocalGameLoop.getInstance().sendEvent(event);
		hasTriedToCastAbility = false;
	}

	/**
	 * Return the relayer's character
	 * @return the relayer's character
	 */
	public Character getCharacter(){
		return character;
	}

	/**
	 * Check at each frame if the relayer has to transmit some event.
	 */
	@Override
	public void run(){
		while(!end && !character.isDead()){
			applyMove();
			if(hasTriedToCastAbility && !character.isDead())
				applyAbility();
			long ctime= System.currentTimeMillis();
			try{
			    Thread.sleep(ServerLoop.frame - (ctime%ServerLoop.frame));
			}catch(InterruptedException e){
			}
		}

	}


	public void reset() {
		end=false;
	}
}
