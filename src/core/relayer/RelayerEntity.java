package core.relayer;

import core.event.Event;
import core.event.MoveEvent;
import core.gamestate.Action;
import core.gamestate.Entity;
import core.zone.Direction;
import core.zone.Translation;
import gameloop.LocalGameLoop;
import gameloop.ServerLoop;

import java.util.Objects;

/**
 * A simpler version of theRelayer made in order to match the entity instead of character
 * -It allow entity to move, smoothly at each refresh of the gamestate.
 * Relayer is an extension of this now
 */
public class RelayerEntity extends Thread{

	/**
	 * the relayer's id
	 */
	protected int ID;
	/**
	 * the entity for which the relayer create event
	 */
	protected Entity character;
	/**
	 * kill the relayer if true
	 */
	protected boolean end=false;
	/**
	 * the direction the relayer should move the character to
	 */
	protected Direction direction=Direction.NONE;

	public RelayerEntity(){
	
	}
	
	public RelayerEntity(int ID, Entity entity){
		this.ID=ID;
		this.character=Objects.requireNonNull(entity);
	}

	/**
	 * Return the relayer's ID
	 * @return the relayer's ID
	 */
	public final int getID(){
		return ID;
	}

	private void applyMove() {
		if(direction==Direction.NONE){
			stopMove();
			return;
		}
		Event event = new MoveEvent(character.getID(), Translation.construct(direction,character.getSpeed()));
		LocalGameLoop.getInstance().sendEvent(event);
	}

	private void stopMove(){
		character.removeAction(Action.WALK);
	}


	public Entity getCharacter(){
		return character;
	}

	@Override
	public void run(){
		while(!end){
			applyMove();
			long ctime= System.currentTimeMillis();
			try{
			    Thread.sleep(ServerLoop.frame - (ctime%ServerLoop.frame));
			}catch(InterruptedException e){
			}
		}

	}

	/**
	 * Change the direction the relayer will move to.
	 * @param dir the new direction.
	 */
	public void move(Direction dir){
		direction=dir;
	}

	/**
	 * Return the relayer's direction
	 * @return the relayer's direction
	 */
	public Direction getDirection(){
		return direction;
	}

	public void kill() {
		end=true;
	}
}
