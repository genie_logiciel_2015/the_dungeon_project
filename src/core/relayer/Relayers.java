package core.relayer;

import java.util.ArrayList;
import core.gamestate.Character;

import core.gamestate.Entity;

/**
 * This class is used to instantiate and store all relayers.
 * @author Guerquin Arnaud
 * @author Toussain Etienne
 */
public class Relayers{
	private static ArrayList<RelayerEntity> relayers=new ArrayList<>();
	private static int ID=0;
	private static Relayer followedRelayer;

	/**
	 * Add a new relayer
	 * @param entity the entity of the new relayer
	 * @return a new relayer
	 */
	public static Relayer addNewRelayer(Character character){
		Relayer relayer=new Relayer(ID++,character);
		relayers.add(relayer);
		relayer.start();
		return relayer;
	}
	
	public static RelayerEntity addNewRelayer(Entity entity){
		RelayerEntity relayer=new RelayerEntity(ID++,entity);
		relayers.add(relayer);
		relayer.start();
		return relayer;
	}
	
	public static RelayerEntity addnewRelayer(Entity entity){
		RelayerEntity relayer=new RelayerEntity(ID++,entity);
		relayers.add(relayer);
		relayer.start();
		return relayer;
	}

	/**
	 * kill all relayer
	 */
	public static void killAll(){
		for(RelayerEntity relayer:relayers)
			relayer.kill();
		relayers.clear();
	}

	public static void resetRelayers(){
		killAll();
		if(followedRelayer!=null){
			followedRelayer.reset();
			relayers.add(followedRelayer);
			followedRelayer.start();
		}
	}

	public static Relayer addFollowedRelayer(Character character) {
		Relayer relayer = addNewRelayer(character);
		if(followedRelayer!=null){
			followedRelayer.kill();
			followedRelayer=relayer;
		}
		return relayer;
	}
}
