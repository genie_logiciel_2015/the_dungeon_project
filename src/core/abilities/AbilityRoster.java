package core.abilities;

import core.abilities.effects.EffectDescriptor;
import core.abilities.effects.EffectKey;
import core.gamestate.Action;
import graphics.graphical_utility.abilityIconDisplay.AbilityIconKeys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

/**
 * Created by dupriez on 23/12/15.
 *
 * Contains all the bindings AbilityKey -> AbilityGenerator of the game, and a method ("registerAllAbilityGenerators")to
 * initialise an AbilityFactory with them
 */
public class AbilityRoster {

	static void registerAllAbilityGenerators(AbilityFactory abilityFactory) {
		Supplier<Ability> abilityGenerator;

		/** ############## ADD NEW ABILITY GENERATORS HERE ############## **/

		abilityGenerator =
			() -> new Ability(
				500,
				AbilityIconKeys.NULL_ABILITY,
				Action.ATTACK,
				new ArrayList<EffectDescriptor>()
			);
		abilityFactory.registerAbilityGenerator(AbilityKey.NULL_ABILITY, abilityGenerator);

		abilityGenerator =
			() -> new Ability(
				100,
				AbilityIconKeys.SWORD_RED_1,
				Action.ATTACK,
				new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.TEST_ATTACK_EFFECT, new int[]{1})))
			);
		abilityFactory.registerAbilityGenerator(AbilityKey.TEST_ATTACK_ABILITY, abilityGenerator);

		abilityGenerator =
				() -> new Ability(
						100,
						AbilityIconKeys.SWORD_RED_1,
						Action.ATTACK,
						new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.TEST_ATTACK_FRIENDLY, new int[]{1})))
				);
		abilityFactory.registerAbilityGenerator(AbilityKey.TEST_ATTACK_ABILITY_FRIENDLY, abilityGenerator);



		abilityGenerator =
			() -> new Ability(
				2000,
				AbilityIconKeys.MENTALI_ATTACK,
				Action.ATTACK,
				new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.MENTALI_ATTACK_DAMAGE, new int[]{10000,32})))
			);
		abilityFactory.registerAbilityGenerator(AbilityKey.MENTALI_ATTACK, abilityGenerator);






		abilityGenerator =
				() -> new Ability(
						2000,
						AbilityIconKeys.FROST_BOLT_1,
						Action.ATTACK,
						new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.TEST_FIREBOLT, new int[]{5,10})))
				);
		abilityFactory.registerAbilityGenerator(AbilityKey.TEST_FIREBOLT, abilityGenerator);

		abilityGenerator =
				() -> new Ability(
						2000,
						AbilityIconKeys.FIRE_BOLT_1,
						Action.ATTACK,
						new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.TEST_LP_FIREBOLT, new int[]{5,10})))
				);
		abilityFactory.registerAbilityGenerator(AbilityKey.TEST_LP_FIREBOLT, abilityGenerator);

		abilityGenerator =
				() -> new Ability(
						2000,
						AbilityIconKeys.SHIELD_1,
						Action.ATTACK,
						new ArrayList<>(Arrays.asList(new EffectDescriptor(EffectKey.TEST_SUICIDE, new int[]{2})))
				);
		abilityFactory.registerAbilityGenerator(AbilityKey.TEST_SUICIDE, abilityGenerator);
	}

}
