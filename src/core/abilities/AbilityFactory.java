package core.abilities;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by dupriez on 19/12/15.
 *
 * Builds instances of Ability from AbilityKey.
 * To be used by the CharacterFactory, when creating the abilities of the Character.
 *
 * Implements the Singleton pattern
 */
public class AbilityFactory {
	/** Implementation of the Singleton pattern **/
	private static AbilityFactory ourInstance = new AbilityFactory();

	private EnumMap<AbilityKey,Supplier<Ability>> abilityGeneratorMap = new EnumMap<>(AbilityKey.class);
	public static AbilityFactory getInstance() {return ourInstance;}
	private  AbilityFactory() {
		AbilityRoster.registerAllAbilityGenerators(this);
	}

	/**
	 * Search abilityGeneratorMap for the AbilityGenerator associated to the abilityKey argument. Call the generate() method
	 * of this generator to get a fresh instance of Ability and returns it.
	 * @param abilityKey Indicates the Ability the caller wants to get from this method
	 * @return A fresh instance of Ability, corresponding to abilityKey
	 */
	public Ability generateAbilityFromKey(AbilityKey abilityKey) {
		Supplier<Ability> ag = abilityGeneratorMap.get(abilityKey);
		if (ag == null) {
			throw new AbilityKeyNotRegisteredException(abilityKey);
		}
		return ag.get();
	}
	private class AbilityKeyNotRegisteredException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public AbilityKeyNotRegisteredException(AbilityKey abilityKey) {
			super("in AbilityFactory: method \"generateAbilityFromKey\" was called with argument:" + abilityKey +", which is not registered in AbilityFactory");
		}
	}


	/**
	 * Adds the binding AbilityKey -> AbilityGenerator to the abilityGeneratorMap
	 * Throws an exception if abilityKey was already bound in the hashmap
	 * Test that abilityGenerator works by executing it once. This may throw an exception.
	 * @param abilityKey
	 * @param abilityGenerator
	 */
	void registerAbilityGenerator(AbilityKey abilityKey, Supplier<Ability> abilityGenerator) {
		//Checking if abilityKey is not already bound to an abilityGenerator
		if (abilityGeneratorMap.containsKey(abilityKey) == false) {
			//The hashmap does not contain the key abilityKey

			// Checking that the abilityGenerator works
			abilityGenerator.get();

			//If the abilityGenerator works (if the preceding code ran without throwing exceptions), register the couple of arguments normally
			abilityGeneratorMap.put(abilityKey, abilityGenerator);		}
		else {
			//The hashmap already contains a mapping for the key abilityKey, throwing an exception
			throw new AbilityKeyAlreadyRegisteredException(abilityKey, abilityGenerator);
		}
	}
	/**
	 * An exception class for the registerAbilityGenerator method
	 */
	private class AbilityKeyAlreadyRegisteredException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		AbilityKeyAlreadyRegisteredException(AbilityKey abilityKey, Supplier<Ability> abilityGenerator) {
			super("in AbilityFactory: someone tried to register an AbilityGenerator (= " + abilityGenerator + ") " +
					"with an already used AbilityKey (= " + abilityKey.name() + ")");
		}
	}

	/**
	 * For each of the values of the AbilityKey enum, check that it is registered in abilityGeneratorMap. Return the list
	 * of the AbilityKeys that are not registered.
	 * @return The list of the AbilityKeys that are not registered.
	 */
	List<AbilityKey> checkAbilityKeyRegisteringExhaustivity() {
		List<AbilityKey> unregisteredAbilityKeys = new ArrayList<>();
		for (AbilityKey abilityKey : AbilityKey.values()) {
			if (abilityGeneratorMap.containsKey(abilityKey) == false) {
				//abilityKey is not registered in abilityGeneratorMap,
				unregisteredAbilityKeys.add(abilityKey);
			}
		}
		return unregisteredAbilityKeys;
	}

}
