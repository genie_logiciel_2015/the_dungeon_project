package core.abilities;

/**
 * Created by dupriez on 19/12/15.
 */
public enum AbilityKey {
	NULL_ABILITY,
	TEST_ATTACK_ABILITY,
	TEST_ATTACK_ABILITY_FRIENDLY,
	TEST_LP_FIREBOLT,
	TEST_FIREBOLT,
	MENTALI_ATTACK,
	TEST_SUICIDE;
}
