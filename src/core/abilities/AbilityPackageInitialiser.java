package core.abilities;

import logging.Logging;

import java.util.logging.Logger;

/**
 * Created by dupriez on 23/12/15.
 *
 * This class is there to contain the method "initialisePackage" (see its description)
 */
public class AbilityPackageInitialiser {
	/**
	 * To be called at the beginning of the program. Make the JVM load some classes of the "ability" package, so that
	 * the initialisation of these classes is done as early as possible, because it may throw exceptions if there are
	 * uncorrectly defined Abilities
	 * Also prints warnings for AbilityKeys that are not registered in the AbilityFactory, which may comes from an error in
	 * AbilityRoster.
	 */
	public static void initialisePackage() {
		Logger LOGGER = Logging.getInstance().getLogger();
		LOGGER.info("Initialising package abilities...");
		AbilityFactory.getInstance();
		for (AbilityKey unregisteredAbilityKey : AbilityFactory.getInstance().checkAbilityKeyRegisteringExhaustivity()) {
			LOGGER.warning("* Warning: AbilityKey not registered in AbilityFactory: \"" + unregisteredAbilityKey.name() + "\"");
		}
		LOGGER.info("...package abilities initialised");
	}

}
