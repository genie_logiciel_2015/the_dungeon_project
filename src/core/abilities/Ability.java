package core.abilities;

import core.abilities.effects.EffectDescriptor;
import core.gamestate.Action;
import graphics.graphical_utility.abilityIconDisplay.AbilityIconKeys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hilaire on 16/12/15.
 *
 * cooldown system originally designed by ghocquet
 */
public class Ability implements Serializable{

	private static final long serialVersionUID = 1L;
	private int cooldown_ms;
    private long lastUseTime = 0 ;
    private AbilityIconKeys iconKey ;
    private Action action;
    private List<EffectDescriptor> effectDescriptorList = new ArrayList<>();

    public Ability(int cooldown_ms, AbilityIconKeys iconKey, Action action, List<EffectDescriptor> effectDescriptorList) {
        if( cooldown_ms <= 0){
            throw new IllegalArgumentException("in class Ability: Ability constructor called with cooldown <= 0)");
        }
        this.cooldown_ms = cooldown_ms;
        this.iconKey = iconKey ;
        this.action = action;
        this.effectDescriptorList = effectDescriptorList;
    }
    /**
     * try to cast the ability if it is ready to be cast (cooldown_ms).
//     * @param castingCharacterID the ID of the Character casting this Ability
     * @return A list of EffectDescriptor that is either the list of the EffectDescriptors of the Ability if it is successfully cast, or null if not
     */
    public List<EffectDescriptor> cast(/*int castingCharacterID*/) {
        if (isReady()) {
            lastUseTime = new java.util.Date().getTime() ;
            return effectDescriptorList;
        }
        return null;
    }

    /**
     *
     * @return a float indicating the ratio of the cooldown_ms that remains to wait before another cast is possible
     * (a.k.a. time that remains to wait / cooldown_ms )
     */
    public float getCharged() {
        long now = new java.util.Date().getTime() ;
        if (now > lastUseTime + cooldown_ms) {
            return 0;
        }
        return (float) (lastUseTime + cooldown_ms - now )/ cooldown_ms;
    }

    /**
     *
     * @return whether the ability is ready to be cast or not
     */
    public boolean isReady() {
        return (getCharged()==0) ;
    }

    /**
     *
     * @return the AbilityIconKey that defines the icon that will be used to display
     * the ability on the user interface
     */
    public AbilityIconKeys getAbilityIconKey() {
        return iconKey ;
    }

    /**
     *
     * @return the Action defining what the Character
     */
    public Action getAction() {
        return action;
    }
}
