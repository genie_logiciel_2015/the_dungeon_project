package core.abilities.effects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dupriez on 19/12/15.
 *
 * Usage:
 * This class is used by the AbilityEvent to generate an instance of Effect corresponding to an instance of EffectDescriptor
 * and the id of the Character that casted the Ability that triggered the Effect.
 *
 * Implements the Singleton pattern
 */
public class EffectFactory {
	/** Singleton pattern implementation **/
	private static EffectFactory ourInstance = new EffectFactory();
	private HashMap<EffectKey,EffectGenerator> effectGeneratorMap = new HashMap<>();
	

	/**
	 * An exception class for the registerEffectGenerator method
	 */
	private class EffectKeyAlreadyRegisteredException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		EffectKeyAlreadyRegisteredException(EffectKey effectKey, EffectGenerator effectGenerator) {
			super("in EffectFactory: someone tried to register an EffectGenerator (= " + effectGenerator + ") " +
				"with an already used EffectKey (= " + effectKey.name() + ")");
		}
	}
	
	private class EffectKeyNotRegisteredException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public EffectKeyNotRegisteredException(EffectKey effectKey) {
			super("in AbilityFactory: method \"generateAbilityFromKey\" was called with argument:" + effectKey +", which is not registered in AbilityFactory");
		}
	}

	
	private EffectFactory() {
		EffectGeneratorRoster.registerAllEffectGenerators(this);
	}

	public static EffectFactory getInstance() {
		return ourInstance;
	}
	
	/**
	 * Generate an instance of Effect corresponding to an effectDescriptor, and with
	 * the id of the Character that casted the Ability that triggered the Effect.
	 * @param effectDescriptor
	 * @param casterCharacterID
	 * @return
	 */
	public Effect getEffectFromEffectDescriptorAndCasterID(EffectDescriptor effectDescriptor, int casterCharacterID) {
		return getEffectGeneratorFromKey(effectDescriptor.getEffectKey()).generate(casterCharacterID, effectDescriptor);
	}


	/**
	 * Adds the binding effectKey -> effectGenerator to effectGeneratorMap.
	 * Throws an exception if effectKey was already bound in the hashmap
	 * Test that effectGenerator works by executing it once. This may throw an exception.
	 * @param effectKey
	 * @param effectGenerator
	 */
	void registerEffectGenerator(EffectKey effectKey, EffectGenerator effectGenerator) {
		//Checking if effectKey is not already bound to an effectGenerator
		if (effectGeneratorMap.containsKey(effectKey) == false) {
			//The hashmap does not contain the key effectKey

			/** Checking that the effectGenerator works **/
			int[] testParameter = new int[effectKey.getParameterNb()];
			EffectDescriptor testEffectDescriptor = new EffectDescriptor(effectKey, testParameter);
			effectGenerator.generate(0,testEffectDescriptor);

			//If the effectGenerator works (if the preceding code ran without throwing exceptions), register the couple of arguments normally
			effectGeneratorMap.put(effectKey, effectGenerator);
		}
		else {
			//The hashmap already contains a mapping for the key effectKey, throwing an exception
			throw new EffectKeyAlreadyRegisteredException(effectKey, effectGenerator);
		}
	}

	private EffectGenerator getEffectGeneratorFromKey(EffectKey effectKey) {
		EffectGenerator effectGenerator = effectGeneratorMap.get(effectKey);
		if (effectGenerator == null) {
			throw new EffectKeyNotRegisteredException(effectKey);
		}
		return effectGenerator;
	}
	
	/**
	 * For each of the values of the EffectKey enum, check that it is registered in effectGeneratorMap. Return the list
	 * of the EffectKeys that are not registered.
	 * @return The list of the EffectKeys that are not registered.
	 */
	List<EffectKey> checkEffectKeyRegisteringExhaustivity() {
		List<EffectKey> unregisteredEffectKeys = new ArrayList<>();
		for (EffectKey effectKey : EffectKey.values()) {
			if (effectGeneratorMap.containsKey(effectKey) == false) {
				//effectKey is not registered in effectGeneratorMap,
				unregisteredEffectKeys.add(effectKey);
			}
		}
		return unregisteredEffectKeys;
	}

}
