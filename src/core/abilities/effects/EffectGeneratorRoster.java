package core.abilities.effects;

import core.zone.Translation;

/**
 * Created by dupriez on 22/12/15.
 *
 * Contains all the bindings EffectKey -> EffectGenerator of the game, and a method ("registerAllEffectGenerators")to
 * initialise an EffectFactory with them
 */
public final class EffectGeneratorRoster {

	private static EffectBuilder eb = EffectBuilder.getInstance();

	/**
	 * Called by the initialiser of the effectGeneratorMap of the EffectFactory class to register all the
	 * couples (effectKey, effectGenerator) that exists in the game.
	 * @param effectFactory The EffectFactory whose effectGeneratorMap should be initialised
	 */
	static void registerAllEffectGenerators(EffectFactory effectFactory) {
		EffectGenerator effectGenerator;

		/** ############## ADD NEW EFFECT GENERATORS HERE ############## **/
		effectGenerator =
			(casterCharacterID, parameters) -> {
				int amountOfDamage = parameters.getNthParameter(0);
				return eb.affectZone((character -> character.getHitbox()),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
								eb.reduceHP(amountOfDamage)));
			};
		effectFactory.registerEffectGenerator(EffectKey.TEST_ATTACK_EFFECT, effectGenerator);

		effectGenerator =
				(casterCharacterID, parameters) -> {
					int amountOfDamage = parameters.getNthParameter(0);
					return eb.affectZone((character -> character.getHitbox()),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
									eb.reduceHP_friendly(amountOfDamage)));
				};
		effectFactory.registerEffectGenerator(EffectKey.TEST_ATTACK_FRIENDLY, effectGenerator);



			effectGenerator =
			(casterCharacterID, parameters) -> {
				int amountOfDamage = parameters.getNthParameter(0);
				int range = parameters.getNthParameter(1);
				return eb.affectZone((character -> (character.getHitbox().clone().translate(Translation.construct(character.getDirection(),range)) ) ),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
								eb.reduceHP(amountOfDamage)));
			};
		effectFactory.registerEffectGenerator(EffectKey.MENTALI_ATTACK_DAMAGE, effectGenerator);


		effectGenerator =
				(casterCharacterID, parameters) -> {
					return eb.suicide();
				};
		effectFactory.registerEffectGenerator(EffectKey.TEST_SUICIDE, effectGenerator);



		effectGenerator =
				(casterCharacterID, parameters) -> {
					int amountOfDamage = parameters.getNthParameter(0);
					int speed = parameters.getNthParameter(1);
					return eb.spawnFrostBolt(amountOfDamage,speed);
				};
		effectFactory.registerEffectGenerator(EffectKey.TEST_FIREBOLT, effectGenerator);

		effectGenerator =
				(casterCharacterID, parameters) -> {
					int amountOfDamage = parameters.getNthParameter(0);
					int speed = parameters.getNthParameter(1);
					return eb.spawnLPFireBolt(amountOfDamage, speed);
				};
		effectFactory.registerEffectGenerator(EffectKey.TEST_LP_FIREBOLT, effectGenerator);

		effectGenerator =
				(casterCharacterID, parameters) -> {
					return eb.affectZone((character -> character.getHitbox()),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
									eb.trigger_degat()));
				};
		effectFactory.registerEffectGenerator(EffectKey.TRIGGER_DEGAT, effectGenerator);
		effectGenerator =
				(casterCharacterID, parameters) -> {
					return eb.affectZone((character -> character.getHitbox()),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
									eb.trigger_degat_lp()));
				};
		effectFactory.registerEffectGenerator(EffectKey.TRIGGER_DEGAT_LP, effectGenerator);
		effectGenerator =
				(casterCharacterID, parameters) -> {
					return eb.affectZone((character -> character.getHitbox()),
							eb.filter( CommonCharacterIDListFilter.allExceptOne(casterCharacterID),
									eb.trigger_gold()));
				};
		effectFactory.registerEffectGenerator(EffectKey.TRIGGER_GOLD, effectGenerator);


	}

}
