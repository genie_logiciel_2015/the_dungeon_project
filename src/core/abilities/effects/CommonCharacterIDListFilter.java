package core.abilities.effects;

import core.gamestate.*;

import java.io.Serializable;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

/**
 * Created by dupriez on 20/12/15.
 *
 * This class contains some premade and of common use CharacterIDListFilter methods
 */
public class CommonCharacterIDListFilter {

	/**
	 *
	 * @param characterIDToIgnore
	 * @return A CharacterIDListFilter that only remove elements equal to characterIDToIgnore
	 */

	static UnaryOperator<List<Integer>> allExceptOne(int characterIDToIgnore) {
		return charaIDList -> {charaIDList.removeIf(x->(x==characterIDToIgnore));return charaIDList;};
	}

	/**
	 *
	 * @param characterIDToIgnore
	 * @param characterIDToIgnore2
	 * @returnA CharacterIDListFilter that both remove elements equal to characterIDToIgnore and characterIDToIgnore2
	 */
	static UnaryOperator<List<Integer>> allExceptTwo(int characterIDToIgnore, int characterIDToIgnore2) {
		return charaIDList -> {charaIDList.removeIf(x->(x==characterIDToIgnore ||x== characterIDToIgnore2 ));return charaIDList;};
	}


}
