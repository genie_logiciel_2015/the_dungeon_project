package core.abilities.effects;

import logging.Logging;

/**
 * Created by dupriez on 23/12/15.
 *
 * This class is there to contain the method "initialisePackage" (see its description)
 */
public class EffectPackageInitialiser {

	/**
	 * To be called at the beginning of the program. Make the JVM load some classes of the "ability" package, so that
	 * the initialisation of these classes is done as early as possible, because it may throw exceptions if there are
	 * uncorrectly defined Effects
	 * Also prints warnings for EffectKeys that are not registered in the EffectFactory, which may comes from an error in
	 * EffectRoster.
	 */
	public static void initialisePackage() {
		Logging.getInstance().getLogger().info("Initialising package effects...");
		EffectFactory.getInstance();
		for (EffectKey unregisteredEffectKey : EffectFactory.getInstance().checkEffectKeyRegisteringExhaustivity()) {
			Logging.getInstance().getLogger().info("* Warning: EffectKey not registered in EffectFactory: \"" + unregisteredEffectKey.name() + "\"");
		}
		Logging.getInstance().getLogger().info("...package effects initialised");
	}

}
