package core.abilities.effects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;

import core.gamestate.SpeciesArray;

/**
 * Created by dupriez on 21/12/15.
 *
 * Contains an EffectKey and an array of int. The EffectKey defines an Effect (basic or composed), and the array of int
 * is the set of parameters required by the Effect to be instanciated.
 */
public class EffectDescriptor implements Serializable{

	private static final long serialVersionUID = 1L;
	private EffectKey effectKey;
	private int[] parametersArray;

	public EffectDescriptor(EffectKey effectKey, int[] parametersArray) {
		if (parametersArray.length != effectKey.getParameterNb()) {
			//Problem, someone tried to intanciate an EffectDescriptor with an int array that does not contain as many values
			// as the value of the parameterNb field of the effectKey passed.
			//Throwing exception
			throw new InvalidEffectDescriptorInstanciationException(parametersArray.length, effectKey);
		}
		this.effectKey = effectKey;
		this.parametersArray = parametersArray;
	}
	private class InvalidEffectDescriptorInstanciationException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		InvalidEffectDescriptorInstanciationException(int parameterArrayLength, EffectKey effectKey) {
			super("in EffectDescriptor constructor: length of argument \"parameterArray\"" +
				" is different from the parameterNb field associated to the argument \"effectKey\":\n" +
				"\t parameterArray.length = " + parameterArrayLength + "\n" +
				"\t effectKey = " + effectKey.name() + "\n" +
				"\t effectKey.parameterNb = " + effectKey.getParameterNb()
			);
		}
	}

	/**
	 * This method provide access to the value of the parameters stored in this EffectDescriptor.
	 * It checks if it's argument parameterIndex is:
	 * 	- non-negative
	 * 	- strictly lesser than the number of parameters declared in the EffectKey associated to this EffectDescriptor
	 * It throws an ArrayIndexOutOfBoundsException if one of these checks fails.
	 * @param parameterIndex the index of the parameter in the parameterArray the caller wants.
	 * @return the value stored at index parameterIndex in the array parameterArray
	 */
	public int getNthParameter(int parameterIndex) {
		//Check if parameterIndex is non-negative
		if (0 <= parameterIndex) {
			//Check passed, parameterIndex is non-negative, continuing normal execution.

			//Check if parameterIndex is in the bounds of the parameterArray of this EffectDescriptor
			// (Reminder: parameterArray.length == effectKey.getParameterNb() is enforced by the constructor of EffectDescriptor)
			if (parameterIndex < effectKey.getParameterNb()) {
				//Check passed, returning the asked parameter
				return parametersArray[parameterIndex];
			}
			//Check did not passed, parameterIndex is too big, throwing exception
			throw new ArrayIndexOutOfBoundsException("in EffectDescriptor: method getNthParameter was called with an" +
					" argument (= "+ parameterIndex +") greater or equal to the number of parameters (= " + effectKey.getParameterNb() +
					") declared in the EffectKey (= " + effectKey + ") associated to this EffectDescriptor (=" + this + ")");
		}
		//Check did not pass, parameterIndex is negative, throwing Exception
		throw new ArrayIndexOutOfBoundsException("in EffectDescriptor: method getNthParameter was called with a negative (<0) argument");
	}

	public EffectKey getEffectKey() {
		return effectKey;
	}

	public static EffectDescriptor readFromFile(BufferedReader br) throws IOException{

		EffectKey key=EffectKey.valueOf(br.readLine());
		SpeciesArray.lineError++;
		int[] parameters=new int[key.getParameterNb()];
		for(int i=0;i<key.getParameterNb();i++){
			parameters[i]=Integer.parseInt(br.readLine());
			SpeciesArray.lineError++;
		}
		return new EffectDescriptor(key,parameters);
	}
}
