package core.abilities.effects;

import core.event.AbilityEvent;
import core.event.Event;
import core.gamestate.*;
import core.gamestate.Character;
import core.relayer.RelayerEntity;
import core.relayer.Relayers;
import core.zone.Direction;
import core.zone.Zone;
import gameloop.LocalGameLoop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Created by dupriez on 19/12/15.
 *
 * Contains static methods to build instances of the Effect class, either directly or by composing them.
 * Implements the Singleton Pattern rather than an all-static pattern so that users of this class can use it by putting
 * it in a variable with a short name.
 */
public class EffectBuilder implements Serializable{
	private static final long serialVersionUID = 1L;
	private static EffectBuilder ourInstance = new EffectBuilder();
	public static EffectBuilder getInstance() {return ourInstance;}
	private EffectBuilder(){}

	/** Methods to build basic effect (base cases) **/

	/**
	 * Build a simple effect, that reduce the HP of the target Being by max(0,howMany). Note that it cannot heal.
	 * @param howMany the number of HP that the target Being should lose.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect reduceHP(int howMany) {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				for (Integer targetID : targetCharacterIDList) {
					try {
						gameContent.getGameState().getCharacter(targetID).takeDamage(Math.max(howMany, 0));
					} catch (EntityNotFoundExeption entityNotFoundExeption) {
					}
				}
			}
		};
	}

	/**
	 * Build a simple effect, that reduce the HP of the target Being (if not same faction) by max(0,howMany). Note that it cannot heal.
	 * @param howMany the number of HP that the target Being should lose.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect reduceHP_friendly(int howMany) {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				for (Integer targetID : targetCharacterIDList) {
					try {
						if(gameContent.getGameState().getCharacter(targetID).getFaction() != gameContent.getGameState().getCharacter(casterCharacterID).getFaction())
							gameContent.getGameState().getCharacter(targetID).takeDamage(Math.max(howMany, 0));
					} catch (EntityNotFoundExeption entityNotFoundExeption) {
					}
				}
			}
		};
	}

	/**
	 * Build an effect, which create a new entity, which move and inficlit dommage to every characters it cross.
	 * @param degat amount of dammage
	 * @param speed speed the entity move.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect spawnFireBolt(int degat,int speed) {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				Character charac = null;
				try {
					charac = gameContent.getGameState().getCharacter(casterCharacterID);
				} catch (EntityNotFoundExeption entityNotFoundExeption) {
					return ;
				}
				Entity entity = SpeciesArray.create(charac.getX(), charac.getY(), 0, "FireBolt", "FireBolt");
				entity.setOrientation(charac.getOrientation());
				entity.setSpeed(speed);
				entity.setOwned_character(casterCharacterID);
				gameContent.getGameState().addEntity(entity);
				gameContent.addTriggerToCheck(entity);
				RelayerEntity relayer = Relayers.addNewRelayer(entity);
				if(LocalGameLoop.getInstance().getFollowedRelayer().getDirection() != Direction.NONE) {
					relayer.move(LocalGameLoop.getInstance().getFollowedRelayer().getDirection());
				}
				else{
					relayer.move(charac.getDirection());
				}
			}
		};
	}

	/**
	 * Build an effect, which create a new entity, which move and inficlit dommage to every characters it cross.
	 * @param degat amount of dammage
	 * @param speed speed the entity move.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect spawnFrostBolt(int degat,int speed) {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				Character charac = null;
				try {
					charac = gameContent.getGameState().getCharacter(casterCharacterID);
				} catch (EntityNotFoundExeption entityNotFoundExeption) {
					return ;
				}
				Entity entity = SpeciesArray.create(charac.getX(), charac.getY(), 0, "FrostBolt", "FrostBolt");
				entity.setOrientation(charac.getOrientation());
				entity.setSpeed(speed);
				entity.setOwned_character(casterCharacterID);
				gameContent.getGameState().addEntity(entity);
				gameContent.addTriggerToCheck(entity);
				RelayerEntity relayer = Relayers.addNewRelayer(entity);

				if(LocalGameLoop.getInstance().getFollowedRelayer().getDirection() != Direction.NONE) {
					relayer.move(LocalGameLoop.getInstance().getFollowedRelayer().getDirection());
				}
				else{
					relayer.move(charac.getDirection());
				}
			}
		};
	}

	/**
	 * Build an effect, which create a new entity, which move and inficlit dammage to the first character it meets.
	 * @param degat amount of dammage
	 * @param speed speed the entity move.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect spawnLPFireBolt(int degat,int speed) {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				Character charac = null;
				try {
					charac = gameContent.getGameState().getCharacter(casterCharacterID);
				} catch (EntityNotFoundExeption entityNotFoundExeption) {
					return ;
				}
				Entity entity = SpeciesArray.create(charac.getX(), charac.getY(), 0, "FireBoltLP", "FireBoltLP");
				entity.setOrientation(charac.getOrientation());
				entity.setSpeed(speed);
				entity.setOwned_character(casterCharacterID);
				gameContent.getGameState().addEntity(entity);
				gameContent.addTriggerToCheck(entity);
				RelayerEntity relayer = Relayers.addNewRelayer(entity);
				if(LocalGameLoop.getInstance().getFollowedRelayer().getDirection() != Direction.NONE) {
					relayer.move(LocalGameLoop.getInstance().getFollowedRelayer().getDirection());
				}
				else{
					relayer.move(charac.getDirection());
				}
			}
		};
	}

	/**
	 * A trigger inflicting damage to any character in the area.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect trigger_degat() {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				for (Integer targetID : targetCharacterIDList) {
					try {
						if(gameContent.getGameState().getEntity(casterCharacterID).getOwned_character() != targetID) {
							if (gameContent.getGameState().getCharacter(targetID).getSpeciesName().equals("Ponyta")) {
						      gameContent.getGameState().getCharacter(targetID).takeDamage(2);
						   } else {
							   gameContent.getGameState().getCharacter(targetID).takeDamage(Math.max(1, 0));
							}
						}
					} catch (EntityNotFoundExeption entityNotFoundExeption) {
					}
				}
			}
		};
	}

	/**
	 * A trigger inflicting damage to any character in the area. if it happens then send a destroy event to the entity responsable.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect trigger_degat_lp() {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				boolean hit = false;
				for (Integer targetID : targetCharacterIDList) {
					try {
						if(gameContent.getGameState().getEntity(casterCharacterID).getOwned_character() != targetID) {
						// test pour l'effet soin de la boule de feu sur les ponytas
						   if (gameContent.getGameState().getCharacter(targetID).getSpeciesName().equals("Ponyta")) {
						      gameContent.getGameState().getCharacter(targetID).takeDamage(-2);
						   } else {
							   gameContent.getGameState().getCharacter(targetID).takeDamage(Math.max(1, 0));
							}
							hit =true;
						}

					} catch (EntityNotFoundExeption entityNotFoundExeption) {
					}
				}
				if(hit){
					Event event = new AbilityEvent(casterCharacterID, 0);
					LocalGameLoop.getInstance().sendEvent(event);
				}
			}
		};
	}

	/**
	 * An effect killing the entity properly.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect suicide(){
		return new Effect(){
			private static final long serialVersionUID = 1L;
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				try {
					gameContent.removeTriggerToCheck(gameContent.getGameState().getEntity(casterCharacterID));
					gameContent.getGameState().removeEntity(gameContent.getGameState().getEntity(casterCharacterID));
				} catch (EntityNotFoundExeption entityNotFoundExeption) {

				}
			}
		};
	}


	/**
	 * A trigger increasing the amount of gold of the characters in the area. if it happens then send a destroy event
	 * to the entity responsable.
	 * @return An instance of Effect doing what is described above.
	 */
	Effect trigger_gold() {
		return new Effect() {
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				boolean hit = false;
				for (Integer targetID : targetCharacterIDList) {
					try {
						if(gameContent.getGameState().getEntity(casterCharacterID).getOwned_character() != targetID) {
							gameContent.getGameState().getCharacter(targetID).setGold( gameContent.getGameState().getCharacter(targetID).getGold() + 1 );
							hit =true;
						}
					} catch (EntityNotFoundExeption entityNotFoundExeption) {
					}
				}
				if(hit){
					Event event = new AbilityEvent(casterCharacterID, 0);
					LocalGameLoop.getInstance().sendEvent(event);
				}
			}
		};
	}

	/** Methods to be applied on an Effect to transform it into another one **/

	/**
	 * Take an instance of Effect and return another instance of Effect that, for each Character it receives as input,
	 * makes a random test that has chances/outOf probability of success. On a success, the character is added to a list l.
	 * After this loop, the original Event is applied to the list l.
	 * SPECIAL CASES:
	 * 	- if outOf <= 0 : The method throws an exception.
	 * 	- if chances == 0 : The random test will always fail.
	 * 	- if chances >= outOf : The random test will always succeed.
	 * @param chances The random test has chances/outOf probability of success
	 * @param outOf The random test has chances/outOf probability of success
	 * @param baseEffect The original Effect that we want to trigger with a given probability
	 * @return see Method description
	 */
	Effect randomTest(int chances, int outOf, Effect baseEffect) {
		//TODO: Instead of creating a new Random generator in the Event, we could use one that we can access on the server
		if (outOf <= 0) {
			throw new BoundMustBePositive();
		}
		return new Effect() {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;
			private Random randomGenerator = new Random();

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				List<Integer> idsOfCharactersWhoPassedTheTest = new ArrayList<>();
				for (Integer targetID : targetCharacterIDList) {
					if (randomGenerator.nextInt(outOf) < chances) {
						//dice roll succeeded, adding the Character to the list
						idsOfCharactersWhoPassedTheTest.add(targetID);
					}
					//dice roll failed, doing nothing
				}
				//If at least one of the Characters passed the test
				if (idsOfCharactersWhoPassedTheTest.size() > 0) {
					baseEffect.effect(idsOfCharactersWhoPassedTheTest, gameContent, casterCharacterID);
				}
			}
		};
	}

	/**
	 * An exception class for the method randomTest
	 * TODO: Don't know if it should really be private
	 */
	private class BoundMustBePositive extends RuntimeException {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		public BoundMustBePositive(){
			super("In core.ability.effects.Effect: method randomTest was called with a (<=0) third argument");
		}
	}

	/**
	 * Take an instance of Effect and return another instance of Effect that applies the same effect, but on all the characters
	 * in a given zone of the gameState. The zone is given as a method that maps an instance of Character to a Zone (crz).
	 * Behaviour of the "effect" method of the resulting Effect:
	 * 	For each character in the list it is given:
	 * 		- apply crz on it to get a zone
	 * 		- get all the characters in this zone of the gameState
	 * 		- apply the effect of baseEffect on each of these characters
	 * @param crz A method that maps a Character to a Zone
	 * @param baseEffect The original effect we want a modified version of
	 * @return Another instance of Effect (see general description of this method)
	 */
	Effect affectZone(Function<Character,Zone> crz, Effect baseEffect) {
		return new Effect() {

			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				List<Integer> newTargetCharacterIDList = new ArrayList<>();
				//TODO: The following could be written in a more functional way
				for (Integer targetID : targetCharacterIDList) {
					Character target = null;
					try {
						target = gameContent.getGameState().getCharacter(targetID);
					} catch (EntityNotFoundExeption entityNotFoundExeption) {
						//entityNotFoundExeption.printStackTrace();
						continue;
					}
					for (Character newTarget : gameContent.getGameState().getCharacters(crz.apply(target))) {
						newTargetCharacterIDList.add(newTarget.getID());
					}
				}
				baseEffect.effect(newTargetCharacterIDList, gameContent, casterCharacterID);
			}
		};
	}

	/**
	 * Take an instance of Effect and returns another instance of Effect whose method "effect" does:
	 * 	- filter the list of Character it is applied by using charaListFilter
	 * 	- apply the "effect" method of baseEffect on the filtered list.
	 * @param charaListFilter A method that filter a list of Character
	 * @param baseEffect The original effect we want a modified version of
	 * @return Another instance of Effect (see general description of this method)
	 */
	Effect filter(UnaryOperator<List<Integer>> charaListFilter, Effect baseEffect) {
		return new Effect() {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				baseEffect.effect(charaListFilter.apply(targetCharacterIDList), gameContent, casterCharacterID);
			}
		};
	}

	/**
	 * Allows to build an Effect that executes several other Effects
	 * Take an array of Effects, and return another instance of Effect whose method "effect" does:
	 * 	- For each Effect contained in the array, apply its "effect" method
	 * @param baseEffectsArray an array containing all the effects the caller want to combine in a sequence
	 * @return Another instance of Effect (see general description of this method)
	 */
	Effect sequence(Effect[] baseEffectsArray) {
		return new Effect() {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException {
				for (Effect baseEffect : baseEffectsArray) {
					baseEffect.effect(targetCharacterIDList, gameContent, casterCharacterID);
				}
			}
		};
	}
}
