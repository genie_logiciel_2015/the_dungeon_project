package core.abilities.effects;

import core.gamestate.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dupriez on 17/12/15.
 *
 * To be added to an ability to define what it should do when casted
 * Effect can only apply and being used by a Character. I was not able to design it otherwise.
 */
public abstract class Effect implements Serializable{

	private static final long serialVersionUID = 1L;
	Effect() {

	}

	/**
	 * What the Effect does to its targets, in a given gameContent
	 * @param targetCharacterIDList a List of the IDs of the Characters to apply this effect on
	 * @param gameContent the gameContent in which the target characters lives
	 * @param casterCharacterID the ID of the Character that casted the Ability containing this Effect
	 * @throws InterruptedException
	 */
	abstract public void effect(List<Integer> targetCharacterIDList, GameContent gameContent, int casterCharacterID) throws InterruptedException;
}
