package core.abilities.effects;

/**
 * Created by dupriez on 19/12/15.
 *
 * The type of functions able to generate instances of Effect out of the ID of the character that cast the ability triggering this effect,
 * and a given number (this number depends on the actual EffectGenerator instance) of int values, provided in an array.
 */
public interface EffectGenerator {
	Effect generate(int casterCharacterID, EffectDescriptor parameters);
}
