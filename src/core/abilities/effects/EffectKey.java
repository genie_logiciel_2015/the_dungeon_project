package core.abilities.effects;

/**
 * Created by dupriez on 19/12/15.
 *
 * Used in EffectFactory to be bound to instances of EffectGenerator.
 * The EffectKeys contain an int field ("parameterNb") which defines the number of parameters the EffectGenerator they
 * are bound to requires (in addition to the ID of the caster character).
 */
public enum EffectKey {
	TEST_ATTACK_EFFECT(1),
	TEST_ATTACK_FRIENDLY(1),
	TEST_LP_FIREBOLT(2),
	TEST_FIREBOLT(2),
	TEST_SUICIDE(1),
	TRAP_TRIGGER(0),
	TRIGGER_DEGAT(0),
	TRIGGER_DEGAT_LP(0),
	MENTALI_ATTACK_DAMAGE(2),
	TRIGGER_GOLD(0),
	SPAWN_GOLD(1);

	private EffectKey(int parameterNb) {
		this.parameterNb = parameterNb;
	}
	private final int parameterNb;
	public int getParameterNb() {
		return parameterNb;
	}
}
