package artificial_intelligence.AlphaStar;


import java.util.Comparator;

/**
 * @author Yann RAMUSAT
 *
 * Comparator for tiles. Needed for the A* algorithm.
 */

public class AITileComparator implements Comparator<AITile> {
    private AITile obj;

    public AITileComparator(AITile obj) {
        this.obj = obj;
    }

    /**
     * Allows to compare two tiles with respect to their distances to the obj tile.
     * @param c1
     * @param c2
     * @return heuristic distance between both element
     */

    public int compare(AITile c1, AITile c2)
    {
        return heuristique(c1)- heuristique(c2);
    }

    /**
     * Retourne l'heuristique associee a la case (par exemple sa distance euclidienne par rapport a la case objectif)
     * @return
     */
    public int heuristique(AITile c) {
        int x=(c.getX()-obj.getX())/32;
        int y=(c.getY()-obj.getY())/32;
        return x*x+y*y;
    }
}