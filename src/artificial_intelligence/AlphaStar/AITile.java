package artificial_intelligence.AlphaStar;

import artificial_intelligence.AIEntities.AbstractEntity;
import core.relayer.Relayer;
import core.zone.Direction;
import map_generation.map.Map;
import map_generation.tiles.Tile;
import map_generation.tiles.TilePropertyVector;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Remy GARNIER && Yann RAMUSAT
 *
 * This class stands for a tile like in MapGen but with the needs of AI.
 */

public class AITile {
    private int x;
    private int y;


    public AITile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public boolean isObstacle(Map map){

        Tile tile=map.getTileAt(x/32,y/32);
        TilePropertyVector tpv=tile.getTilePropertyVector();
        List list= new ArrayList<TilePropertyVector.TileProperty>();
        list.add(TilePropertyVector.TileProperty.IMPASSABLE);
        TilePropertyVector impassable=new TilePropertyVector(list);
        return tpv.intersect(impassable);}

    /**
     * Return information	about the closest tiles.
     * @return
     */
    public AITile[] neighbors(){
        AITile[] neighbors=new AITile[4];
        neighbors[0]=new AITile(x,y+32);
        neighbors[1]=new AITile(x+32,y);
        if(y!=0)
        {neighbors[2]=new AITile(x,y-32);}
        else //Useless, but necesserary for the moment TODO: Improve it
        {neighbors[2]=new AITile(x,0);}
        if(x!=0)
        {neighbors[3]=new AITile(x-32,y);}
        else //Useless, but necesserary for the moment TODO: Improve it
        {neighbors[3]=new AITile(0,y);}
        return neighbors;
    }

    /**
     * Sert à renvoyer une orientation lorsque l'on veut arriver sur la case
     */
    public core.zone.Direction orientation(AITile start)
    {
        //On a cherché le chemin dans le sens inverse, on doit le retourner pour arriver dans le von sen
        int difx= (x-start.getX())/32;
        int dify= (y-start.getY())/32;

        switch (difx+2*dify)
        {
            case 2:
                return Direction.LEFT;
            case -2:
                return Direction.RIGHT;
            case 1:
                return Direction.UP;
            case -1:
                return Direction.DOWN;
            default:
                return Direction.NONE;
        }
    }
}