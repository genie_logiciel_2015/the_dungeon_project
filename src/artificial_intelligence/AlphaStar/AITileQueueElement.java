package artificial_intelligence.AlphaStar;

import java.util.Objects;

/**
 * @author Yann RAMUSAT and Remy GARNIER
 *
 * Implement a queue for A* Algorithms
 */
public class AITileQueueElement implements Comparable<AITileQueueElement>{
    private AITile caseElement;
    private double value;

    /**
     * Add an eleement to the queue.
     * @param element
     * @param d
     */
    public AITileQueueElement(AITile element, double d) {
        caseElement=Objects.requireNonNull(element);
        value=d;
    }

    /**
     * Getter caseElement
     * @return the tile
     */
    public AITile getCase() {
        return caseElement;
    }


    public int compareTo(AITileQueueElement cqe)
    {
        return (value>cqe.value)?1:-1;
    }
}