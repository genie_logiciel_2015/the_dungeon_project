package artificial_intelligence.AlphaStar;

import java.util.PriorityQueue;
import artificial_intelligence.GameOpt;
import core.zone.Direction;
import logging.Logging;
import map_generation.map.Map;


/**
 * @author Remy GARNIER && Yann RAMUSAT
 *
 * This class implements algorithms A* to moves some AIs
 */

public class AI {
	/* Some example of general options for the AI */
	private GameOpt opt;
	/**
	 * Constructor
	 */
	public AI(GameOpt opt) {
		this.opt = opt;
	}
	/**
	 *
	 * @return the Game Options of the game
	 */
	public GameOpt getOpt() {
		return opt;
	}
	/**
	 * Set a game opt.
	 * @param opt Game Options
	 */
	public void setOpt(GameOpt opt) {
		this.opt = opt;
	}
	/**
	 * Simple algorithm to get the direction when the both object share the Same AI Tile
	 * @param (obj_x,obj_y): Objective coordinates
	 * @param (source_x,source_y): Object coordinates
	 * @return the direction to go
	 *
	 * */
	public static Direction DirectPath(int obj_x, int obj_y, int source_x, int source_y)
	{
		int disx=(obj_x-source_x);
		int disy=(obj_y-source_y);
		//In the case where the object are in the same AITile
		if (java.lang.Math.abs(disy)>java.lang.Math.abs(disx))
		{
			if (disy>0) {
				return Direction.UP;}
			else
			{
				return Direction.DOWN;}}
		else {
			if (disx < 0) {
				return Direction.RIGHT;
			} else {
				return Direction.LEFT;
			}}}

	/**
	 * Implementation of the A* algorithm.
	 *
	 * @param (obj_x,obj_y): Objective coordinates
	 * @param (source_x,source_y): Object coordinates
	 * @param (width,heigth): size of the map
	 * @param map: Map
	 * @return the direction to go
	 */

	public static Direction alpha_star(Map map, int obj_x, int obj_y, int source_x, int source_y, int width, int height) {
		AITile obj = new AITile(obj_x, obj_y);
		AITile begin = new AITile(source_x, source_y);

		//On inversera l'objectif et le départ, il est plus simple de partir de l'arrivée
		int disx=(obj_x-source_x);
		int disy=(obj_y-source_y);

		//In the case where the object are in the same AITile
		if (java.lang.Math.abs(disx)<32 && java.lang.Math.abs(disy)<32)
		{
			return 	DirectPath(obj_x,obj_y,source_x,source_y);
		}
		AITileComparator comparator = new AITileComparator(begin);
		Direction[][] direction = new Direction[width][height];
		int[][] value=new int[width][height];
		value[obj_x/32][obj_y/32]=0;
		direction[obj_x/32][obj_y/32]=Direction.NONE;
		PriorityQueue<AITileQueueElement> queue= new PriorityQueue<>();
		queue.add(new AITileQueueElement(obj,0.0));

		while(!queue.isEmpty()) {
			AITile a = queue.remove().getCase();
			if (comparator.compare(a, begin) == 0) {

				return direction[a.getX()/32][a.getY()/32];
			}
			AITile[] neighbhor = a.neighbors();
			int vala = value[a.getX()/32][a.getY()/32];
			int patha = vala - comparator.heuristique(a);
			for (int i = 0; i < 4; i++)
			{
				AITile b = neighbhor[i];

				if (!b.isObstacle(map)) {
					int valb = value[b.getX()/32][b.getY()/32];
					if (valb == 0.0 && comparator.compare(b, obj) != 0) {
						int newval = comparator.heuristique(b) + 1 + patha;
						value[b.getX()/32][b.getY()/32]= newval;
						queue.add(new AITileQueueElement(b, newval));
						direction[b.getX()/32][b.getY()/32]=a.orientation(b);
					}

				}
			}
		}
		return Direction.NONE;
	}
}