package artificial_intelligence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import artificial_intelligence.AIEntities.*;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import logging.Logging;
import ingame_programming.IGPEntity;

/**
 * @author Guerquin Arnaud && Ramusat Yann
 *
 * This class allows to create a new entity of indicated type.
 * This class provides a way to start or stop all the current working entities (for example when initializing and quitting a map).
 *
 */
public class AIControler {
	private static ArrayList<AbstractEntity> list=new ArrayList<>();
	private static HashMap<Integer,AbstractEntity> hashMap = new HashMap<>();
    static Logger LOGGER = Logging.getInstance().getLogger();

	/**
	 * Create a new AI of indicated type given as context a gameContent and a relayer to contact.
	 * @param gameContent
	 * @param dmr
	 * @param type the behaviour of the AI to create
     */
	public static void add(GameContent gameContent, Relayer dmr, EnumBehaviourType type) {
		AbstractEntity ai = null;
		switch(type) {
		case Agressive:
		    ai = new AgressiveEntity(gameContent,dmr);
		    break;
		case Basic:
		    ai = new BasicEntity(gameContent,dmr);
		    break;
		case Defender:
		    ai = new DefenderEntity(gameContent,dmr);
		    break;
		case Follower:
		    ai = new IGPEntity(gameContent,dmr);
			break;
		case Straight:
			ai = new StraightEntity(gameContent,dmr);
			break;
		case Boss:
			ai = new BossEntity(gameContent,dmr);
			break;

		default:
		    LOGGER.severe("Trying to instanciate unrecognized type of AI.");
		    break;
		}
		list.add(ai);
        hashMap.put(dmr.getCharacter().getID(),ai);
	}

	/**
	 * Return all the AIs active or created but not active.
	 * @return the list of AIs
     */
	public static ArrayList<AbstractEntity> getList() {
		return list;
	}

	/**
	 * In order to know how many AIs are created.
	 * @return the size of the list
     */
	public static int getNumber() {
		return list.size();
	}


	/**
	 * To use for example at the end of a map.
	 */
	public static void killAll(){
		for(AbstractEntity ai :list){
			ai.setEndThread(true);
		}
		list.clear();
        hashMap.clear();
	}

	/**
	 * To use when one specific monster die.
     * @param entityID ID of the entity that should die
	 */
	public static boolean killOne(int entityID){
		AbstractEntity ai = hashMap.get(entityID);
        if (ai!=null) {
            ai.setEndThread(true);
            list.remove(ai);
            hashMap.remove(entityID);
            return true;
        } else {
            return false;
        }
	}

	public static void killAllBut(List<Integer> idList){
		for(AbstractEntity entity:list){
			int id = entity.GetRelay().getCharacter().getID();
			if(!idList.contains(id)){
				entity.setEndThread(true);
			}
		}
		list.clear();
		HashMap<Integer,AbstractEntity> aux = new HashMap<>();
		for(int id : idList){
			AbstractEntity entity = hashMap.get(id);
			list.add(entity);
			aux.put(id, entity);
		}
		hashMap = aux;
	}

	/**
	 * To start all the AIs at the begining of the map and after init complete inialization.
	 */
	public static void startAll() {
		for(AbstractEntity ai :list){
			ai.start();
		}
	}
}
