package artificial_intelligence.AIEntities;

/**
 * Created by yann on 30/12/15.
 */
public enum EnumBehaviourType {
    Agressive,
    Basic,
    Defender,
    Follower,
    Boss,
    Straight;
}
