package artificial_intelligence.AIEntities;

import core.gamestate.GameContent;
import core.relayer.Relayer;

/**
 * Created by Yann RAMUSAT on 30/12/15.
 */
public class DefenderEntity extends AbstractEntity {
    /**
     * Instanciate a defender AI given as context a GameContent and a Relayer to contact.
     *
     * This use the classes used by the network.
     *
     * @param gameContent the actual content of the game. Map + Entities.
     * @param relayer the relayer to contact.
     */
    public DefenderEntity(GameContent gameContent, Relayer relayer) {
        super(gameContent, relayer);
    }

    /**
     *  This is the function sequentially called by run.
     *  The behaviour of the defender AI is implemented here.
     */
    @Override
    public void act() {

    }
}
