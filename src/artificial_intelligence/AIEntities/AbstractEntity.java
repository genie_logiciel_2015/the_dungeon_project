package artificial_intelligence.AIEntities;

import core.gamestate.Being;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.gamestate.GameState;
import core.relayer.Relayer;
import core.zone.Direction;
import gameloop.ServerLoop;
import logging.Logging;
import map_generation.map.Map;

import java.util.List;
import java.util.logging.Logger;

import static java.lang.Math.abs;
import static java.lang.Math.random;
import static java.lang.Math.sqrt;


/**
 * Created by Yann RAMUSAT on 17/11/15.
 *
 * This class provides a functional AI working on an independant thread.
 * Please instanciate the class correctly an then call start() function on this.
 *
 * All specific AI class will extend this class.
 */
public abstract class AbstractEntity extends Thread {

    /* Extern information */
    protected GameState gameState;
    // to have direct access to the map
    protected Map map;
    protected Relayer relayer;
    /* Intern information */
    protected boolean endThread = false;

    public Map GetMap()
    {
        return map;
    }

    public Relayer GetRelay()
    {
        return relayer;
    }

    private Logger LOGGER = Logging.getInstance().getLogger();

    /**
     * Instanciate by default an autonomous AI given as context a GameContent and a Relayer to contact.
     *
     * This use the classes used by the network.
     *
     * @param gameContent the actual content of the game. Map + Entities.
     * @param relayer the relayer to contact.
     */
    public AbstractEntity(GameContent gameContent, Relayer relayer) {
        if(gameContent != null) {
            this.gameState = gameContent.getGameState();
            this.map = gameContent.getMap();
        }
        this.relayer = relayer;
    }


    /**
     * Default constructor.
     */
    public AbstractEntity() {
    }

    /**
     * Select the target.
     *
     * @return the index of the victim in the EntityList.
     */
    public int Choose_victim(){
        double dmin =0 ;
        double dmax = 32*15; // tiles of length 32 : so 15 tiles
        int victim = -1;
        List<Being> entityList=gameState.getAllBeings();
        for (int i=0;i<entityList.size();i++) {
        	Being act_entity = entityList.get(i);
            if (act_entity.getFaction() == 1) { // if act_entity is a player
                //euclidian distance and distance of manathan
                //double d = sqrt((act_entity.getX() - this.relayer.getCharacter().getX())*(act_entity.getX() - this.relayer.getCharacter().getX()) + (act_entity.getY() - this.relayer.getCharacter().getY())*(act_entity.getY() - this.relayer.getCharacter().getY()));
                double d = abs( (act_entity.getX() - this.relayer.getCharacter().getX()) )+abs(act_entity.getY() - this.relayer.getCharacter().getY());
                if (d<dmax) {
                    if (dmin == 0) {
                        dmin = d;
                        victim = i;
                    } else {
                        if (d != 0 && dmin < d) {
                            dmin = d;
                            victim = i;
                        }
                    }
                }
            }
        }
        return(victim);
    }

    public double Distance_to_victim(Entity victim){
        //euclidian distance and distance of manathan
        //double d = sqrt((victim.getX() - this.relayer.getCharacter().getX())*(victim.getX() - this.relayer.getCharacter().getX()) + (victim.getY() - this.relayer.getCharacter().getY())*(victim.getY() - this.relayer.getCharacter().getY()));
        double d = abs( (victim.getX() - this.relayer.getCharacter().getX()) )+abs(victim.getY() - this.relayer.getCharacter().getY());
        return(d);
    }

    /**
     * Given a target return the next move to do.
     *
     * @param victim_i the pointer to the victim
     */




    public void move_to_victim(Entity victim_i){
        Entity victim= (victim_i);
        // X is UP-DOWN
        int Delta_x = this.relayer.getCharacter().getX()-victim.getX();
        // Y is LEFT-RIGHT
        int Delta_y = this.relayer.getCharacter().getY()-victim.getY();
        if(abs(Delta_x)>abs(Delta_y)){
            if(Delta_x>0) {
                this.relayer.move(Direction.UP);
            }
            else {
                this.relayer.move(Direction.DOWN);
            }
        }
        else{ // REPAIRE INVERSE !
            if (Delta_y>0) {
                this.relayer.move(Direction.LEFT);
            }
            else{
                this.relayer.move(Direction.RIGHT);
            }
        }

    }

    public void run_from_victim(Entity victim_i){ //same then move_to_victim we just inverted the directions
        Entity victim= (victim_i);
        // X is UP-DOWN
        int Delta_x = this.relayer.getCharacter().getX()-victim.getX();
        // Y is LEFT-RIGHT
        int Delta_y = this.relayer.getCharacter().getY()-victim.getY();
        if(abs(Delta_x)>abs(Delta_y)){
            if(Delta_x>0) {
                this.relayer.move(Direction.DOWN);
            }
            else {
                this.relayer.move(Direction.UP);
            }
        }
        else{ // REPAIRE INVERSE !
            if (Delta_y>0) {
                this.relayer.move(Direction.RIGHT);
            }
            else{
                this.relayer.move(Direction.LEFT);
            }
        }

    }


    public void random_move(){
        double r = random();
        if(r<0.25){
           this.relayer.move(Direction.RIGHT);
       }
        else {
            if (r < 0.5) {
                this.relayer.move(Direction.UP);
            } else {
                if (r < 0.75) {
                    this.relayer.move(Direction.DOWN);
                } else {
                    this.relayer.move(Direction.LEFT);
                }
            }
        }
    }
    /**
     *  This is the function sequentially called by run.
     *  The behaviour of the AI is implemented here.
     *
     *  This is the only function that have to be overriden by specifics AIs.
     */
    public void move_and_shoot(Entity victim){
        // X is UP-DOWN
        int Delta_x = this.relayer.getCharacter().getX()-victim.getX();
        // Y is LEFT-RIGHT
        int Delta_y = this.relayer.getCharacter().getY()-victim.getY();
        int distance_to_shoot = 24; // distance of tolerance of alignment
        if(abs(Delta_x)<distance_to_shoot){ //we are well enough aligned to shoot
            if(Delta_y<0){ // choose side to shoot
                if(relayer.getCharacter().getDirection()==Direction.RIGHT){ //right side, we shoot
                    this.relayer.move(Direction.NONE);
                    this.relayer.tryToCastAbility(1);
                }
                else{
                    this.relayer.move(Direction.RIGHT); //wrong side, we turn
                }

            }
            else{
                if(relayer.getCharacter().getDirection()==Direction.LEFT){
                    this.relayer.move(Direction.NONE);
                    this.relayer.tryToCastAbility(1);
                }
                else{
                    this.relayer.move(Direction.LEFT);
                }
            }
        }
        else if(abs(Delta_y)<distance_to_shoot){
            if(Delta_x<0){
                if(relayer.getCharacter().getDirection()==Direction.DOWN){
                    this.relayer.move(Direction.NONE);
                    this.relayer.tryToCastAbility(1);
                }
                else{
                    this.relayer.move(Direction.DOWN);
                }
            }
            else{
                if(relayer.getCharacter().getDirection()==Direction.UP){
                    this.relayer.move(Direction.NONE);
                    this.relayer.tryToCastAbility(1);
                }
                else{
                    this.relayer.move(Direction.UP);
                }
            }
        }
        else{ // we have to move to be align
            if(abs(Delta_x)<abs(Delta_y)){
                if(Delta_x<0){
                    this.relayer.move(Direction.DOWN);
                }
                else{
                    this.relayer.move(Direction.UP);
                }
            }
            else{
                if(Delta_y<0){
                    this.relayer.move(Direction.RIGHT);
                }
                else{
                    this.relayer.move(Direction.LEFT);
                }
            }
        }
    }

    public void waiting_move(){
        Direction dir = this.relayer.getCharacter().getDirection();
        double r = random();
        if(r<0.8){
            for(int i=0; i<1;i++) {
                this.relayer.move(dir);
            }
        }
        else{this.random_move();}
    }

    public void act() {
    }



    /**
     * Function called by the core/game_loop (call the function start() not run() because run is sequential) to launch the effective AI.
     */
    @Override
    public void run() {
        LOGGER.info("AI launched.");
        /* Running opt 1 */
        /*while(!relayer.getCharacter().isDead() && !endThread) {
            act();
            try {
                sleep(GameOpt.getInstance().getSpeedMs());
            } catch (InterruptedException e) {
                LOGGER.info("sleep()");
            }
        }
        Relayers.killOne(relayer.getID());*/
        /* Running opt 2 */
        long ltime = System.currentTimeMillis();
        while(!relayer.getCharacter().isDead() && !endThread){
            long ctime = System.currentTimeMillis();
            while(ctime - ltime > ServerLoop.frame) {
                ltime += ServerLoop.frame;
                ctime= System.currentTimeMillis();
            }
            act();
            try{
                Thread.sleep(ServerLoop.frame - (ctime - ltime));
            }catch(InterruptedException e){
                //Logging.getInstance().getLogger().info("got interrupted!");
            }
        }
    }

    /**
     * Enable or disable the AI.
     *
     * @param endThread enable or not.
     */
    public synchronized void setEndThread(boolean endThread) {
        this.endThread = endThread;
    }
}
