package artificial_intelligence.AIEntities;

import artificial_intelligence.AlphaStar.AI;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import core.zone.Direction;
import logging.Logging;

import java.util.List;

import static java.lang.Math.random;

/**
 * Created by Yann RAMUSAT on 30/12/15.
 */
public class StraightEntity extends AbstractEntity {
    /**
     * Instanciate a basic AI given as context a GameContent and a Relayer to contact.
     *
     * This use the classes used by the network.
     *
     * @param gameContent the actual content of the game. Map + Entities.
     * @param relayer the relayer to contact.
     */
    public StraightEntity(GameContent gameContent, Relayer relayer) {
        super(gameContent, relayer);
    }

    /**
     *  This is the function sequentially called by run.
     *  The behaviour of the basic AI is implemented here.
     */
    @Override
    public void act() {
        // basic targeting and pathfinding
        int victim_i = this.Choose_victim();
        if (victim_i!=-1) {
            double r = random();
            if(r<0.9){
                //this.move_to_victim(gameState.getAllEntities().get(this.Choose_victim()));
                if (relayer.getCharacter().getCollisionBox().intersect(gameState.getAllEntities().get(this.Choose_victim()).getCollisionBox())) {
                    this.relayer.move(Direction.NONE);
                    this.relayer.tryToCastAbility(0); // if we are close enough we fight and don't move
                }
                else {
                    this.move_to_victim(gameState.getAllEntities().get(this.Choose_victim()));
                }
            }
            else{
                this.random_move();
            }
        }
        else{
            this.waiting_move();
        }
        // attack
    }
}
