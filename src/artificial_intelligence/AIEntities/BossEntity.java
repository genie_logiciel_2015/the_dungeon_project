package artificial_intelligence.AIEntities;

import artificial_intelligence.AlphaStar.AI;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import core.zone.Direction;
import logging.Logging;

import java.util.List;

import static java.lang.Math.random;

/**
 * Created by Yann RAMUSAT on 30/12/15.
 */
public class BossEntity extends AbstractEntity {
    /**
     * Instanciate a basic AI given as context a GameContent and a Relayer to contact.
     *
     * This use the classes used by the network.
     *
     * @param gameContent the actual content of the game. Map + Entities.
     * @param relayer the relayer to contact.
     */
    public BossEntity(GameContent gameContent, Relayer relayer) {
        super(gameContent, relayer);
    }

    /**
     *  This is the function sequentially called by run.
     *  The behaviour of the basic AI is implemented here.
     */
    @Override
    public void act() {
        // basic targeting and pathfinding
        int victim_i = this.Choose_victim();
        if (victim_i!=-1) {
            Entity victim = this.gameState.getAllEntities().get(victim_i);
            /* d_min  minimum distance to not run away
            * dmax maximum distance to not get closer*/
            double d_min = 32*5; // tiles of length 32 : so 5 and 14 tiles
            double d_max = 32*14;
            double d = Distance_to_victim(victim);
            if(d<d_min){
                this.run_from_victim(victim);
            }
            else {
                if (d > d_max) {
                    this.move_to_victim(victim);
                }
                else{
                    move_and_shoot(victim);
                }
            }
        }
        else{

            this.waiting_move();

        }

        // attack
     }
}
