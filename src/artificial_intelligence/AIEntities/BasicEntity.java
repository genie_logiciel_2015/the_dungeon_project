package artificial_intelligence.AIEntities;

import artificial_intelligence.AlphaStar.AI;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import core.zone.Direction;
import core.zone.ZoneUnion;
import logging.Logging;

import java.util.List;

import static java.lang.Math.random;

/**
 * Created by Yann RAMUSAT on 30/12/15.
 */
public class BasicEntity extends AbstractEntity {
    /**
     * Instanciate a basic AI given as context a GameContent and a Relayer to contact.
     *
     * This use the classes used by the network.
     *
     * @param gameContent the actual content of the game. Map + Entities.
     * @param relayer the relayer to contact.
     */
    public BasicEntity(GameContent gameContent, Relayer relayer) {
        super(gameContent, relayer);
    }

    /**
     *  This is the function sequentially called by run.
     *  The behaviour of the basic AI is implemented here.
     */
    @Override
    public void act() {
        // basic targeting and pathfinding
        int victim_i = this.Choose_victim();

        if (victim_i != -1) {
            //this.move_to_victim(gameState.getAllEntities().get(this.Choose_victim()));


            // A*
            List<Entity> entityList = gameState.getAllEntities();
            int dX = entityList.get(this.Choose_victim()).getX();
            int dY = entityList.get(this.Choose_victim()).getY();
            int sX = relayer.getCharacter().getX();
            int sY = relayer.getCharacter().getY();
            Direction dir = AI.alpha_star(map, sX, sY, dX, dY, map.getWidth(), map.getHeight());
            //Logging.getInstance().getLogger().info("Normal"+dir.toString());
            if (relayer.getCharacter().getCollisionBox().intersect(entityList.get(victim_i).getCollisionBox())) {
                this.relayer.move(Direction.NONE);
            } else {
                this.relayer.move(dir);
            }
            // attack
            this.relayer.tryToCastAbility(0);
        }
    }
}
