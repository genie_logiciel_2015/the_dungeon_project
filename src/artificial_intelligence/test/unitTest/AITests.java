package artificial_intelligence.test.unitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Yann Ramusat on 31/12/15.
 *
 * TestSuite for the AI package.
 * Will launch automatically all the JUnits tests.
 *
 * Note: very helpful, code found in NetworkTests.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AIControlerTest.class,
        AITileComparatorTest.class
})
public class AITests{}
