package artificial_intelligence.test.unitTest;

import artificial_intelligence.AIEntities.AbstractEntity;
import artificial_intelligence.AIEntities.AgressiveEntity;
import artificial_intelligence.AIEntities.BasicEntity;
import artificial_intelligence.AIEntities.EnumBehaviourType;
import artificial_intelligence.AIControler;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Yann RAMUSAT on 30/12/15.
 *
 * This is the unit test class for the AIControler class.
 */

public class AIControlerTest {
    @Test
    public void testadd() throws Exception {
        AIControler.add(null, null, EnumBehaviourType.Basic);
        assertEquals(1, AIControler.getNumber());

        AIControler.add(null, null, EnumBehaviourType.Agressive);
        boolean obt = AIControler.getList().get(1) instanceof AgressiveEntity;
        assertEquals(true, obt);
    }

    @Test
    public void testkillAll() throws Exception {
        AIControler.killAll();
        assertEquals(0, AIControler.getNumber());
    }
}