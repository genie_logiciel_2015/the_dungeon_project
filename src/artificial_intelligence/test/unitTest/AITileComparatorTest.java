package artificial_intelligence.test.unitTest;

import artificial_intelligence.AlphaStar.AITileComparator;
import artificial_intelligence.AlphaStar.AITile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Yann RAMUSAT on 31/12/15.
 *
 * This is the unit test class for the AITileComparator class.
 */

public class AITileComparatorTest {
    @Test
    public void compareTest() {
        AITile obj = new AITile(0,0);
        AITile c1 = new AITile(2,1);
        AITile c2 = new AITile(1,2);

        AITileComparator comparator = new AITileComparator(obj);

        int res = comparator.compare(c1,c2);

        assertEquals(0, res);
    }

    @Test
    public void heuristiqueTest() {
        AITile obj = new AITile(0,0);
        AITile c = new AITile(1,1);

        AITileComparator comparator = new AITileComparator(obj);

        assertEquals(0, comparator.heuristique(c));
    }
}
