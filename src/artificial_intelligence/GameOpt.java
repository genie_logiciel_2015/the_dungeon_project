package artificial_intelligence;

/**
 * Created by Yann RAMUSAT on 21/11/15.
 *
 * This class is a singleton accessible by all modules.
 * Allowing to set and access to the options of the game that would impact the behaviour of the AIs.
 *
 */
public final class GameOpt {

    /* Begin design pattern implementation */
        /** Instance unique pre-initialisee */
        private static GameOpt instance = new GameOpt();

        /** Constructeur prive */
        private GameOpt() {
        }

        /** Point d'acces pour l'instance unique du singleton */
        public static GameOpt getInstance() {
            return instance;
        }
    /* End design pattern implementation */


    /**
     * Indicate the speed of the AIs (i.e. the time between two loops).
     */
    private int speedMs = 150;

    /**
     * Get the speedMs.
     * @return int
     */
    public int getSpeedMs() {
        return speedMs;
    }

    /**
     * Set the speedMs.
     * @param speedMs
     */
    public void setSpeedMs(int speedMs) {
        this.speedMs = speedMs;
    }
}
