package ingame_programming;

import graphics.ingame_input_listener.Input;
import logging.Logging;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.*;
import core.gamestate.GameState;
import core.gamestate.Character;
import core.zone.EmptyZone;
import core.zone.Zone;
import map_generation.tiles.TilePropertyVector;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import core.gamestate.TriggerDescriptor;
import core.gamestate.DamageTypeVector;
import core.abilities.Ability;
import core.abilities.AbilityFactory;
import core.abilities.AbilityKey;
/* Pour effectuer le test en commentaire, il faut ajouter un public aux constructeur character dans core.gamestate.character */

public class Testig {
    public static void main(String argv[]) throws ParseException {
    Logging.getInstance().getLogger().info("Bonjour");
	Script parser = new Script(stringToStream("attaque ennemi (avec PV < 50% et (avec DEF < 50 ou non possedant potion)) ou avec DEF = 50"));
	parser.Instruction();
	Zone zone = EmptyZone.getEmptyZone();
	TilePropertyVector tpv = new TilePropertyVector();
	EntityDisplayerType edt=EntityDisplayerType.valueOf("PONYTA");
	ArrayList <TriggerDescriptor> list = new ArrayList<TriggerDescriptor>();
	DamageTypeVector dtv = new DamageTypeVector();
	ArrayList<Ability> abl = new ArrayList<Ability>();
	/* HP => DEF => MENTAL => ATK => INTEL */
	/*
	Character ch1 = new Character("nom1","spnom1",10,10,10,1,zone,tpv,zone,edt,list,20,20,20,dtv,zone,20,20,abl);
	ch1.setFaction(1);
	Character ch2 = new Character("nom2","spnom1",10,10,10,1,zone,tpv,zone,edt,list,30,30,30,dtv,zone,30,30,abl);
	ch2.takeDamage(20);
	Character ch3 = new Character("nom3","spnom1",10,10,10,1,zone,tpv,zone,edt,list,40,40,40,dtv,zone,40,40,abl);
	Character ch4 = new Character("nom4","spnom1",10,10,10,1,zone,tpv,zone,edt,list,50,30,50,dtv,zone,50,50,abl);
	Character ch5 = new Character("nom5","spnom1",10,10,10,1,zone,tpv,zone,edt,list,60,30,60,dtv,zone,60,60,abl);
	ch5.takeDamage(10);
	ArrayList<Character> chlist = new ArrayList<Character>();
	chlist.add(ch1);
	chlist.add(ch2);
	chlist.add(ch3);
	chlist.add(ch4);
	chlist.add(ch5);
	FollowerBehaviour fb = new FollowerBehaviour(3);
	ArrayList<String> target3 = new ArrayList<String>();
	target3.add("NULL");
	fb.setNthInstruction(target3,"nothing",0);
	ArrayList<String> target2 = new ArrayList<String>();
	target2.add("CTR");
	target2.add("allie");
	target2.add("NO");
	target2.add("AVEC");
	target2.add("MEN");
	target2.add("=");
	target2.add("20");
	fb.setNthInstruction(target2,"proteger",1);
	ArrayList<String> target = new ArrayList<String>();
	target.add("CTR");
	target.add("ennemi");
	target.add("OU");
	target.add("(");
	target.add("ET");
	target.add("(");
	target.add("AVEC");
	target.add("DEF");
	target.add(">=");
	target.add("40");
	target.add(")");
	target.add("NO");
	target.add("NOMME");
	target.add("nom3");
	target.add(")");
	target.add("AVEC");
	target.add("PV");
	target.add("=");
	target.add("%");
	target.add("100");
	target.add("EOF");
	fb.setNthInstruction(target,"attaquer",2);
	BehaviourHolder bh = BehaviourHolder.getInstance();
	bh.SetFollowerBehaviour(fb);
        Character ch = bh.GetFollowerBehaviour().FindTarget(chlist);
	System.out.println(ch.getName());
	System.out.println(fb.getAction());
	*/
    }

    private static InputStream stringToStream(String s) {
	InputStream is = new ByteArrayInputStream(s.getBytes());
	return is;
    }
}
