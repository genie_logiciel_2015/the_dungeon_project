package ingame_programming;

import graphics.ingame_input_listener.Input;
import logging.Logging;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.*;
import core.gamestate.GameState;
import core.gamestate.Character;
import core.zone.EmptyZone;
import core.zone.Zone;
import map_generation.tiles.TilePropertyVector;
import graphics.guiSkeleton.entityDisplayer.EntityDisplayerType;
import core.gamestate.TriggerDescriptor;
import core.gamestate.DamageTypeVector;
import core.abilities.Ability;
import core.abilities.AbilityFactory;
import core.abilities.AbilityKey;
/* Pour effectuer le test en commentaire, il faut ajouter un public aux constructeur character dans core.gamestate.character */

public class TestIGP {
    public static void main(String argv[]) throws ParseException {
    Logging.getInstance().getLogger().info("Bonjour");
	FollowerBehaviour fb = new FollowerBehaviour(4);
	Script parser = new Script(stringToStream("attaque ennemi (avec PV < 50% et (avec DEF < 50 ou non avec MEN < 50)) ou avec DEF = 50"));
	ArrayList<String> targett = parser.checkInput();
	if (targett.get(0) != "NULL") {
	    String act = targett.get(0);
	    targett.remove(0);
	    fb.setNthInstruction(targett,act,3);
	}
	else {
	    fb.setNthInstruction(targett,"nothing",3);
	}
	Zone zone = EmptyZone.getEmptyZone();
	TilePropertyVector tpv = new TilePropertyVector();
	EntityDisplayerType edt=EntityDisplayerType.valueOf("PONYTA");
	ArrayList <TriggerDescriptor> list = new ArrayList<TriggerDescriptor>();
	DamageTypeVector dtv = new DamageTypeVector();
	ArrayList<Ability> abl = new ArrayList<Ability>();
	/* HP => DEF => MENTAL => ATK => INTEL */
	/*
	Character ch1 = new Character("nom1","spnom1",10,10,10,1,zone,tpv,zone,edt,list,20,20,20,dtv,zone,20,20,abl);
	ch1.setFaction(1);
	Character ch2 = new Character("nom2","spnom1",10,10,10,1,zone,tpv,zone,edt,list,30,30,30,dtv,zone,30,30,abl);
	ch2.takeDamage(20);
	Character ch3 = new Character("nom3","spnom1",10,10,10,1,zone,tpv,zone,edt,list,40,40,40,dtv,zone,40,40,abl);
	ch3.takeDamage(20);
	Character ch4 = new Character("nom4","spnom1",10,10,10,1,zone,tpv,zone,edt,list,50,30,50,dtv,zone,50,50,abl);
	ch4.takeDamage(20);
	Character ch5 = new Character("nom5","spnom1",10,10,10,1,zone,tpv,zone,edt,list,60,30,60,dtv,zone,60,60,abl);
	ch5.takeDamage(10);
	ArrayList<Character> chlist = new ArrayList<Character>();
	chlist.add(ch1);
	chlist.add(ch2);
	chlist.add(ch3);
	chlist.add(ch4);
	chlist.add(ch5);
	Script parser2 = new Script(stringToStream("Bonjour"));
	ArrayList<String> target3 = parser2.checkInput();
	if (target3.get(0) != "NULL") {
	    String act = target3.get(0);
	    target3.remove(0);
	    fb.setNthInstruction(target3,act,0);
	}
	else {
	    fb.setNthInstruction(target3,"nothing",0);
	}
	Script parser3 = new Script(stringToStream("proteger allie non avec MEN = 20"));
	ArrayList<String> target2 = parser3.checkInput();
	if (target2.get(0) != "NULL") {
	    String act = target2.get(0);
	    target2.remove(0);
	    fb.setNthInstruction(target2,act,1);
	}
	else {
	    fb.setNthInstruction(target2,"nothing",1);
	}
	Script parser4 = new Script(stringToStream("soigner ennemi (avec DEF >= 40 et non nommé nom3 ) ou avec PV = 100%"));
	ArrayList<String> target = parser4.checkInput();
	if (target.get(0) != "NULL") {
	    String act = target.get(0);
	    target.remove(0);
	    fb.setNthInstruction(target,act,2);
	}
	else {
	    fb.setNthInstruction(target,"nothing",2);
	}
	fb.PrintElements();
	BehaviourHolder bh = BehaviourHolder.getInstance();
	bh.SetFollowerBehaviour(fb);
        Character ch = bh.GetFollowerBehaviour().FindTarget(chlist);
	System.out.println(ch.getName());
	System.out.println(fb.getAction());
	*/
    }

    private static InputStream stringToStream(String s) {
	InputStream is = new ByteArrayInputStream(s.getBytes());
	return is;
    }
}
