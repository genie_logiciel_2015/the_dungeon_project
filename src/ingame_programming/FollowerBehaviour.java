package ingame_programming;

import java.io.InputStream;
import java.util.*;
import core.gamestate.GameState;
import logging.Logging;
import core.gamestate.Character;
/**
 * This class is used to register data from IGPpanel.
 * @author William Babonnaud & Alexis Ghyselen
 */
public class FollowerBehaviour {
    /** Number of rules to consider */
    private int numberOfRules;

    /** Current rule to consider according to the gameState */
    private int ruletoconsider ;

    /** Array to store targets as ArrayListString */
    private ArrayList<ArrayList<String>> targets;

    /** Array to store actions as String */
    private String[] actions;

    /** Constructor
     * @param nRules is the number of rules to consider
     */
    public FollowerBehaviour(int nRules) {
	numberOfRules = nRules;
	ruletoconsider = -1;
	targets = new ArrayList<ArrayList<String>>(nRules);
	actions = new String[nRules];
	for(int i=0; i< nRules;i++) {
	    ArrayList<String> str = new ArrayList<String>();
	    str.add("NULL");
	    targets.add(str);
	    actions[i] = "nothing";
	}
    }

    /**
     * Set instruction value, given its position in the rules array.
     * @param target the "parsed sentence" target
     * @param action the String representing the action
     * @param n an integer strictly lesser than numberOfRules
     */
    public void setNthInstruction(ArrayList<String> target,String action, int n) {
	targets.set(n,target);
	actions[n] = action;
    }

    /**
     * Switch the elements i and j of targets and action, used when you make a switch in the IGPpanel */
    public void SwitchInstructions(int i, int j) {
	ArrayList<String> tar = targets.get(i);
	String act = actions[i];
	targets.set(i,targets.get(j));
	actions[i]=actions[j];
	targets.set(j,tar);
	actions[j]=act;

    }

    /** Output the list of Character satysfying all the properties in target
     * @param list a list of Character in which you want to keep only the one satisfying the propertie in target
     * @param target a list of words, representing a target in an instruction in the IGP Panel
     */
    private List<Character> findCharacter(List<Character> list, ArrayList<String> target) {
	switch (target.get(0)) {
	case "NULL":
	    ArrayList<Character> emptylist = new ArrayList<Character>();
	    return emptylist;
	case "ET":
	    int count1 = 1;
	    target.remove(0);
	    target.remove(0);
	    ArrayList<String> target1 = new ArrayList<String>();
	    // We find the word between parenthesis that follow our "ET"
	    while (count1 > 0) {
		String s = target.get(0);
		target.remove(0);
		switch (s) {
		case "(":
		    count1 = count1 + 1;
		    target1.add(s);
		    break;
		case ")":
		    count1 = count1 - 1;
		    if (count1 > 0) {
			target1.add(s);
		    }
		    break;
		default :
		    target1.add(s);
		}
	    }
	    target1.add("EOF");
	    return findCharacter(findCharacter(list,target1),target);
	case "OU":
	    int count2 = 1;
	    target.remove(0);
	    target.remove(0);
	    ArrayList<String> target2 = new ArrayList<String>();
	    while (count2 > 0) {
                String s = target.get(0);
		target.remove(0);
                switch (s) {
                case "(":
                    count2 = count2 + 1;
                    target2.add(s);
                    break;
                case ")":
                    count2 = count2 - 1;
                    if (count2 > 0) {
                        target2.add(s);
                    }
                    break;
                default :
                    target2.add(s);
                }
            }
	    target2.add("EOF");
	    List<Character> l1 = findCharacter(list,target2);
	    List<Character> l2 = findCharacter(list,target);
	    l1.addAll(l2);
	    return l1;
	case "NO":
	    target.remove(0);
	    List<Character> l3 = findCharacter(list,target);
	    list.removeAll(l3);
	    return list;
	case "CTR":
	    target.remove(0);
	    String s = target.get(0);
	    target.remove(0);
	    switch(s) {
	    case "humain" :
		// For now, we consider that a humain is just an ally
	    case "allie" :
		List<Character> l4 = new ArrayList<Character>();
		for (Character ch : list) {
		    if (ch.getFaction() == 1) {
			l4.add(ch);
		    }
		}
		return findCharacter(l4,target);
	    case "ennemi" :
                List<Character> l5 = new ArrayList<Character>();
                for (Character ch : list) {
                    if (ch.getFaction() == 0) {
                        l5.add(ch);
                    }
                }
                return findCharacter(l5,target);
	    default :
		return findCharacter(list,target);
	    }
	case "CLASS" :
	    switch (target.get(1)) {
	    case "guerrier" :
	    case "mage" :
	    case "rogue" :
		// For now, classes are not yet implemented in the game
		break;
	    }
	    return list;
	case "AVEC" :
	    List<Character> l6 = new ArrayList<Character>();
	    for (Character ch : list)  {
		int stat = 0;
		switch (target.get(1)) {
		case "PV" :
		    stat = ch.getHP();
		    break;
		case "ATK" :
		    stat = ch.getAtk();
		    break;
		case "DEF" :
		    stat = ch.getDef();
		    break;
		case "INT" :
		    stat = ch.getIntel();
		    break;
		case "MEN" :
		    stat = ch.getMental();
		    break;
		default :
		    stat = 0;
		}
		int limit = 0;
		switch (target.get(3)) {
		case "%" :
		    if (target.get(1) == "PV") {
			limit = ((ch.getMaxHP())*(Integer.parseInt(target.get(4))))/100;
		    }
		    else {
			limit = stat;
		    }
		    break;
		default :
		    limit = Integer.parseInt(target.get(3));
		}
		switch (target.get(2)) {
		case "=":
		    if (stat == limit) {
			l6.add(ch);
		    }
		    break;
		case ">":
		    if (stat > limit) {
                        l6.add(ch);
                    }
                    break;
		case "<":
		    if (stat < limit) {
                        l6.add(ch);
                    }
                    break;
		case ">=":
		    if (stat >= limit) {
                        l6.add(ch);
                    }
                    break;
		case "<=":
		    if (stat <= limit) {
                        l6.add(ch);
                    }
                    break;
		default :
		}
	    }
	    return l6;
	case "POSSEDANT" :
	    // For now, items are not yet implemented in the game
	    return list;
	case "NOMME" :
	    List<Character> l7 = new ArrayList<Character>();
	    for (Character ch : list) {
		if (target.get(1).equals(ch.getName())) {
		    l7.add(ch);
		}
	    }
	    return l7;
	default :
	    return list;
	}
    }

    /** Give the current action to execute according to "ruletoconsider" */
    public String getAction() {
	if (ruletoconsider == -1) {
	    return "nothing";
	}
	else {
	    return actions[ruletoconsider];
	}
    }

    /** Give the current Character to target, and adapt "ruletoconsider" to this result
     * @param list Is the list of all visible character
     */
    public List<Character> FindTarget(List<Character> list) {

	ruletoconsider = -1;
	int k = 0;
	while(k<numberOfRules) {
	    ArrayList<String> copyk = new ArrayList<String>();
	    copyk.addAll(targets.get(k));
	    List<Character> result = findCharacter(list,copyk);
	    if (! result.isEmpty()) {
		ruletoconsider = k;
		return result;
	    }
	    else {
		k = k+1;
	    }
	}
	ArrayList<Character> emptyList = new ArrayList<Character>();
	return emptyList;
    }

    /** A debug fonction that allows to Print the Instructions according to the FollowerBehaviour */
    public void PrintElements() {
	for(int i = 0; i< numberOfRules; i++) {
		Logging.getInstance().getLogger().info("Instruction number" + i);
		Logging.getInstance().getLogger().info("   Action :" + actions[i]);
	    String s = "   Target :";
	    for (String t : targets.get(i)) {
		s = s + " " + t;
	    }
	    Logging.getInstance().getLogger().info(s);
	}
    }
}
