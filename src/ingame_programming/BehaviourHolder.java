package ingame_programming;

/**
 * This class is used to store and analyse data from IGPpanel.
 * @author William Babonnaud & Alexis Ghyselen
 */
public class BehaviourHolder {
    /** Data are stored under the form of a FollowerBehaviour instance */
    private FollowerBehaviour fob = new FollowerBehaviour(0);

    /** Constructor : BehaviourHolder is a singleton */
    private BehaviourHolder() {
    }

    /** Allow the package to set a new FollowerBehaviour for the BehaviourHolder */
    void SetFollowerBehaviour(FollowerBehaviour fb) {
	fob = fb;
    }

    private static BehaviourHolder Instance = new BehaviourHolder();

    /** Get the unique instance of BehaviourHolder */
    public static BehaviourHolder getInstance() {
	return Instance;
    }

    /** Get the FollowerBehaviour stored by the BehaviourHolder */
    public FollowerBehaviour GetFollowerBehaviour() {
	return fob;
    }
}
