package ingame_programming;

import graphics.ingame_input_listener.Input;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.*;

/** Panel for ingame programming
 * @author Alexis Ghyselen & William Babonnaud
 */

public class IGPpanel extends graphics.guiSkeleton.guiPanel.GUIPanel{
    private static final long serialVersionUID = 1L;
    /** nbf is the number of rules we want in our panel, here we decide that we will work with 8 possible rules */
    private int nbf = 8 ;

    /** vswitch remember the last button used, that will allow us to switch two rules */
    private int vswitch = -1 ;

    private String[] examples = {"Examples : proteger allié avec PV<10%","attaquer ennemi non nommé PONYTA","attaquer ennemi avec ATK >= 100 ou avec INT >= 100","proteger allié avec DEF<20 et avec MEN<20","soigner allié avec PV<80%  (healing is not yet implemented in the game)","attaquer ennemi (avec PV<10 et avec DEF<20) ou nommé RONFLEX","If the IA has nothing to do, it will follow his owner","The IA totally ignore false or empty lines" };

    /** fob is the FollowerBheaviour created by the IGPpanel, it's is linked to the BehaviourHolder when you press back */
    private FollowerBehaviour fob = new FollowerBehaviour(nbf);

    /** This is the elements of our panel, for each rule, we got a switch button, a textfield to write the rule and an hint label give example of sentences */
    private JButton[] bswitch = new JButton[nbf];
    private JTextField[] prflabel = new JTextField[nbf];
    private JLabel[] hintlabel = new JLabel[nbf];

    public IGPpanel(GraphicsMaster graphicsMaster) {
       	super(graphicsMaster);

	/**
	 *  ActionListener to parse an instruction.
	 *  Change the color of the textfield in green if the
	 *  instruction is correct, and in red otherwise.
	 *  And add the new instruction to the FollowerBehaviour fob
	 */
	parselistener = new ActionListener[nbf];

	for(int i=0; i<nbf;i++) {
	    int j = i;
	    parselistener[j] = new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			Script parser = new Script(stringToStream(prflabel[j].getText()));
			ArrayList<String> target = parser.checkInput();
			if (target.get(0) != "NULL") {
			    prflabel[j].setBackground(Color.green);
			    String act = target.get(0);
			    target.remove(0);
			    target.add("EOF");
			    fob.setNthInstruction(target,act,j);
			}
			else {
			    prflabel[j].setBackground(Color.red);
			    fob.setNthInstruction(target,"nothing",j);
			}
		    }
		};
	};

	/** Creation of the content of the IGP_Panel */

	setLayout(new GridBagLayout());

	GridBagConstraints c;

	/** Creation of the titlePanel */
	JPanel titlePanel = new JPanel();
	titlePanel.setOpaque(true);
	titlePanel.setBackground(GUIColorsAndFonts.backGround2);

	JLabel titleLabel = new JLabel("The Dungeon Project");
	titleLabel.setForeground(GUIColorsAndFonts.titleColor1);
	titleLabel.setFont(GUIColorsAndFonts.titleFont1);
	titlePanel.add(titleLabel);

        /** Grid constraint for the title panel */
	c = new GridBagConstraints();
	c.gridx=0; c.gridy=0; c.gridwidth=1; c.gridheight=1;c.weightx=1;c.weighty=0.5;c.fill=GridBagConstraints.BOTH;
	add(titlePanel, c);

        /** Creation of the buttonPanel */
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridBagLayout());
	buttonPanel.setOpaque(true);
	buttonPanel.setBackground(GUIColorsAndFonts.backGround1);
	GridBagConstraints c2;

	/** Creation of the elements of the IGP_Panel */
	for (int i =0; i < nbf ; i++) {
	    /** The button that will allow the user to switch rules */
	    bswitch[i] = new JButton(Integer.toString(i));
	    bswitch[i].setFont(GUIColorsAndFonts.buttonFont1);
	    bswitch[i].setForeground(GUIColorsAndFonts.fontColor1);
	    bswitch[i].setOpaque(true);
	    bswitch[i].setBackground(GUIColorsAndFonts.buttonBackground1);
	    bswitch[i].addActionListener(bswitchlistener);
	    c2 = new GridBagConstraints();
	    c2.gridx=1; c2.gridy=(i+1); c2.gridwidth=1; c2.gridheight=1; c2.weightx=1; c2.weighty=1; c2.fill=GridBagConstraints.BOTH;
	    buttonPanel.add(bswitch[i], c2);
	    /** Textfield to enter the rules */
	    prflabel[i] = new JTextField();
	    prflabel[i].setColumns(20);
	    prflabel[i].setFont(GUIColorsAndFonts.smallFont1);
	    prflabel[i].setText("");
	    prflabel[i].setForeground(GUIColorsAndFonts.fontColor1);
	    prflabel[i].setBackground(GUIColorsAndFonts.buttonBackground1);
	    prflabel[i].addActionListener(parselistener[i]);
	    c2 = new GridBagConstraints();
	    c2.gridx=3; c2.gridy=(i+1); c2.gridwidth=3; c2.gridheight=1; c2.weightx=3; c2.weighty=1; c2.fill=GridBagConstraints.BOTH;
	    buttonPanel.add(prflabel[i],c2);
	    /** Label that will give hint to the user */
	    hintlabel[i] = new JLabel(examples[i]);
	    hintlabel[i].setFont(GUIColorsAndFonts.smallFont1);
	    hintlabel[i].setForeground(GUIColorsAndFonts.fontColor1);
	    c2 = new GridBagConstraints();
	    c2.gridx=6; c2.gridy=(i+1); c2.gridwidth=1; c2.gridheight=1; c2.weightx=1; c2.weighty=1; c2.fill=GridBagConstraints.BOTH;
	    buttonPanel.add(hintlabel[i],c2);

	}

        /** Back button */
	JButton exitButton = new JButton();
	exitButton.setFont(GUIColorsAndFonts.buttonFont1);
	exitButton.setText("Back");
	exitButton.setForeground(GUIColorsAndFonts.fontColor1);
	exitButton.setOpaque(true);
	exitButton.setBackground(GUIColorsAndFonts.buttonBackground1);
	exitButton.addActionListener(exitButtonListener);
	c2 = new GridBagConstraints();
	c2.gridx=0; c2.gridy=(nbf+1); c2.gridwidth=2; c2.gridheight=1; c2.weightx=1; c2.weighty=1; c2.fill=GridBagConstraints.BOTH;
	buttonPanel.add(exitButton, c2);

	/** Adding the button panel to the menu */
	c = new GridBagConstraints();
	c.gridx=0; c.gridy=1; c.gridwidth=1; c.gridheight=1;c.weightx=1;c.weighty=10;c.fill=GridBagConstraints.BOTH;
	add(buttonPanel, c);
    }

    /** ActionListener that will allow to switch rules */
    private ActionListener bswitchlistener = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		int i = Integer.parseInt(((JButton)e.getSource()).getText());
		if (vswitch == -1) {
		    vswitch = i;
		    bswitch[i].setBackground(Color.green);
		}
		else {
		    fob.SwitchInstructions(i,vswitch);
		    String s = prflabel[i].getText();
		    Color clr = prflabel[i].getBackground();
		    prflabel[i].setText(prflabel[vswitch].getText());
		    prflabel[i].setBackground(prflabel[vswitch].getBackground());
		    prflabel[vswitch].setBackground(clr);
		    prflabel[vswitch].setText(s);
		    s = hintlabel[i].getText();
		    hintlabel[i].setText(hintlabel[vswitch].getText());
		    hintlabel[vswitch].setText(s);
		    bswitch[vswitch].setBackground(GUIColorsAndFonts.buttonBackground1);
		    vswitch = -1;

		}
	    }
	};
    /** ActionListener to exit the IGP_Panel and update BehaviourHolder */
    private ActionListener exitButtonListener = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent actionEvent) {
	        for(int i = 0; i<nbf; i++){
		  prflabel[i].setBackground(GUIColorsAndFonts.buttonBackground1);
		}
		BehaviourHolder.getInstance().SetFollowerBehaviour(fob);
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
		/* Print the instruction, used for tests
		   BehaviourHolder.getInstance().GetFollowerBehaviour().PrintElements(); */
	    }
	};

    private ActionListener[] parselistener;

    /**
     * Convert a String to an equivalent InputStream.
     * @param s the String to convert.
     * @return the equivalent InputStream.
     */
    private InputStream stringToStream(String s) {
       InputStream is = new ByteArrayInputStream(s.getBytes());
       return is;
    }

    @Override
    public void initialise() {

    }

    @Override
    public void finalise() {

    }

    @Override
    public void keyPressedHandler(Input e) {

    }

    @Override
    public void keyReleasedHandler(Input e) {

    }

    @Override
    public void keyTypedHandler(Input e) {

    }

}
