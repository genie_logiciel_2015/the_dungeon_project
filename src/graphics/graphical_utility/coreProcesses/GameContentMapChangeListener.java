package graphics.graphical_utility.coreProcesses;

/**
 * Created by dupriez on 14/11/15.
 *
 * This interface must be satisfied by objects that want to be warned when the content of GameContent changes.
 * Part of the observer pattern.
 */
public interface GameContentMapChangeListener {
	/**
	 * What to do when the map of the GameContent changes.
	 */
	void mapChange();
}
