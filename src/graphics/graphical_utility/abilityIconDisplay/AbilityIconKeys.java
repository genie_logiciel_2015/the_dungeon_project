package graphics.graphical_utility.abilityIconDisplay;

/**
 * Created by dupriez on 16/12/15.
 *
 * The icons that are available for abilities (to be used in the class Ability from the core)
 */
public enum AbilityIconKeys {
	SWORD_RED_1,
	SHIELD_1,
	VIAL_RED_1,
	FIRE_BOLT_1,
	FROST_BOLT_1,
	MENTALI_ATTACK,
	EXPLOSION_1,
	NULL_ABILITY,
}
