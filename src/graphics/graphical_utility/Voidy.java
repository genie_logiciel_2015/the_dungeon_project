package graphics.graphical_utility;

/**
 * Created by etouss on 20/12/2015.
 * A tricky class to be able to implements easily trigger effect on menu element in the terminal_interface.
 */
public interface Voidy {
    public void apply();
}
