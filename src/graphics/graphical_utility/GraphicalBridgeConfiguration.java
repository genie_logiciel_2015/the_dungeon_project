package graphics.graphical_utility;

import core.zone.Point;
import graphics.guiSkeleton.mapManagement.GraphicsMapPoint;
import logging.Logging;

/**
 * Created by etouss on 20/12/2015.
 */
public final class GraphicalBridgeConfiguration {
    /*FAUX POUR LE MOMENT LA valeur rechercher est celle du la sprite la plus grande divisé par le mouvement minimal*/
    /*Ainsi les mouvements seront tous détecté  ???important???*/
    public static final int RATIO_TERM_GUI = 10;

    /*LIEN ENTRE PIXEL ET TILES*/
    public static final int TILE_SCALE=32;
    public static final int TILE_SIZE=16;

    /**
     * Converts a distance expressed in the Core coordinate system to the same distance, expressed in the Graphics coordinate system
     * @param distanceInCoreSystem
     * @return
     */
    public static final int core2Graphics(int distanceInCoreSystem) {
        return ( (int) ((float) distanceInCoreSystem * (float) TILE_SCALE / (float) (Point.TileScale)));
    }

    //For checking the core2Graphics method
    public static void main(String[] args){
    	Logging.getInstance().getLogger().info("DistanceConverterCheck: Input="+50+", Output="+ core2Graphics(50));
    }

    /**
     * Convert the coordinates of a Point instance (defined in the core) to an instance of GraphicsMapPoint (with coordinates in the Swing system)
     * @param x: the x coordinate of an instance of Point (IN THE CORE COORDINATE SYSTEM !)
     * @param y: the y coordinate of the same instance of Point (IN THE CORE COORDINATE SYSTEM !)
     * @return
     */
    public static GraphicsMapPoint makeMapPointFromCoreCoordinates(int x, int y)
    {
        return new GraphicsMapPoint(core2Graphics(y), core2Graphics(x));
    }

}
