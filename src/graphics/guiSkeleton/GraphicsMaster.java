package graphics.guiSkeleton;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.guiSkeleton.guiPanel.GamePanel;
import graphics.guiSkeleton.guiPanel.UnitialisedGUIPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.*;
import graphics.guiSkeleton.guiPanel.menuPanel.characterChoice.MultiPlayer_CharacterChoicePanel;
import graphics.guiSkeleton.guiPanel.menuPanel.characterChoice.SinglePlayer_CharacterChoicePanel;
import graphics.guiSkeleton.guiPanel.menuPanel.configuration.ConfigurationPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.gameCreation.MultiPlayer_GameCreationPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.gameCreation.SinglePlayer_GameCreationPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.gameLoad.SinglePlayer_LoadGamePanel;
import graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel.MultiPlayer_MenuPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel.SeverCreatedPanel;
import graphics.guiSkeleton.inputManagement.MainFrameKeyListener;
import graphics.ingame_input_listener.InputConfiguration;
import ingame_programming.IGPpanel;

import javax.swing.*;
import java.awt.event.WindowEvent;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class is the maestro of the GUI, it knows each element of it, and defines what should be done for each button
 * of it.
 *
 * Structure: 1 JFrame (MainFrame). Multiple JPanels (MainMenuTermPanel, GamePanel...).
 * 		GraphicsMaster changes the Panel that is displayed in the MainFrame according to what the GUI should display
 *
 * It implements the singleton pattern
 */
public class GraphicsMaster extends GraphicsMasterAbstraction {
	// Indicates whether the gui has already been launched (so that it is not launched again if GraphicsMaster is
	// instantiated a second time by mistake)

	/** Singleton pattern implementation **/
	private MainFrame mainFrame;

	private GraphicsMaster() {
		mapGUIStatesToGUIPanel.put(GUIStates.UNINITIALISED, new UnitialisedGUIPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MAIN_MENU, new MainMenuPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.GAME_PANEL, new GamePanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_MENU, new SinglePlayer_MenuPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_MENU, new MultiPlayer_MenuPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.INPUT_CONFIG_PANEL, new ConfigurationPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.IGP_PANEL, new IGPpanel(this));

		mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_GAME_CREATION, new MultiPlayer_GameCreationPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_CHARACTER_CHOICE, new MultiPlayer_CharacterChoicePanel(this));

		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_GAME_CREATION, new SinglePlayer_GameCreationPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_CHARACTER_CHOICE, new SinglePlayer_CharacterChoicePanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_LOADGAME, new SinglePlayer_LoadGamePanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.LOST_CONNECTION, new LostConnectionPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.PLEASE_WAIT, new PleaseWaitPanel(this));

		mapGUIStatesToGUIPanel.put(GUIStates.SERVER_ADDRESS_PANEL, new ServerAddressPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.GAME_OVER, new GameOverPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.GAME_WIN, new GameWinPanel(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SERVER_CREATED_PANEL, new SeverCreatedPanel(this));
	}

	public static void build() {
		thisGraphicsMaster = new GraphicsMaster();
		thisGraphicsMaster.mainFrameKeyListener = new MainFrameKeyListener((GraphicsMaster)thisGraphicsMaster);
	}

	/**
	 * This method launches the GUI (the name is quite self-explanatory :))
	 */
	@Override
	public void launchGUI() {
		if(guiLaunched == false) {
			launchSwingApplication();
			guiLaunched = true;
		}
	}

	private void launchSwingApplication() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				mainFrame = new MainFrame();
				mainFrame.init(mainFrameKeyListener);
				changeGUIStateTo(GUIStates.MAIN_MENU);
			}
		});
	}

	/**######################################################
	 * The following section contains methods used to change the state of the GUI. The state of the gui is basically
	 * the panel that is currently displayed in mainFrame.
	 */

	/**
	 *	close the mainframe and terminates the program
	 *	NOT FINISHED
	 */
	@Override
	public void exitGUI() {
		InputConfiguration.UpdateConfigurationWhenGameLeaves();
		System.exit(0);
		mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * This method changes the state of the GUI.
	 * It firsts do a switch on the current state of the GUI, to do the necessary clean up.
	 * It then asks the method setGUIState with arg_guiState.
	 * @param arg_guiState: the new GUIState the caller want the guiState to be changed to
	 */
	@Override
	public void changeGUIStateTo(GUIStates arg_guiState) {
		mainFrame.remove(mapGUIStatesToGUIPanel.get(guiState));
		mapGUIStatesToGUIPanel.get(guiState).finalise();
		setGUIStateTo(arg_guiState);
	}

	/**
	 * This method set the state of the GUI to the value received in argument.
	 * This method MUST NOT be called by anywhere else than in the changeGUIState method's body.
	 * @param arg_guiState: the GUIState the caller want the guiState to be set to
	 */
	private void setGUIStateTo(GUIStates arg_guiState) {
		PanelAbstraction guiPanelToDisplay = mapGUIStatesToGUIPanel.get(arg_guiState);
		mainFrame.add(guiPanelToDisplay);
		guiPanelToDisplay.initialise();
		guiState = arg_guiState;
	}
}
