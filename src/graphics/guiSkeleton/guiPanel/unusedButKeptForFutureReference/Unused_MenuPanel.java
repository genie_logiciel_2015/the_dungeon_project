package graphics.guiSkeleton.guiPanel.unusedButKeptForFutureReference;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.GUIPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by dupriez on 29/11/15.
 *
 * An abstract class for creating menu panels
 */
public abstract class Unused_MenuPanel extends GUIPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1486820374120351897L;
	private JPanel titlePanel;
	private JPanel buttonPanel;
	private JPanel bottomPanel;

	//TODO c never used ?
	public Unused_MenuPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
		setLayout(new GridBagLayout());
		titlePanel = new JPanel();
		setTitlePanel("titlePanel undefined");
		buttonPanel = new JPanel();
		buttonPanel.add(new JLabel("buttonPanel undefined"));
		bottomPanel = new JPanel();
		setBottomPanel("");
	}

	/**
	 * Create a new titlePanel, and replace the one already used in this menuPanel
	 * @param title: The string to be displayed by the titlePanel.
	 */
	protected void setTitlePanel(String title) {
		/** Create the new titlePanel **/

		JPanel newTitlePanel = new JPanel();
		newTitlePanel.setOpaque(true);
		newTitlePanel.setBackground(GUIColorsAndFonts.backGround2);

		//TODO: move the titleLabel to the center of the newTitlePanel
		JLabel titleLabel = new JLabel(title);
		titleLabel.setForeground(GUIColorsAndFonts.titleColor1);
		titleLabel.setFont(GUIColorsAndFonts.titleFont1);
		newTitlePanel.add(titleLabel);

		/** Swap the newTitlePanel with the one already in place **/

		this.remove(titlePanel);
		titlePanel = newTitlePanel;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=0; c.gridwidth=1; c.gridheight=1;c.weightx=1;c.weighty=0.5;c.fill=GridBagConstraints.BOTH;
		add(titlePanel, c);
	}

	/**
	 * Create a new buttonPanel, and replace the one already used in this menuPanel
	 * @param buttonDefList: A list of buttonPanelButtonDefinition, defining the buttons this buttonPanel must contain
	 */
	protected void setButtonPanel(ArrayList<buttonPanelButtonDefinition> buttonDefList){
		/** Create the new buttonPanel **/
		JPanel newButtonPanel = makeButtonPanel(buttonDefList);

		/** Swap the newTitlePanel with the one already in place **/
		this.remove(buttonPanel);
		buttonPanel = newButtonPanel;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=1; c.gridwidth=1; c.gridheight=1;c.weightx=1;c.weighty=10;c.fill=GridBagConstraints.BOTH;
		add(buttonPanel, c);
	}

	/**
	 * This class defines the buttons that can be displayed in the central row of the MainMenuTermPanel
	 */
	protected class buttonPanelButtonDefinition {
		private String buttonText;
		private ActionListener buttonActionListener;

		buttonPanelButtonDefinition(String buttonText, ActionListener buttonActionListener) {
			this.buttonText = buttonText;
			this.buttonActionListener = buttonActionListener;
		}

		public String getButtonText() {
			return buttonText;
		}

		public ActionListener getButtonActionListener() {
			return buttonActionListener;
		}
	}

	/**
	 * This method creates a buttonPanel to be displayed on the MainMenuTermPanel
	 * @param buttonDefList: A list of buttonPanelButtonDefinition
	 * @return	A JPanel with all the asked buttons in the central row (buttonPanel)
	 */
	private JPanel makeButtonPanel(ArrayList<buttonPanelButtonDefinition> buttonDefList) {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		buttonPanel.setOpaque(true);
		buttonPanel.setBackground(GUIColorsAndFonts.backGround1);

		ArrayList<Component> verticalBoxesList = new ArrayList<>();
		ArrayList<Component> horizontalBoxesList = new ArrayList<>();
		ArrayList<JComponent> buttonList = new ArrayList<>();

		/** Create vertical boxes **/
		//Add an initial verticalBox, but do not put it in verticalBoxesList.
		Component firstVerticalBox = Box.createVerticalGlue();
		//For each buttonDef in buttonDefList, create a verticalBox
		for (int i = 0;i < buttonDefList.size();i++) {
			verticalBoxesList.add(Box.createVerticalGlue());
		}

		/** Create horizontal boxes **/
		//For each buttonDef in buttonDefList, create 2 horizontallBox (one on each side of the button
		for (int i = 0;i < buttonDefList.size(); i++) {
			horizontalBoxesList.add(Box.createHorizontalGlue());
			horizontalBoxesList.add(Box.createHorizontalGlue());
		}

		/** Create buttons **/
		//Create all the button whose definition are in buttonDefList
		JButton buttonUnderConstruction;
		for (buttonPanelButtonDefinition buttonDef : buttonDefList) {
			buttonUnderConstruction = new JButton();
			buttonUnderConstruction.setFocusPainted(false);
			buttonUnderConstruction.setFont(GUIColorsAndFonts.buttonFont1);
			buttonUnderConstruction.setText(buttonDef.getButtonText());
			buttonUnderConstruction.setForeground(GUIColorsAndFonts.fontColor1);
			buttonUnderConstruction.setOpaque(true);
			buttonUnderConstruction.setBackground(GUIColorsAndFonts.buttonBackground1);
			buttonUnderConstruction.addActionListener(buttonDef.getButtonActionListener());

			buttonList.add(buttonUnderConstruction);
		}

		/** Compute the weight of the components **/
		int buttonWeightX = 1;
		int buttonWeightY = 1;
		int verticalBoxWeightX = 1;
		int verticalBoxWeightY = (15 * (buttonWeightY * buttonDefList.size())) / (buttonDefList.size() + 1);
		int horizontalBoxWeightX = 3;
		int horizontalBoxWeightY = 1;

		/** Create the gridBagConstraints templates **/
		//hbc -> horizontalBoxConstraint
		GridBagConstraints hbc = new GridBagConstraints();
		hbc.gridwidth=1;hbc.gridheight=1;hbc.weightx=horizontalBoxWeightX;hbc.weighty=horizontalBoxWeightY;

		//vbc -> verticalBoxConstraint
		GridBagConstraints vbc = new GridBagConstraints();
		vbc.gridwidth=1;vbc.gridheight=1;vbc.weightx=verticalBoxWeightX;vbc.weighty=verticalBoxWeightY;

		//bc -> buttonBoxConstraint
		GridBagConstraints bc = new GridBagConstraints();
		bc.gridwidth=1;bc.gridheight=1;bc.weightx=buttonWeightX;bc.weighty=buttonWeightY;bc.fill=GridBagConstraints.BOTH;


		/** Add all the component to the buttonPanel **/
		//Add the first vertical box
		vbc.gridx = 1; vbc.gridy = 0;
		buttonPanel.add(firstVerticalBox, vbc);
		//Add the other components
		for (int i = 0; i < buttonDefList.size(); i++) {
			hbc.gridx = 0; hbc.gridy = 2*i+1;
			buttonPanel.add(horizontalBoxesList.get(2*i), hbc);
			hbc.gridx = 2; hbc.gridy = 2*i+1;
			buttonPanel.add(horizontalBoxesList.get(2*i + 1), hbc);
			bc.gridx = 1; bc.gridy = 2*i+1;
			buttonPanel.add(buttonList.get(i), bc);
			vbc.gridx = 1; vbc.gridy = 2*i+2;
			buttonPanel.add(verticalBoxesList.get(i), vbc);
		}

		return buttonPanel;
	}

	/**
	 * Create a new bottomPanel, and replace the one already used in this menuPanel
	 * @param text: The string to be displayed by the bottomPanel.
	 */
	protected void setBottomPanel(String text){
		/** Creation of the newBottomPanel **/

		JPanel newBottomPanel = new JPanel();
		newBottomPanel.setLayout(new BorderLayout());
		newBottomPanel.setOpaque(true);
		newBottomPanel.setBackground(GUIColorsAndFonts.backGround1);

		JLabel bottomLabel = new JLabel();
		bottomLabel.setFont(GUIColorsAndFonts.smallFont1);
		bottomLabel.setText(text);
		bottomLabel.setForeground(GUIColorsAndFonts.fontColor1);
		newBottomPanel.add(bottomLabel, BorderLayout.LINE_START);

		/** Swap the newBottomPanel with the one already in place **/
		this.remove(bottomPanel);
		bottomPanel = newBottomPanel;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=2; c.gridwidth=1; c.gridheight=1;c.weightx=0;c.weighty=0;c.fill=GridBagConstraints.BOTH;
		add(bottomPanel, c);
	}
}
