package graphics.guiSkeleton.guiPanel.unusedButKeptForFutureReference;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ghocquet on 05/12/15.
 *
 * All the spell of the different classes
 */
public class SpellCatalog
{
	private ArrayList<Integer> classSpellWarrior = new ArrayList<>(
			Arrays.asList(1,2,3));
	private ArrayList<Integer> classSpellMage = new ArrayList<>(
			Arrays.asList(4,5,3));
	private ArrayList<ArrayList<Integer>> classSpellList = new ArrayList<>(
			Arrays.asList(classSpellWarrior,classSpellMage));

	private ArrayList<Integer> spellFromClass;
	private ArrayList<Long> spellTime;

	private int cooldown[] = {0, 1000, 10000, 1000, 5000, 5000};

	public SpellCatalog(int characterClass)
	{
		spellFromClass = classSpellList.get(characterClass);
		spellTime = new ArrayList<>();
		for (int i = 0; i < spellFromClass.size(); i++)
			spellTime.add((long) 0);
	}

	public ArrayList<Integer> getSpellFromClass()
	{
		return spellFromClass;
	}

	public float getCharged(int spellRank)
	{
		long now = new java.util.Date().getTime();
		long cd = cooldown[spellFromClass.get(spellRank)];
		long castTime = spellTime.get(spellRank);
		if (now > castTime + cd)
		{
			return 0;
		}
		else
		{
			return (float)(castTime + cd - now) / cd;
		}
	}

	public boolean isReady(int spellRank){
		return getCharged(spellRank) == 0;
	}

	public void cast(int spellRank)
	{
		spellTime.set(spellRank, new java.util.Date().getTime());
		//System.out.println("Cast : " + spellRank);
	}
}
