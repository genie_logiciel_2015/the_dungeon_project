package graphics.guiSkeleton.guiPanel;

import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.guiSkeleton.GraphicsMaster;

import javax.swing.*;
/**
 * Created by dupriez on 12/11/15.
 *
 * This abstract class defines the kind of panel the gui can display
 * The gui changes what it displays by swapping GUIPanels in the mainFrame.
 */
public abstract class GUIPanel extends JPanel implements PanelAbstraction{

	//Store the graphicsMaster that created the guiPanel (there should be only one GraphicsMaster instanciation per application,
	// this variable is just there to give the guiPanel the right to call the method changeGUIStateTo de graphicsMaster)

	private static final long serialVersionUID = 1L;
	private GraphicsMaster graphicsMaster;
	protected GraphicsMaster getGraphicsMaster() {return graphicsMaster;}

	public GUIPanel(GraphicsMaster graphicsMaster) {
		this.graphicsMaster = graphicsMaster;

		setFocusable(true);
	}
}