package graphics.guiSkeleton.guiPanel.menuPanel.characterChoice;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 */
public class SinglePlayer_CharacterChoicePanel extends MenuPanel implements SinglePlayer_CharacterChoicePanelAbstraction{

	private static final long serialVersionUID = 1L;
	private SinglePlayer_CharacterChoicePanelAbstractionController controller = new SinglePlayer_CharacterChoicePanelAbstractionController(this);

	public SinglePlayer_CharacterChoicePanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
		setTitlePanel("SP Character Choice");
		addToCentralColumn(new CharacterChoice_SubPanel(), 1, 1);
		addToCentralColumn(ButtonMaker.makeButton("Play", playButton_ActionListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Back", backButton_ActionListener),1,1);
	}

	private ActionListener playButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.playButtonPressed();
		}
	};

	private ActionListener backButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.backButtonPressed();
		}
	};

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void playButtonAction() {
		//Not working for now
//			getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
	}

	@Override
	public void backButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_GAME_CREATION);
	}
}
