package graphics.guiSkeleton.guiPanel.menuPanel.characterChoice;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.MultiPlayer_CharacterChoicePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.MultiPlayer_CharacterChoicePanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_CharacterChoicePanel extends MenuPanel implements MultiPlayer_CharacterChoicePanelAbstraction {

	private static final long serialVersionUID = 1L;
	private MultiPlayer_CharacterChoicePanelAbstractionController controller = new MultiPlayer_CharacterChoicePanelAbstractionController(this);

	public MultiPlayer_CharacterChoicePanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
		setTitlePanel("MP Character Choice");
		addToCentralColumn(new CharacterChoice_SubPanel(), 1, 1);
		addToCentralColumn(ButtonMaker.makeButton("Return to Lounge", returnToLoungeButton_ActionListener),1,1);
	}

	private ActionListener returnToLoungeButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.returnToLoungeButtonPressed();
		}
	};

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void returnToLoungeButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOUNGE);
	}
}
