package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.SinglePlayer_MenuPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.SinglePlayer_MenuPanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;

import gameloop.GameStarter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player to create or load a game session
 */
public class SinglePlayer_MenuPanel extends MenuPanel implements SinglePlayer_MenuPanelAbstraction {

	private static final long serialVersionUID = 1L;
	private SinglePlayer_MenuPanelAbstractionController controller = new SinglePlayer_MenuPanelAbstractionController(this);

	public SinglePlayer_MenuPanel(GraphicsMaster graphicsMaster){
		super(graphicsMaster);
		setTitlePanel("SinglePlayer Menu");
//		addToCentralColumn(ButtonMaker.makeButton("Create Game", createGameButton_ActionListener),1,1);
//		addToCentralColumn(ButtonMaker.makeButton("Load Game", loadGameButton), 1, 1);
		JButton oldPlayButton = ButtonMaker.makeButton("Play", oldPlayButton_ActionListener);
		oldPlayButton.setBackground(new Color(174,0,0));
		addToCentralColumn(oldPlayButton, 1, 1);
		addToCentralColumn(ButtonMaker.makeButton("Back", backButton_ActionListener),1,1);
	}

	private ActionListener createGameButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.createGameButtonButtonPressed();
		}
	};

	private ActionListener loadGameButton = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.loadGameButtonButtonPressed();
		}
	};

	private ActionListener oldPlayButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.oldPlayButtonPressed();
		}
	};

	private ActionListener backButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.backButtonPressed();
		}
	};


	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void createGameButtonButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_GAME_CREATION);
	}

	@Override
	public void loadGameButtonButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_LOADGAME);
	}

	@Override
	public void oldPlayButtonAction() {
		GameStarter.startGameSinglePlayer();
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
	}

	@Override
	public void backButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MAIN_MENU);
	}
}
