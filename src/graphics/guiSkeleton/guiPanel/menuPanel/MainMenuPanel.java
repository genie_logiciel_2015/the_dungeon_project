package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.MainMenuPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.MainMenuPanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;
import logging.Logging;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 14/11/15.
 */
public class MainMenuPanel extends MenuPanel implements MainMenuPanelAbstraction{

	private static final long serialVersionUID = 1L;

	private MainMenuPanelAbstractionController controller = new MainMenuPanelAbstractionController(this);

	public MainMenuPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
		setTitlePanel("The Dungeon Project");

		addToCentralColumn(ButtonMaker.makeButton("single player", singlePlayerButtonListener),1,1);
		//addToCentralColumn(ButtonMaker.makeButtonWithImage("src/graphics/guiSkeleton/guiPanel/interfaceElements/singlePlayerButton.png", "src/graphics/guiSkeleton/guiPanel/interfaceElements/singlePlayerPressedButton.png",singlePlayerButtonListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("multi player", multiPlayerButtonListener), 1, 1);
		addToCentralColumn(ButtonMaker.makeButton("configuration", configurationButtonListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("IGP", IGPButtonListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("exit", exitButtonListener),1,1);
	}

	private ActionListener singlePlayerButtonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.singlePlayerButtonPressed();
}
};

	private ActionListener multiPlayerButtonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.multiPlayerButtonPressed();
		}
	};

        private ActionListener configurationButtonListener = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent actionEvent) {
			controller.configurationButtonPressed();
	    }
	};

        private ActionListener IGPButtonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.IGPButtonPressed();
		}
	};

	private ActionListener exitButtonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.exitButtonPressed();
		}
	};


	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {
//		underlyingMainMenuPanel.setPreferredSize(new Dimension(getSize().width, getSize().height));
//		Logging.getInstance().getLogger().info"Dimensions= "+getSize().width+", "+getSize().height);
//		setSize(new Dimension(getSize().width, getSize().height));

	}

	@Override
	public void finalise() {

	}

	@Override
	public void singlePlayerButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.SINGLEPLAYER_MENU);
	}

	@Override
	public void multiPlayerButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.SERVER_ADDRESS_PANEL);
	}

	@Override
	public void configurationButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.INPUT_CONFIG_PANEL);
	}

	@Override
	public void IGPButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.IGP_PANEL);
	}

	@Override
	public void exitButtonAction() {
		getGraphicsMaster().exitGUI();
	}

}
