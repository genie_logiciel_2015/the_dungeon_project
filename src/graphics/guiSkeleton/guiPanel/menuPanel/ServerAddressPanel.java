package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;
import network.outer_shell.RPCClient;
import network.outer_shell.server.RPCServer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by bogdanbear on 04/01/2016.
 */
public class ServerAddressPanel extends MenuPanel{
    JTextField addressTextField=new JTextField("127.0.0.1");;
    public ServerAddressPanel(GraphicsMaster graphicsMaster) {
        super(graphicsMaster);

        JLabel inputLabel = new JLabel("To play please write the server address");
        inputLabel.setFont(GUIColorsAndFonts.smallFont1);
        inputLabel.setForeground(GUIColorsAndFonts.fontColor1);
        setTitlePanel("IP address.");
        addToCentralColumn(ButtonMaker.makeButton("Host a server", serverButtonActionListener),1,1);
        addToCentralColumn(inputLabel,1,1);
        addToCentralColumn(addressTextField,1,1);
        //addToCentralColumn(inputLabel,1,1);
        addToCentralColumn(ButtonMaker.makeButton("Connection", confirmButtonActionListener),1,1);
        addToCentralColumn(ButtonMaker.makeButton("Back", backButtonActionListener),1,1);


    }

    private ActionListener confirmButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                String address = addressTextField.getText();
                RPCClient.setIp(address);
                //it should start RPC calls
                //LocalGameLoop.getInstance().setShouldRPC(true);
                getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MULTIPLAYER_MENU);
                RPCClient.connect();
            }
            catch (RemoteException |MalformedURLException |NotBoundException e1) {
                getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.LOST_CONNECTION);
            }
        }
    };

    private ActionListener backButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
        }
    };

    private ActionListener serverButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            RPCServer.startServer();
            getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.SERVER_CREATED_PANEL);
        }
    };

    @Override
    public void initialise() {

    }

    @Override
    public void finalise() {

    }

    @Override
    public void keyPressedHandler(Input e) {

    }

    @Override
    public void keyReleasedHandler(Input e) {

    }

    @Override
    public void keyTypedHandler(Input e) {

    }
}
