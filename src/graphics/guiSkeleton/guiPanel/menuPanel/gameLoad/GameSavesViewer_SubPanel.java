package graphics.guiSkeleton.guiPanel.menuPanel.gameLoad;

import javax.swing.*;

/**
 * Created by dupriez on 07/12/15.
 *
 * Displays all the previously saved games.
 * To be included in SinglePlayer_LoadGamePanel
 */
public class GameSavesViewer_SubPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	public GameSavesViewer_SubPanel(){
		add(new JLabel("GameSavesViewer_SubPanel"));
	}

}
