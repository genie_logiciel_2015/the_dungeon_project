package graphics.guiSkeleton.guiPanel.menuPanel.gameLoad;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.gameLoad.SinglePlayer_LoadGamePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.gameLoad.SinglePlayer_LoadGamePanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player load a game session
 */
public class SinglePlayer_LoadGamePanel extends MenuPanel implements SinglePlayer_LoadGamePanelAbstraction {

	private static final long serialVersionUID = 1L;
	private SinglePlayer_LoadGamePanelAbstractionController controller = new SinglePlayer_LoadGamePanelAbstractionController(this);

	public SinglePlayer_LoadGamePanel(GraphicsMaster graphicsMaster){
		super(graphicsMaster);
		setTitlePanel("Load Game");
		addToCentralColumn(new GameSavesViewer_SubPanel(),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Load", loadButton_ActionListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Back", backButton_ActionListener),1,1);
	}

	private ActionListener loadButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.loadButtonPressed();
		}
	};

	private ActionListener backButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.backButtonPressed();
		}
	};


	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void loadButtonAction() {
		//Not working for now
		//getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
	}

	@Override
	public void backButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_MENU);
	}
}
