package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by bogdanbear on 02/01/2016.
 */
public class LostConnectionPanel extends MenuPanel{
    public LostConnectionPanel(GraphicsMaster graphicsMaster) {
        super(graphicsMaster);

        JLabel sorryLabel = new JLabel("Connection Lost or Bad Server Address...");
        sorryLabel.setFont(GUIColorsAndFonts.smallFont1);
        sorryLabel.setForeground(GUIColorsAndFonts.fontColor1);

        setTitlePanel("Connection Lost");
        addToCentralColumn(sorryLabel,1,1);
        addToCentralColumn(ButtonMaker.makeButton("Return to main menu", returnButtonActionListener),1,1);
    }

    private ActionListener returnButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
        }
    };

    @Override
    public void initialise() {

    }

    @Override
    public void finalise() {

    }

    @Override
    public void keyPressedHandler(Input e) {

    }

    @Override
    public void keyReleasedHandler(Input e) {

    }

    @Override
    public void keyTypedHandler(Input e) {

    }


}
