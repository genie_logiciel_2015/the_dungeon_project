package graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;

import gameloop.GameStarter;
import network.outer_shell.Lounge;
import network.outer_shell.RPCClient;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player to host or join multiplayer games
 */
public class MultiPlayer_MenuPanel extends MenuPanel {

	private static final long serialVersionUID = 1L;
	public MultiPlayer_MenuPanel(GraphicsMaster graphicsMaster){
		super(graphicsMaster);
		JLabel inputLabel = new JLabel("Choose the game you want to join :");
		inputLabel.setFont(GUIColorsAndFonts.smallFont1);
		inputLabel.setForeground(GUIColorsAndFonts.fontColor1);
		setTitlePanel("MultiPlayer Thessia");
		//addToCentralColumn(ButtonMaker.makeButton("Join Lounge", joinLoungeButton_ActionListener),1,1);
		addToCentralColumn(inputLabel,1,1);
		addToCentralColumn(new LoungeList_SubPanel(getGraphicsMaster()),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Refresh Lounges List", refreshGameButton_ActionListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Create a new game", hostNewGameButton_ActionListener),1,1);
		JButton oldPlayButton = ButtonMaker.makeButton("Old play button", oldPlayButton_ActionListener);
		oldPlayButton.setBackground(new Color(174,0,0));
		addToCentralColumn(oldPlayButton, 1, 1);
		addToCentralColumn(ButtonMaker.makeButton("Back", backButton_ActionListener),1,1);
	}

	private ActionListener hostNewGameButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			try{
				Lounge lounge=RPCClient.createLounge("test");
				if (lounge!=null) {
					//there have been no errors in lounge creation
					RPCClient.joinLounge(lounge.getLoungeID());
					GameStarter.startGameMultiplayerPlayer(RPCClient.getIp(),lounge.getPort());
					getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.PLEASE_WAIT);
					getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.GAME_PANEL);
				}
			}
			catch (RemoteException |MalformedURLException |NotBoundException e1) {
				getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.LOST_CONNECTION);
			}
		}
	};
	private ActionListener refreshGameButton_ActionListener=new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				JPanel listPanel = new JPanel();
				ArrayList<Lounge> lounges = RPCClient.getLoungeList();
				if (lounges != null) {
					for (Lounge lounge : lounges) {
						JPanel loungePanel = PickLoungeSubPanelFactory.createPickLoungeSubPanel(lounge.getName() + "  " + (lounge.MAX_PLAYERS - lounge.numberFreeSpots()) + "/" + lounge.MAX_PLAYERS, lounge, LoungeList_SubPanel.getGraphicsMaster());
						listPanel.add(loungePanel);
					}

					BoxLayout boxLayout = new BoxLayout(listPanel, BoxLayout.Y_AXIS);
					listPanel.setLayout(boxLayout);
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.getViewport().add(listPanel);
					add(scrollPane);
				}
				LoungeList_SubPanel.getUniqueInstance().removeAll();
				LoungeList_SubPanel.getUniqueInstance().add(listPanel);
				LoungeList_SubPanel.getUniqueInstance().revalidate();
				LoungeList_SubPanel.getUniqueInstance().repaint();
			}
			catch (RemoteException |MalformedURLException |NotBoundException e1) {
				getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.LOST_CONNECTION);
			}
		}
	};

//	private ActionListener joinLoungeButton_ActionListener = new ActionListener() {
//		@Override
//		public void actionPerformed(ActionEvent actionEvent) {
//			getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.PLEASE_WAIT);
//		}
//	};

	private ActionListener joinGameAtIP_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOUNGE);
		}
	};

	private ActionListener oldPlayButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			GameStarter.startGameMultiplayerPlayer("127.0.0.1",8888);
			getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.GAME_PANEL);
		}
	};

	private ActionListener backButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MAIN_MENU);
		}
	};


	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}
}
