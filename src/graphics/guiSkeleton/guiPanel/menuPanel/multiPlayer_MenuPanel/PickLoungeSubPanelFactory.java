package graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel;

import gameloop.GameStarter;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import logging.Logging;
import network.outer_shell.Lounge;
import network.outer_shell.RPCClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by bogdanbear on 04/01/2016.
 */
public class PickLoungeSubPanelFactory {
    public static JPanel createPickLoungeSubPanel(String info,Lounge lounge, GraphicsMaster graphicsMaster)
    {
        JPanel result=new JPanel(new FlowLayout());
        JLabel infoLabel = new JLabel(info);
        infoLabel.setFont(GUIColorsAndFonts.smallFont1);
        infoLabel.setForeground(GUIColorsAndFonts.fontColor1);
        //ImageIcon imageIcon=new ImageIcon("src/graphics/guiSkeleton/guiPanel/interfaceElements/playicon.jpg");
        JButton playButton=new JButton("Join");
        playButton.addActionListener(new PlayButtonActionListener(lounge,graphicsMaster));
        result.add(infoLabel);
        result.add(playButton);
        return result;
    }

    private static class PlayButtonActionListener implements ActionListener{
        private Lounge lounge;
        private GraphicsMaster graphicsMaster;
        public PlayButtonActionListener(Lounge lounge, GraphicsMaster graphicsMaster)
        {
            this.lounge=lounge;
            this.graphicsMaster=graphicsMaster;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (RPCClient.joinLounge(lounge.getLoungeID()))
                {
                    graphicsMaster.changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.PLEASE_WAIT);
                    Logging.getInstance().getLogger().info("PickLoungeSubPanel in the PickLoungeSubPanelFactory: server gives green light. Initiate join...");
                    GameStarter.startGameMultiplayerPlayer(RPCClient.getIp(),lounge.getPort());
                    graphicsMaster.changeGUIStateTo(GraphicsMaster.GUIStates.GAME_PANEL);
                }
                else{
                    Logging.getInstance().getLogger().warning("PickLoungeSubPanel in the PickLoungeSubPanelFactory: join failed");
                }
            } catch (RemoteException|MalformedURLException|NotBoundException e1) {
                    graphicsMaster.changeGUIStateTo(GraphicsMaster.GUIStates.LOST_CONNECTION);
            }
        }
    }

}
