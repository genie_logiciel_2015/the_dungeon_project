package graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Myriam Bégel
 */
public class SeverCreatedPanel extends MenuPanel{
    public SeverCreatedPanel(GraphicsMaster graphicsMaster) {
        super(graphicsMaster);

        JLabel label1 = new JLabel("A server has been launched on your computer.");
        label1.setFont(GUIColorsAndFonts.smallFont1);
        label1.setForeground(GUIColorsAndFonts.fontColor1);
        addToCentralColumn(label1,1,1);

        JLabel label2 = new JLabel("People can now connect to it,");
        label2.setFont(GUIColorsAndFonts.smallFont1);
        label2.setForeground(GUIColorsAndFonts.fontColor1);
        addToCentralColumn(label2,1,1);

        JLabel label3 = new JLabel("please give them your IP address");
        label3.setFont(GUIColorsAndFonts.smallFont1);
        label3.setForeground(GUIColorsAndFonts.fontColor1);
        addToCentralColumn(label3,1,1);

        JLabel label4 = new JLabel("and don't close this window");
        label4.setFont(GUIColorsAndFonts.smallFont1);
        label4.setForeground(GUIColorsAndFonts.fontColor1);
        addToCentralColumn(label4,1,1);

        addToCentralColumn(ButtonMaker.makeButton("Back to Main Menu", backButtonActionListener),1,1);
        setTitlePanel("Server Created");
    }

    private ActionListener backButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
        }
    };

    @Override
    public void initialise() {

    }

    @Override
    public void finalise() {

    }

    @Override
    public void keyPressedHandler(Input e) {

    }

    @Override
    public void keyReleasedHandler(Input e) {

    }

    @Override
    public void keyTypedHandler(Input e) {

    }


}
