package graphics.guiSkeleton.guiPanel.menuPanel.multiPlayer_MenuPanel;

import graphics.guiSkeleton.GraphicsMaster;

import javax.swing.*;

/**
 * Created by dupriez on 07/12/15.
 *
 * To be included in the Multiplayer_MenuPanel.
 * Shows the list of all exiting lounges on the server.
 *
 */
public class LoungeList_SubPanel extends JPanel {

	//CODE FROM BoxLayout tutorial,  ListDialog.java
	private static final long serialVersionUID = 1L;
	private static GraphicsMaster graphicsMaster;
	//by design, we use the same panels forever anyway, so a static variable here is not problematic
	private static LoungeList_SubPanel uniqueInstance;
	public LoungeList_SubPanel(GraphicsMaster graphicsMaster) {
		this.graphicsMaster=graphicsMaster;
		uniqueInstance=this;
	}


	public static LoungeList_SubPanel getUniqueInstance() {
		return uniqueInstance;
	}

	public static GraphicsMaster getGraphicsMaster() {
		return graphicsMaster;
	}
}
