package graphics.guiSkeleton.guiPanel.menuPanel.configuration;

import graphics.graphical_abstraction.panel.menu.ConfigurationPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.ConfigurationPanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;
import graphics.ingame_input_listener.InputConfiguration;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zobi on 25/11/15.
 */

/**
 * Manages the inputs : if the users wants to show the controls and/or change them
 */
public class ConfigurationPanel extends MenuPanel implements ConfigurationPanelAbstraction{

	private static final long serialVersionUID = 1L;
    private static Input InputBeingChanged = Input.NONE;
    private ConfigurationPanelAbstractionController controller = new ConfigurationPanelAbstractionController(this);
    //Le bouton "back" n'est pas dans la liste
    private List<JButton> buttonList = new ArrayList<>();

    private void createAndAddButton(String label, ActionListener buttonActionListener) {
        JButton newButton = ButtonMaker.makeButton(label, buttonActionListener);
        buttonList.add(newButton);
        addToCentralColumn(newButton, 1,1);
    }

    public void refreshButtonLabels() {
        buttonList.get(0).setText("move up: "+MoveUpConfig());
        buttonList.get(1).setText("move down: "+MoveDownConfig());
        buttonList.get(2).setText("move right: "+MoveRightConfig());
        buttonList.get(3).setText("move left: "+MoveLeftConfig());
        buttonList.get(4).setText("attack: "+AttackConfig());
        buttonList.get(5).setText("ability 1: "+Ability1Config());
        buttonList.get(6).setText("ability 2: "+Ability2Config());
        buttonList.get(7).setText("ability 3: "+Ability3Config());
        buttonList.get(8).setText("back to main: "+BackToMainConfig());
    }

    /**
     * @return the formal input whose key is being modified
     */
    public Input GetInputBeingChanged(){return InputBeingChanged;}

    public void ResetInputBeingChanged(){InputBeingChanged=Input.NONE;}

    public ConfigurationPanel(GraphicsMaster graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("Configuration");
        createAndAddButton("", configUpListener);
        createAndAddButton("", configDownListener);
        createAndAddButton("", configRightListener);
        createAndAddButton("", configLeftListener);
        createAndAddButton("", configAttackListener);
        createAndAddButton("", configAbility1Listener);
        createAndAddButton("", configAbility2Listener);
        createAndAddButton("", configAbility3Listener);
        createAndAddButton("", configBackToMainListener);
        addToCentralColumn(ButtonMaker.makeButton("back", backButtonListener), 1,1);
    }

    /**
     * List of the action Listeners : one for each formal input and one
     * for back button
     */

    private ActionListener configUpListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigUpButtonPressed();
            }
    };
    private ActionListener configDownListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigDownButtonPressed();
        }
    };
    private ActionListener configLeftListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigLeftButtonPressed();
            }
    };
    private ActionListener configRightListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigRightButtonPressed();
        }
    };
    private ActionListener configAttackListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigAttackButtonPressed();
            }
    };
    private ActionListener configAbility1Listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigAbility1ButtonPressed();
           }
    };
    private ActionListener configAbility2Listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigAbility2ButtonPressed();
             }
    };
    private ActionListener configAbility3Listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigAbility3ButtonPressed();
           }
    };
    private ActionListener configBackToMainListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.ConfigBackToMainButtonPressed();
            }
    };

    private ActionListener backButtonListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.BackButtonPressed();
        }
    };

    //gives the string key linked to the formal input, used below
    private String MoveUpConfig(){return InputConfiguration.GetFormalInputString(Input.UP_ARROW);}
    private String MoveDownConfig(){return InputConfiguration.GetFormalInputString(Input.DOWN_ARROW);}
    private String MoveRightConfig(){return InputConfiguration.GetFormalInputString(Input.RIGHT_ARROW);}
    private String MoveLeftConfig(){return InputConfiguration.GetFormalInputString(Input.LEFT_ARROW);}
    private String AttackConfig(){return InputConfiguration.GetFormalInputString(Input.ATTACK);}
    private String Ability1Config(){return InputConfiguration.GetFormalInputString(Input.ABILITY_1);}
    private String Ability2Config(){return InputConfiguration.GetFormalInputString(Input.ABILITY_2);}
    private String Ability3Config(){return InputConfiguration.GetFormalInputString(Input.ABILITY_3);}
    private String BackToMainConfig(){return InputConfiguration.GetFormalInputString(Input.ESCAPE);}

    /**#######################################################**/
    /** necessary as implements ConfigurationPanelAbstraction **/
    /**#######################################################**/

    /**
     * List of the input change managers
     * checks that only one key is being modified at a time
     */
    public void ChangeMoveUpConfig(){
        if (InputBeingChanged==Input.NONE){
        InputBeingChanged=Input.UP_ARROW;
        new ConfigurationFrame("Move Up: current control: " + MoveUpConfig(),ConfigurationPanel.this);
        }
    }
    public void ChangeMoveDownConfig(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.DOWN_ARROW;
            new ConfigurationFrame("Move Down: current control: " + MoveDownConfig(),ConfigurationPanel.this);
            }
    }
    public void ChangeMoveRightConfig(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.RIGHT_ARROW;
            new ConfigurationFrame("Move Right: current control: " + MoveRightConfig(),ConfigurationPanel.this);
            }
    }
    public void ChangeMoveLeftConfig(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.LEFT_ARROW;
            new ConfigurationFrame("Move Left: current control: " + MoveLeftConfig(),ConfigurationPanel.this);
            }
    }
    public void ChangeAttackConfig(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.ATTACK;
            new ConfigurationFrame("Move Attack: current control: " + AttackConfig(),ConfigurationPanel.this);
            }
    }
    public void ChangeAbility1Config(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.ABILITY_1;
            new ConfigurationFrame("Ability 1: current control: " + Ability1Config(),ConfigurationPanel.this);
            }
    }
    public void ChangeAbility2Config(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.ABILITY_2;
            new ConfigurationFrame("Ability 2: current control: " + Ability2Config(),ConfigurationPanel.this);
            }
    }
    public void ChangeAbility3Config(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.ABILITY_3;
            new ConfigurationFrame("Ability 3: current control: " + Ability3Config(),ConfigurationPanel.this);
        }
    }
    public void ChangeBackToMainConfig(){
        if (InputBeingChanged==Input.NONE){
            InputBeingChanged=Input.ESCAPE;
            new ConfigurationFrame("Escape: current control: " + BackToMainConfig(),ConfigurationPanel.this);}
    }
    public void BackToMainMenu(){getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
    }

    /**###################################**/
    /** necessary as extension of guiPanel **/
    /**###################################**/
    @Override
    public void keyPressedHandler(Input e) {}
    @Override
    public void keyReleasedHandler(Input e) {}
    @Override
    public void keyTypedHandler(Input e) {}
    @Override
    public void initialise() {refreshButtonLabels();}
    @Override
    public void finalise() {}
}