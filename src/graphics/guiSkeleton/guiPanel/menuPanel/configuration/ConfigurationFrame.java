package graphics.guiSkeleton.guiPanel.menuPanel.configuration;
import graphics.ingame_input_listener.InputConfiguration;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by zobi on 02/01/16.
 */

/**
 * We create a frame that plays the role of a virtual keyboard to change
 * the key linked to an input (which has been previously selected)
 */
public class ConfigurationFrame extends Frame implements ActionListener{
    private ArrayList<Button> b= new ArrayList<>();
    private ConfigurationPanel conf;

    public ConfigurationFrame(String title,ConfigurationPanel conf) {
        super(title);
        this.setSize(600, 600);
        this.setVisible(true);
        this.conf=conf;

        // monoalphabetic keys
        String simpleKeyboard="abcdefghijklmnopqrstuvwxyz0123456789+-*(_)=,;:!^$/";
        Button button;

        int LayoutHeight=simpleKeyboard.length()/2;
        int LayoutWidth=3;
        setLayout(new GridLayout(LayoutHeight,LayoutWidth));
        //Adding monoalphabetic keyBoard input
        for (int i=0;i<simpleKeyboard.length();i++) {
            b.add(new Button(Character.toString(simpleKeyboard.charAt(i))));
            b.get(i).addActionListener(this);
            add(b.get(i));
        }
        //Adding arrow inputs
        button=new Button("Right arrow");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Left arrow");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Up arrow");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Down arrow");
        button.addActionListener(this);
        b.add(button);
        add(button);
        //Adding Fi inputs
        button=new Button("F1");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F2");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F3");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F4");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F5");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F6");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F7");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F8");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F9");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F10");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F11");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("F12");
        button.addActionListener(this);
        b.add(button);
        add(button);
        //Adding other inputs
        button=new Button("Escape");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Ctrl");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Caps lock");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Shift");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Back space");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Alt");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Tab");
        button.addActionListener(this);
        b.add(button);
        add(button);
        button=new Button("Enter");
        button.addActionListener(this);
        b.add(button);
        add(button);
    }

    /**
     * @param e the user has clicked on a button
     * the key linked to the formal input linked with the frame
     * (the user has clicked on an input to change before) is changed
     * into the name of the button pressed
     */
    public void actionPerformed(ActionEvent e) {
        Object source=e.getSource();
        for(int i=0;i<b.size();i++){
            if(source==b.get(i)){
                InputConfiguration.ChangeInput(conf.GetInputBeingChanged(),b.get(i).getLabel());
                conf.ResetInputBeingChanged();
                conf.refreshButtonLabels();
                dispose();
            }
        }
    }
}