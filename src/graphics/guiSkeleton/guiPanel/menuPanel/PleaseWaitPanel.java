package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by bogdanbear on 02/01/2016.
 */
public class PleaseWaitPanel extends MenuPanel{
    public PleaseWaitPanel(GraphicsMaster graphicsMaster) {
        super(graphicsMaster);

        JLabel sorryLabel = new JLabel("Please wait until the other players join in...");
        sorryLabel.setFont(GUIColorsAndFonts.smallFont1);
        sorryLabel.setForeground(GUIColorsAndFonts.fontColor1);
        addToCentralColumn(sorryLabel,1,1);
        setTitlePanel("Please wait");
    }
    @Override
    public void initialise() {

    }

    @Override
    public void finalise() {

    }

    @Override
    public void keyPressedHandler(Input e) {

    }

    @Override
    public void keyReleasedHandler(Input e) {

    }

    @Override
    public void keyTypedHandler(Input e) {

    }


}
