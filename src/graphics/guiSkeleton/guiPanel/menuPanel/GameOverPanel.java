package graphics.guiSkeleton.guiPanel.menuPanel;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.GameOverPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.GameOverPanelAbstractionController;
import graphics.graphical_abstraction.panel.menu.MainMenuPanelAbstraction;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 01/01/16.
 */
public class GameOverPanel extends MenuPanel implements GameOverPanelAbstraction {

	GameOverPanelAbstractionController controller = new GameOverPanelAbstractionController(this);

	public GameOverPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);

		JLabel sorryLabel = new JLabel("sorry, you died...");
		sorryLabel.setFont(GUIColorsAndFonts.smallFont1);
		sorryLabel.setForeground(GUIColorsAndFonts.fontColor1);

		setTitlePanel("Game Over");
		addToCentralColumn(sorryLabel,1,1);
		addToCentralColumn(ButtonMaker.makeButton("Return to main menu", returnButtonActionListener),1,1);
	}

	private ActionListener returnButtonActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.returnButtonPressed();
		}
	};

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void returnButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);
	}
}
