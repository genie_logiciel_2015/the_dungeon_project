package graphics.guiSkeleton.guiPanel.menuPanel.facilities;

import graphics.guiSkeleton.GUIColorsAndFonts;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 *
 * To create buttons using the colours and fonts of GUIColorsAndFonts
 */
public class ButtonMaker {

	public static JButton makeButton(String buttonName, ActionListener buttonAction){
		JButton button = new JButton();
		button.setFocusPainted(false);
		button.setFont(GUIColorsAndFonts.buttonFont1);
		button.setText(buttonName);
		button.setForeground(GUIColorsAndFonts.fontColor1);
		button.setOpaque(true);
		button.setBackground(GUIColorsAndFonts.buttonBackground1);
		button.addActionListener(buttonAction);

		return button;
	}

	public static JButton makeButtonWithImage(String unPressed,String Pressed, ActionListener buttonAction){
		JButton button = new JButton(new ImageIcon(unPressed));
		button.setPressedIcon(new ImageIcon(Pressed));
		//button.setBorder(new EmptyBorder(1,0,0,0));
		button.setOpaque(true);
		button.setVisible(true);
		button.setFocusable(true);
		//button.setFocusPainted(false);
		button.addActionListener(buttonAction);
		return button;
	}

}
