package graphics.guiSkeleton.guiPanel.menuPanel.facilities;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;
import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.guiPanel.GUIPanel;
import graphics.guiSkeleton.GraphicsMaster;

import javax.swing.*;
import java.awt.*;

/**
 * Created by dupriez on 05/12/15.
 *
 * The purpose of this class is to be used to create easily simple panels for the gui.
 * It can contain more general components than the MenuPanel one (in fact any JComponent).
 */
public abstract class MenuPanel extends GUIPanel implements MenuPanelAbstraction {

	private static final long serialVersionUID = 1L;

	/** titlePanel variables **/
	private JPanel titlePanel;

	/** contentPanel variables **/
	private JPanel contentPanel;
	private int centralColumnIndex = 0;
	private int contentPanelVBoxWeightX = 0;
	private int contentPanelVBoxWeightY = 5;
	private int contentPanelHBoxWeightX = 3;
	private int contentPanelHBoxWeightY = 0;

	public MenuPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);

		setLayout(new GridBagLayout());

		titlePanel = new JPanel();
		setTitlePanel("titlePanel undefined");

		createAndAddContentPanel();

	}

	/**
	 * Create a new titlePanel, and replace the one already used in this menuPanel
	 * @param title: The string to be displayed by the titlePanel.
	 */
	public void setTitlePanel(String title) {
		/** Create the new titlePanel **/

		JPanel newTitlePanel = new JPanel();
		newTitlePanel.setOpaque(true);
		newTitlePanel.setBackground(GUIColorsAndFonts.backGround2);

		//TODO: move the titleLabel to the center of the newTitlePanel
		JLabel titleLabel = new JLabel(title);
		titleLabel.setForeground(GUIColorsAndFonts.titleColor1);
		titleLabel.setFont(GUIColorsAndFonts.titleFont1);
		newTitlePanel.add(titleLabel);

		/** Swap the newTitlePanel with the one already in place **/

		this.remove(titlePanel);
		titlePanel = newTitlePanel;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=0; c.gridwidth=1; c.gridheight=1;c.weightx=1;c.weighty=0;c.fill=GridBagConstraints.BOTH;
		add(titlePanel, c);
		revalidate();
	}

	/**
	 * To be used once by the constructor of MenuPanel to create and add the contentPanel to it
	 */
	private void createAndAddContentPanel() {
		contentPanel = new JPanel();
		contentPanel.setLayout(new GridBagLayout());
		contentPanel.setBackground(GUIColorsAndFonts.backGround1);
		contentPanel.setOpaque(true);

		/** Adding an initial vertical box to the contentPanel to fill up the space between the titlePanel and the components
		 * that will later be added to the contentPanel **/
		Component initVerticalBox = Box.createVerticalGlue();
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=1;c.gridy=centralColumnIndex;c.gridwidth=1;c.gridheight=1;c.weightx=contentPanelVBoxWeightX;c.weighty=contentPanelVBoxWeightY;
		contentPanel.add(initVerticalBox, c);
		centralColumnIndex++;

		/** Adds the contentPanel to the MenuPanel **/
		GridBagConstraints c2 = new GridBagConstraints();
		c2.gridx=0;c2.gridy=1;c2.gridheight=1;c2.gridwidth=1;c2.weightx=1;c2.weighty=1;c2.fill=GridBagConstraints.BOTH;
		add(contentPanel,c2);
		revalidate();
	}

	/**
	 * Add a JComponent to the central column of contentPanel, with the given resize weights
	 * @param jComponent
	 * @param weightX
	 * @param weightY
	 */
	protected void addToCentralColumn(JComponent jComponent, int weightX, int weightY){
		Component vBox = Box.createVerticalGlue();
		Component hBoxLeft = Box.createHorizontalGlue();
		Component hBoxRight = Box.createHorizontalGlue();

		/** Add left Horizontal box **/
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth=1;c.gridheight=1;
		c.gridx=0;c.gridy=centralColumnIndex;c.weightx=contentPanelHBoxWeightX;c.weighty=contentPanelHBoxWeightY;
		contentPanel.add(hBoxLeft, c);

		/** Add right Horizontal box **/
		c = new GridBagConstraints();
		c.gridwidth=1;c.gridheight=1;
		c.gridx=2;c.gridy=centralColumnIndex;c.weightx=contentPanelHBoxWeightX;c.weighty=contentPanelHBoxWeightY;
		contentPanel.add(hBoxRight, c);

		/** Add the component **/
		c = new GridBagConstraints();
		c.gridwidth=1;c.gridheight=1;
		c.gridx=1;c.gridy=centralColumnIndex;c.weightx=weightX;c.weighty=weightY;c.fill=GridBagConstraints.BOTH;
		contentPanel.add(jComponent,c);

		/** Add the vertical box below the component **/
		c = new GridBagConstraints();
		c.gridwidth=1;c.gridheight=1;
		c.gridx=1;c.gridy=centralColumnIndex+1;c.weightx=contentPanelVBoxWeightX;c.weighty=contentPanelVBoxWeightY;
		contentPanel.add(vBox, c);

		//Update centralColumnIndex
		centralColumnIndex = centralColumnIndex+2;

		revalidate();
	}

}
