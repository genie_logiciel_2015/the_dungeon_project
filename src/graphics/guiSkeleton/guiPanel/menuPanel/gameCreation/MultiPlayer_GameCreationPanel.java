package graphics.guiSkeleton.guiPanel.menuPanel.gameCreation;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.MultiPlayer_GameCreationPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.MultiPlayer_GameCreationPanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_GameCreationPanel extends MenuPanel implements MultiPlayer_GameCreationPanelAbstraction {

	private static final long serialVersionUID = 1L;
	private MultiPlayer_GameCreationPanelAbstractionController controller = new MultiPlayer_GameCreationPanelAbstractionController(this);

	public MultiPlayer_GameCreationPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
		setTitlePanel("MP Game Creation");
		addToCentralColumn(new GameCreationInput_SubPanel(),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Create", createButton_ActionListener),1,1);
		addToCentralColumn(ButtonMaker.makeButton("Back",backButton_ActionListener),1,1);
	}

	private ActionListener createButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.createButtonPressed();
		}
	};

	private ActionListener backButton_ActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			controller.backButtonPressed();
		}
	};

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}

	@Override
	public void createButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOUNGE);
	}

	@Override
	public void backButtonAction() {
		getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MULTIPLAYER_MENU);
	}
}
