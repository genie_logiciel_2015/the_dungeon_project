package graphics.guiSkeleton.guiPanel.menuPanel.gameCreation;

import javax.swing.*;
import java.awt.*;

/**
 * Created by dupriez on 05/12/15.
 *
 * A panel to be included in GUIPanels, that allow the player to enter settings to create a game
 */
public class GameCreationInput_SubPanel extends JPanel {

	private JTextField gameName=new JTextField();
	private static final long serialVersionUID = 1L;
	GameCreationInput_SubPanel() {
		add(new Label("GameCreationInput_SubPanel| Room Name:"));
		this.setPreferredSize(new Dimension( 500, 54 ));
		gameName.setPreferredSize( new Dimension( 200, 24 ) );
		add(gameName);
	}
	public String getGameName()
	{
		return gameName.getText();
	}

}
