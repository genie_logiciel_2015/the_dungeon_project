package graphics.guiSkeleton.guiPanel;

import graphics.guiSkeleton.GraphicsMaster;
import graphics.ingame_input_listener.Input;

/**
 * Created by dupriez on 14/11/15.
 *
 * a blank guiPanel corresponding to the guiState "UNITIALISED" of graphicsMaster
 */
public class UnitialisedGUIPanel extends GUIPanel {

	private static final long serialVersionUID = 1L;
	public UnitialisedGUIPanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
	}

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}
}
