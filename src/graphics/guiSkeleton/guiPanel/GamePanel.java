package graphics.guiSkeleton.guiPanel;

import core.gamestate.Being;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import core.zone.Circle;
import core.zone.Point;
import core.zone.Rectangle;
import core.zone.Zone;
import gameloop.LocalGameLoop;
import gameloop.GameStarter;
import graphics.graphical_abstraction.panel.GamePanelAbstraction;
import graphics.graphical_abstraction.panel.GamePanelAbstractionController;
import graphics.graphical_utility.GraphicalBridgeConfiguration;
import graphics.graphical_utility.coreProcesses.GameContentMapChangeListener;
import graphics.guiSkeleton.*;
import graphics.guiSkeleton.entityDisplayer.HealthBarDisplayer;
import graphics.guiSkeleton.mapManagement.*;
import graphics.guiSkeleton.sprites.Animation;
import graphics.guiSkeleton.sprites.EntityAnimation;
import graphics.ingame_input_listener.Input;
import logging.Logging;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class is the panel that displays the game window (what the player sees when he's playing)
 */
public class GamePanel extends GUIPanel implements GameContentMapChangeListener, GamePanelAbstraction {

	private GamePanelAbstractionController controller = new GamePanelAbstractionController(this);

	private static final long serialVersionUID = -4309173330106121587L;
	private boolean initialised = false;
	private JLabel topLeftLabel;
	private boolean debugMapPoint = false;
	//If set to true, this class will print information in the console
	private boolean debug = false;
	//The relayer of the entity that the player plays
	private Relayer followedRelayer;
	//The place where are stored the information the game panel has to display (entities + map)
	private GameContent gameContent;
	//Defines the portion of the map this panel must display
	private VisionBox visionBox;
	//Spells of the character
	private SpellDisplayer MySpellDisplayer;
	//Avatar of the character
	private AvatarDisplayer MyAvatarDisplayer;
	//Minimap
	private MinimapDisplayer MyMinimapDisplayer;
	private Logger LOGGER = Logging.getInstance().getLogger();

	public GamePanel(GraphicsMaster graphicsMaster) {
		super(graphicsMaster);
	}

	@Override
	public void keyTypedHandler(Input e) {}

	/**
	 * Function to be called when the gui goes into the state GAME_PANEL, to prepare the Game Panel
	 */
	@Override
	public void initialise(){

		RepaintTimer.getInstance().enterPlayingFramerateMode();
		controller.initialise();
		followedRelayer = LocalGameLoop.getInstance().getFollowedRelayer();

		gameContent = LocalGameLoop.getInstance().getContent();
		//subscribe to the map changes of gameContent
		//gameContent.addMapChangeListener(this);

		//Getting the map, creating a bufferedImage out of it and storing it
		GraphicsMap graphicsMap = MapDrawer.drawMap(gameContent.getMap());
		GraphicsMapPoint initialTopLeftGraphicsMapPoint = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(followedRelayer.getCharacter().getX(), followedRelayer.getCharacter().getY());
		visionBox = new VisionBox(initialTopLeftGraphicsMapPoint, getSize().width, getSize().height, graphicsMap);

		topLeftLabel = new JLabel("Press ESC to go back to main menu");
		topLeftLabel.setFont(new Font("Inconsolata", Font.BOLD, 26));
		topLeftLabel.setForeground(new Color(255, 185, 15));
		topLeftLabel.setHorizontalAlignment(SwingConstants.LEFT);
		topLeftLabel.setOpaque(true);
		topLeftLabel.setBackground(new Color(0, 0, 0));
		this.add(topLeftLabel, BorderLayout.NORTH);

		//Draw the minimap
		MyMinimapDisplayer = new MinimapDisplayer(gameContent, followedRelayer, this);

		initialised = true;
	}

	/**
	 * Function to be called when the gui leave the state GAME_PANEL, to clean up the GamePanel
	 */
	@Override
	public void finalise() {
		initialised = false;
		RepaintTimer.getInstance().exitPlayingFrameRateMode();
		this.remove(topLeftLabel);
	}

	/**
	 * Draw a PositionedSprite on the GamePanel
	 * @param positionedSpriteToDraw
	 * @param g
	 */
	private void drawPositionedSprite (PositionedSprite positionedSpriteToDraw, Graphics g)
	{
		GamePanelMapPoint gpmp = new GamePanelMapPoint(positionedSpriteToDraw.getTopLeftGraphicsMapPoint(), visionBox.getTopLeftGraphicsMapPoint());
		if(debugMapPoint)
		{
			LOGGER.info("gpmpX="+gpmp.getX()+", gpmpY="+gpmp.getY());
		}
		g.drawImage(positionedSpriteToDraw.getSpriteImage(), gpmp.getX(), gpmp.getY(), null);
	}

	/**
	 * Draw an entity on the gamePanel
	 * @param entityToDraw
	 * @param g
	 */
	private void drawEntity(Entity entityToDraw, Graphics g)
	{
		/** Get the PositionedSprite of entityToDraw and call the method drawPositionedSprite **/
		PositionedSprite entityPositionedSprite = entityToDraw.getEntityDisplayer().getPositionedSprite(visionBox.getGraphicsMap());
		if (debugMapPoint)
		{
			LOGGER.info("PositionedSprite GraphicsMapPoint: "+entityPositionedSprite.getTopLeftGraphicsMapPoint());
		}
		drawPositionedSprite(entityPositionedSprite, g);
	}


	/**
	 * Draw a zone on the gamePanel (using g), with the specified color and thickness
	 * @param zone
	 * @param color
	 * @param stroke
	 * @param g
	 */
	private void drawZone(Zone zone, Color color, Stroke stroke, Graphics g){
		//I know that instanceOf and Casts are highly discouraged, but I don't want to put graphics code in the Zone classes
		//Moreover, this display is just for helping finding out bugs and to be able to "see" things in the game like hitbox.
		Point coreTopLeft =zone.getTopLeft();
		GraphicsMapPoint graphicsTopLeft = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(coreTopLeft.getPosX(), coreTopLeft.getPosY());
		GamePanelMapPoint gpmp = new GamePanelMapPoint(graphicsTopLeft, visionBox.getTopLeftGraphicsMapPoint());
		g.setColor(color);
		((Graphics2D) g).setStroke(stroke);
		if(zone instanceof Rectangle){
			Rectangle rectColBox = (Rectangle) zone;
			//getWidth and getHeight may have to be swapped (these are method from the core, and I'm unsure about what their return mean in swing coordinate system)
			g.drawRect(gpmp.getX(), gpmp.getY(), GraphicalBridgeConfiguration.core2Graphics(rectColBox.getWidth()), GraphicalBridgeConfiguration.core2Graphics(rectColBox.getHeight()));
		}
		if(zone instanceof Circle){
			Circle circleColBox = (Circle) zone;
			g.drawOval(gpmp.getX(), gpmp.getY(), GraphicalBridgeConfiguration.core2Graphics(2 * circleColBox.getRadius()), GraphicalBridgeConfiguration.core2Graphics(2 * circleColBox.getRadius()));
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(initialised) {
			if (debug) {
				LOGGER.info("gamePanel repaint, width=" + getSize().width + ", height=" + getSize().height);
			}

			/** Update the Animations **/
			Animation.updateAnimation();
			EntityAnimation.updateAnimation();

			synchronized (LocalGameLoop.getInstance())
			{
				/** Get the GraphicsMapPoint of the position of the entity followed by the gamePanel **/
				GraphicsMapPoint followedEntityPositionGraphicsMapPoint = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(followedRelayer.getCharacter().getX(), followedRelayer.getCharacter().getY());
				if (debugMapPoint) {
					LOGGER.info("followedEntityPositionGraphicsMapPoint:" + followedEntityPositionGraphicsMapPoint);
				}

				/** Update VisionBox parameters **/
				visionBox.moveVisionBox(followedEntityPositionGraphicsMapPoint, getSize().width, getSize().height);

				/** Displaying the map **/
				visionBox.drawMap(g);

				/** Getting and displaying the entities **/
				//Getting all the entities to process in the following loop
				java.util.List<Entity> entityList = gameContent.getEntityList();
				//The entity currently processed in the following loop
				for (Entity processedEntity : entityList) {
					if (debugMapPoint) {
						LOGGER.info("Entity : X=" + processedEntity.getX() + ", Y=" + processedEntity.getY() + " (Core's coordinates)");
					}
					drawEntity(processedEntity, g);

					//Displays the healthBar of processedEntity, if it's a Being
					//Ugly, but the required method to get all the beings of the gamestate are not implemented, 30/11/2015
					if (processedEntity instanceof Being) {
						if (processedEntity instanceof core.gamestate.Character) {
							core.gamestate.Character processedChar = (core.gamestate.Character) processedEntity;
							if (processedChar.getName() != followedRelayer.getCharacter().getName()) {
								drawPositionedSprite(HealthBarDisplayer.getPositionedSpriteOfHealthBar((Being) processedEntity), g);
							}
						} else {
							drawPositionedSprite(HealthBarDisplayer.getPositionedSpriteOfHealthBar((Being) processedEntity), g);
						}
					}

					//Display the collisionBox of the entity
					drawZone(processedEntity.getCollisionBox(), new Color(0, 0, 255), new BasicStroke(1), g);

					//Display also the avatar (image on the top left of the screen)
					//TODO: remove dirty instanceOf and cast
					if (processedEntity instanceof core.gamestate.Character) {
						core.gamestate.Character processedChar = (core.gamestate.Character) processedEntity;

						// Avatar Display :
						if (processedChar.getName() == followedRelayer.getCharacter().getName()) {
							MyAvatarDisplayer = new AvatarDisplayer(g, processedEntity);
							MyAvatarDisplayer.displayAvatar();
						}

					}
				}
				/** Spell Display **/
				MySpellDisplayer = new SpellDisplayer(g, followedEntityPositionGraphicsMapPoint, getWidth(), getHeight(), followedRelayer.getCharacter().getAbilityList());
				MySpellDisplayer.displaySpell();

				/** Minimap Display **/
				MyMinimapDisplayer.displayMap(g);
			}
		}
	}

	/** Handlers for KeyEvents **/

	@Override
	public void keyPressedHandler(Input e) {
		switch(e){
		case ESCAPE:
			GameStarter.leaveGame();
			getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);break;
		default:
			controller.keyPressed(e);
			break;
		}
	}

	@Override
	public void keyReleasedHandler(Input e) {
		controller.keyReleased(e);
	}

	/** DummyGameContentMapChangeListener implementation **/
	@Override
	public void mapChange() {
		GraphicsMap graphicsMap = MapDrawer.drawMap(LocalGameLoop.getInstance().getContent().getMap());
		GraphicsMapPoint initialTopLeftMapPoint = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(followedRelayer.getCharacter().getX(), followedRelayer.getCharacter().getY());
		visionBox = new VisionBox(initialTopLeftMapPoint, getSize().width, getSize().height, graphicsMap);
		repaint();
	}
}
