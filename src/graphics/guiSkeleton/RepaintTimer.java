package graphics.guiSkeleton;

import logging.Logging;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

/**
 * Created by dupriez on 14/11/15.
 *
 * This class is a singleton
 * Used by the gui to repaint periodically the mainFrame (hence all the gui)
 */
public class RepaintTimer {

	/** singleton implemenation **/
	private static RepaintTimer ourInstance = new RepaintTimer();

	public static RepaintTimer getInstance() {
		return ourInstance;
	}

	private Logger LOGGER = Logging.getInstance().getLogger();


	private boolean debug = false;
	private boolean printWhenTimerFiresEvent = false;
	//used when printWhenTimerFiresEvent is true
	int i = 0;

	private int PLAYING_FRAMERATE = 33; // unit: ms
	private boolean mainFrameToRepaintSet = false;
	private MainFrame mainFrameToRepaint;
	// the java Timer the class RepaintTimer uses
	private Timer swingTimer;
	//The ActionListener swingTimer will send events to
	private ActionListener repaintActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if(printWhenTimerFiresEvent) {
				LOGGER.info("RepaintTimer " + i);
				i++;
			}
			if (mainFrameToRepaintSet) {
				mainFrameToRepaint.repaint();
			}
		}
	};

	private RepaintTimer() {
	}

	public void setMainFrameToRepaint(MainFrame mainFrameToRepaint) {
		this.mainFrameToRepaint = mainFrameToRepaint;
		mainFrameToRepaintSet = true;
	}

	/**
	 * Set the delay of the RepaintTimer to PLAYING_FRAMERATE
	 */
	public void enterPlayingFramerateMode() {
		if(debug){LOGGER.info("playingFramerateMode entered");}

		swingTimer = new javax.swing.Timer(PLAYING_FRAMERATE, repaintActionListener);
		swingTimer.start();
	}

	/**
	 * Set the delay of the RepaintTimer to COOL_FRAMERATE
	 */
	public void exitPlayingFrameRateMode() {
		if(debug){LOGGER.info("playingFramerateMode exited");}

		swingTimer.stop();
	}

}
