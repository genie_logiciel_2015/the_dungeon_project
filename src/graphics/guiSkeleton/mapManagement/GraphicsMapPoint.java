package graphics.guiSkeleton.mapManagement;

import logging.Logging;

/**
 * Created by dupriez on 14/11/15.
 * Redone by Colin on 21/11/15
 *
 * This class represent a point on a displayed map, in the swing coordinate system ((0,0) top left, x to
 * the right, y to the bottom)
 * the core coordinate system is assumed to be:
 * 	- (0,0) at the top left
 * 	- x-axis to the down
 * 	- y-axis to the right
 */
public class GraphicsMapPoint {

	private int X;
	private int Y;

	/**
	 * Create a GraphicsMapPoint whose coordinates are those given in arguments
	 * @param x
	 * @param y
	 */
	public GraphicsMapPoint(int x, int y)
	{
		X = x;
		Y = y;
	}

	/**
	 * Returns a new GraphicsMapPoint whose coordinates were shifted by the given vector.
	 * @param xShift_swingCoordinate: the x-coordinate of the shift vector (IN THE SWING COORDINATE SYSTEM !)
	 * @param yShift_swingCoordinate: the y-coordinate of the shift vector (IN THE SWING COORDINATE SYSTEM !)
	 * @return
	 */
	public GraphicsMapPoint translate(int xShift_swingCoordinate, int yShift_swingCoordinate) {
		return (new GraphicsMapPoint(this.X + xShift_swingCoordinate, this.Y + yShift_swingCoordinate));
	}

	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}

	public void print() {
		Logging.getInstance().getLogger().info("X="+X+", Y="+Y);
	}
}
