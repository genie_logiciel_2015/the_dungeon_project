package graphics.guiSkeleton.mapManagement;


import graphics.guiSkeleton.sprites.Displayable;
import graphics.guiSkeleton.tileSpriteLinker.TileSprite;

import java.awt.image.BufferedImage;

/**
 * Created by Colin on 21/11/15.
 * This class represents the informations needed and sufficient to display the map :
 * - a matrix of pointers to sprites (loaded by spriteLoader and maintained by tileSpriteLinker), each one related to a quarter of tile
 *  (the coordinates are : (0,0) is top left, x is to the right, y to the down)
 * - the size of the map (in quarter of tile)
 */

public class GraphicsMap{

    private final int Height;
    private final int Width;
    private Displayable[][] Sprites;

    public GraphicsMap(int w, int h, Displayable[][] s)
    {
        Width = w;
        Height = h;
        Sprites = s;
    }

    public BufferedImage getSprite(int x, int y)
    {
        if (x < 0 || y < 0 || x >= Width || y >= Height)
        {
            throw new ArrayIndexOutOfBoundsException("Trying to get tile sprite at "+x+" , "+y+" but map has dimension "+Height+" , "+Width+"(unit = 1/4 tile)");
        }
        return Sprites[x][y].getSpriteImage();
    }

    public int getMapPixelHeight()
    {
        return Height * TileSprite.TileSpriteHeight();
    }

    public int getMapPixelWidth()
    {
        return Width * TileSprite.TileSpriteWidth();
    }


}
