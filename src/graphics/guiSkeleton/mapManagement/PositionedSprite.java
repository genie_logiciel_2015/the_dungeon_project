package graphics.guiSkeleton.mapManagement;

import graphics.guiSkeleton.sprites.Sprite;

import java.awt.image.BufferedImage;

/**
 * Created by dupriez on 14/11/15.
 *
 * this class is a sprite positioned on a map, thanks to a GraphicsMapPoint (representing the point of the map corresponding to
 * the top left point of the sprite)
 */
public class PositionedSprite extends Sprite {

	private static final long serialVersionUID = 1L;
	private GraphicsMapPoint topLeftGraphicsMapPoint;

	public PositionedSprite(BufferedImage arg_spriteImage, GraphicsMapPoint spriteTopLeftGraphicsMapPoint) {
		super(arg_spriteImage);
		this.topLeftGraphicsMapPoint = spriteTopLeftGraphicsMapPoint;
	}

	/**
	 * A static method to convert a Sprite into a positionedSprite by providing a GraphicsMapPoint
	 * @param sprite
	 * @param graphicsMapPoint
	 * @return
	 */
	static public PositionedSprite PositionSprite(Sprite sprite, GraphicsMapPoint graphicsMapPoint) {
		return new PositionedSprite(sprite.getSpriteImage(), graphicsMapPoint);
	}

	public GraphicsMapPoint getTopLeftGraphicsMapPoint() {
		return topLeftGraphicsMapPoint;
	}
}
