package graphics.guiSkeleton.mapManagement;

/**
 * Created by dupriez on 14/11/15.
 *
 * this class represent a point on the gamePanel in the swing coordinate system ((0,0) top left, x to the right, y to the bottom)
 */
public class GamePanelMapPoint {

	private int X;
	private int Y;
	private GraphicsMapPoint graphicsMapPoint;

	/**
	 * Convert a GraphicsMapPoint to a GamePanelMapPoint which represent the "same point" on the displayed map, but whose coordinates
	 * are relative to the topLeft GraphicsMapPoint of the gamePanel (the gamePanelMapPoint representing the top left point of
	 * the game panel as coordinates (0,0))
	 * @param graphicsMapPoint: The GraphicsMapPoint to convert
	 * @param gamePanelTopLeftGraphicsMapPoint: The GraphicsMapPoint corresponding to the top left point of the GamePanel
	 */
	public GamePanelMapPoint(GraphicsMapPoint graphicsMapPoint, GraphicsMapPoint gamePanelTopLeftGraphicsMapPoint)
	{
		this.graphicsMapPoint = graphicsMapPoint;
		this.X = graphicsMapPoint.getX() - gamePanelTopLeftGraphicsMapPoint.getX();
		this.Y = graphicsMapPoint.getY() - gamePanelTopLeftGraphicsMapPoint.getY();
	}

	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}
}
