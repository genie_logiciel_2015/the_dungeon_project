package graphics.guiSkeleton.mapManagement;


import java.awt.image.BufferedImage;

import graphics.graphical_utility.GraphicalBridgeConfiguration;
import graphics.guiSkeleton.sprites.Displayable;
import graphics.guiSkeleton.tileSpriteLinker.TileSprite;
import graphics.guiSkeleton.tileSpriteLinker.TileSpriteLinker;


/**
 * Created by Colin on 21/11/15.
 * The purpose of this class is to produce a GraphicsMap object representing a dungeon map out of a Map object
 * This class does not care about line of sight or whatever effect that would hide portions of the map.
 *
 * This class is abstract. It is done on purpose, because the goal of this class is not to be instantiated, but to
 * provide a method drawMap which generates a GraphicsMap out of a map.
 */

public abstract class MapDrawer {
    public static GraphicsMap drawMap(map_generation.map.Map map)
    {
        // The height and the width of the map (unit = number of tiles)
        int mapWidth = map.getWidth();
        int mapHeight = map.getHeight();
        // The height and the width of the GraphicsMap object (unit = 1/4 tiles)
        int mapDWidth = 2 * mapWidth;
        int mapDHeight = 2 * mapHeight;

        TileSpriteLinker tsl = TileSpriteLinker.getInstance();
        Displayable[][] sprites = new Displayable[mapDHeight][mapDWidth];
        for (int i = 0 ; i < mapHeight; i++)
        {
            for (int j = 0 ; j < mapWidth; j++)
            {
                // Building of a mask of neighbour tiles
                TileSprite.TileAbstractType[][] tileMask = new TileSprite.TileAbstractType[3][3];
                for (int k = -1; k < 2; k++)
                {
                    for (int l = -1; l < 2; l++)
                    {

                        if (i + k < 0 || j + l < 0 || i + k >= mapWidth || j + l >= mapHeight)
                            tileMask[k+1][l+1] = TileSprite.TileAbstractType.NONE;
                        else
                            tileMask[k+1][l+1] = TileSprite.convertTileType(map.getTileAt(j+l,i+k).getType(), map.getTileAt(j,i).getType());
                    }
                }
                // Getting the sprites via the tileSpriteLinker
                Displayable[][] linkedSprites = tsl.getSpriteOfTile(map.getTileAt(j,i).getType(),tileMask);
                for (int k = 0; k < 2; k++)
                {
                    for (int l = 0; l < 2; l++)
                    {
                        sprites[2 * i + k][2 * j + l] = linkedSprites[k][l];
                    }
                }
            }
        }
        return new GraphicsMap(mapDWidth, mapDHeight, sprites);
    }
}
