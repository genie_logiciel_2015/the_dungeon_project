package graphics.guiSkeleton.sprites;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Colin on 30/12/15.
 * Instances of this class are looping animation (looping on the frame list).
 * They can be constructed via spritesheet (where sprites appear horizontally, left to right), or via sprite list.
 */
public class Animation implements Serializable, Displayable
{
    // global counter incremented each time we display game panel
    private static int globalCounter = 0;

    // local counter owned by the instance, used to know if the instance has to be updated
    private int localCounter = globalCounter;
    // local counter used to know when we are in the displaying of a frame
    private int inFrameCounter = 0;
    private int currentFrame = 0;
    private List<Frame> frames = new ArrayList<Frame>();

    public Animation(Sprite spriteSheet, int[] delayTable)
    {
        for (int i = 0 ; i < delayTable.length ; i++)
        {
            int width = spriteSheet.getSpriteImage().getWidth();
            int height = spriteSheet.getSpriteImage().getHeight();
            frames.add(new Frame(spriteSheet.getSpriteImage().getSubimage(i*width, 0, width, height), delayTable[i]));
        }
    }

    public Animation(Sprite[] sprites, int[] delayTable)
    {
        for (int i = 0 ; i < delayTable.length ; i++)
        {
            frames.add(new Frame(sprites[i].getSpriteImage(), delayTable[i]));
        }
    }

    public Animation(Sprite spriteSheet, int nbFrames, int delay)
    {
        for (int i = 0 ; i < nbFrames ; i++)
        {
            int width = spriteSheet.getSpriteImage().getWidth()/nbFrames;
            int height = spriteSheet.getSpriteImage().getHeight();
            frames.add(new Frame(spriteSheet.getSpriteImage().getSubimage(i*width, 0, width, height), delay));;
        }
    }

    public Animation(Sprite[] sprites, int delay)
    {
        for (int i = 0 ; i < sprites.length ; i++)
        {
            frames.add(new Frame(sprites[i].getSpriteImage(), delay));
        }
    }

    private void update()
    {
        localCounter ++;
        inFrameCounter ++;
        if (inFrameCounter > frames.get(currentFrame).getDelay())
        {
            inFrameCounter = 0;
            currentFrame ++;
            if (currentFrame >= frames.size())
                currentFrame = 0;
        }
    }

    public static void updateAnimation()
    {
        globalCounter++;
    }

    public BufferedImage getSpriteImage()
    {
        while (localCounter < globalCounter)
            update();
            BufferedImage sprite = frames.get(currentFrame).getSpriteImage();
            return sprite;
    }

    public void restart()
    {
        currentFrame = 0;
        inFrameCounter = 0;
    }

    public int getCurrentSpriteHeight(){return frames.get(currentFrame).getSpriteImage().getHeight();}

    public int getCurrentSpriteWidth(){return frames.get(currentFrame).getSpriteImage().getWidth();}
}
