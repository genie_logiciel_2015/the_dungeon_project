package graphics.guiSkeleton.sprites;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Colin on 30/12/15.
 * Instances of this class are looping animation (looping on the frame list).
 * They can be constructed via spritesheet (where sprites appear horizontally, left to right), or via sprite list.
 */
public class EntityAnimation implements Serializable, Displayable
{
    // global counter incremented each time we display game panel
    private static int globalCounter = 0;

    // local counter owned by the instance, used to know if the instance has to be updated
    private int localCounter = globalCounter;
    // local counter used to know when we are in the displaying of a frame
    private int inFrameCounter = 0;
    private int currentFrame = 0;
    private List<Frame> frames = new ArrayList<Frame>();
    private List<Frame> framesRed = new ArrayList<Frame>();

    private static void colorInRed(BufferedImage sprite){
        int width = sprite.getWidth();
        int height = sprite.getHeight();

        WritableRaster raster = sprite.getRaster();

        for (int xx = 0; xx < width; xx++) {
            for (int yy = 0; yy < height; yy++) {
                int[] pixels = raster.getPixel(xx, yy, (int[]) null);
                pixels[0] = 255;
                pixels[1] = 0;
                pixels[2] = 0;
                raster.setPixel(xx, yy, pixels);
            }
        }
    }

    public EntityAnimation(BufferedImage spriteCouleur, BufferedImage spriteRed, int width, int height, int nbFrames, int delay, int line)
    {
        for (int i = 0 ; i < nbFrames ; i++)
        {
            frames.add(new Frame(spriteCouleur.getSubimage(i*width, line*height, width, height), delay));;
        }
        colorInRed(spriteRed);
        for (int i = 0 ; i < nbFrames ; i++) {
            framesRed.add(new Frame(spriteRed.getSubimage(i * width, line*height, width, height), delay));
        }
    }

    private void update()
    {
        localCounter ++;
        inFrameCounter ++;
        if (inFrameCounter > frames.get(currentFrame).getDelay())
        {
            inFrameCounter = 0;
            currentFrame ++;
            if (currentFrame >= frames.size())
                currentFrame = 0;
        }
    }

    public static void updateAnimation()
    {
        globalCounter++;
    }


    public BufferedImage getSpriteImage()
    {
        while (localCounter < globalCounter)
            update();
            BufferedImage sprite = frames.get(currentFrame).getSpriteImage();
            return sprite;
    }

    public BufferedImage getSpriteImageRed() {
        while (localCounter < globalCounter)
            update();
        BufferedImage spritered = framesRed.get(currentFrame).getSpriteImage();
        return spritered;
    }


    public void restart()
    {
        currentFrame = 0;
        inFrameCounter = 0;
    }

    public int getCurrentSpriteHeight(){return frames.get(currentFrame).getSpriteImage().getHeight();}

    public int getCurrentSpriteWidth(){return frames.get(currentFrame).getSpriteImage().getWidth();}
}
