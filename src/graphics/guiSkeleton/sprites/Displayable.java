package graphics.guiSkeleton.sprites;

import java.awt.image.BufferedImage;

/**
 * Created by Colin on 30/12/15.
 * Interface for displayable object (for now Sprite and Animation)
 * Can be used to represent an object that is one of those.
 */
public interface Displayable
{
    BufferedImage getSpriteImage();
}
