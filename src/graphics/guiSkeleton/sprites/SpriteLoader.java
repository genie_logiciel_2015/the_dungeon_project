package graphics.guiSkeleton.sprites;

import assets.UsedForLoadingSprites;
import logging.Logging;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by dupriez on 04/11/15.
 *
 * The purpose of this class is to supply instances of Sprite to the rest of the application
 * It is aimed at loading each image required only once, and only if they are asked for.
 * It does this thanks to memoisation using a hashmap
 */
public abstract class SpriteLoader {

	// A map storing (pathToImage, SpriteGeneratedFromThisPath) pairs. The first argument of HashMap is the initial capacity
	private static HashMap<String, Sprite> spriteMap = new HashMap<>(30);

	/**
	 *
	 * @param path: a string containing the path to the image that the caller want a Sprite of (you must provide a relative
	 *               path to the image, from the assets folder.
	 * @return	an instance of Sprite, whose spriteImage field contains the image loaded from the file found at path
	 */
	public static final Sprite getSpriteFromPath(String path) {
		if (!spriteMap.containsKey(path)) {
			Sprite newSprite = null;
			//The sprite asked is not in the map, so we load it and add it to the map
			try {
				URL url=UsedForLoadingSprites.getInstance().getClass().getResource(path);
				if(url==null)
					throw new IllegalStateException("Missing sprite :"+path+ "!");
				newSprite = new Sprite(ImageIO.read(url));
			} catch (IOException e) {
				Logging.getInstance().getLogger().severe(e.getMessage());
				throw new IllegalStateException("Missing sprite !");
			}
			spriteMap.put(path, newSprite);
			return newSprite;
		}
		return (spriteMap.get(path));
	}
}