package graphics.guiSkeleton.sprites;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * Created by dupriez on 03/11/15.
 * Represents a basic sprite with no animation (just an image in fact).
 */
public class Sprite implements Serializable, Displayable{

	private static final long serialVersionUID = 1L;
	private BufferedImage spriteImage;

	@Override
	public final BufferedImage getSpriteImage() {
		return spriteImage;
	}

	public Sprite(BufferedImage arg_spriteImage) {
		spriteImage = arg_spriteImage;
	}

	public Sprite(BufferedImage arg_spriteImage, int x, int y, int width, int height) {
		spriteImage = arg_spriteImage.getSubimage(x, y, width, height);
	}
}
