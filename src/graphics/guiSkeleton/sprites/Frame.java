package graphics.guiSkeleton.sprites;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * Created by Colin on 30/12/15.
 * Only used in Animation class, represents a Frame of an animation (a sprite + a duration)
 */
public class Frame implements Serializable
{
    private BufferedImage spriteImage;
    private int delay;

    public Frame(BufferedImage arg_spriteImage, int arg_delay)
    {
        spriteImage = arg_spriteImage;
        if(arg_delay > 0)
            delay = arg_delay;
        else
            delay = 0;
    }

    public BufferedImage getSpriteImage()
    {
        return spriteImage;
    }

    protected int getDelay()
    {
        return delay;
    }
}
