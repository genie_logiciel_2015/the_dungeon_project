package graphics.guiSkeleton.sprites;

import assets.UsedForLoadingSprites;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;

/**
 * Created by dupriez on 23/11/15.
 *
 * This singleton class holds all the sprites for Entities. The reason is that, due to network things, Sprite cannot be transmitted
 * over the network, but entityDisplayer currently need to be transferred over the network.
 */
public class SpriteStorage {
	private static SpriteStorage ourInstance = new SpriteStorage();
	public static SpriteStorage getInstance() {
		return ourInstance;
	}


	private SpriteStorage() {
		try {
			chatRose = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("chatrose.png")));
			ronflex = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("ronflex.png")));
			healthBar1 = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("HealthBar1.png")));
			healthBar1Container = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("HealthBar1_Container.png")));
			invisible = new Sprite(new BufferedImage(1,1,BufferedImage.TYPE_INT_ARGB));
			ponyta = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("ponyta.png")));
			diagla = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("diagla.png")));
			firebolt = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("firebolt.png")));
			frostbolt = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("frostbolt.png")));
			gold = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("gold.png")));
			chatRoseRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("chatrose.png")));
			ronflexRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("ronflex.png")));
			healthBar1RED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("HealthBar1.png")));
			healthBar1ContainerRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("HealthBar1_Container.png")));
			invisibleRED = new Sprite(new BufferedImage(1,1,BufferedImage.TYPE_INT_ARGB));
			ponytaRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("ponyta.png")));
			diaglaRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("diagla.png")));
			fireboltRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("firebolt.png")));
			frostboltRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("frostbolt.png")));
			goldRED = new Sprite(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("gold.png")));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Sprite chatRose;
	private Sprite healthBar1;
	private Sprite healthBar1Container;
	private Sprite ronflex;
	private Sprite invisible;
	private Sprite ponyta;
	private Sprite diagla;
	private Sprite firebolt;
	private Sprite frostbolt;
	private Sprite gold;
	private Sprite chatRoseRED;
	private Sprite healthBar1RED;
	private Sprite healthBar1ContainerRED;
	private Sprite ronflexRED;
	private Sprite invisibleRED;
	private Sprite ponytaRED;
	private Sprite diaglaRED;
	private Sprite fireboltRED;
	private Sprite frostboltRED;
	private Sprite goldRED;

	public Sprite getHealthBar1Container() {
		return healthBar1Container;
	}

	public Sprite getHealthBar1() {
		return healthBar1;
	}

	public Sprite getChatRose() {
		return chatRose;
	}

	public Sprite getRonflex() {
		return ronflex;
	}

	public Sprite getInvisible() {
		return invisible;
	}

	public Sprite getPonyta() {
		return ponyta;
	}

	public Sprite getGold(){
		return gold;
	}
	
	public Sprite getDiagla(){
		return diagla;
	}
	public Sprite getFirebolt(){
		return firebolt;
	}
	public Sprite getFrostbolt(){
		return frostbolt;
	}

	public Sprite getHealthBar1ContainerRED() {
		return healthBar1ContainerRED;
	}

	public Sprite getHealthBar1RED() {
		return healthBar1RED;
	}

	public Sprite getChatRoseRED() {
		return chatRoseRED;
	}

	public Sprite getRonflexRED() {
		return ronflexRED;
	}

	public Sprite getInvisibleRED() {
		return invisibleRED;
	}

	public Sprite getPonytaRED() {
		return ponytaRED;
	}

	public Sprite getGoldRED(){
		return goldRED;
	}

	public Sprite getDiaglaRED(){
		return diaglaRED;
	}
	public Sprite getFireboltRED(){
		return fireboltRED;
	}
	public Sprite getFrostboltRED(){		return frostboltRED;	}

}
