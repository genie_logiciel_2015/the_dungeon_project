package graphics.guiSkeleton;

import core.abilities.AbilityPackageInitialiser;
import core.abilities.effects.EffectPackageInitialiser;
import gameloop.LocalGameLoop;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import logging.Logging;

/**
 * Created by dupriez on 10/11/15.
 *
 * The class that contains the main method calling the "launchGUI" method from GraphicsMaster
 * It's just a starter
 */
public class Igniter_Main {

	public static void main(String[] args) {

		/** Initialise some packages to detect instantly uncorrect declarations **/
		AbilityPackageInitialiser.initialisePackage();
		EffectPackageInitialiser.initialisePackage();
		//System.out.println();

		/** launch the LocalGameLoop**/
		LocalGameLoop.getInstance().setStartedBefore();
		LocalGameLoop.getInstance().start();

		/** Launch the GUI (and thus the game) **/
		GraphicsMaster.build();
		GraphicsMasterAbstraction gm = GraphicsMasterAbstraction.getInstance();
		Logging.getInstance().getLogger().info("Starting GUI...");
		gm.launchGUI();
	}
}
