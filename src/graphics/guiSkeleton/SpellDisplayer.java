package graphics.guiSkeleton;

import core.abilities.Ability;
import graphics.guiSkeleton.mapManagement.GraphicsMapPoint;
import graphics.guiSkeleton.sprites.SpriteLoader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghocquet on 05/12/15.
 *
 * Display the pictures of the spells at the bottom of the screen.
 */
public class SpellDisplayer
{
	// Variables
	private int panelWidth;
	private int panelHeight;
	private GraphicsMapPoint entityPos;
	private Graphics g;
	private ArrayList<BufferedImage> spellIcone = new ArrayList<>();
	private List<Ability> MySpellCatalog;

	// Parameters
	private int spellSpace		= 10;
	private int spellIconeW		= 56;
	private int spellIconeH		= 56;
	private String pathSpell	= "/assets/spell/";
	private String imgTypeSpell	= ".jpg";
	private Color strongOverlay	= new Color(0,0,0, 240);
	private Color weakOverlay	= new Color(0,0,0, 180);

	/**
	 * @param graphics: Element to paint on
	 * @param followedEntityPositionGraphicsMapPoint: GraphicsMapPoint of the position of the entity followed by the gamePanel
	 * @param width: The width of the zone displayed on the screen
	 * @param height: The height of the zone displayed on the screen
	 * @param entitySpell: The SpellCatalog of the entity
	 */
	public SpellDisplayer(Graphics graphics, GraphicsMapPoint followedEntityPositionGraphicsMapPoint,
						  int width, int height, List<Ability> entitySpell)
	{
		g = graphics;
		entityPos = followedEntityPositionGraphicsMapPoint;
		panelWidth = width;
		panelHeight = height;
		MySpellCatalog = entitySpell;
		for (Ability aMySpellCatalog : MySpellCatalog)
			spellIcone.add(SpriteLoader.getSpriteFromPath(pathSpell + aMySpellCatalog.getAbilityIconKey().name() + imgTypeSpell).getSpriteImage());
	}

	public void displaySpell()
	{
		int spellNb = spellIcone.size();
		int leftMostX = (panelWidth - 1) / 2 - ((spellIconeW + spellSpace) * spellNb) / 2;
		int topMostY = (entityPos.getY() + panelHeight - 1) - entityPos.getY() - spellIconeH;
		for (int i = 0; i < spellNb; i++) {
			g.drawImage(spellIcone.get(i), leftMostX + i * (spellIconeW + spellSpace), topMostY, null);
			g.setColor(weakOverlay);
			if (!MySpellCatalog.get(i).isReady())
				g.fillRect(leftMostX + i * (spellIconeW + spellSpace), topMostY, spellIconeW, spellIconeH);
			int margin = Math.round(spellIconeH * MySpellCatalog.get(i).getCharged());
			g.setColor(strongOverlay);
			g.fillRect(leftMostX + i * (spellIconeW + spellSpace), topMostY + spellIconeW - margin, spellIconeW, margin);
		}
	}
}
