package graphics.guiSkeleton.inputManagement;

import core.relayer.Relayer;
import core.zone.Direction;

/**
 * Created by ghocquet
 * translate the input messages to be used be the core
 */
public class InputInterpreter
{
	private Relayer MyRelayer;
	private Direction direction = Direction.NONE;

	public InputInterpreter(Relayer r) {
		MyRelayer = r;
	}

	/**
	 * Apply the changes
	 */
	private void move(){
		MyRelayer.move(direction);
	}

	/**
	 * Changes the orientation on x of the Entity
	 * @param bool the new orientation on x
	 */
	public void setMoveXRight(boolean bool) {
		if(bool)
			direction=direction.add(Direction.RIGHT);
		else
			direction=direction.remove(Direction.RIGHT);
		move();
	}

	/**
	 * Changes the orientation on x of the Entity
	 * @param bool the new orientation on x
	 */
	public void setMoveXLeft(boolean bool) {
		if(bool)
			direction=direction.add(Direction.LEFT);
		else
			direction=direction.remove(Direction.LEFT);
		move();
	}

	public void setMoveYTop(boolean bool) {
		if(bool)
			direction=direction.add(Direction.UP);
		else
			direction=direction.remove(Direction.UP);
		move();
	}

	public void setMoveYBottom(boolean bool) {
		if(bool)
			direction=direction.add(Direction.DOWN);
		else
			direction=direction.remove(Direction.DOWN);
		move();
	}

	/**
	 * Called by the GamePanel KeyPressedHandler in response to one of the Ability keys of the keyboard.
	 *
	 * @param abilityNb the number of the Ability in the abilityList of the followedCharacter corresponding to the pressed key
	 */
	public void tryToCastAbility(int abilityNb) {
		MyRelayer.tryToCastAbility(abilityNb);
	}
}