package graphics.guiSkeleton.inputManagement;

import graphics.graphical_abstraction.FrameKeyListenerAbstraction;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.ingame_input_listener.InputConfiguration;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class will listen to all the keys pressed/released/typed in the mainFrame, and relays them to GraphicsMaster
 */
public class MainFrameKeyListener implements KeyListener,FrameKeyListenerAbstraction{

	private GraphicsMaster graphicsMaster;

	public MainFrameKeyListener(GraphicsMaster gm) {
		graphicsMaster = gm;
	}

	/**
	 * This method relays the keyPressed KeyEvent recorded by the mainFrame to GraphicsMaster
	 * @param e: the KeyEvent that was recorded by the mainFrame
	 */
	@Override
	public final void keyPressed(KeyEvent e) {
		graphicsMaster.keyPressedHandler(InputConfiguration.GetInput(e));}

	/**
	 * This method relays the keyReleased KeyEvent recorded by the mainFrame to GraphicsMaster
	 * @param e: the KeyEvent that was recorded by the mainFrame
	 */
	@Override
	public final void keyReleased(KeyEvent e) {graphicsMaster.keyReleasedHandler(InputConfiguration.GetInput(e));}

	/**
	 * This method relays the keyTyped KeyEvent recorded by the mainFrame to GraphicsMaster
	 * @param e: the KeyEvent that was recorded by the mainFrame
	 */

	@Override
	public final void keyTyped(KeyEvent e) {graphicsMaster.keyTypedHandler(InputConfiguration.GetInput(e));}

	@Override
	public void check() {

	}
}
