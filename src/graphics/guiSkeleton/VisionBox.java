package graphics.guiSkeleton;

import graphics.guiSkeleton.mapManagement.GraphicsMap;
import graphics.guiSkeleton.mapManagement.GraphicsMapPoint;
import graphics.guiSkeleton.tileSpriteLinker.TileSprite;
import logging.Logging;

import java.awt.*;
import java.util.logging.Logger;

/**
 * Created by dupriez on 11/11/15.
 * Redone by Colin on 28/11/15
 *
 * Define the portion of the map that is displayed on the screen (and is able to display it).
 */
public class VisionBox
{
	//If set to true, this class will print information in the console
	private boolean debug = false;
	private boolean debugMapPoint = false;
	//VisionBox size (in pixels)
	private int boxWidth;
	private int boxHeight;
	//Map size (in pixels)
	// We could have access to those values via graphicsMap but that would need computation, as those values won't change
	// and we need them often, we better stock them at the start
	private int mapWidth;
	private int mapHeight;
	//The GraphicsMapPoint representing the top left point of the visionBox
	private GraphicsMapPoint topLeftGraphicsMapPoint;
	//The GraphicsMap related to the VisionBox
	private GraphicsMap graphicsMap;

	private Logger LOGGER = Logging.getInstance().getLogger();

	/**
	 * @param topLeftGraphicsMapPoint: a GraphicsMapPoint representing the initial topLeftPoint of the VisionBox
	 * @param boxWidth: The width of the zone to display on the screen (width of the panel)
	 * @param boxHeight: The height of the zone to display on the screen (height of the panel)
	 */
	public VisionBox(GraphicsMapPoint topLeftGraphicsMapPoint, int boxWidth, int boxHeight, GraphicsMap graphicsMap)
	{
        this.mapWidth = graphicsMap.getMapPixelWidth();
		this.mapHeight = graphicsMap.getMapPixelHeight();
		this.graphicsMap = graphicsMap;
		if(debug)
		{
			LOGGER.info("map dimensions =(" + mapWidth + "," + mapHeight + ")");
		}
		this.topLeftGraphicsMapPoint = topLeftGraphicsMapPoint;
		setBoxWidth(boxWidth);
		setBoxHeight(boxHeight);
		setTopLeftGraphicsMapPoint(topLeftGraphicsMapPoint);
	}

	public void printlnVisionBox()
	{
		LOGGER.info("topLeft=(" + topLeftGraphicsMapPoint.getX() + "," + topLeftGraphicsMapPoint.getY() + "), dimensions=(" + boxWidth + "," + boxHeight + ")");
	}

	/**
	 * An intelligent setter which makes sure we don't display the outside of the map
	 * @param newTopLeftGraphicsMapPoint: a GraphicsMapPoint representing the topLeftPoint we want to have for the VisionBox
	 */
	private void setTopLeftGraphicsMapPoint(GraphicsMapPoint newTopLeftGraphicsMapPoint)
	{
		if(debugMapPoint)
		{
			LOGGER.info("setTopLeftGraphicsMapPoint called with: newTopLeftGraphicsMapPoint="+newTopLeftGraphicsMapPoint);
		}
		int x = newTopLeftGraphicsMapPoint.getX();
		int y = newTopLeftGraphicsMapPoint.getY();
		if(x <= 0)
			x = 0;
		else if(mapWidth - boxWidth <= x)
			x = mapWidth - boxWidth;
		if(y <= 0)
			y = 0;
		else if(mapHeight - boxHeight <= y)
			y = mapHeight - boxHeight;
		topLeftGraphicsMapPoint = new GraphicsMapPoint(x,y);
	}

	/**
	 * An intelligent setter which makes sure we don't display the outside of the map
	 * @param bw: a the width we want to have for the VisionBox
	 */
	private void setBoxWidth(int bw)
	{
		if(debug)
		{
			LOGGER.info("setBoxWidth called with argument " + bw);
		}
		if(bw < 0)
		{
			LOGGER.severe("setBoxWidth has been called with negative argument: "+bw);
		}
		else
		{
			if(mapWidth < bw)
			{
				LOGGER.warning("IN VisionBox: setBoxWidth has been called with an argument superior to the width of the map");
				boxWidth = mapWidth;
				setTopLeftGraphicsMapPoint(new GraphicsMapPoint(0, topLeftGraphicsMapPoint.getY()));
			}
			else
			{
				boxWidth = bw;
				if(topLeftGraphicsMapPoint.getX() + bw > mapWidth)
				{
					//The VisionBox exceeds the map on the right and is smaller than the map  (other wise we would have
					// enter the previous if)
					setTopLeftGraphicsMapPoint(new GraphicsMapPoint( mapWidth - bw, topLeftGraphicsMapPoint.getY()));
				}

			}
		}
	}

	/**
	 * An intelligent setter which makes sure we don't display the outside of the map
	 * @param bh: a the height we want to have for the VisionBox
	 */
	private void setBoxHeight(int bh)
	{
		if(debug)
		{
			LOGGER.info("setBoxHeight called with argument " + bh);
		}
		if(bh < 0)
		{
			LOGGER.severe("setBoxHeight has been called with negative argument: "+bh);
		}
		else
		{
			if(mapHeight < bh)
			{
				LOGGER.info("setBoxHeight has been called with an argument superior to the height of the map");
				boxHeight = mapHeight;
				setTopLeftGraphicsMapPoint(new GraphicsMapPoint(topLeftGraphicsMapPoint.getX(),0));
			}
			else
			{
				boxHeight = bh;
				if(topLeftGraphicsMapPoint.getY() + bh > mapHeight)
				{
					//The VisionBox exceeds the map on the right and is smaller than the map  (other wise we would have
					// enter the previous if)
					setTopLeftGraphicsMapPoint(new GraphicsMapPoint( topLeftGraphicsMapPoint.getX(), mapHeight - bh));
				}

			}
		}
	}

	/**
	 * returns the GraphicsMapPoint corresponding to the top left point of the visionBox
	 */
	public GraphicsMapPoint getTopLeftGraphicsMapPoint() {
		return topLeftGraphicsMapPoint;
	}

	public int getBoxWidth() {
		return boxWidth;
	}

	public int getBoxHeight() {
		return boxHeight;
	}

	public GraphicsMap getGraphicsMap() {
		return graphicsMap;
	}

	/**
	 * Recompute the new topLeftGraphicsMapPoint of the visionBox and call setTopLeftGraphicsMapPoint with it
	 * @param newCenterGraphicsMapPoint
	 */
	private void setCenterMapPoint(GraphicsMapPoint newCenterGraphicsMapPoint)
	{
		GraphicsMapPoint newTopLeftGraphicsMapPoint = newCenterGraphicsMapPoint.translate(-boxWidth/2, -boxHeight/2);
		setTopLeftGraphicsMapPoint(newTopLeftGraphicsMapPoint);
	}

	/**
	 * Change the visionBox's width and height, and move it so that its center is on the newCenterGraphicsMapPoint argument,
	 * @param newCenterGraphicsMapPoint
	 * @param newBoxWidth: expressed in pixels (typically the width a Swing Panel)
	 * @param newBoxHeight: expressed in pixels (typically the height a Swing Panel)
	 */
	public void moveVisionBox(GraphicsMapPoint newCenterGraphicsMapPoint, int newBoxWidth, int newBoxHeight)
	{
		if(debugMapPoint)
		{
			LOGGER.info("moveVisionBox called with: newBoxWidth="+newBoxWidth+", newBoxHeight="+newBoxHeight+", newCenter="+newCenterGraphicsMapPoint);
		}
		setBoxWidth(newBoxWidth);
		setBoxHeight(newBoxHeight);
		setCenterMapPoint(newCenterGraphicsMapPoint);
	}

	/**
	 * Draw the part of the map in the vision box on the screen
	 * @param g
	 */
	public void drawMap(Graphics g)
	{
		int tileW = TileSprite.TileSpriteWidth();
		int tileH = TileSprite.TileSpriteHeight();
		for (int i = topLeftGraphicsMapPoint.getX() / tileW; i <= (topLeftGraphicsMapPoint.getX() + boxWidth -1) / tileW ; i++)
		{
			for (int j = topLeftGraphicsMapPoint.getY() / tileH; j <= (topLeftGraphicsMapPoint.getY() + boxHeight -1) / tileH ; j++)
			{
                g.drawImage(graphicsMap.getSprite(i,j), i * tileW - topLeftGraphicsMapPoint.getX(), j * tileH - topLeftGraphicsMapPoint.getY(), null);
			}
		}
	}
}
