package graphics.guiSkeleton;

import java.awt.*;

/**
 * Created by dupriez on 20/11/15.
 *
 * The purpose of this class is to define the colours and the fonts that are used in all the gui
 */
public class GUIColorsAndFonts {

	/** Colours **/
//	public static final Color backGround1 = new Color(106,27,73); //tawnyPort
//	public static final Color backGround2 = new Color(75,51,138); //darkSlateBlue
//	public static final Color fontColor1 = new Color(217,161,14); //gamboge
//	public static final Color buttonBackground1 = new Color(138,28,98); //darkRaspberry

//	public static final Color backGround1 = new Color(0,0,0);
//	public static final Color backGround2 = new Color(224,161,11);
//	public static final Color fontColor1 = new Color(146,248,0);
//	public static final Color buttonBackground1 = new Color(138,3,162);

//	public static final Color backGround1 = new Color(45,46,41);
//	public static final Color backGround2 = new Color(253,190,25);
//	public static final Color fontColor1 = new Color(177,248,34);
//	public static final Color buttonBackground1 = new Color(208,0,233);
//	public static final Color titleColor1 = new Color(67,150,200);

	public static final Color backGround1 = new Color(45,46,41);
	public static final Color backGround2 = new Color(253,190,25);
	public static final Color fontColor1 = new Color(67,150,200);
	public static final Color buttonBackground1 = new Color(253,190,25);
	public static final Color titleColor1 = new Color(49,113,159);

//	public static final Color getBackGround1() {return backGround1;}

	/** Fonts **/
	public static final Font titleFont1 = new Font("Inconsolata", Font.BOLD, 40);
	public static final Font buttonFont1 = new Font("Purisa", Font.BOLD, 30);
	public static final Font smallFont1 = new Font("Arial", Font.PLAIN, 20);
}
