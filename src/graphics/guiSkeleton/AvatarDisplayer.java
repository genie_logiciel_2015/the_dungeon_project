package graphics.guiSkeleton;

import core.gamestate.Character;
import core.gamestate.Entity;
import graphics.guiSkeleton.sprites.SpriteLoader;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by ghocquet on 13/12/15.
 *
 * Display the avatar, life, exp and buffs of the character at the top of the screen.
 */
public class AvatarDisplayer
{
	// Variables
	private Graphics g;
	private Entity entity;
	private Character character;
	private String name;
	private ArrayList<String> buffList;

	// Parameters
	private int buffSize	= 17;
	private int pictureSize	= 80;
	private int barreSizeW	= 100;
	private int barreSizeH	= 10;
	private Color lifeColorActive	= new Color(255,0,0, 255);
	private Color lifeColorPassive	= new Color(255,0,0, 80);
	private Color goldColor			= new Color(255, 209, 61,255);
//	private Color expColorActive	= new Color(125, 46, 116, 255);
//	private Color expColorPassive	= new Color(125, 46, 116, 80);
	private String pathCharacter	= "/assets/character/";
	private String imgTypeCharacter	= ".gif";
	private String pathBuff			= "/assets/";
	private String imgTypeBuff		= ".png";


	/**
	 * @param graphics: Element to paint on
	 * @param ent: the entity to represent
	 */
	public AvatarDisplayer(Graphics graphics, Entity ent)
	{
		g = graphics;
		entity = ent;
		character = (core.gamestate.Character) entity;
		name = character.getSpeciesName();
		buffList = character.getBuffList();
	}

	public void displayAvatar()
	{
		/** Picture **/
		g.drawImage(SpriteLoader.getSpriteFromPath(pathCharacter + name + imgTypeCharacter).getSpriteImage(), 0, 0 ,pictureSize,pictureSize,null);

		/** Life **/
		g.setColor(lifeColorPassive);
		g.fillRect(pictureSize + 10,10, barreSizeW,barreSizeH);
		g.setColor(lifeColorActive);
		g.fillRect(pictureSize + 10,10,Math.round(barreSizeW * (float)character.getHP() / character.getMaxHP()),barreSizeH);

		/** Gold **/
		g.setColor(goldColor);
		g.setFont(new Font("Arial", Font.PLAIN, 20));
		g.drawString("Gold : " + character.getGold(), 100,50);

//		/** Exp **/
//		g.setColor(expColorPassive);
//		g.fillRect(pictureSize + 10,25, barreSizeW,barreSizeH);
//		g.setColor(expColorActive);
//		g.fillRect(pictureSize + 10,25,Math.round(barreSizeW * (float)character.getExp() / character.getMaxExp()),barreSizeH);

		/** Buffs **/
		for (int i = 0; i < buffList.size(); i++)
			g.drawImage(SpriteLoader.getSpriteFromPath(pathBuff + buffList.get(i) + imgTypeBuff).getSpriteImage(), 90 + (buffSize + 5) * i, 50 ,buffSize,buffSize,null);
	}
}
