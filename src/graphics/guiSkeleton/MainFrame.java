package graphics.guiSkeleton;

import assets.UsedForLoadingSprites;
import graphics.graphical_abstraction.FrameAbstraction;
import graphics.graphical_abstraction.FrameKeyListenerAbstraction;
import graphics.graphical_abstraction.panel.PanelAbstraction;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.io.IOException;

/**
 * Created by dupriez on 10/11/15.
 */
public class MainFrame extends JFrame implements FrameAbstraction {

	private static final long serialVersionUID = 1L;
	public MainFrame() {
		//Annoying for network tests
		setInFullScreen();
//		setSize(new Dimension(500,500));

		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		RepaintTimer.getInstance().setMainFrameToRepaint(this);
		try {
			setIconImage(ImageIO.read(UsedForLoadingSprites.getInstance().getClass().getResource("OrbsIcon.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set this Frame into fullscreen
	 */
	public void setInFullScreen() {
		// Getting the size of the user's computer screen and setting winWidth and winHeight accordingly
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Rectangle fullScreenDimension = ge.getMaximumWindowBounds();
		setSize((int)fullScreenDimension.getWidth(), (int)fullScreenDimension.getHeight());
	}

	@Override
	public void init(FrameKeyListenerAbstraction keyListenerAbstraction) {
		boolean fullscreen = true;
		this.setFocusable(true);	//For the keyListener to work
		this.addKeyListener((KeyListener)keyListenerAbstraction);
		this.revalidate();
		if (fullscreen) {
			this.setExtendedState(JFrame.MAXIMIZED_BOTH);
			this.setUndecorated(true);
		}
		this.setVisible(true);
	}

	@Override
	public void remove(PanelAbstraction panel) {
		super.remove((JPanel)panel);
	}

	@Override
	public void add(PanelAbstraction panel){
		super.add((JPanel) panel);
		this.revalidate();
		this.repaint();
	}
}
