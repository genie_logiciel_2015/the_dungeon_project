package graphics.guiSkeleton.entityDisplayer;

import core.gamestate.Action;
import core.gamestate.Entity;

import core.zone.Direction;
import graphics.graphical_utility.GraphicalBridgeConfiguration;
import graphics.guiSkeleton.mapManagement.GraphicsMapPoint;
import graphics.guiSkeleton.mapManagement.PositionedSprite;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import graphics.guiSkeleton.mapManagement.GraphicsMap;
import graphics.guiSkeleton.sprites.EntityAnimation;
import graphics.guiSkeleton.sprites.EntityAnimation;
import graphics.guiSkeleton.sprites.Sprite;


/**
 * Created by dupriez on 14/11/15.
 *
 * Each entity should have one instance of this class
 * It is used by the gui to know how to display the said entity
 */
public class EntityDisplayer implements Serializable {

	private static final long serialVersionUID = 1L;
	//The Entity this Displayer is associated to
	private Entity associatedEntity;
	private Map<Action, EntityAnimation>[] EntityAnimationMapTab= new Map[Direction.values().length] ;
	private char charForTerminalSprite;
	private boolean HIT = false;
	private int delay_HIT = 0;
	private int delay_HIT_max = 5;

	public EntityDisplayer(Entity associatedEntity, Map<Action, EntityAnimation>[] EntityAnimationMapTab, char charForTerminalSprite) {
		this.associatedEntity = associatedEntity;
		this.charForTerminalSprite = charForTerminalSprite;
		this.EntityAnimationMapTab = EntityAnimationMapTab;
		}

	/**
	 * The purpose of this function is to be called by the gui when it displays the entity related to this displayer
	 * @param graphicsMap: the GraphicsMap of the map the associatedEntity is on
	 * @return The PositionedSprite that the gui should display to represent this entity
	 */

	public PositionedSprite getPositionedSprite(GraphicsMap graphicsMap) {
		//Get the good image
		int dir = associatedEntity.getDirection().ordinal();
		EntityAnimation entityAnimation;
		if (associatedEntity.hasAction(Action.ATTACK)) {
			entityAnimation = EntityAnimationMapTab[dir].get(Action.ATTACK);
			associatedEntity.removeAction(Action.ATTACK);
		}
		else if (associatedEntity.hasAction(Action.WALK)) {
			entityAnimation = EntityAnimationMapTab[dir].get(Action.WALK);
			associatedEntity.removeAction(Action.WALK);
		}
		else {
			entityAnimation = EntityAnimationMapTab[dir].get(Action.NONE);
		}
		Sprite sprite;
		if (associatedEntity.hasAction(Action.HIT)) {
			this.HIT = true;
			associatedEntity.removeAction(Action.HIT);
		}
		if (this.HIT){
			this.delay_HIT++;
			if (this.delay_HIT > this.delay_HIT_max) {this.delay_HIT = 0; this.HIT = false;}
			sprite = new Sprite(entityAnimation.getSpriteImageRed());
		}
		else sprite = new Sprite(entityAnimation.getSpriteImage());
		GraphicsMapPoint entityPositionGraphicsMapPoint = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(associatedEntity.getX(), associatedEntity.getY());
		GraphicsMapPoint spriteTopLeftGraphicsMapPoint = entityPositionGraphicsMapPoint.translate(-entityAnimation.getCurrentSpriteWidth() / 2, -entityAnimation.getCurrentSpriteHeight() / 2);
		return PositionedSprite.PositionSprite(sprite, spriteTopLeftGraphicsMapPoint);

	}
	public char getTermSprite() {
		return charForTerminalSprite;
	}

	public Entity getAssociatedEntity() {
		return associatedEntity;
	}
}
