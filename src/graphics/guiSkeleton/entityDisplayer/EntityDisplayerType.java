package graphics.guiSkeleton.entityDisplayer;

/**
 * Created by dupriez on 25/11/15.
 *
 * The types of entityDisplayer that are available for entities
 */
public enum EntityDisplayerType {

	INVISIBLE,
	RONFLEX,
	CHATROSE,
	PONYTA,
	DIAGLA,
	FIREBOLT,
	FROSTBOLT,
	GOLD
}
