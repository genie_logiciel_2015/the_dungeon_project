package graphics.guiSkeleton.entityDisplayer;

import core.gamestate.Action;
import core.zone.Direction;

/**
 * Created by gwendoline on 05/01/16.
 *
 * Contains an Orientation and an Action.
 * To be mapped to instances of Animation
 */
public class DirectionAndAction {

    private Direction direction;
    private Action action;

    public DirectionAndAction(Direction direction, Action action) {
        this.direction = direction;
        this.action = action;
    }

    public Direction getDirection() {
        return direction;
    }

    public Action getAction() {
        return action;
    }
}
