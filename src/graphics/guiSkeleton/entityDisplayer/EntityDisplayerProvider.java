package graphics.guiSkeleton.entityDisplayer;

import core.gamestate.Action;
import core.zone.Direction;
import core.gamestate.Entity;
import graphics.guiSkeleton.sprites.EntityAnimation;
import graphics.guiSkeleton.sprites.Sprite;
import graphics.guiSkeleton.sprites.SpriteStorage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dupriez on 25/11/15.
 *
 * The purpose of this class is to define the method createEntityDisplayer.
 */
public class EntityDisplayerProvider {
 //temps par frame d'une EntityAnimation
	/**
	 *
	 * @param entityDisplayerType: the type of entityDisplayer to create
	 * @param associatedEntity: the entity to which the entityDisplayer created must be bound
	 * @return an entityDisplayer of the type given in argument, and bound to the entity given in argument
	 */


    public static Map<Action, EntityAnimation>[] constructeurHashMap(BufferedImage perso, BufferedImage persoRed){

        Map<Action, EntityAnimation>[] entityAnimationMapTab= new Map[Direction.values().length] ;

        for (Direction dir : Direction.values()) {
            entityAnimationMapTab[dir.ordinal()] = new HashMap<Action, EntityAnimation>();
        }

        for (Action action : Action.values()) {

            int nb_frames=2;
            int frameDelay = 5;
            if (action == Action.NONE) nb_frames=1;
            EntityAnimation persoRIGHT = new EntityAnimation(perso, persoRed, 32, 32, nb_frames, frameDelay, 0);
            entityAnimationMapTab[Direction.RIGHT.ordinal()].put(action, persoRIGHT);
            entityAnimationMapTab[Direction.RIGHTUP.ordinal()].put(action, persoRIGHT);
            EntityAnimation persoUP = new EntityAnimation(perso, persoRed, 32, 32, nb_frames, frameDelay, 1);
            entityAnimationMapTab[Direction.UP.ordinal()].put(action, persoUP);
            entityAnimationMapTab[Direction.LEFTUP.ordinal()].put(action, persoUP);
            EntityAnimation persoLEFT = new EntityAnimation(perso, persoRed, 32, 32, nb_frames, frameDelay, 2);
            entityAnimationMapTab[Direction.LEFT.ordinal()].put(action, persoLEFT);
            entityAnimationMapTab[Direction.LEFTDOWN.ordinal()].put(action, persoLEFT);
            EntityAnimation persoDOWN = new EntityAnimation(perso, persoRed, 32, 32, nb_frames, frameDelay, 3);
            entityAnimationMapTab[Direction.DOWN.ordinal()].put(action, persoDOWN);
            entityAnimationMapTab[Direction.RIGHTDOWN.ordinal()].put(action, persoDOWN);
        }
        return entityAnimationMapTab;
    }

	public static final EntityDisplayer createEntityDisplayer(EntityDisplayerType entityDisplayerType, Entity associatedEntity) {
		switch (entityDisplayerType){
			case INVISIBLE: {
                BufferedImage perso = SpriteStorage.getInstance().getInvisible().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getInvisibleRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), ' ');
			}
			case CHATROSE: {
                BufferedImage perso = SpriteStorage.getInstance().getChatRose().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getChatRoseRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'C');
			}
			case RONFLEX: {
                BufferedImage perso = SpriteStorage.getInstance().getRonflex().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getRonflexRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'R');
            }
			case PONYTA: {
                BufferedImage perso = SpriteStorage.getInstance().getPonyta().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getPonytaRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'P');
            }
			case DIAGLA: {
                BufferedImage perso = SpriteStorage.getInstance().getDiagla().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getDiaglaRED().getSpriteImage();

                Map<Action, EntityAnimation>[] entityAnimationMapTab= new Map[Direction.values().length] ;

                for (Direction dir : Direction.values()) {
                    entityAnimationMapTab[dir.ordinal()] = new HashMap<Action, EntityAnimation>();
                }

                for (Action action : Action.values()) {

                    int nb_frames=2;
                    int frameDelay = 5;
                    if (action == Action.NONE) nb_frames=1;
                    EntityAnimation persoRIGHT = new EntityAnimation(perso, persoRED, 64, 64, nb_frames, frameDelay, 0);
                    entityAnimationMapTab[Direction.RIGHT.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.RIGHTUP.ordinal()].put(action, persoRIGHT);
                    EntityAnimation persoUP = new EntityAnimation(perso, persoRED, 64, 64, nb_frames, frameDelay, 1);
                    entityAnimationMapTab[Direction.UP.ordinal()].put(action, persoUP);
                    entityAnimationMapTab[Direction.LEFTUP.ordinal()].put(action, persoUP);
                    EntityAnimation persoLEFT = new EntityAnimation(perso, persoRED, 64, 64, nb_frames, frameDelay, 2);
                    entityAnimationMapTab[Direction.LEFT.ordinal()].put(action, persoLEFT);
                    entityAnimationMapTab[Direction.LEFTDOWN.ordinal()].put(action, persoLEFT);
                    EntityAnimation persoDOWN = new EntityAnimation(perso, persoRED, 64, 64, nb_frames, frameDelay, 3);
                    entityAnimationMapTab[Direction.DOWN.ordinal()].put(action, persoDOWN);
                    entityAnimationMapTab[Direction.RIGHTDOWN.ordinal()].put(action, persoDOWN);
                }
                return new EntityDisplayer(associatedEntity, entityAnimationMapTab, 'D');
            }
			case FIREBOLT: {
                BufferedImage perso = SpriteStorage.getInstance().getFirebolt().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getFireboltRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'F');
            }
			case FROSTBOLT: {
                BufferedImage perso = SpriteStorage.getInstance().getFrostbolt().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getFrostboltRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'O');
            }
			case GOLD : {
                BufferedImage perso = SpriteStorage.getInstance().getGold().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getGoldRED().getSpriteImage();
                Map<Action, EntityAnimation>[] entityAnimationMapTab= new Map[Direction.values().length] ;

                for (Direction dir : Direction.values()) {
                    entityAnimationMapTab[dir.ordinal()] = new HashMap<Action, EntityAnimation>();
                }

                for (Action action : Action.values()) {

                    int nb_frames=1;
                    int frameDelay = 1;
                    if (action == Action.NONE) nb_frames=1;
                    EntityAnimation persoRIGHT = new EntityAnimation(perso, persoRED, 32, 23, nb_frames, frameDelay, 0);
                    entityAnimationMapTab[Direction.RIGHT.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.RIGHTUP.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.UP.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.LEFTUP.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.LEFT.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.LEFTDOWN.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.DOWN.ordinal()].put(action, persoRIGHT);
                    entityAnimationMapTab[Direction.RIGHTDOWN.ordinal()].put(action, persoRIGHT);
                }
                return new EntityDisplayer(associatedEntity, entityAnimationMapTab, 'G');
			}
			default:  {
                BufferedImage perso = SpriteStorage.getInstance().getRonflex().getSpriteImage();
                BufferedImage persoRED = SpriteStorage.getInstance().getRonflexRED().getSpriteImage();
                return new EntityDisplayer(associatedEntity, constructeurHashMap(perso, persoRED), 'R');
            }
		}
	}
}
