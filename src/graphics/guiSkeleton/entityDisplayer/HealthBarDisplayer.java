package graphics.guiSkeleton.entityDisplayer;

import core.gamestate.Being;
import graphics.graphical_utility.GraphicalBridgeConfiguration;
import graphics.guiSkeleton.mapManagement.GraphicsMap;
import graphics.guiSkeleton.mapManagement.GraphicsMapPoint;
import graphics.guiSkeleton.mapManagement.PositionedSprite;
import graphics.guiSkeleton.sprites.SpriteStorage;
import graphics.guiSkeleton.sprites.Sprite;

import java.awt.image.BufferedImage;

/**
 * Created by dupriez on 30/11/15.
 *
 * Called by the gamePanel to display the healthBar of Beings
 */
public class HealthBarDisplayer {

	//TODO: The next 2 variables should not be hardcoded
	private static int assumedSpriteHeight = 32;
	private static int assumedSpriteWidth = 32;
	//In the swing coordinate system, the offset applied to the topLeft point of the displayed healthBarContainer (this topLeft point
	//is initially the bottom left point of the box surrounding the entity
	private static int yOffsetForHealthBarContainer = 0;
	//In the swing coordinate system, the offset applied to the topLeft point of the displayed healthBar (this topLeft point
	//is initially the top left point of the healthBarContainer
	private static int xOffsetForHealthBar = 2;
	private static int yOffsetForHealthBar = 2;
	//These 2 variables are are multiplied to the dimensions of the images of healthBar and healthBarContainer to reduce them
	private static float xShrinkRatio = ((float) 2)/3;
	private static float yShrinkRatio = ((float) 2)/3;

	public static PositionedSprite getPositionedSpriteOfHealthBar(Being being) {
		int hp = being.getHP();
		int maxHP = being.getMaxHP();
		float healthRatio = ((float)hp)/maxHP;
		GraphicsMapPoint beingPosition = GraphicalBridgeConfiguration.makeMapPointFromCoreCoordinates(being.getX(), being.getY());
		//applying translation
		GraphicsMapPoint healthBarContainerTopLeft = beingPosition.translate(- assumedSpriteWidth/2, assumedSpriteHeight + yOffsetForHealthBarContainer);

		//Get the raw image of healthBarContainer
		BufferedImage rawHealthBarContainer = SpriteStorage.getInstance().getHealthBar1Container().getSpriteImage();
		//Compute the final dimensions of the healthBarContainer image
		int healthBarContainerImageWidth = (int) (rawHealthBarContainer.getWidth()*xShrinkRatio);
		int healthBarContainerImageHeight = (int) (rawHealthBarContainer.getHeight()*yShrinkRatio);
		//Create a blank BufferedImage with these dimensions
		BufferedImage healthBarContainer = new BufferedImage(healthBarContainerImageWidth, healthBarContainerImageHeight, BufferedImage.TYPE_INT_RGB);
		//Draw the rawHealthBarContainer image on this blank image, with the right dimensions
		healthBarContainer.getGraphics().drawImage(rawHealthBarContainer, 0, 0, healthBarContainerImageWidth, healthBarContainerImageHeight, null);
		//Get the raw image of healthBarContainer
		BufferedImage rawHealthBar = SpriteStorage.getInstance().getHealthBar1().getSpriteImage();
		//Compute the final dimensions of the healthBar image
		int healthBarImageWidth = (int) (rawHealthBar.getWidth()*xShrinkRatio*healthRatio);
		int healthBarImageHeight = (int) (rawHealthBar.getHeight()*yShrinkRatio);
		//Draw the healthBar image, with the computed dimensions and with a width multiplied by healthRatio, on the blank image of healthBarContainer
		healthBarContainer.getGraphics().drawImage(rawHealthBar, xOffsetForHealthBar, yOffsetForHealthBar, healthBarImageWidth, healthBarImageHeight, null);

		return PositionedSprite.PositionSprite(new Sprite(healthBarContainer), healthBarContainerTopLeft);
	}

}
