package graphics.guiSkeleton.tileSpriteLinker;

import graphics.guiSkeleton.sprites.Displayable;
import graphics.guiSkeleton.sprites.SpriteLoader;
import map_generation.tiles.TileType;

import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * Created by Colin on 22/11/15.
 *
 * This class is a singleton, linking a TileType to a TileSprite.
 */
public class TileSpriteLinker
{
    private static HashMap<TileType, TileSprite> spriteMap = new HashMap<>(30);
    private static TileSpriteLinker ourInstance = new TileSpriteLinker();

    public static TileSpriteLinker getInstance() {
        return ourInstance;
    }

    /**
     * You should add new tile's sprite here.
     */
    private TileSpriteLinker()
    {
        spriteMap.put(TileType.GROUND, new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/Ground.png").getSpriteImage()));
        spriteMap.put(TileType.WALL, new WallTileSprite(SpriteLoader.getSpriteFromPath("/assets/Wall_test.png").getSpriteImage()));
        spriteMap.put(TileType.EMPTY, new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/Top_wall.png").getSpriteImage()));
        spriteMap.put(TileType.STAIRS , new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/dungeon_set_1/stairs/stairs2.bmp").getSpriteImage()));
        spriteMap.put(TileType.WATER , new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/Lava.png").getSpriteImage()));
        spriteMap.put(TileType.TORCH , new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/Torch.png").getSpriteImage()));
        spriteMap.put(TileType.CHEST , new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/chest.png").getSpriteImage()));
        spriteMap.put(TileType.OBSIDIAN , new GroundTileSprite(SpriteLoader.getSpriteFromPath("/assets/obsidian.png").getSpriteImage()));
    }

    public Displayable[][] getSpriteOfTile(TileType type, TileSprite.TileAbstractType[][] mask)
    {
        TileSprite tileSprite = spriteMap.get(type);
        return tileSprite.getSprites(mask);
    }
}
