package graphics.guiSkeleton.tileSpriteLinker;

import graphics.guiSkeleton.sprites.Animation;
import graphics.guiSkeleton.sprites.Displayable;
import graphics.guiSkeleton.sprites.Sprite;
import graphics.guiSkeleton.sprites.SpriteLoader;
import map_generation.tiles.TileType;
import java.awt.image.BufferedImage;
import java.util.Random;

import core.zone.Point;

/**
 * Created by Colin on 22/11/15.
 *
 * The goal of this class (and its descendants) is to provide the sprites of the 4 quarters of tiles to display from a tile and the mask
 * of its neighbour tiles. This is done via cutting a sprite sheet into a table of sprites.
 *
 * This class is abstract because its provides only the bases cases (if the sprite sheet doesn't need reassembling)
 */
public abstract class TileSprite {
    protected Displayable[][] spriteTable;

    // true if the sprite sheet doesn't need reassembling
    protected boolean base;

    // the number of variation for each quartet of tile on the sprite sheet
    // the variations' spritesheets must be concatenated horizontally in the image loaded
    protected int variations;

    // random generator used to choose which variation to choose
    static Random randomGenerator = new Random();

    // frequency of the animated tileSprite (in frames)
    protected static final int animationDelay = 10;

    // enumeration of the abstract TileTypes used in the mask given in argument to getSprites method
    // they are linked to different behaviours for the different subclasses
    public static enum TileAbstractType {SELF, NONE, PLAIN, GROUND, WALL}

    // static integers defining the size of a quarter of tile in pixels
    //TODO: there are no reason that these should be equal to the size of a tile's edge in the core coordinate system
    public static final int TileSpriteHeight() {return Point.TileScale/2;}
    public static final int TileSpriteWidth() {return Point.TileScale/2;}
    //public static final int TileSpriteHeight() {return 8;}
    //public static final int TileSpriteWidth() {return 8;}

    private static Sprite ErrorSprite = SpriteLoader.getSpriteFromPath("/assets/Test_Error_Tiles/black_64_64.bmp");

    /**
    * Initialisation method for the base case (no reassembling)
    */
    protected void BaseInitialisation(BufferedImage sprite)
    {
        base = true;
        variations = sprite.getWidth() / (2*TileSpriteWidth());
        if (sprite.getHeight() > 2*TileSpriteHeight())
        {
            spriteTable = new Animation[variations][4];
            int nbFrames = sprite.getHeight() / (2*TileSpriteHeight());
            for (int k = 0; k < variations; k++)
            {
                for (int i = 0; i < 4; i++)
                {
                    Sprite[] animationSpriteTable = new Sprite[nbFrames];
                    for (int j = 0 ; j < nbFrames ; j++)
                    {
                        animationSpriteTable[j] = new Sprite(sprite.getSubimage((2 * k + (i / 2)) * TileSpriteWidth(), (2*j+(i % 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                    }
                    spriteTable[k][i] = new Animation(animationSpriteTable,animationDelay);
                }
            }
        }
        else
        {
            spriteTable = new Sprite[variations][4];
            for (int k = 0; k < variations; k++)
            {
                for (int i = 0; i < 4; i++)
                {
                    spriteTable[k][i] = new Sprite(sprite.getSubimage((2 * k + (i / 2)) * TileSpriteWidth(), (i % 2) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                }
            }
        }
    }

    /**
     * Initialisation method used if the sprite doesn't represent a sprite (its size is not in multiple of tile)
     */
    protected void ErrorTileSpriteInitialisation()
    {
        BaseInitialisation(ErrorSprite.getSpriteImage());
    }

    public abstract Displayable[][] getSprites(TileAbstractType[][] mask);

    public abstract char[][] getChar(TileAbstractType[][] mask);

    /**
     * getSprites method for the base case (no reassembling)
     */
    protected Displayable[][] BaseGetSprites(@SuppressWarnings("unused") TileAbstractType[][] mask)
    {
        int k = randomGenerator.nextInt(variations);
        Displayable[][] result = new Displayable[2][2];
        for (int i = 0 ; i < 2 ; i++)
        {
            for (int j = 0 ; j < 2 ; j++)
            {
                result[i][j] = spriteTable[k][2*i+j];
            }
        }
        return result;
    }

    /**
     * Check if the sprite size is multiple of tile size
     */
    protected static boolean checkSprite(BufferedImage sprite)
    {
        if (sprite.getWidth() % TileSpriteWidth() != 0 || sprite.getHeight() % TileSpriteHeight() != 0)
            return false;
        return true;
    }

    /**
     * Convert a TileType into an TileAbstractType (used to construct the mask)
     */
    public static TileAbstractType convertTileType(TileType initial, TileType reference)
    {
    	if (initial == reference)
    		return TileAbstractType.SELF;
    	switch (initial)
    	{
    	case GROUND:
    		return TileAbstractType.GROUND;
    	case WALL:
    		return TileAbstractType.WALL;
    	case STAIRS:
    		return TileAbstractType.GROUND;
    	case EMPTY:
    		return TileAbstractType.PLAIN;
    		//WATER is missing
    	default:
    		return TileAbstractType.GROUND;


    	}
    }
}