package graphics.guiSkeleton.tileSpriteLinker;

import graphics.guiSkeleton.sprites.Animation;
import graphics.guiSkeleton.sprites.Displayable;
import graphics.guiSkeleton.sprites.Sprite;

import java.awt.image.BufferedImage;


/**
 * Created by Colin on 22/11/15.
 *
 * The TileSprite class used for everything else than walls
 */

public class GroundTileSprite extends TileSprite
{
    public GroundTileSprite(BufferedImage sprite)
    {
        if (!checkSprite(sprite))
            ErrorTileSpriteInitialisation();
        else if (sprite.getRGB(0,0) == 0xffff00ff)
        {
            if (sprite.getWidth() % (TileSpriteWidth()*4) != 0 || sprite.getHeight() % (TileSpriteHeight()*6) != 0)
                ErrorTileSpriteInitialisation();
            else
            {
                base = false;
                variations = sprite.getWidth() / (4*TileSpriteWidth());
                if (sprite.getHeight() > 6*TileSpriteHeight())
                {
                    spriteTable = new Animation[variations][20];
                    int nbFrames = sprite.getHeight() / (6*TileSpriteHeight());
                    for (int k = 0; k < variations; k++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            Sprite[][] animationSpriteTable = new Sprite[5][nbFrames];
                            for (int j = 0 ; j < nbFrames ; j++)
                            {
                                animationSpriteTable[0][j] = new Sprite(sprite.getSubimage((4 * k + 3 * (i % 2)) * TileSpriteWidth(), (6*j+2 + 3 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                                animationSpriteTable[1][j] = new Sprite(sprite.getSubimage((4 * k + 1 + 2 * (i % 2) - (i / 2)) * TileSpriteWidth(), (6*j+2 + (i % 2) + 2 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                                animationSpriteTable[2][j] =new Sprite(sprite.getSubimage((4 * k + 2 * (i % 2) + (i / 2)) * TileSpriteWidth(), (6*j+3 - (i % 2) + 2 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                                animationSpriteTable[3][j] = new Sprite(sprite.getSubimage((4 * k + 2 + i % 2) * TileSpriteWidth(), (6*j+i / 2) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                                animationSpriteTable[4][j] = new Sprite(sprite.getSubimage((4 * k + 1 + (i+1) % 2) * TileSpriteWidth(), (6*j+ 4 - i / 2) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                            }
                            spriteTable[k][i] = new Animation(animationSpriteTable[0],animationDelay);
                            spriteTable[k][4+i] = new Animation(animationSpriteTable[1],animationDelay);
                            spriteTable[k][8+i] = new Animation(animationSpriteTable[2],animationDelay);
                            spriteTable[k][12+i] = new Animation(animationSpriteTable[3],animationDelay);
                            spriteTable[k][16+i] = new Animation(animationSpriteTable[4],animationDelay);
                        }
                    }
                }
                else
                {
                    spriteTable = new Sprite[variations][20];
                    for (int k = 0; k < variations; k++) {
                        for (int i = 0; i < 4; i++) {
                            // i%2 -> i     ;   i/2 -> j
                            spriteTable[k][i] = new Sprite(sprite.getSubimage((4 * k + 3 * (i % 2)) * TileSpriteWidth(), (2 + 3 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                            spriteTable[k][4 + i] = new Sprite(sprite.getSubimage((4 * k + 1 + 2 * (i % 2) - (i / 2)) * TileSpriteWidth(), (2 + (i % 2) + 2 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                            spriteTable[k][8 + i] = new Sprite(sprite.getSubimage((4 * k + 2 * (i % 2) + (i / 2)) * TileSpriteWidth(), (3 - (i % 2) + 2 * (i / 2)) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                            spriteTable[k][12 + i] = new Sprite(sprite.getSubimage((4 * k + 2 + i % 2) * TileSpriteWidth(), (i / 2) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                            spriteTable[k][16 + i] = new Sprite(sprite.getSubimage((4 * k + 1 + (i+1) % 2) * TileSpriteWidth(), (4 - i / 2) * TileSpriteHeight(), TileSpriteWidth(), TileSpriteHeight()));
                        }
                    }
                }
            }
        }
        else
            BaseInitialisation(sprite);
    }

    @Override
    public Displayable[][] getSprites(TileAbstractType[][] mask)
    {
        if (base)
            return BaseGetSprites(mask);
		int k = randomGenerator.nextInt(variations);
		Displayable[][] result = new Displayable[2][2];
		for (int i = 0 ; i < 2 ; i++)
		{
		    for (int j = 0 ; j < 2 ; j++)
		    {
		        int temp = 0;
		        if (mask[j+i][1+j-i] == TileAbstractType.SELF || mask[j+i][1+j-i] == TileAbstractType.NONE)
		            temp += 1;
		        if (mask[1+i-j][j+i] == TileAbstractType.SELF || mask[1+i-j][j+i] == TileAbstractType.NONE)
		            temp += 2;
		        if (temp == 3 && (mask[2*i][2*j] == TileAbstractType.SELF || mask[2*i][2*j] == TileAbstractType.NONE))
		            temp = 4;
		        result[i][j] = spriteTable[k][4*temp+i+2*j];
		    }
		}
		return result;
    }

    @Override
    public char[][] getChar(TileAbstractType[][] mask) {
        char result[][] =  new char[2][2];
        for (int i = 0 ; i < 2 ; i++)
        {
            for (int j = 0 ; j < 2 ; j++)
            {
                result[i][j] = 'X';
            }
        }
        return result;
    }
}