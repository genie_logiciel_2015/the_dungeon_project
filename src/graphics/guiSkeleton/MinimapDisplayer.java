package graphics.guiSkeleton;

import core.gamestate.Character;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import graphics.guiSkeleton.guiPanel.GamePanel;
import graphics.guiSkeleton.mapManagement.GraphicsMap;
import graphics.guiSkeleton.mapManagement.MapDrawer;
import graphics.guiSkeleton.tileSpriteLinker.TileSprite;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;

/**
 * Created by ghocquet on 02/01/16.
 *
 * Display the minimap
 */
public class MinimapDisplayer
{
	// Variables
	private Graphics2D g_map;
	private Relayer relayer;
	private GamePanel gamePanel;
	private GameContent gameContent;
	private BufferedImage map;
	private int mapWidth;
	private int mapHeight;

	// Parameters
	private int tileW = TileSprite.TileSpriteWidth();
	private int tileH = TileSprite.TileSpriteHeight();
	private Color playerColor = new Color(255,0,0);
	private Color stairsColor = new Color(255,255,0);
	private Color friendColor = new Color(0,255,0);

	/**
	 * @param gameContent: used to get the map generated
	 * @param followedRelayer: used to get the position of the player
	 * @param gamePanel: used to get the width of the displayed screen
	 */
	public MinimapDisplayer(GameContent gameContent, Relayer followedRelayer, GamePanel gamePanel)
	{
		this.gameContent = gameContent;
		this.relayer = followedRelayer;
		this.gamePanel = gamePanel;

		// Compute the Map
		GraphicsMap graphicsMap = MapDrawer.drawMap(gameContent.getMap());
		mapWidth = graphicsMap.getMapPixelWidth();
		mapHeight = graphicsMap.getMapPixelWidth();
		map = new BufferedImage((mapWidth-1) / tileW + 1, (mapHeight-1) / tileH + 1, BufferedImage.TYPE_INT_ARGB);
		g_map = map.createGraphics();
		for (int i = 0; i <= (mapWidth-1) / tileW ; i++)
		{
			for (int j = 0; j <= (mapHeight-1) / tileH ; j++)
			{
				g_map.drawImage(graphicsMap.getSprite(i,j), i, j, 1, 1, null);
			}
		}

		// Draw the Stairs
		core.zone.Point out = gameContent.getMap().getPositionStairs().getPoint();
		g_map.setColor(stairsColor);
		g_map.drawRect(out.getPosY()/tileW-1,out.getPosX()/tileH-1,3,3);
	}

	public void displayMap(Graphics g) {
		// Copy the map
		ColorModel cm = map.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = map.copyData(null);
		BufferedImage newMap = new BufferedImage(cm, raster, isAlphaPremultiplied, null);

		// Update the map
		Graphics2D g_newMap = newMap.createGraphics();
		ArrayList<Character> playerList = gameContent.getAllPlayers();
		for (Character player : playerList) {
			if (player.getName().equals(relayer.getCharacter().getName())) {
				g_newMap.setColor(playerColor);
				g_newMap.drawRect(player.getY() / tileW - 1, player.getX() / tileH - 1, 3, 3);
			}
			else {
				g_newMap.setColor(friendColor);
				g_newMap.drawRect(player.getY() / tileW - 1, player.getX() / tileH - 1, 3, 3);
			}
		}

		// Draw on the Screen
		g.drawImage(newMap, gamePanel.getSize().width - (mapWidth-1) / tileW, 0, null);
	}
}
