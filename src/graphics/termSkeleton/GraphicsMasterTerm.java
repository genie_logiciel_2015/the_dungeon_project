package graphics.termSkeleton;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.termSkeleton.panel.GamePanelTerminal;
import graphics.termSkeleton.panel.menu.ConfigurationPanelTerminal;
import graphics.termSkeleton.panel.menu.MainMenuPanelTerminal;
import graphics.termSkeleton.panel.menu.SinglePlayer_MenuPanelTerminal;
import graphics.termSkeleton.panel.menu.characterChoice.MultiPlayer_CharacterChoicePanelTerminal;
import graphics.termSkeleton.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelTerminal;
import graphics.termSkeleton.panel.menu.gameCreation.MultiPlayer_GameCreationPanelTerminal;
import graphics.termSkeleton.panel.menu.gameCreation.SinglePlayer_GameCreationPanelTerminal;
import graphics.termSkeleton.panel.menu.gameLoad.SinglePlayer_LoadGamePanelTerminal;
import graphics.termSkeleton.termPanel.*;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class is the maestro of the GUI, it knows each element of it, and defines what should be done for each button
 * of it.
 *
 * Structure: 1 JFrame (MainFrame). Multiple JPanels (MainMenuTermPanel, GamePanel...).
 * 		GraphicsMaster changes the Panel that is displayed in the MainFrame according to what the GUI should display
 *
 * It implements the singleton pattern
 */
public class GraphicsMasterTerm extends GraphicsMasterAbstraction {
	// Indicates whether the gui has already been launched (so that it is not launched again if GraphicsMaster is
	// instanciated a second time by mistake)

	/** Singleton pattern implementation **/
	//private static GraphicsMaster thisGraphicsMaster
    //protected MainFrame mainFrame;

	//protected static GraphicsMasterAbstraction thisGraphicsMaster = new GraphicsMaster();

	private GraphicsMasterTerm() {
		mapGUIStatesToGUIPanel.put(GUIStates.UNINITIALISED, new UnitialisedGUIPanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MAIN_MENU, new MainMenuPanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.GAME_PANEL, new GamePanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_MENU, new SinglePlayer_MenuPanelTerminal(this));
		//mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_MENU, new MultiPlayer_MenuPanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.INPUT_CONFIG_PANEL, new ConfigurationPanelTerminal(this));
		//mapGUIStatesToGUIPanel.put(GUIStates.IGP_PANEL, new IGPpanelTerminal(this));

//		mapGUIStatesToGUIPanel.put(GUIStates.LOUNGE, new LoungePanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_GAME_CREATION, new MultiPlayer_GameCreationPanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.MULTIPLAYER_CHARACTER_CHOICE, new MultiPlayer_CharacterChoicePanelTerminal(this));

		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_GAME_CREATION, new SinglePlayer_GameCreationPanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_CHARACTER_CHOICE, new SinglePlayer_CharacterChoicePanelTerminal(this));
		mapGUIStatesToGUIPanel.put(GUIStates.SINGLEPLAYER_LOADGAME, new SinglePlayer_LoadGamePanelTerminal(this));
	}


	public static void build() {
		thisGraphicsMaster = new GraphicsMasterTerm();
		thisGraphicsMaster.mainFrameKeyListener = new TermFrameKeyListener((GraphicsMasterTerm)thisGraphicsMaster);
		//((TermFrameKeyListener)(thisGraphicsMaster.mainFrameKeyListener)).start();
	}

	/**
	 * This method launches the GUI (the name is quite self-explanatory :))
	 */
	@Override
	public void launchGUI() {
		if(guiLaunched == false) {
			launchTermApplication();
			guiLaunched = true;
		}
	}

	private void launchTermApplication() {
		new Runnable() {
			@Override
			public void run() {
				mainFrame = new TermFrame();
				mainFrame.init(mainFrameKeyListener);
				changeGUIStateTo(GUIStates.MAIN_MENU);
				while(true){
					mapGUIStatesToGUIPanel.get(guiState).repaint();
					mainFrameKeyListener.check();
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.run();
	}


	@Override
	public void exitGUI() {
        //((MainFrame)mainFrame).setVisible(false);
		//Maybe "dispose" is not as definitive as the method exitGUI() should be
        //((MainFrame)mainFrame).dispose();
		//TODO: make it terminates the program
		System.exit(1);
	}

	/**
	 * This method changes the state of the GUI.
	 * It firsts do a switch on the current state of the GUI, to do the necessary clean up.
	 * It then asks the method setGUIState with arg_guiState.
	 * @param arg_guiState: the new GUIState the caller want the guiState to be changed to
	 */
	@Override
	public void changeGUIStateTo(GUIStates arg_guiState) {
		mainFrame.remove(mapGUIStatesToGUIPanel.get(guiState));
		mapGUIStatesToGUIPanel.get(guiState).finalise();
		setGUIStateTo(arg_guiState);
	}

	/**
	 * This method set the state of the GUI to the value received in argument.
	 * This method MUST NOT be called by anywhere else than in the changeGUIState method's body.
	 * @param arg_guiState: the GUIState the caller want the guiState to be set to
	 */
	private void setGUIStateTo(GUIStates arg_guiState) {
		PanelAbstraction guiPanelToDisplay = mapGUIStatesToGUIPanel.get(arg_guiState);
		guiPanelToDisplay.initialise();
		guiState = arg_guiState;
	}
}
