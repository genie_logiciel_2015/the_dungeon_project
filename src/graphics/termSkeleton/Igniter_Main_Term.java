package graphics.termSkeleton;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;

/**
 * Created by dupriez on 10/11/15.
 *
 * The class that contains the InputConfigurationFrame method calling the "launchGUI" method from GraphicsMaster
 * It's just a starter
 */
public class Igniter_Main_Term {

	public static void main(String[] args) {
		GraphicsMasterTerm.build();
		GraphicsMasterAbstraction gm = GraphicsMasterAbstraction.getInstance();
		gm.launchGUI();
	}
}
