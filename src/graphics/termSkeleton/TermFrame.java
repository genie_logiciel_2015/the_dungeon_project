package graphics.termSkeleton;

import graphics.graphical_abstraction.FrameAbstraction;
import graphics.graphical_abstraction.FrameKeyListenerAbstraction;
import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.termSkeleton.panel.PanelTerminal;

import java.util.ArrayList;

/**
 * Created by dupriez on 10/11/15.
 */
public class TermFrame implements FrameAbstraction {

	private ArrayList<PanelTerminal> panels = new ArrayList<>();

	public TermFrame() {
	}

	@Override
	public void init(FrameKeyListenerAbstraction keyListenerAbstraction) {

	}

	@Override
	public void remove(PanelAbstraction panel) {
		panels.remove(panel);
	}

	@Override
	public void add(PanelAbstraction panel){
		panels.add((PanelTerminal)panel);
		this.repaint();
	}

	@Override
	public void repaint() {
		for (PanelTerminal p : panels) {
			p.repaint();
		}
	}
}
