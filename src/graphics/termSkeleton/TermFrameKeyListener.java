package graphics.termSkeleton;

import graphics.graphical_abstraction.FrameKeyListenerAbstraction;
import graphics.ingame_input_listener.Input;
import graphics.ingame_input_listener.InputConfiguration;

import java.io.*;
import java.util.HashSet;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class will listen to all the keys pressed/released/typed in the mainFrame, and relays them to GraphicsMaster
 */
public class TermFrameKeyListener implements FrameKeyListenerAbstraction{

	private GraphicsMasterTerm graphicsMaster;
	private HashSet<Input> inputs_current = new HashSet<>();
	private HashSet<Input> inputs_previous = new HashSet<>();

	public TermFrameKeyListener(GraphicsMasterTerm gm) {
		graphicsMaster = gm;
	}


	public BufferedReader in;

	private static void setTerminalToCBreak() throws IOException, InterruptedException {

		stty("-g");

		// set the console to be character-buffered instead of line-buffered
		stty("-icanon min 1");

		// disable character echoing
		stty("-echo");
	}

	private static String stty(final String args)
			throws IOException, InterruptedException {
		String cmd = "stty " + args + " < /dev/tty";

		return exec(new String[] {
				"sh",
				"-c",
				cmd
		});
	}

	private static String exec(final String[] cmd)
			throws IOException, InterruptedException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		Process p = Runtime.getRuntime().exec(cmd);
		int c;
		try(InputStream in = p.getInputStream()){

			while ((c = in.read()) != -1) {
				bout.write(c);
			}
		}
		try(InputStream in = p.getErrorStream()){

			while ((c = in.read()) != -1) {
				bout.write(c);
			}

			p.waitFor();

			String result = new String(bout.toByteArray());
			return result;
		}
	}

	public void check()
	{

		try {
			setTerminalToCBreak();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		in = new BufferedReader( new InputStreamReader(System.in) );

			int line;
			try {
				while (in.ready()) {
					line = in.read();

					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.ESCAPE)) {
						//graphicsMaster.keyPressedHandler(Input.ESCAPE);
						inputs_current.add(Input.ESCAPE);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.LEFT_ARROW)) {
						//graphicsMaster.keyPressedHandler(Input.LEFT_ARROW);
						inputs_current.add(Input.LEFT_ARROW);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.UP_ARROW)) {
						//graphicsMaster.keyPressedHandler(Input.UP_ARROW);
						inputs_current.add(Input.UP_ARROW);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.RIGHT_ARROW)) {
						//graphicsMaster.keyPressedHandler(Input.RIGHT_ARROW);
						inputs_current.add(Input.RIGHT_ARROW);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.DOWN_ARROW)) {
						//graphicsMaster.keyPressedHandler(Input.DOWN_ARROW);
						inputs_current.add(Input.DOWN_ARROW);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.ATTACK)) {
						//graphicsMaster.keyPressedHandler(Input.DOWN_ARROW);
						inputs_current.add(Input.ATTACK);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.ABILITY_1)) {
						//graphicsMaster.keyPressedHandler(Input.DOWN_ARROW);
						inputs_current.add(Input.ABILITY_1);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.ABILITY_2)) {
						//graphicsMaster.keyPressedHandler(Input.DOWN_ARROW);
						inputs_current.add(Input.ABILITY_2);
					}
					if (line == InputConfiguration.GetFormalInputIntTerminal(Input.ABILITY_3)) {
						//graphicsMaster.keyPressedHandler(Input.DOWN_ARROW);
						inputs_current.add(Input.ABILITY_3);
					}
				}
			}
			catch(IOException e)
			{
				System.out.println("IOException: " + e);
			}

			for(Input input:inputs_current){
				graphicsMaster.keyPressedHandler(input);
			}
			inputs_previous.removeAll(inputs_current);
			for(Input input:inputs_previous){
				graphicsMaster.keyReleasedHandler(input);
				//System.out.println(input);
			}
			inputs_previous.clear();
			inputs_previous.addAll(inputs_current);
			inputs_current.clear();


		}

}
