package graphics.termSkeleton.panel;

import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.termSkeleton.GraphicsMasterTerm;

/**
 * Created by dupriez on 12/11/15.
 *
 * This abstract class defines the kind of panel the gui can display
 * The gui changes what it displays by swapping GUIPanels in the mainFrame.
 */
public abstract class PanelTerminal implements PanelAbstraction{

    /*Configuration*/
    protected int number_line = 40;
    protected int number_column = 140;

    public int getHeight(){return number_line;}
    public int getWidth(){return number_column;}

    //The following line is commented because KeyListener does not seem to work in a JPanel
    //Private KeyListenerForGUIPanel associatedKeyHandler;

    //Store the graphicsMaster that created the guiPanel (there should be only one GraphicsMaster instanciation per application,
    // this variable is just there to give the guiPanel the right to call the method changeGUIStateTo de graphicsMaster)
    private GraphicsMasterTerm graphicsMaster;
    protected GraphicsMasterTerm getGraphicsMaster() {return graphicsMaster;}
    protected StringBuilder[] myPanel;

    public PanelTerminal(GraphicsMasterTerm graphicsMaster) {
        String s = new String();
        for(int i = 0; i< number_column; i++){
            s+= "\0";
        }
        this.graphicsMaster = graphicsMaster;
        myPanel = new StringBuilder[number_line];
        for(int i = 0; i<number_line ; i++){
            myPanel[i] = new StringBuilder(s);
        }

    }

    @Override
    public void repaint() {
        System.out.print("\033[H\033[2J");
        for(StringBuilder s : myPanel){
            if(s != null)System.out.println(s.toString());
            else System.out.println("");
        }
    }

}