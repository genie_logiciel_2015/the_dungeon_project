package graphics.termSkeleton.panel;

import core.abilities.Ability;
import core.gamestate.Being;
import core.gamestate.Entity;
import core.gamestate.GameContent;
import core.relayer.Relayer;
import gameloop.LocalGameLoop;
import gameloop.GameStarter;
import graphics.graphical_abstraction.panel.GamePanelAbstraction;
import graphics.graphical_abstraction.panel.GamePanelAbstractionController;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.ingame_input_listener.Input;
import graphics.termSkeleton.GraphicsMasterTerm;
import map_generation.map.Map;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by etouss on 20/12/2015.
 */
public class GamePanelTerminal extends PanelTerminal implements GamePanelAbstraction{


    public GamePanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
    }

    private GamePanelAbstractionController controller = new GamePanelAbstractionController(this);
    private Relayer followedRelayer;
    private GameContent gameContent;



    @Override
    public void keyPressedHandler(Input e) {
        switch(e){
            case ESCAPE:
                GameStarter.leaveGame();
                getGraphicsMaster().changeGUIStateTo(GraphicsMaster.GUIStates.MAIN_MENU);break;
            default:
                controller.keyPressed(e);
                break;
        }
    }

    @Override
    public void keyReleasedHandler(Input e) {
        controller.keyReleased(e);
    }

    @Override
    public void keyTypedHandler(Input e) {

    }

    @Override
    public void initialise() {
        controller.initialise();
        followedRelayer = LocalGameLoop.getInstance().getFollowedRelayer();

        //MyInputInterpreter = new InputInterpreter(followedRelayer);

        gameContent = LocalGameLoop.getInstance().getContent();
    }

    @Override
    public void finalise() {

    }

    @Override
    public void repaint() {
        super.repaint();
		/*What part of the map should i display ? */ //MapCoordinate
			/*Determine by Screen size + Ratio == 32 */
			/*And by the hero position*/
				/*Return init point*/

		/*What part of the game should i display ?*/ //GameCoreCoordinate
			/*Determine by Screen size + + Ratio == 32*/
			/*And by hero position*/
				/*Return Coordinates Zone*/

		/*DisplayMap easy with initPoint*/
		/*DisplayEntity with check before*/



		/* Locate bound to display */

        int ratio_core_map = 32;
        int ratio_screen_map = 1;
        int ratio_screen_core = ratio_core_map/ratio_screen_map;

        Map map = LocalGameLoop.getInstance().getContent().getMap();
        int x_max_map = map.getHeight();
        int y_max_map = map.getWidth();

        core.gamestate.Character hero = followedRelayer.getCharacter();
        int x_hero_core = hero.getX();
        int y_hero_core = hero.getY();
        //System.out.println(x_hero_core);
        //System.out.println(y_hero_core);

        int y_max_true_screen = this.getHeight();
        int y_max_screen = y_max_true_screen - y_max_true_screen/4;

        int x_max_screen = this.getWidth();



		/*TopLeft point to print*/
        int init_map_point_x = (x_hero_core/ratio_core_map)-(y_max_screen*ratio_screen_map/2);
        int init_map_point_y = (y_hero_core/ratio_core_map)-(x_max_screen*ratio_screen_map/2);

        for(int x = 0; x<x_max_screen; x++){
            for(int y = 0;y<y_max_screen; y++){
                myPanel[y].setCharAt(x, ' ');
                if(init_map_point_x+y>0 && init_map_point_x+y< x_max_map && init_map_point_y+x>0 && init_map_point_y+x< y_max_map) {
                    myPanel[y].setCharAt(x, map.getTileAt(init_map_point_x + y, init_map_point_y + x).term_return());
                }
            }
        }

        /*Hero displayer i know he is in the middle but it's trash*/
        //Not use anymore
        //myPanel[y_max_screen/2].setCharAt(x_max_screen/2,'@');

         /*Entity to display*/

        int x_core_min_to_display = x_hero_core-(y_max_screen*ratio_screen_core/2);
        int y_core_min_to_display = y_hero_core-(x_max_screen*ratio_screen_core/2);
        int x_core_max_to_display = x_hero_core+(y_max_screen*ratio_screen_core/2);
        int y_core_max_to_display = y_hero_core+(x_max_screen*ratio_screen_core/2);

        List<Entity> entityList = gameContent.getEntityList();
        /*List to sort entity by distance*/
        ArrayList<Being> entityListSorted = new ArrayList();

        /*Display Entity*/
        for(Entity processedEntity : entityList) {
            /*Check if to display*/
            if(processedEntity.getX() <= x_core_min_to_display || processedEntity.getX() >= x_core_max_to_display
                    || processedEntity.getY() <= y_core_min_to_display || processedEntity.getY() >= y_core_max_to_display) {
                continue;
            }

            /*To display*/
            myPanel[(processedEntity.getX() - x_core_min_to_display) / ratio_screen_core].setCharAt((processedEntity.getY() - y_core_min_to_display) / ratio_screen_core, processedEntity.getEntityDisplayer().getTermSprite());
            if(processedEntity instanceof Being) {
                entityListSorted.add((Being)processedEntity);
            }
        }
        /*Display Information*/
        /*Sort by carthesienne distance*/
        entityList.sort((a, b) -> ((b.getY()-y_hero_core)*(b.getY()-y_hero_core)+(b.getX()-x_hero_core)*(b.getX()-x_hero_core)-(a.getY()-y_hero_core)*(a.getY()-y_hero_core)-(a.getX()-x_hero_core)*(a.getX()-x_hero_core)));

        int nb_entity_max = y_max_true_screen - y_max_screen -1;
        int nb_entity = 0;

        for (Being processedEntity : entityListSorted){
            if(nb_entity == nb_entity_max) break;
            this.myPanel[y_max_screen+nb_entity].replace(0, this.myPanel[y_max_screen + nb_entity].length(), "HP: "+Integer.toString(processedEntity.getHP()));
            nb_entity++;
        }
        for(;nb_entity<nb_entity_max;nb_entity++){
            this.myPanel[y_max_screen+nb_entity].replace(0, this.myPanel[y_max_screen + nb_entity].length(), "");
        }

        /*COOL DOWN*/
        StringBuilder cd = new StringBuilder(getWidth());
        for(int i = 0; i<followedRelayer.getCharacter().getAbilityList().size();i++ ){
            Ability abi = followedRelayer.getCharacter().getAbilityList().get(i);
            StringBuilder s = new StringBuilder(getWidth());
            s.append("AB"+i+": |");
            int nb_cross = Math.round(abi.getCharged()*10);
            //System.out.println(abi.getCharged()*10);
            for(int j = 0;j<10;j++){
                if(nb_cross<=j)
                    s.append('X');
                else
                    s.append(' ');
            }
            s.append('|');
            cd.append(s);
            cd.append("   ");
        }
        this.myPanel[y_max_true_screen-1].replace(0, this.myPanel[y_max_true_screen-1].length(), cd.toString());






    }
}
