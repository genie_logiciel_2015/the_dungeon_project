package graphics.termSkeleton.panel.menu.gameCreation;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.MultiPlayer_GameCreationPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.MultiPlayer_GameCreationPanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.menu.MenuPanelTerminal;

import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_GameCreationPanelTerminal extends MenuPanelTerminal implements MultiPlayer_GameCreationPanelAbstraction {

    private MultiPlayer_GameCreationPanelAbstractionController controller = new MultiPlayer_GameCreationPanelAbstractionController(this);
    public MultiPlayer_GameCreationPanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("MP Game Creation");
        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("Create", () -> controller.createButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("Back",() -> controller.backButtonPressed()));
        setButtonPanel(buttonDefList);
    }

    @Override
    public void createButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOUNGE);
    }

    @Override
    public void backButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MULTIPLAYER_MENU);
    }
}
