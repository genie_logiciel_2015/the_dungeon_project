package graphics.termSkeleton.panel.menu.gameCreation;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.SinglePlayer_GameCreationPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.gameCreation.SinglePlayer_GameCreationPanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.menu.MenuPanelTerminal;

import java.util.ArrayList;


/**
 * Created by dupriez on 05/12/15.
 */
public class SinglePlayer_GameCreationPanelTerminal extends MenuPanelTerminal implements SinglePlayer_GameCreationPanelAbstraction {

    private SinglePlayer_GameCreationPanelAbstractionController controller = new SinglePlayer_GameCreationPanelAbstractionController(this);

    public SinglePlayer_GameCreationPanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("SP Game Creation");
        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("Create", () -> controller.createButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("Back",() -> controller.backButtonPressed()));
        setButtonPanel(buttonDefList);
    }

    @Override
    public void createButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_CHARACTER_CHOICE);
    }

    @Override
    public void backButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_MENU);
    }
}
