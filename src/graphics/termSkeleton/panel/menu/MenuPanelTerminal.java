package graphics.termSkeleton.panel.menu;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;
import graphics.graphical_utility.Voidy;
import graphics.ingame_input_listener.Input;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.PanelTerminal;

import java.util.ArrayList;
import java.util.function.Function;
/**
 * Created by dupriez on 05/12/15.
 *
 * The purpose of this class is to be used to create easily simple panels for the gui.
 * It can contain more general components than the MenuPanel one (in fact any JComponent).
 */

/**Not to be directly implements**/
public class MenuPanelTerminal extends PanelTerminal implements MenuPanelAbstraction {

	protected int nb_panel;
	protected int current_position;
	protected ArrayList<buttonPanelButtonDefinition> buttonDefList;

	public MenuPanelTerminal(GraphicsMasterTerm graphicsMaster) {
		super(graphicsMaster);
		//setTitlePanel("titlePanel undefined");
		nb_panel = 0;
		current_position = 1;
		setBottomPanel("");
	}

	/**
	 * Create a new titlePanel, and replace the one already used in this menuPanel
	 * @param title: The string to be displayed by the titlePanel.
	 */
	public void setTitlePanel(String title) {
		/** Create the new titlePanel **/
		this.myPanel[0].append(title);
	}

	/**
	 * Create a new buttonPanel, and replace the one already used in this menuPanel
	 * @param buttonDefList: A list of buttonPanelButtonDefinition, defining the buttons this buttonPanel must contain
	 */
	protected void setButtonPanel(ArrayList<buttonPanelButtonDefinition> buttonDefList){
		makeButtonPanel(buttonDefList);
	}

	/**
	 * This class defines the buttons that can be displayed in the central row of the MainMenuTermPanel
	 */
	protected class buttonPanelButtonDefinition {
		private String buttonText;
		private Voidy func;

		public buttonPanelButtonDefinition(String buttonText, Voidy func) {
			this.buttonText = buttonText;
			this.func = func;
		}

		public String getButtonText() {
			return buttonText;
		}

		public Voidy getAction() {
			return func;
		}
	}

	/**
	 * This method creates a buttonPanel to be displayed on the MainMenuTermPanel
	 * @param buttonDefList: A list of buttonPanelButtonDefinition
	 * @return	A JPanel with all the asked buttons in the central row (buttonPanel)
	 */
	private void makeButtonPanel(ArrayList<buttonPanelButtonDefinition> buttonDefList) {

		for (int i = 0;i<buttonDefList.size();i++) {
			nb_panel ++;
			myPanel[i+1].append(buttonDefList.get(i).getButtonText());
		}
	}

	/**
	 * Create a new bottomPanel, and replace the one already used in this menuPanel
	 * @param text: The string to be displayed by the bottomPanel.
	 */
	protected void setBottomPanel(String text){
		myPanel[number_line-1].append(text);
	}

	@Override
	public void repaint(){
		StringBuilder tmp = new StringBuilder(myPanel[current_position]);
		myPanel[current_position] = new StringBuilder("*").append(myPanel[current_position]);
		super.repaint();
		myPanel[current_position] = tmp;
	}

	@Override
	public void keyPressedHandler(Input e) {
		if(e == Input.UP_ARROW){
			if(current_position > 1) current_position--;
		}
		if(e == Input.DOWN_ARROW){
			if(current_position < nb_panel) current_position++;
		}
		if(e == Input.ATTACK){
			this.buttonDefList.get(this.current_position - 1).getAction().apply();
		}
	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {

	}

	@Override
	public void finalise() {

	}
}
