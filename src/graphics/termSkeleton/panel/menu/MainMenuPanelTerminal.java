package graphics.termSkeleton.panel.menu;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.MainMenuPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.MainMenuPanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;

import java.util.ArrayList;

/**
 * Created by dupriez on 14/11/15.
 */
public class MainMenuPanelTerminal extends MenuPanelTerminal implements MainMenuPanelAbstraction {

    private MainMenuPanelAbstractionController controller = new MainMenuPanelAbstractionController(this);

    public MainMenuPanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("The Dungeon Project");

        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("single player", () -> controller.singlePlayerButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("multi player",() -> controller.multiPlayerButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("configuration",() -> controller.configurationButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("igp",() -> controller.IGPButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("exit",() -> controller.exitButtonPressed()));
        setButtonPanel(buttonDefList);

        setBottomPanel("By default : ZQSD to move, O to validate, V to back during the Game");
    }

    @Override
    public void singlePlayerButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_MENU);
    }

    @Override
    public void multiPlayerButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MULTIPLAYER_MENU);

    }

    @Override
    public void configurationButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.INPUT_CONFIG_PANEL);

    }

    @Override
    public void IGPButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.IGP_PANEL);

    }

    @Override
    public void exitButtonAction() {
        System.exit(1);
        //getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.UNINITIALISED);
    }
}
