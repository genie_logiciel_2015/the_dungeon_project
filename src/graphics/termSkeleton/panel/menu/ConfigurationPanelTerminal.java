package graphics.termSkeleton.panel.menu;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.ConfigurationPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.ConfigurationPanelAbstractionController;
import graphics.ingame_input_listener.Input;
import graphics.ingame_input_listener.InputConfiguration;
import graphics.termSkeleton.GraphicsMasterTerm;

import java.io.IOException;
import java.util.ArrayList;

public class ConfigurationPanelTerminal extends MenuPanelTerminal implements ConfigurationPanelAbstraction {

    ConfigurationPanelAbstractionController controller = new ConfigurationPanelAbstractionController(this);

    /**
     * Menu to change the in-game configuration
     * @param graphicsMaster the main menu manager
     */
    public ConfigurationPanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("Configuration");
        buttonDefList = new ArrayList<>();
        /* a button Listener calls a lambda function managed by the controller which then calls
        a Change______Config function below which manages the change of input */
        buttonDefList.add(new buttonPanelButtonDefinition("move up: ", ()->controller.ConfigUpButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("move down: ", ()->controller.ConfigDownButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("move left: ", ()->controller.ConfigLeftButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("move right: ", ()->controller.ConfigRightButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("attack: ", ()->controller.ConfigAttackButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("ability 1: ", ()->controller.ConfigAbility1ButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("ability 2: ", ()->controller.ConfigAbility2ButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("ability 3: ", ()->controller.ConfigAbility3ButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("back to main: ", ()->controller.ConfigBackToMainButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("back", ()->controller.BackButtonPressed()));
        setButtonPanel(buttonDefList); //places the buttons above on the configuration panel
    }

    /**
     * Asks the user for an input on the terminal and changes the key linked to the formal input in argument
     * into this input
     * @param input the formal input being changed
     */
    private void ChangeInput(Input input){
        System.out.println("Enter a Character please:");
        try {
            InputConfiguration.ChangeInputTerminal(input, System.in.read());
        }
        catch (IOException e){
            System.out.println("Error reading from user while trying to change a command");
        }
    }

    /**
     * List of functions that manage the change of keys of formal inputs
     */
    @Override
    public void ChangeMoveUpConfig() {
        ChangeInput(Input.UP_ARROW);
    }

    @Override
    public void ChangeMoveDownConfig() {
        ChangeInput(Input.DOWN_ARROW);
    }

    @Override
    public void ChangeMoveRightConfig() {
        ChangeInput(Input.RIGHT_ARROW);
    }

    @Override
    public void ChangeMoveLeftConfig() {
        ChangeInput(Input.LEFT_ARROW);
    }

    @Override
    public void ChangeAttackConfig() {
        ChangeInput(Input.ATTACK);
    }

    @Override
    public void ChangeAbility1Config() {
        ChangeInput(Input.ABILITY_1);
    }

    @Override
    public void ChangeAbility2Config() {
        ChangeInput(Input.ABILITY_2);
    }

    @Override
    public void ChangeAbility3Config() {
        ChangeInput(Input.ABILITY_3);
    }

    @Override
    public void ChangeBackToMainConfig() {
        ChangeInput(Input.ESCAPE);
    }

    @Override
    public void BackToMainMenu() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MAIN_MENU);
    }
}