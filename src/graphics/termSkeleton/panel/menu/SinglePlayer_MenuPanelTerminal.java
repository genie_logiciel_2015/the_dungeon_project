package graphics.termSkeleton.panel.menu;

import gameloop.GameStarter;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.SinglePlayer_MenuPanelAbstraction;
import graphics.graphical_abstraction.panel.menu.SinglePlayer_MenuPanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;

import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player to create or load a game session
 */
public class SinglePlayer_MenuPanelTerminal extends MenuPanelTerminal implements SinglePlayer_MenuPanelAbstraction {

    private SinglePlayer_MenuPanelAbstractionController controller = new SinglePlayer_MenuPanelAbstractionController(this);

    public SinglePlayer_MenuPanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);

        setTitlePanel("SinglePlayer");

        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("create game", ()->controller.createGameButtonButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("load game", ()->controller.loadGameButtonButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("old play", ()->controller.oldPlayButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("back", ()->controller.backButtonPressed()));
        setButtonPanel(buttonDefList);
    }


    @Override
    public void initialise() {


    }

    @Override
    public void createGameButtonButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_GAME_CREATION);
    }

    @Override
    public void loadGameButtonButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_LOADGAME);
    }

    @Override
    public void oldPlayButtonAction() {
        GameStarter.startGameSinglePlayer();
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
    }

    @Override
    public void backButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.MAIN_MENU);
    }
}
