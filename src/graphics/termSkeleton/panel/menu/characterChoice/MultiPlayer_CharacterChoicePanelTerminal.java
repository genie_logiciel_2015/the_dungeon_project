package graphics.termSkeleton.panel.menu.characterChoice;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.MultiPlayer_CharacterChoicePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.MultiPlayer_CharacterChoicePanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.menu.MenuPanelTerminal;

import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_CharacterChoicePanelTerminal extends MenuPanelTerminal implements MultiPlayer_CharacterChoicePanelAbstraction {

    private MultiPlayer_CharacterChoicePanelAbstractionController controller = new MultiPlayer_CharacterChoicePanelAbstractionController(this);

    public MultiPlayer_CharacterChoicePanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("MP Character Choice");

        buttonDefList = new ArrayList<>();
        //buttonDefList.add(new buttonPanelButtonDefinition("Play", () -> controller.playButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("return",() -> controller.returnToLoungeButtonPressed()));
        setButtonPanel(buttonDefList);
    }

    @Override
    public void returnToLoungeButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.LOUNGE);
    }
}
