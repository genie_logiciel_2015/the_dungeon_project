package graphics.termSkeleton.panel.menu.characterChoice;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.menu.MenuPanelTerminal;

import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 */
public class SinglePlayer_CharacterChoicePanelTerminal extends MenuPanelTerminal implements SinglePlayer_CharacterChoicePanelAbstraction {

    private SinglePlayer_CharacterChoicePanelAbstractionController controller = new SinglePlayer_CharacterChoicePanelAbstractionController(this);

    public SinglePlayer_CharacterChoicePanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("SP Character Choice");

        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("Play", () -> controller.playButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("back",() -> controller.backButtonPressed()));
        setButtonPanel(buttonDefList);

    }

    @Override
    public void playButtonAction() {
        //Not working for now
//			getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
    }

    @Override
    public void backButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_GAME_CREATION);
    }
}
