package graphics.termSkeleton.panel.menu.gameLoad;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.gameLoad.SinglePlayer_LoadGamePanelAbstraction;
import graphics.graphical_abstraction.panel.menu.gameLoad.SinglePlayer_LoadGamePanelAbstractionController;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.menu.MenuPanelTerminal;

import java.util.ArrayList;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player load a game session
 */
public class SinglePlayer_LoadGamePanelTerminal extends MenuPanelTerminal implements SinglePlayer_LoadGamePanelAbstraction {

    private SinglePlayer_LoadGamePanelAbstractionController controller = new SinglePlayer_LoadGamePanelAbstractionController(this);

    public SinglePlayer_LoadGamePanelTerminal(GraphicsMasterTerm graphicsMaster) {
        super(graphicsMaster);
        setTitlePanel("Load Game");
        buttonDefList = new ArrayList<>();
        buttonDefList.add(new buttonPanelButtonDefinition("Load", () -> controller.loadButtonPressed()));
        buttonDefList.add(new buttonPanelButtonDefinition("Back",() -> controller.backButtonPressed()));
        setButtonPanel(buttonDefList);
    }

    @Override
    public void loadButtonAction() {
        //Not working for now
        //getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
    }

    @Override
    public void backButtonAction() {
        getGraphicsMaster().changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.SINGLEPLAYER_MENU);
    }
}
