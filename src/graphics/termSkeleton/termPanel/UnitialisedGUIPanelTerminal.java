package graphics.termSkeleton.termPanel;

import graphics.ingame_input_listener.Input;
import graphics.termSkeleton.GraphicsMasterTerm;
import graphics.termSkeleton.panel.PanelTerminal;

/**
 * Created by dupriez on 14/11/15.
 *
 * a blank guiPanel corresponding to the guiState "UNITIALISED" of graphicsMaster
 */
public class UnitialisedGUIPanelTerminal extends PanelTerminal {

	public UnitialisedGUIPanelTerminal(GraphicsMasterTerm graphicsMaster) {
		super(graphicsMaster);
	}

	@Override
	public void keyPressedHandler(Input e) {

	}

	@Override
	public void keyReleasedHandler(Input e) {

	}

	@Override
	public void keyTypedHandler(Input e) {

	}

	@Override
	public void initialise() {
		System.exit(1);
	}

	@Override
	public void finalise() {

	}
}
