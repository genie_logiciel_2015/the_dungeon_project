package graphics.display_test;

/**
 * Created by etouss on 06/01/2016.
 */
import gameloop.LocalGameLoop;
import gameloop.GameStarter;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.ingame_input_listener.Input;
import graphics.termSkeleton.GraphicsMasterTerm;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Etienne Toussaint on 06/01/16.
 * This is the unit test class for the GamePanelTerm class.
 */

public class GamePanelTermTest {
    @Test
    public void test_move() {
        GraphicsMasterTerm.build();
        GraphicsMasterAbstraction gm = GraphicsMasterAbstraction.getInstance();
        Thread runable =  new Thread(new Runnable(){

            @Override
            public void run() {
                gm.launchGUI();
            }
        });
        runable.start();

        GameStarter.startGameSinglePlayer();
        //gm.keyPressedHandler(Input.ATTACK);
        gm.changeGUIStateTo(GraphicsMasterAbstraction.GUIStates.GAME_PANEL);
        gm.keyPressedHandler(Input.LEFT_ARROW);
        gm.keyPressedHandler(Input.UP_ARROW);
        assertEquals(LocalGameLoop.getInstance().getFollowedRelayer().getDirection(), core.zone.Direction.LEFTUP);
    }
}
