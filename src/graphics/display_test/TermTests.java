package graphics.display_test;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * TestSuite for the terminal package.
 * Will launch automatically all the JUnits tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        GamePanelTermTest.class
})
public class TermTests {}