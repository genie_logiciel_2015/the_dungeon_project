package graphics.graphical_abstraction;
import graphics.graphical_abstraction.panel.PanelAbstraction;
import graphics.ingame_input_listener.Input;

import java.util.HashMap;

/**
 * Created by dupriez on 10/11/15.
 *
 * This class is the maestro of the GUI, it knows each element of it, and defines what should be done for each button
 * of it.
 *
 *
 * It implements the singleton pattern
 */
public abstract class GraphicsMasterAbstraction {
    // Indicates whether the GUI has already been launched (so that it is not launched again if GraphicsMaster is
    // instantiated a second time by mistake)
    protected FrameAbstraction mainFrame;
    public FrameKeyListenerAbstraction mainFrameKeyListener;
    protected boolean guiLaunched = false;

    //The different states the GUI can be in.
    public enum GUIStates {
        UNINITIALISED,
        MAIN_MENU,
        GAME_PANEL,
        SINGLEPLAYER_MENU,
        MULTIPLAYER_MENU,
        INPUT_CONFIG_PANEL,
        IGP_PANEL,
        SINGLEPLAYER_LOADGAME,
        LOUNGE,
        MULTIPLAYER_GAME_CREATION,
        SINGLEPLAYER_GAME_CREATION,
        MULTIPLAYER_CHARACTER_CHOICE,
        SINGLEPLAYER_CHARACTER_CHOICE,
        GAME_OVER,
        LOST_CONNECTION,
        PLEASE_WAIT,
        SERVER_ADDRESS_PANEL,
        SERVER_CREATED_PANEL, 
        GAME_WIN;
    }
    //This map indicates to graphicsMaster which guiPanel is associated to each GUIState, it is filled in Graphicsmaster's constructor
    protected HashMap<GUIStates, PanelAbstraction> mapGUIStatesToGUIPanel = new HashMap<>();
    //The current state of the GUI
    protected GUIStates guiState = GUIStates.UNINITIALISED;

    /** Singleton pattern implementation **/
    protected static GraphicsMasterAbstraction thisGraphicsMaster;

    public static void build(){
        thisGraphicsMaster = null;
    }

    public final static GraphicsMasterAbstraction getInstance() {
        return thisGraphicsMaster;
    }

    /**
     * This method launches the GUI (the name is quite self-explanatory :))
     */
    public abstract void launchGUI();// {

    /**
     * This method defines what to do when a key is Pressed in the mainFrame
     * @param e The reconaizable input e that have been pressed
     */
    public final void keyPressedHandler(Input e) {
        mapGUIStatesToGUIPanel.get(guiState).keyPressedHandler(e);
    }

    /**
     * This method defines what to do when a key is Released in the mainFrame
     * @param e The reconaizable input e that have been released
     */
    public final void keyReleasedHandler(Input e) {
        mapGUIStatesToGUIPanel.get(guiState).keyReleasedHandler(e);
    }

    /**
     * This method defines what to do when a key is typed in the mainFrame
     * @param e The reconaizable input e that have been typed
     */
    public final void keyTypedHandler(Input e) {
        mapGUIStatesToGUIPanel.get(guiState).keyTypedHandler(e);
    }

    /**
     *	close the mainframe and terminates the program
     *	NOT FINISHED
     */
    public abstract void exitGUI();

    /**
     * This method changes the state of the GUI.
     * It firsts do a switch on the current state of the GUI, to do the necessary clean up.
     * It then asks the method setGUIState with arg_guiState.
     * @param arg_guiState: the new GUIState the caller want the guiState to be changed to
     */
    public abstract void changeGUIStateTo(GUIStates arg_guiState);
}
