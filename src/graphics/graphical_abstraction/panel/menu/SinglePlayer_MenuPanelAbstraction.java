package graphics.graphical_abstraction.panel.menu;

import gameloop.GameStarter;
import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player to create or load a game session
 */
public interface SinglePlayer_MenuPanelAbstraction extends MenuPanelAbstraction {


	/*What to do when it have been pressed*/
	public void createGameButtonButtonAction();
	public void loadGameButtonButtonAction();
	public void oldPlayButtonAction();
	public void backButtonAction();

}
