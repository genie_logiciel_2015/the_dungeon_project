package graphics.graphical_abstraction.panel.menu;


/**
 * Created by zobi on 25/11/15.
 */

/**
 * Like the other interfaces Panel abstraction, it is used to force the implementation
 * of what we expect of a ConfigurationPanel in both cases graphical and terminal versions
 */
public interface ConfigurationPanelAbstraction extends MenuPanelAbstraction {
    //To call for a change of the underlying formal Input
    void ChangeMoveUpConfig();
    void ChangeMoveDownConfig();
    void ChangeMoveRightConfig();
    void ChangeMoveLeftConfig();
    void ChangeAttackConfig();
    void ChangeAbility1Config();
    void ChangeAbility2Config();
    void ChangeAbility3Config();
    void ChangeBackToMainConfig();
    //To call to get back to mainMenu
    void BackToMainMenu();
}