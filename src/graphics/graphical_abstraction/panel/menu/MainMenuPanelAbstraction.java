package graphics.graphical_abstraction.panel.menu;

/**
 * Created by dupriez on 14/11/15.
 */
public interface MainMenuPanelAbstraction extends MenuPanelAbstraction {

    /*What to do when it have been pressed*/
    public void singlePlayerButtonAction();
    public void multiPlayerButtonAction();
    public void configurationButtonAction();
    public void IGPButtonAction();
    public void exitButtonAction();

}
