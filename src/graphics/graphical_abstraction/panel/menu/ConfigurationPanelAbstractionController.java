package graphics.graphical_abstraction.panel.menu;

/**
 * Created by zobi on 25/11/15.
 */

/**
 * Like the other Controller abstraction panels, manages what is done
 * in Configuration panels in both cases graphical and terminal versions
 */
public class ConfigurationPanelAbstractionController extends MenuPanelAbstractionController {

    private ConfigurationPanelAbstraction configurationMenuPanel;

    public ConfigurationPanelAbstractionController(ConfigurationPanelAbstraction configurationMenuPanel){
        this.menuPanel = configurationMenuPanel;
        this.configurationMenuPanel = configurationMenuPanel;
    }

    //To handle the ask from the user to change the configuration of an input
    public void ConfigUpButtonPressed(){this.configurationMenuPanel.ChangeMoveUpConfig();}
    public void ConfigDownButtonPressed(){this.configurationMenuPanel.ChangeMoveDownConfig();}
    public void ConfigRightButtonPressed(){this.configurationMenuPanel.ChangeMoveRightConfig();}
    public void ConfigLeftButtonPressed(){this.configurationMenuPanel.ChangeMoveLeftConfig();}
    public void ConfigAttackButtonPressed(){this.configurationMenuPanel.ChangeAttackConfig();}
    public void ConfigAbility1ButtonPressed(){this.configurationMenuPanel.ChangeAbility1Config();}
    public void ConfigAbility2ButtonPressed(){this.configurationMenuPanel.ChangeAbility2Config();}
    public void ConfigAbility3ButtonPressed(){this.configurationMenuPanel.ChangeAbility3Config();}
    public void ConfigBackToMainButtonPressed(){this.configurationMenuPanel.ChangeBackToMainConfig();}
    //To handle the ask from the user to get back to the main menu
    public void BackButtonPressed(){this.configurationMenuPanel.BackToMainMenu();}
}