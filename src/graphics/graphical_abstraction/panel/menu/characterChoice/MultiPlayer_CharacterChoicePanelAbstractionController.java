package graphics.graphical_abstraction.panel.menu.characterChoice;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstractionController;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_CharacterChoicePanelAbstractionController extends MenuPanelAbstractionController {

	private MultiPlayer_CharacterChoicePanelAbstraction charac_panel;

	public MultiPlayer_CharacterChoicePanelAbstractionController(MultiPlayer_CharacterChoicePanelAbstraction charac_panel){
		this.menuPanel = charac_panel;
		this.charac_panel = charac_panel;
	}

	public void returnToLoungeButtonPressed(){
		this.charac_panel.returnToLoungeButtonAction();
	}

}
