package graphics.graphical_abstraction.panel.menu.characterChoice;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;

/**
 * Created by dupriez on 05/12/15.
 */
public interface MultiPlayer_CharacterChoicePanelAbstraction extends MenuPanelAbstraction {

	public void returnToLoungeButtonAction();

}
