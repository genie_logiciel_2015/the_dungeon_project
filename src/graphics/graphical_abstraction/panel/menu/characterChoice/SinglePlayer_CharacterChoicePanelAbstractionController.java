package graphics.graphical_abstraction.panel.menu.characterChoice;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstractionController;

/**
 * Created by dupriez on 05/12/15.
 */
public class SinglePlayer_CharacterChoicePanelAbstractionController extends MenuPanelAbstractionController {

	private SinglePlayer_CharacterChoicePanelAbstraction single_menu;

	public SinglePlayer_CharacterChoicePanelAbstractionController(SinglePlayer_CharacterChoicePanelAbstraction single_menu){
		this.menuPanel = single_menu;
		this.single_menu = single_menu;
	}

	public void playButtonPressed(){
		this.single_menu.playButtonAction();
	}
	public void backButtonPressed(){
		this.single_menu.backButtonAction();
	}

}
