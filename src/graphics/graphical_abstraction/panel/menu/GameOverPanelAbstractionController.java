package graphics.graphical_abstraction.panel.menu;

/**
 * Created by dupriez on 14/11/15.
 */
public class GameOverPanelAbstractionController extends MenuPanelAbstractionController {

    private GameOverPanelAbstraction gameOverPanel;

    public GameOverPanelAbstractionController(GameOverPanelAbstraction gameOverPanel){
        this.menuPanel = gameOverPanel;
        this.gameOverPanel = gameOverPanel;
    }

    public void returnButtonPressed(){
        this.gameOverPanel.returnButtonAction();
    }

}
