package graphics.graphical_abstraction.panel.menu;

import graphics.guiSkeleton.guiPanel.menuPanel.MainMenuPanel;

/**
 * Created by dupriez on 14/11/15.
 */
public class MainMenuPanelAbstractionController extends MenuPanelAbstractionController {

    private MainMenuPanelAbstraction mainMenuPanel;

    public MainMenuPanelAbstractionController(MainMenuPanelAbstraction mainMenuPanel){
        this.menuPanel = mainMenuPanel;
        this.mainMenuPanel = mainMenuPanel;
    }

    public void singlePlayerButtonPressed(){
        this.mainMenuPanel.singlePlayerButtonAction();
    }
    public void multiPlayerButtonPressed(){
        this.mainMenuPanel.multiPlayerButtonAction();
    }
    public void configurationButtonPressed(){
        this.mainMenuPanel.configurationButtonAction();
    }
    public void IGPButtonPressed(){
        this.mainMenuPanel.IGPButtonAction();
    }
    public void exitButtonPressed(){
        this.mainMenuPanel.exitButtonAction();
    }

}
