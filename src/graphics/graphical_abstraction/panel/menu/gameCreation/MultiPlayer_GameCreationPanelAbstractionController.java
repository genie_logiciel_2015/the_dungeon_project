package graphics.graphical_abstraction.panel.menu.gameCreation;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstractionController;

/**
 * Created by dupriez on 05/12/15.
 */
public class MultiPlayer_GameCreationPanelAbstractionController extends MenuPanelAbstractionController {

	private MultiPlayer_GameCreationPanelAbstraction multi_panel;

	public MultiPlayer_GameCreationPanelAbstractionController(MultiPlayer_GameCreationPanelAbstraction multi_panel){
		this.menuPanel = multi_panel;
		this.multi_panel = multi_panel;
	}

	public void createButtonPressed(){
		this.multi_panel.createButtonAction();
	}
	public void backButtonPressed(){
		this.multi_panel.backButtonAction();
	}

}
