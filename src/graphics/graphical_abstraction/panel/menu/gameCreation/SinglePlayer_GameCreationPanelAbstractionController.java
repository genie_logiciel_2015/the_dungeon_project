package graphics.graphical_abstraction.panel.menu.gameCreation;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstractionController;
import graphics.graphical_abstraction.panel.menu.characterChoice.SinglePlayer_CharacterChoicePanelAbstractionController;


/**
 * Created by dupriez on 05/12/15.
 */
public class SinglePlayer_GameCreationPanelAbstractionController extends MenuPanelAbstractionController {

	private SinglePlayer_GameCreationPanelAbstraction single_panel;

	public SinglePlayer_GameCreationPanelAbstractionController(SinglePlayer_GameCreationPanelAbstraction single_panel){
		this.menuPanel = single_panel;
		this.single_panel = single_panel;
	}

	public void createButtonPressed(){
		this.single_panel.createButtonAction();
	}
	public void backButtonPressed(){
		this.single_panel.backButtonAction();
	}
}
