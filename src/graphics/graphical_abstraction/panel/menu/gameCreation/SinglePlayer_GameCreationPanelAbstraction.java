package graphics.graphical_abstraction.panel.menu.gameCreation;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;


/**
 * Created by dupriez on 05/12/15.
 */
public interface SinglePlayer_GameCreationPanelAbstraction extends MenuPanelAbstraction {

	public void createButtonAction();
	public void backButtonAction();
}
