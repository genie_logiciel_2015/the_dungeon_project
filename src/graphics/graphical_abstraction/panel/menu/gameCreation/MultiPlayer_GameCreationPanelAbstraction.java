package graphics.graphical_abstraction.panel.menu.gameCreation;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 */
public interface MultiPlayer_GameCreationPanelAbstraction extends MenuPanelAbstraction {

	public void createButtonAction();
	public void backButtonAction();

}
