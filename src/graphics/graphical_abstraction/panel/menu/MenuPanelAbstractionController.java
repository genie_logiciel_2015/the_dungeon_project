package graphics.graphical_abstraction.panel.menu;

/**
 * Created by dupriez on 05/12/15.
 *
 * The purpose of this class is to be used to create easily simple panels for the gui.
 * It can contain more general components than the MenuPanel one (in fact any JComponent).
 */

/**Not to be directly implements**/
public abstract class MenuPanelAbstractionController  {

	public MenuPanelAbstraction menuPanel;

	public void setTitlePanel(String title){
		this.menuPanel.setTitlePanel(title);
	}

}
