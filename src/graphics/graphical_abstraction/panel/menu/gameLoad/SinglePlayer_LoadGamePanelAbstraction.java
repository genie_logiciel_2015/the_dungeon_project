package graphics.graphical_abstraction.panel.menu.gameLoad;

import graphics.graphical_abstraction.GraphicsMasterAbstraction;
import graphics.graphical_abstraction.panel.menu.MenuPanelAbstraction;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.guiSkeleton.guiPanel.menuPanel.gameLoad.GameSavesViewer_SubPanel;
import graphics.ingame_input_listener.Input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player load a game session
 */
public interface SinglePlayer_LoadGamePanelAbstraction extends MenuPanelAbstraction {


	public void loadButtonAction();
	public void backButtonAction();

}
