package graphics.graphical_abstraction.panel.menu.gameLoad;

import graphics.graphical_abstraction.panel.menu.MenuPanelAbstractionController;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player load a game session
 */
public class SinglePlayer_LoadGamePanelAbstractionController extends MenuPanelAbstractionController {

	private SinglePlayer_LoadGamePanelAbstraction single_panel;

	public SinglePlayer_LoadGamePanelAbstractionController(SinglePlayer_LoadGamePanelAbstraction single_panel){
		this.menuPanel = single_panel;
		this.single_panel = single_panel;
	}

	public void loadButtonPressed(){
		this.single_panel.loadButtonAction();
	}
	public void backButtonPressed(){
		this.single_panel.backButtonAction();
	}

}
