package graphics.graphical_abstraction.panel.menu;

import graphics.guiSkeleton.GUIColorsAndFonts;
import graphics.guiSkeleton.GraphicsMaster;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.ButtonMaker;
import graphics.guiSkeleton.guiPanel.menuPanel.facilities.MenuPanel;
import graphics.ingame_input_listener.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dupriez on 01/01/16.
 */
public interface GameOverPanelAbstraction extends MenuPanelAbstraction {

	/*What to do when it have been pressed*/
	public void returnButtonAction();

}
