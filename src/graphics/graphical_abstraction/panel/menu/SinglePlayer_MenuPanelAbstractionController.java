package graphics.graphical_abstraction.panel.menu;

/**
 * Created by dupriez on 05/12/15.
 *
 * Used by the player to create or load a game session
 */
public class SinglePlayer_MenuPanelAbstractionController extends MenuPanelAbstractionController {

	private SinglePlayer_MenuPanelAbstraction singlePlayerMenuPanel;

	public SinglePlayer_MenuPanelAbstractionController(SinglePlayer_MenuPanelAbstraction singlePlayerMenuPanel){
		this.menuPanel = singlePlayerMenuPanel;
		this.singlePlayerMenuPanel = singlePlayerMenuPanel;
	}

	/*What to do when it have been pressed*/
	public void createGameButtonButtonPressed(){
		this.singlePlayerMenuPanel.createGameButtonButtonAction();
	}
	public void loadGameButtonButtonPressed(){
		this.singlePlayerMenuPanel.loadGameButtonButtonAction();
	}
	public void oldPlayButtonPressed(){
		this.singlePlayerMenuPanel.oldPlayButtonAction();
	}
	public void backButtonPressed(){
		this.singlePlayerMenuPanel.backButtonAction();
	}
	
}
