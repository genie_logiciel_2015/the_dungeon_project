package graphics.graphical_abstraction.panel.menu;

import graphics.graphical_abstraction.panel.PanelAbstraction;

/**
 * Created by dupriez on 05/12/15.
 *
 * The purpose of this class is to be used to create easily simple panels for the gui.
 * It can contain more general components than the MenuPanel one (in fact any JComponent).
 */

/**Not to be directly implements**/
public interface MenuPanelAbstraction extends PanelAbstraction {

	public void setTitlePanel(String title);

}
