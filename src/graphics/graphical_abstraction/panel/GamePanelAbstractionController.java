package graphics.graphical_abstraction.panel;

import core.relayer.Relayer;
import gameloop.LocalGameLoop;
import graphics.guiSkeleton.inputManagement.InputInterpreter;
import graphics.ingame_input_listener.Input;

/**
 * Created by etouss on 20/12/2015.
 */
public class GamePanelAbstractionController {
    private Relayer followedRelayer;
    private InputInterpreter MyInputInterpreter;
    private GamePanelAbstraction game_panel;
    private boolean spellReleased=true;
    private boolean attackReleased=true;

    public GamePanelAbstractionController(GamePanelAbstraction game_panel){
        this.game_panel = game_panel;
    }

    public void initialise(){
        followedRelayer = LocalGameLoop.getInstance().getFollowedRelayer();
        MyInputInterpreter = new InputInterpreter(followedRelayer);
    }

    /**
     * Fonction which interpret an input and tell the core what to do.
     * @param e input
     */
    public void keyPressed(Input e){
        switch(e){
            case LEFT_ARROW:
                MyInputInterpreter.setMoveXLeft(true);break;
            case UP_ARROW:
                MyInputInterpreter.setMoveYTop(true);break;
            case RIGHT_ARROW:
                MyInputInterpreter.setMoveXRight(true);break;
            case DOWN_ARROW:
                MyInputInterpreter.setMoveYBottom(true);break;
            case ABILITY_1:
                if(spellReleased){
                    MyInputInterpreter.tryToCastAbility(1);
                    spellReleased=false;
                }
                break;
            case ABILITY_2:
                if(spellReleased){
                    MyInputInterpreter.tryToCastAbility(2);
                    spellReleased=false;
                }
                break;
            case ABILITY_3:
                if(spellReleased){
                    MyInputInterpreter.tryToCastAbility(3);
                    spellReleased=false;
                }
                break;
            case ATTACK:
                if(attackReleased){
                    MyInputInterpreter.tryToCastAbility(0);
                    attackReleased=false;
                }
                break;
            default:
                break;
        }
    }
    /**
     * Fonction which interpret an input and tell the core what to do.
     * @param e input
     */
    public void keyReleased(Input e){
        switch(e){
            case LEFT_ARROW:
                MyInputInterpreter.setMoveXLeft(false);break;
            case UP_ARROW:
                MyInputInterpreter.setMoveYTop(false);break;
            case RIGHT_ARROW:
                MyInputInterpreter.setMoveXRight(false);break;
            case DOWN_ARROW:
                MyInputInterpreter.setMoveYBottom(false);break;
            case ATTACK:
                attackReleased=true;break;
            case ABILITY_1:
                spellReleased=true;break;
            case ABILITY_2:
                spellReleased=true;break;
            case ABILITY_3:
                spellReleased=true;break;
            default:
                break;
        }
    }

}
