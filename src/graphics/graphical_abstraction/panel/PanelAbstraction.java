package graphics.graphical_abstraction.panel;

import graphics.ingame_input_listener.Input;

/**
 * Created by etouss on 18/11/2015.
 */
public interface PanelAbstraction {

    void keyPressedHandler(Input e);
    void keyReleasedHandler(Input e);
    void keyTypedHandler(Input e);
    void initialise();
    void finalise();
    void repaint();
}
