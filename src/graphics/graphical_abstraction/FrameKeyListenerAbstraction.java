package graphics.graphical_abstraction;

/**
 * Created by etouss on 18/11/2015.
 */
public interface FrameKeyListenerAbstraction {
    /**
     * Method call to force checking if something have been clicked
     */
    public void check();
}
