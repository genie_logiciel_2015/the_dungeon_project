package graphics.graphical_abstraction;

import graphics.graphical_abstraction.panel.PanelAbstraction;

/**
 * Abstraction of the displayer
 */
public interface FrameAbstraction {
    /**
     * Displayer Object initialisation, linked to a key_listener
     * @param keyListenerAbstraction, the key listener linked to the frame
     */
    public void init(FrameKeyListenerAbstraction keyListenerAbstraction);

    /**
     * Remove a panel (an item to display) form the frame
     * @param panel the panel to remove
     */
    public void remove(PanelAbstraction panel);

    /**
     * Add a panel (an item to display) to the frame
     * @param panel the panel to add
     */
    public void add(PanelAbstraction panel);

    /**
     * Method call to refresh the screen.
     */
    public void repaint();
}
