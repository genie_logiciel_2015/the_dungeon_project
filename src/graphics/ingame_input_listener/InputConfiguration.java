package graphics.ingame_input_listener;

import assets.UsedForLoadingSprites;
import logging.Logging;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by etouss on 09/11/2015.
 */
public class InputConfiguration {

	/**
	 * Each action that can be performed in the game
	 * is firstly linked with some input which is
	 * converted to a formal one from the Input enum
	 */

	// initial keys that may be changed in the game
	private static int attack = KeyEvent.VK_A;
	private static int ability1 = KeyEvent.VK_Z;
	private static int ability2 = KeyEvent.VK_E;
	private static int ability3 = KeyEvent.VK_R;

	private static int escape = KeyEvent.VK_ESCAPE;
	private static int space = KeyEvent.VK_SPACE;

	private static int up = KeyEvent.VK_UP;
	private static int down = KeyEvent.VK_DOWN;
	private static int right = KeyEvent.VK_RIGHT;
	private static int left = KeyEvent.VK_LEFT;

	//keys special for terminal version
	private static int left_terminal = 'q';
	private static int right_terminal = 'd';
	private static int up_terminal = 'z';
	private static int down_terminal = 's';
	private static int escape_terminal = 'v';
	private static int attack_terminal = 'o';
	private static int ability1_terminal = 'k';
	private static int ability2_terminal = 'l';
	private static int ability3_terminal = 'm';

	/**
	 * The input are now persistent, stocked in memory in assets/configuration/configuration.txt file
	 * We just read the good lines in order to configure the inputs
	 */

	static public int CurrentLine=0;
	static{
		URL url= UsedForLoadingSprites.getInstance().getClass().getResource("./configuration/configuration.txt");
		if(url==null)
			throw new IllegalStateException("Configuration file assets/configuration/configuration.txt not found");
		try(BufferedReader br= new BufferedReader(new InputStreamReader(url.openStream()))){
			// Graphical inputs
			up=GetIntFromString(br.readLine());
			down=GetIntFromString(br.readLine());
			right=GetIntFromString(br.readLine());
			left=GetIntFromString(br.readLine());
			attack=GetIntFromString(br.readLine());
			ability1=GetIntFromString(br.readLine());
			ability2=GetIntFromString(br.readLine());
			ability3=GetIntFromString(br.readLine());
			escape=GetIntFromString(br.readLine());
			//Terminal inputs
			up_terminal=br.readLine().charAt(0);
			down_terminal=br.readLine().charAt(0);
			right_terminal=br.readLine().charAt(0);
			left_terminal=br.readLine().charAt(0);
			attack_terminal=br.readLine().charAt(0);
			ability1_terminal=br.readLine().charAt(0);
			ability2_terminal=br.readLine().charAt(0);
			ability3_terminal=br.readLine().charAt(0);
			escape_terminal=br.readLine().charAt(0);
		}catch(IOException e){
			errorMessage(e.getMessage());
			throw new IllegalStateException("Should not access this line");
		}
	}

	/**
	 * Beware, throw an exception with the given message !
	 * @param message
	 */
	private static void errorMessage(String message) {
		throw new IllegalStateException("Error parsing configuration: " + System.lineSeparator()+
				"Line"+CurrentLine + message);
	}

	/**
	 * This is use to update the configuration.txt file w.r.t. the potential modifications
	 * that have been made.
	 */

	public static void UpdateConfigurationWhenGameLeaves(){
		int numberOfInput=9;
		int LineToWrite=0;
		//2*numberOfInput because there are terminal and graphical versions
		String[] configurationText = new String[2*numberOfInput];
		Charset utf8 = StandardCharsets.UTF_8;

		//Updating Configuration text w.r.t current inputs before leaving the game
		//First graphical version inputs
		configurationText[LineToWrite]=GetFormalInputString(Input.UP_ARROW);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.DOWN_ARROW);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.RIGHT_ARROW);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.LEFT_ARROW);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.ATTACK);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.ABILITY_1);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.ABILITY_2);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.ABILITY_3);
		LineToWrite++;
		configurationText[LineToWrite]=GetFormalInputString(Input.ESCAPE);
		LineToWrite++;
		//Then terminal version inputs
		configurationText[LineToWrite]=String.valueOf(Character.toChars(up_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(down_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(right_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(left_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(attack_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(ability1_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(ability2_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(ability3_terminal));
		LineToWrite++;
		configurationText[LineToWrite]=String.valueOf(Character.toChars(escape_terminal));

		//Finally converting the array configurationText to a big string which is to replace configuration.txt
		List<String> lines = Arrays.asList(configurationText);
		try{
			Files.write(Paths.get("assets/configuration/configuration.txt"), lines, utf8);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * @param e : a key pressed
	 * @return the formal name linked to this input
	 */
	public static Input GetInput(KeyEvent e) {
		int event = e.getKeyCode();
		if (event == attack) {
			return Input.ATTACK;
		}
		if (event == ability1) {
			return Input.ABILITY_1;
		}
		if (event == ability2) {
			return Input.ABILITY_2;
		}
		if (event == ability3) {
			return Input.ABILITY_3;
		}
		if (event == escape) {
			return Input.ESCAPE;
		}
		if (event == up) {
			return Input.UP_ARROW;
		}
		if (event == down) {
			return Input.DOWN_ARROW;
		}
		if (event == right) {
			return Input.RIGHT_ARROW;
		}
		if (event == left) {
			return Input.LEFT_ARROW;
		}
		if (event == space) {
			return Input.SPACE;
		}
		return Input.NONE;
	}

	/**
	 * @param j : a keyEvent code
	 * @return the string of the character whose code is j
	 */
	private static String GetString(int j) {
		switch (j) {
			case KeyEvent.VK_A:
				return "a";
			case KeyEvent.VK_B:
				return "b";
			case KeyEvent.VK_C:
				return "c";
			case KeyEvent.VK_D:
				return "d";
			case KeyEvent.VK_E:
				return "e";
			case KeyEvent.VK_F:
				return "f";
			case KeyEvent.VK_G:
				return "g";
			case KeyEvent.VK_H:
				return "h";
			case KeyEvent.VK_I:
				return "i";
			case KeyEvent.VK_J:
				return "j";
			case KeyEvent.VK_K:
				return "k";
			case KeyEvent.VK_L:
				return "l";
			case KeyEvent.VK_M:
				return "m";
			case KeyEvent.VK_N:
				return "n";
			case KeyEvent.VK_O:
				return "o";
			case KeyEvent.VK_P:
				return "p";
			case KeyEvent.VK_Q:
				return "q";
			case KeyEvent.VK_R:
				return "r";
			case KeyEvent.VK_S:
				return "s";
			case KeyEvent.VK_T:
				return "t";
			case KeyEvent.VK_U:
				return "u";
			case KeyEvent.VK_V:
				return "v";
			case KeyEvent.VK_W:
				return "w";
			case KeyEvent.VK_X:
				return "x";
			case KeyEvent.VK_Y:
				return "y";
			case KeyEvent.VK_Z:
				return "z";
			case KeyEvent.VK_UP:
				return "Up arrow";
			case KeyEvent.VK_DOWN:
				return "Down arrow";
			case KeyEvent.VK_LEFT:
				return "Left arrow";
			case KeyEvent.VK_RIGHT:
				return "Right arrow";
			case KeyEvent.VK_NUMPAD0:
				return "0";
			case KeyEvent.VK_NUMPAD1:
				return "1";
			case KeyEvent.VK_NUMPAD2:
				return "2";
			case KeyEvent.VK_NUMPAD3:
				return "3";
			case KeyEvent.VK_NUMPAD4:
				return "4";
			case KeyEvent.VK_NUMPAD5:
				return "5";
			case KeyEvent.VK_NUMPAD6:
				return "6";
			case KeyEvent.VK_NUMPAD7:
				return "7";
			case KeyEvent.VK_NUMPAD8:
				return "8";
			case KeyEvent.VK_NUMPAD9:
				return "9";
			case KeyEvent.VK_F1:
				return "F1";
			case KeyEvent.VK_F2:
				return "F2";
			case KeyEvent.VK_F3:
				return "F3";
			case KeyEvent.VK_F4:
				return "F4";
			case KeyEvent.VK_F5:
				return "F5";
			case KeyEvent.VK_F6:
				return "F6";
			case KeyEvent.VK_F7:
				return "F7";
			case KeyEvent.VK_F8:
				return "F8";
			case KeyEvent.VK_F9:
				return "F9";
			case KeyEvent.VK_F10:
				return "F10";
			case KeyEvent.VK_F11:
				return "F11";
			case KeyEvent.VK_F12:
				return "F12";
			case KeyEvent.VK_ADD:
				return "+";
			case KeyEvent.VK_MINUS:
				return "-";
			case KeyEvent.VK_MULTIPLY:
				return "*";
			case KeyEvent.VK_LEFT_PARENTHESIS:
				return "(";
			case KeyEvent.VK_UNDERSCORE:
				return "_";
			case KeyEvent.VK_RIGHT_PARENTHESIS:
				return ")";
			case KeyEvent.VK_EQUALS:
				return "=";
			case KeyEvent.VK_COMMA:
				return ",";
			case KeyEvent.VK_SEMICOLON:
				return ";";
			case KeyEvent.VK_COLON:
				return ":";
			case KeyEvent.VK_EXCLAMATION_MARK:
				return "!";
			case KeyEvent.VK_CIRCUMFLEX:
				return "^";
			case KeyEvent.VK_DOLLAR:
				return "$";
			case KeyEvent.VK_SEPARATOR:
				return "/";
			case KeyEvent.VK_ENTER:
				return "Enter";
			case KeyEvent.VK_CAPS_LOCK:
				return "Caps lock";
			case KeyEvent.VK_ESCAPE:
				return "Escape";
			case KeyEvent.VK_ALT:
				return "Alt";
			case KeyEvent.VK_CONTROL:
				return "Ctrl";
			case KeyEvent.VK_SPACE:
				return "Space";
			case KeyEvent.VK_SHIFT:
				return "Shift";
			case KeyEvent.VK_TAB:
				return "Tab";
			case KeyEvent.VK_BACK_SPACE:
				return "Back space";
			default:
				throw new IllegalStateException("Unknown int : " + j);
		}
	}

	/**
	 * @param string a one character string representing a keyboard input
	 * @return the int linked to the keyEvent linked with the input underlying the string
	 */
	public static int GetIntFromString(String string) {
		if (string.equals("a")) {
			return KeyEvent.VK_A;
		}
		if (string.equals("b")) {
			return KeyEvent.VK_B;
		}
		if (string.equals("c")) {
			return KeyEvent.VK_C;
		}
		if (string.equals("d")) {
			return KeyEvent.VK_D;
		}
		if (string.equals("e")) {
			return KeyEvent.VK_E;
		}
		if (string.equals("f")) {
			return KeyEvent.VK_F;
		}
		if (string.equals("g")) {
			return KeyEvent.VK_G;
		}
		if (string.equals("h")) {
			return KeyEvent.VK_H;
		}
		if (string.equals("i")) {
			return KeyEvent.VK_I;
		}
		if (string.equals("j")) {
			return KeyEvent.VK_J;
		}
		if (string.equals("k")) {
			return KeyEvent.VK_K;
		}
		if (string.equals("l")) {
			return KeyEvent.VK_L;
		}
		if (string.equals("m")) {
			return KeyEvent.VK_M;
		}
		if (string.equals("n")) {
			return KeyEvent.VK_N;
		}
		if (string.equals("o")) {
			return KeyEvent.VK_O;
		}
		if (string.equals("p")) {
			return KeyEvent.VK_P;
		}
		if (string.equals("q")) {
			return KeyEvent.VK_Q;
		}
		if (string.equals("r")) {
			return KeyEvent.VK_R;
		}
		if (string.equals("s")) {
			return KeyEvent.VK_S;
		}
		if (string.equals("t")) {
			return KeyEvent.VK_T;
		}
		if (string.equals("u")) {
			return KeyEvent.VK_U;
		}
		if (string.equals("v")) {
			return KeyEvent.VK_V;
		}
		if (string.equals("w")) {
			return KeyEvent.VK_W;
		}
		if (string.equals("x")) {
			return KeyEvent.VK_X;
		}
		if (string.equals("y")) {
			return KeyEvent.VK_Y;
		}
		if (string.equals("z")) {
			return KeyEvent.VK_Z;
		}
		if (string.equals("Enter")) {
			return KeyEvent.VK_ENTER;
		}
		if (string.equals("Space")) {
			return KeyEvent.VK_SPACE;
		}
		if (string.equals("Back space")) {
			return KeyEvent.VK_BACK_SPACE;
		}
		if (string.equals("Tab")) {
			return KeyEvent.VK_TAB;
		}
		if (string.equals("Shift")) {
			return KeyEvent.VK_Z;
		}
		if (string.equals("Caps lock")) {
			return KeyEvent.VK_CAPS_LOCK;
		}
		if (string.equals("Escape")) {
			return KeyEvent.VK_ESCAPE;
		}
		if (string.equals("Alt")) {
			return KeyEvent.VK_ALT;
		}
		if (string.equals("Ctrl")) {
			return KeyEvent.VK_CONTROL;
		}
		if (string.equals("Left arrow")) {
			return KeyEvent.VK_LEFT;
		}
		if (string.equals("Right arrow")) {
			return KeyEvent.VK_RIGHT;
		}
		if (string.equals("Up arrow")) {
			return KeyEvent.VK_UP;
		}
		if (string.equals("Down arrow")) {
			return KeyEvent.VK_DOWN;
		}
		if (string.equals("0")) {
			return KeyEvent.VK_NUMPAD0;
		}
		if (string.equals("1")) {
			return KeyEvent.VK_NUMPAD1;
		}
		if (string.equals("2")) {
			return KeyEvent.VK_NUMPAD2;
		}
		if (string.equals("3")) {
			return KeyEvent.VK_NUMPAD3;
		}
		if (string.equals("4")) {
			return KeyEvent.VK_NUMPAD4;
		}
		if (string.equals("5")) {
			return KeyEvent.VK_NUMPAD5;
		}
		if (string.equals("6")) {
			return KeyEvent.VK_NUMPAD6;
		}
		if (string.equals("7")) {
			return KeyEvent.VK_NUMPAD7;
		}
		if (string.equals("8")) {
			return KeyEvent.VK_NUMPAD8;
		}
		if (string.equals("9")) {
			return KeyEvent.VK_NUMPAD9;
		}
		if (string.equals(";")) {
			return KeyEvent.VK_SEMICOLON;
		}
		if (string.equals("=")) {
			return KeyEvent.VK_EQUALS;
		}
		if (string.equals("*")) {
			return KeyEvent.VK_MULTIPLY;
		}
		if (string.equals("^")) {
			return KeyEvent.VK_CIRCUMFLEX;
		}
		if (string.equals(":")) {
			return KeyEvent.VK_COLON;
		}
		if (string.equals("!")) {
			return KeyEvent.VK_EXCLAMATION_MARK;
		}
		if (string.equals("(")) {
			return KeyEvent.VK_LEFT_PARENTHESIS;
		}
		if (string.equals(")")) {
			return KeyEvent.VK_RIGHT_PARENTHESIS;
		}
		if (string.equals("_")) {
			return KeyEvent.VK_UNDERSCORE;
		}
		if (string.equals("$")) {
			return KeyEvent.VK_DOLLAR;
		}
		if (string.equals(",")) {
			return KeyEvent.VK_COMMA;
		}
		if (string.equals("-")) {
			return KeyEvent.VK_MINUS;
		}
		if (string.equals("+")) {
			return KeyEvent.VK_ADD;
		}
		if (string.equals("/")) {
			return KeyEvent.VK_SEPARATOR;
		}
		if (string.equals("F1")) {
			return KeyEvent.VK_F1;
		}
		if (string.equals("F2")) {
			return KeyEvent.VK_F2;
		}
		if (string.equals("F3")) {
			return KeyEvent.VK_F3;
		}
		if (string.equals("F4")) {
			return KeyEvent.VK_F4;
		}
		if (string.equals("F5")) {
			return KeyEvent.VK_F5;
		}
		if (string.equals("F6")) {
			return KeyEvent.VK_F6;
		}
		if (string.equals("F7")) {
			return KeyEvent.VK_F7;
		}
		if (string.equals("F8")) {
			return KeyEvent.VK_F8;
		}
		if (string.equals("F9")) {
			return KeyEvent.VK_F9;
		}
		if (string.equals("F10")) {
			return KeyEvent.VK_F10;
		}
		if (string.equals("F11")) {
			return KeyEvent.VK_F11;
		}
		if (string.equals("F12")) {
			return KeyEvent.VK_F12;
		}
		throw new IllegalStateException("Unknown input : " + string);
	}

	/**
	 * @param input : a formal Input
	 * @return the string of the character linked to input
	 */
	public static String GetFormalInputString(Input input) {
		switch (input) {
			case ATTACK:
				return GetString(attack);
			case ABILITY_1:
				return GetString(ability1);
			case ABILITY_2:
				return GetString(ability2);
			case ABILITY_3:
				return GetString(ability3);
			case ESCAPE:
				return GetString(escape);
			case UP_ARROW:
				return GetString(up);
			case DOWN_ARROW:
				return GetString(down);
			case RIGHT_ARROW:
				return GetString(right);
			case LEFT_ARROW:
				return GetString(left);
			case NONE:
				return "";
			default:
				throw new IllegalStateException("Unknown input : " + input);
		}
	}

	/**
	 * @param input : a formal Input
	 * @return the string of the character linked to input
	 */
	//Will only be used for the terminal version
	public static int GetFormalInputIntTerminal(Input input) {
		switch (input) {
			case ATTACK:
				return attack_terminal;
			case ABILITY_1:
				return ability1_terminal;
			case ABILITY_2:
				return ability2_terminal;
			case ABILITY_3:
				return ability3_terminal;
			case ESCAPE:
				return escape_terminal;
			case LEFT_ARROW:
				return left_terminal;
			case RIGHT_ARROW:
				return right_terminal;
			case UP_ARROW:
				return up_terminal;
			case DOWN_ARROW:
				return down_terminal;
			case NONE:
				return 0;
			default:
				throw new IllegalStateException("Unknown input : " + input);
		}
	}

	/**
	 * @param input  : a formal input
	 * @param string : a letter representing the key to which we want to link the input
	 *               the key pressed linked to "input" is changed to "j"
	 */
	public static void ChangeInput(Input input, String string) {
		Logging.getInstance().getLogger().info("Got here");
		int j = GetIntFromString(string);
		Logging.getInstance().getLogger().info(j+"");
		switch (input) {
			case ATTACK:
				attack = j;
				break;
			case ABILITY_1:
				ability1 = j;
				break;
			case ABILITY_2:
				ability2 = j;
				break;
			case ABILITY_3:
				ability3 = j;
				break;
			case ESCAPE:
				escape = j;
				break;
			case UP_ARROW:
				up = j;
				break;
			case DOWN_ARROW:
				down = j;
				break;
			case RIGHT_ARROW:
				right = j;
				break;
			case LEFT_ARROW:
				left = j;
				break;
			case NONE:
				return;
			default:
				throw new IllegalStateException("Unknown input: " + input);
		}
	}

	/**
	 * changes the key linked to the formal Input input is changed into j
	 * @param input
	 * @param j
	 */
	public static void ChangeInputTerminal(Input input, int j) {
		switch (input) {
			case ATTACK:
				attack_terminal = j;
				break;
			case ABILITY_1:
				ability1_terminal = j;
				break;
			case ABILITY_2:
				ability2_terminal = j;
				break;
			case ABILITY_3:
				ability3_terminal = j;
				break;
			case ESCAPE:
				escape_terminal = j;
				break;
			case UP_ARROW:
				up_terminal = j;
				break;
			case DOWN_ARROW:
				down_terminal = j;
				break;
			case RIGHT_ARROW:
				right_terminal = j;
				break;
			case LEFT_ARROW:
				left_terminal = j;
				break;
			case NONE:
				return;
			default:
				throw new IllegalStateException("Unknown input: " + input);
		}
	}
}
