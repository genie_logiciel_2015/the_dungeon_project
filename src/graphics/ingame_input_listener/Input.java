package graphics.ingame_input_listener;

/**
 * Created by zobi on 18/11/15.
 */

/** When an input is handled, we convert it to a formal
 *  name according to the InputConfiguration, i.e
 *  if "a" is supposed to give a "move left", then this
 *  is linked to LEFT_ARROW */
public enum Input {
    ATTACK, ABILITY_1, ABILITY_2, ABILITY_3, LEFT_ARROW ,RIGHT_ARROW ,UP_ARROW , DOWN_ARROW, ESCAPE, SPACE, NONE
}
