package logging;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.*;

/**
 * Created by myriam on 01/01/16.
 */
public class Logging implements Serializable{
    static final long serialVersionUID = 1L;
    private static Logger LOG;
    private Handler fh;
    private Handler ch;
    private Logging(){
        LOG = Logger.getLogger("Donjon");
        LOG.setLevel(Level.INFO);
        try{
            fh = new FileHandler("log/Logging.log",true);
            fh.setFormatter(new SimpleFormatter());
            LOG.addHandler(fh);
            //ch = new ConsoleHandler();
            //ch.setFormatter(new SimpleFormatter());
            //LOG.addHandler(ch);
        } catch (SecurityException | IOException e) {
            LOG.severe("Impossible d'accéder au fichier de log");
        }
    }

    private static Logging ourInstance = new Logging();
    public static Logging getInstance(){
        return ourInstance;
    }

    public Logger getLogger(){
        return LOG;
    }
}
