package map_generation.map;

import java.util.Random;

import core.gamestate.SpeciesArray;
import core.zone.Point;
import map_generation.tiles.TileBuilder;
import map_generation.tiles.TileType;

/**
 * 
 * @author Lucas Delcros
 * Represents room 
 */
public final class RoomBuilder {
	
	public static enum RoomType{START, END, NORMAL, TORCHES, FULL_LAVA, CHESTS};	
	private static int NB_ENTITIES = 0;//Total number of entities in the game
	
	private Surface surface;
	public RoomType type;
	
	
	public RoomBuilder(Surface surface, RoomType type) {
		this.surface = surface;
		this.type = type;
	}
	
	/**
	 * Resets the static parameters of the class
	 * Must be use before generating a new Map 
	 */
	public static void reset(){
		NB_ENTITIES = 0;
	}
	
	public void setType(RoomType type){
		this.type = type;
	}
	public Surface getSurface(){
		return surface;
	}
	
	/**
	 * Add a lava lake in the room
	 * @param map
	 */
	private void addFullLava(TileBuilder[][] map){
		Random r = new Random();
		int dw = r.nextInt((surface.width-2)/2)+1, dh = r.nextInt((surface.height-2)/2)+1;
		Surface s = new Surface(surface.i1+dh, surface.j1+dw, surface.i2-dh, surface.j2-dw);
		s.fillSurfaceWith(map, TileType.WATER);
	}
	
	/**
	 * Add stairs in the room
	 * @param map
	 * @param mapB
	 */
	private void addStairs(TileBuilder[][] map, MapBuilder mapB){
		int posY=(surface.j1+surface.j2)/2;
		int posX=(surface.i1+surface.i2)/2;
		map[posY][posX].setType(TileType.STAIRS);
		mapB.setStairPosition(new MapPoint(posY, posX));
	}
	
	/**
	 * Add four torches in the room
	 * @param map
	 */
	private void addTorches(TileBuilder[][] map){
		map[surface.j1 + 2][surface.i1 + 2].setType(TileType.TORCH);
		map[surface.j2 - 2][surface.i1 + 2].setType(TileType.TORCH);
		map[surface.j1 + 2][surface.i2 - 2].setType(TileType.TORCH);
		map[surface.j2 - 2][surface.i2 - 2].setType(TileType.TORCH);
	}
	
	/**
	 * Change a NORMAL room to a special room (LAVA, TORCHES) with probability p
	 * @param p
	 */
	public void setRandomlySpecial(double p){
		Random r = new Random();
		if(type == RoomType.NORMAL && r.nextDouble() <= p){
			double pp = r.nextDouble();
			if( pp <= 0.7 )setType(RoomType.FULL_LAVA);
			else setType(RoomType.TORCHES);
		} 
	}
	
	/**
	 * Generates entities
	 * @param tb
	 * @param mapB
	 * @param specieName the name of the entity
	 */
	private void generateEntities(TileBuilder[][] tb, MapBuilder mapB, String specieName) { 
		Random r = new Random();
		int nbRooms = mapB.getNbRooms(), ratio = Integer.max((mapB.getMaxEntities()/nbRooms), 1);
		int srf = Integer.max((int)((surface.width*surface.height)*(0.01)), 2);
		int nbEntities = r.nextInt(ratio+srf)+(int)((srf)/2);
		for(int i=0;i<nbEntities && NB_ENTITIES <= mapB.getMaxEntities();i++) {
			int ii=surface.i1+r.nextInt(surface.height);
			int jj=surface.j1+r.nextInt(surface.width);
			int posY=ii*Point.TileScale;
	        int posX=jj*Point.TileScale;
	        if(SpeciesArray.canSpawn(tb[jj][ii].getType(), specieName)) {
	        	mapB.addEntity(SpeciesArray.create(posX,posY,100,specieName,"Monster "+NB_ENTITIES));
	        	NB_ENTITIES++;
	        }
		}
	}
	
	/**
	 * Builds the room according to its type and generates monsters according to the given patern
	 * @param map
	 * @param mapB
	 * @param iPattern the id of the monster spawning patern
	 */
	public void build(TileBuilder[][] map, MapBuilder mapB, int iPattern){
		surface.fillSurfaceWith(map, TileType.GROUND);
		switch (type){
			case FULL_LAVA : 
				addFullLava(map);
				generateEntities(map, mapB, "Ponyta");
				break;
			case START :
				int posY=(surface.j1+surface.j2)/2;
				int posX=(surface.i1+surface.i2)/2;
				mapB.setPositionPlayerAtStart(new MapPoint(posY, posX));
				break;
			case END:
				surface.fillSurfaceWith(map, TileType.OBSIDIAN);
				addStairs(map, mapB);
				mapB.addEntity(SpeciesArray.create((((surface.j1+surface.j2)/2)-1)*Point.TileScale,(((surface.i1+surface.i2)/2)-1)*Point.TileScale,100,"Diagla","Monster "+mapB.getMaxEntities()+1));
				break;
			case CHESTS:
				addTorches(map);
				map[(surface.j1+surface.j2)/2][(surface.i1+surface.i2)/2].setType(TileType.CHEST);
				break;
			case TORCHES:
				addTorches(map);
			default :
				String specieName="Ronflex";
				MapPattern pattern=MapPatterns.patterns[iPattern];
				Random r=new Random();
				int nb=r.nextInt(pattern.getTotal());
				int sum=0;
				for(int i=0;i<pattern.getNbMonster();i++) {
					if(nb>=sum && nb<sum+pattern.getMonster(i)) specieName=MapPattern.idMonster[i];
					sum+=pattern.getMonster(i);
				}
				generateEntities(map, mapB, specieName);
				
		}
	}
}
