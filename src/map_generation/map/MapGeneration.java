package map_generation.map;

import java.io.Serializable;
import java.util.Random;

import map_generation.map.RoomBuilder.RoomType;

/**
 * 
 * @author mapgen team
 * Main class to generate a map
 */
public class MapGeneration implements Serializable {
	private static final long serialVersionUID = -6471655849722890871L;

	/**
	 * Build a correct map
	 * @param meanRoomSize
	 * @param numberOfRooms
	 * @return
	 */
	public static Map mapGeneration(int meanRoomSize, int numberOfRooms) {
		boolean bb=true;
		Map map = new Map();
		while(bb) {
			map = roomsRandomGeneration(meanRoomSize,numberOfRooms);
			if (map != null && map.getPositionPlayerStart() != null) bb=false;//check correctness 
		}
		return(map);
	}
	
	/**
	 *  Main function to create a map
	 * @param meanRoomSize
	 * @param numberOfRooms
	 * @return
	 */
	private static Map roomsRandomGeneration(int meanRoomSize, int numberOfRooms) {
		// This is for the moment the InputConfigurationFrame method to create a map
		// It first creates the map attributes and then calls the MapBuilder to build the map with those
		int heightMap=(meanRoomSize+6)*(int)Math.sqrt(numberOfRooms);
		int widthMap=(meanRoomSize+6)*(int)Math.sqrt(numberOfRooms);
		MapBuilder map = new MapBuilder();
		Random r = new Random();
		RoomBuilder[] rooms = new RoomBuilder[numberOfRooms];
		int numberOfRoomsCreated=0;
		int nbIteration=0;

		// Creation of the rooms
		while (numberOfRoomsCreated<numberOfRooms) {
			nbIteration++;
			if (nbIteration>500*numberOfRooms) { // The size of the map is initially 10*10 and while in 2000 iterations the rooms aren't created the size increases by a factor of 1.1
				heightMap=(int) ((heightMap)*1.1);
				widthMap=(int) ((widthMap)*1.1);
				numberOfRoomsCreated=0;
				nbIteration=0;
			}
			int height=(int)(meanRoomSize*(1+r.nextGaussian()));
			int width=(int)(meanRoomSize*(1+r.nextGaussian()));
			int positionY=r.nextInt(heightMap);
			int positionX=r.nextInt(widthMap);
			// The positions and sizes of the rooms are choosen randomly
			if (Math.abs(height-width)<meanRoomSize && height>=8 && width>=8 && positionY>0 && positionX>0 && positionY+height<heightMap && positionX+width<widthMap) {
				boolean bb=true;
				for (int i=0;i<numberOfRoomsCreated;i++) { // Test of intersection with a room already created
					// There's a Math.minimum distance between two rooms of 4 tiles
					if(positionY<=rooms[i].getSurface().j2+6 && positionY+height-1>=rooms[i].getSurface().j1-6 && positionX<=rooms[i].getSurface().i2+6 && positionX+width-1>=rooms[i].getSurface().i1-6) bb=false;
				}
				if (bb) { // If the current room fits with the others we had it
					Surface room =  new Surface(positionX, positionY, positionX+width-1, positionY+height-1);
					rooms[numberOfRoomsCreated] = new RoomBuilder(room, RoomType.NORMAL);
					numberOfRoomsCreated++;
				}
			}
		}
		RoomBuilder startRoom=SurfacesMapGeneration.defineStartPosition(rooms);
		map.addRooms(rooms, startRoom);
		SurfacesMapGeneration.addCorridorsByMST(rooms, map); // Creation of the set of corridors
		//		defineStairsPosition(map,heightMap,widthMap,rooms,startRoom);

		map.setHeight(heightMap);
		map.setWidth(widthMap);

		for (RoomBuilder room : rooms) {
			room.setRandomlySpecial(0.3);
		}
		return map.build();
	}
	
	/**
	 * 
	 * @param size
	 * @return A map containing a single room
	 */
	public static Map simpleRoom(int size) {
		// This is a very simple model of a map that contains only one squared room of side size
		MapBuilder map = new MapBuilder();
		map.setHeight(size);
		map.setWidth(size);
		Surface room=new Surface(2,2,size-3,size-3);
		RoomBuilder[] rooms= {new RoomBuilder(room, RoomType.NORMAL)};
		RoomBuilder startRoom=SurfacesMapGeneration.defineStartPosition(rooms);
		map.addRooms(rooms, startRoom);
		SurfacesMapGeneration.addCorridorsByMST(rooms, map);
		return(map.build());
	}
	
	/**
	 * Build a map of size size
	 * @param size
	 * @return
	 */
	public static Map mapGeneration(MapSize size){
		switch(size){
		case SMALL :
			return mapGeneration(15,10);
		case MEDIUM :
			return mapGeneration(15,20);
		case BIG :
			return mapGeneration(17,30);
		default :
			return mapGeneration(15,20);
		}
	}
}
