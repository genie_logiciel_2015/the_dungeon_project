package map_generation.map;

import java.io.Serializable;

import map_generation.tiles.TileBuilder;
import map_generation.tiles.TileType;

/**
 * 
 * @author Lucas Delcros
 * Represents a surface 
 */
public class Surface implements Serializable{
	
	private static final long serialVersionUID = -1607241988633571053L;
	public final int i1, i2, j1, j2, height, width;
	
	/**
	 * 
	 * @param i1 the i position of the upper left corner
	 * @param j1 the j position of the upper left corner
	 * @param i2 the i position of the down right corner
	 * @param j2 the j position of the down right corner
	 */
	public Surface(int i1, int j1, int i2, int j2) {
		this.i1 = i1; 
		this.i2 = i2;
		this.j1 = j1;
		this.j2 = j2;
		width = j2-j1+1;
		height = i2-i1+1;
		checkSurface();
	}
	
	/**
	 * Checks if a surface is coherent
	 */
	private void checkSurface(){
		if (!(height>0 && width>0)) throw new IllegalArgumentException("wrong surface i1 "+i1+" j1 "+j1+" i2 "+i2+" j2 "+j2);
	}
	
	/**
	 * 
	 * @param mp
	 * @return true if the given point is on the surface
	 */
	public boolean isOnSurface(MapPoint mp){
		return mp.getI() >= i1 && mp.getI() <= i2 && mp.getJ() >= j1 && mp.getJ() <= j2;
	}
	
	
	/**
	 * Implements Bresenham's algorithm to make a line of walls between a and b;
	 * @param tilebs
	 * @param xa
	 * @param ya
	 * @param xb
	 * @param yb
	 */
	private static void linkNodes(TileBuilder[][] tilebs, TileType type, int xa, int ya, int xb, int yb){
        int d = 0;
        int dy = Math.abs(yb - ya), dx = Math.abs(xb - xa), dy2 = (dy << 1), dx2 = (dx << 1); 
        int ix = xa < xb ? 1 : -1, iy = ya < yb ? 1 : -1;
 
        if (dy <= dx) {
            for (;;) {
            	tilebs[xa][ya].setType(type);
                if (xa == xb)
                    break;
                xa += ix;
                d += dy2;
                if (d > dx) {
                    ya += iy;
                    d -= dx2;
                }
            }
        } else {
            for (;;) {
                tilebs[xa][ya].setType(type);
                if (ya == yb)
                    break;
                ya += iy;
                d += dx2;
                if (d > dy) {
                    xa += ix;
                    d -= dy2;
                }
            }
        }
	}
	
	
	/**
	 * Surround a surface with the given type
	 * @param map
	 * @param surface
	 * @param type 
	 */
	public void surroundWith(TileBuilder[][] map, TileType type){
		final int x1 = i1 - 1, x2 = i2 +1;
		final int y1 = j1 - 1, y2 = j2 +1;
		
		linkNodes(map, type, y1, x1, y2, x1);
		linkNodes(map, type, y2, x1, y2, x2);
		linkNodes(map, type, y2, x2, y1, x2);
		linkNodes(map, type, y1, x2, y1, x1);
		
	}
	
	/**
	 * Fills a surface with the given type 
	 * @param map
	 * @param surface
	 * @param type 
	 */
	public void fillSurfaceWith(TileBuilder[][] map, TileType tt){
		for (int i = j1; i <= j2; i++) {
			for (int j = i1; j <= i2; j++) {
				map[i][j].setType(tt);
			}
		}
	}
}
