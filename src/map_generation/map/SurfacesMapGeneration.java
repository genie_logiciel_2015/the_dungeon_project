package map_generation.map;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

import map_generation.map.RoomBuilder.RoomType;
import map_generation.tiles.TileBuilder;
/**
 * @author mapgen team
 * Contains static functions that create a map of abstract surfaces
 *
 */
public class SurfacesMapGeneration implements Serializable{

	private static final long serialVersionUID = 5179076009464463054L;
	
	/**
	 * Create a new map of tileBuilders
	 * @param h
	 * @param w
	 * @return
	 */
	public static TileBuilder[][] emptyTiles(int h, int w){
		TileBuilder[][] tilebs = new TileBuilder[h][w];
		for(int i=0;i<h;i++) {
			for(int j=0;j<w;j++) {
				tilebs[i][j]=new TileBuilder();
			}
		}
		return tilebs;
	}
	
	/**
	 * Define a starting room randomly
	 * @param rooms
	 * @return
	 */
	public static RoomBuilder defineStartPosition(RoomBuilder[] rooms) {
		Random r = new Random();
		int idRoom=r.nextInt(rooms.length);
		rooms[idRoom].setType(RoomType.START);
		return rooms[idRoom];
	}
	
	/**
	 * Gives the distance between surface1 and surface2
	 * @param surface1
	 * @param surface2
	 * @return
	 */
	public static int distanceBetweenTwoSurface(Surface surface1,Surface surface2) {
		// Method that computes the distance between two rooms
		int dY=Math.max(surface1.j1-surface2.j2+2,0)+Math.max(surface2.j1-surface1.j2+2,0);
		int dX=Math.max(surface1.i1-surface2.i2+2,0)+Math.max(surface2.i1-surface1.i2+2,0);
		return (dY>0 && dX>0)?dX+dY+4:dX+dY; // There's an extra to the distance between rooms that aren't facing each other horizontally or vertically (to avoid corridors with corners in the MST when possible)
	}
	
	/**
	 * Create corridor(s) between two rooms
	 * If the two rooms are facing each other there will be only one corridor
	 * Other wise there will be two corridor since the rooms wills be linked by a "corner corridor"
	 * @param room1
	 * @param room2
	 * @return
	 */
	public static LinkedList<Surface> creationCorridor(Surface room1,Surface room2) {
		// This method creates a corridor between two rooms
		int dNorth=Math.max(room2.j1-room1.j2,0);
		int dY=Math.max(room2.j1-room1.j2+1,0)+Math.max(room1.j1-room2.j2+1,0);
		int dX=Math.max(room2.i1-room1.i2+1,0)+Math.max(room1.i1-room2.i2+1,0);
		LinkedList<Surface> corridors=new LinkedList<>();
		if (dX==0) {
			// Case where the rooms are facing each other vertically
			int xTarget=(Math.max(room1.i1,room2.i1)+Math.min(room1.i2,room2.i2))/2;
			int yTop=Math.min(room1.j2,room2.j2);
			int yBottom=Math.max(room1.j1,room2.j1);
			Surface corridor = new Surface(xTarget, yTop, xTarget+1, yBottom);
			corridors.addLast(corridor);
			return corridors;
		}
		else if (dY==0) {
			// Case where the rooms are facing each other vertically
			int yTarget=(Math.max(room1.j1,room2.j1)+Math.min(room1.j2,room2.j2))/2;
			int xLeft=Math.min(room1.i2,room2.i2);
			int xRight=Math.max(room1.i1,room2.i1);
			Surface corridor = new Surface(xLeft, yTarget, xRight, yTarget+1);
			corridors.addLast(corridor);
			return corridors;
		}
		else {
			int xTarget=-1;
			int yTarget=-1;
			if(dNorth<=0){
				Surface t = room1;
				room1 = room2;
				room2 = t;
			}
			xTarget=(room1.i1+room1.i2)/2;
			yTarget=(room2.j1+room2.j2)/2;
			Surface corridor1 = new Surface(xTarget, Math.min(room1.j2, yTarget), xTarget+1, Math.max(room1.j2, yTarget));
			corridors.addLast(corridor1);
			if(xTarget<room2.i1) {
				Surface corridor2 = new Surface(xTarget, yTarget-1, room2.i1, yTarget);
				corridors.addLast(corridor2);
			}
			else {
				Surface corridor2 = new Surface(room2.i2, yTarget-1, xTarget, yTarget);
				corridors.addLast(corridor2);
			}
			return(corridors);
		}
	}
	
	/**
	 * Add corridors using MST on the full graph of rooms
	 * If two rooms are close enough there will also be linked 
	 * @param rooms
	 * @param map
	 */
	public static void addCorridorsByMST(RoomBuilder[] rooms, MapBuilder map) {
		// Considering the graph made by the rooms with edges corresponding to the distance between any two of them
		// I find the MST (Math.minimum SpamMath.ming Tree) of this graph in order to create a first set of corridors that connects all the rooms
		int nbRooms=rooms.length;
		int[][] distance=new int[nbRooms][nbRooms];
		for (int i=0;i<nbRooms;i++) {
			for (int j=i+1;j<nbRooms;j++) {
				int d=distanceBetweenTwoSurface(rooms[i].getSurface(),rooms[j].getSurface());
				distance[i][j]=d;
				distance[j][i]=d;
				if(d<12) { // If we find two rooms with a small distance in between, we add a corridor anyway, independantly of the MST
					LinkedList<Surface> corridorsToAdd=creationCorridor(rooms[i].getSurface(),rooms[j].getSurface());
					for (Surface corridor : corridorsToAdd) {
						if(corridorsToAdd != null)map.addCorridor(corridor, rooms[i], rooms[j]);

					}
				}
			}
		}
		boolean[] used=new boolean[nbRooms];
		for (int k=0;k<nbRooms;k++) used[k]=false;
		used[0]=true;
		int nbConnected=1;
		while(nbConnected<nbRooms) {
			int dmin=10000000;
			int node1=-1;
			int node2=-1;
			for (int i=0;i<nbRooms;i++) {
				if (used[i]) {
					for (int j=0;j<nbRooms;j++) {
						if ( (!used[j]) && distance[i][j]<dmin) {
							dmin=distance[i][j];
							node1=i;
							node2=j;
						}
					}
				}
			}
			used[node2]=true;
			nbConnected++;
			LinkedList<Surface> corridorsToAdd=creationCorridor(rooms[node1].getSurface(),rooms[node2].getSurface());
			for (Surface corridor : corridorsToAdd) {
				if(corridorsToAdd != null)map.addCorridor(corridor, rooms[node1], rooms[node2]);

			}
		}
	}
}
