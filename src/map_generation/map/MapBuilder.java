package map_generation.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import map_generation.map.RoomBuilder.RoomType;
import map_generation.tiles.TileBuilder;
import map_generation.tiles.TileType;
import core.gamestate.Entity;

/**
 * @author mapgen team
 * 
 * Builder for the class Map
 *
 */
public final class MapBuilder implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = -3440045962055454975L;

	private ArrayList<RoomBuilder> rooms;
	private ArrayList<Surface> corridors;
	private ArrayList<Entity> entities;
	private RoomGraph roomTree;
	private MapPoint positionPlayerAtStart;
	private MapPoint stairsPosition;
	private int height = 0, width = 0, maxEntities = -1;

	public MapBuilder() {
		corridors = new ArrayList<>();
		entities = new ArrayList<Entity>();
	}
	
	/**
	 * Add rooms to the build map and create a graph with these rooms
	 * @param rooms 
	 * @param start the starting room for the player
	 */
	public void addRooms(RoomBuilder[] rooms, RoomBuilder start){
		this.rooms = new ArrayList<RoomBuilder>(Arrays.asList(rooms));
		roomTree = new RoomGraph(start, this.rooms);
	}
	
	/**
	 * Add a corridor to the building map and an edge in the room graph
	 * If there is all-ready a corridor between this rooms nothing will be changed
	 * @param corridor the corridor between room1  and room2
	 * @param room1
	 * @param room2
	 */
	public void addCorridor(Surface corridor, RoomBuilder room1, RoomBuilder room2){
			corridors.add(corridor);
		if(!roomTree.areLinked(room1, room2)){
			roomTree.linkRooms(room1, room2);
		}
	}
	
	public void addEntity(Entity entity){
		entities.add(entity);
	}
	public int getMaxEntities(){
		return maxEntities;
	}
	public int getNbRooms(){
		return rooms.size();
	}
	
	/**
	 * Generates the ending room.
	 * The ending room is the farthest dead-end room from the starting room
	 */
	private void generateEndRoom(){
		RoomBuilder end=roomTree.getMostFarNode();
		end.setType(RoomType.END);
	}
	
	/**
	 * Generates all the chest rooms
	 * The chest rooms are the dead-end rooms in the graph (ending room excluded)
	 */
	private void generateChestRooms(){
		for (RoomBuilder room : roomTree.getDeadEnds()) {
			room.setType(RoomType.CHESTS);
		}
	}
	public void setPositionPlayerAtStart(MapPoint positionPlayerAtStart) {
		this.positionPlayerAtStart = positionPlayerAtStart;
	}

	public void setStairPosition(MapPoint stairPosition){
		this.stairsPosition = stairPosition;
	}
	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Build the map
	 * @return The map
	 */
	public Map build(){
		generateChestRooms();
		generateEndRoom();
		if (!roomTree.everyRoomIsReachable()) return null;
		TileBuilder[][] map = SurfacesMapGeneration.emptyTiles(height, width);
		int nbRooms=rooms.size();
		Random r=new Random();
		MapPatterns.read();
		RoomBuilder.reset();
		maxEntities=(int) (nbRooms*(3+r.nextGaussian()));
		for (RoomBuilder room : rooms) {
			room.getSurface().surroundWith(map, TileType.WALL);
		}
		for (Surface surface: corridors) {
			surface.surroundWith(map, TileType.WALL);
		}
		//rooms.get(3).setType(RoomType.START);
		for (Surface corridor : corridors) {
			corridor.fillSurfaceWith(map, TileType.GROUND);
		}
		for (RoomBuilder room : rooms) {
			int iPattern=r.nextInt(MapPatterns.patterns.length);
			room.build(map, this, iPattern);
		}

		Surface[] rooms = new Surface[this.rooms.size()];
		for (int i = 0; i < rooms.length; i++) {
			rooms[i] = this.rooms.get(i).getSurface();
		}
		Entity[] entities = this.entities.toArray(new Entity[this.entities.size()]);
		return new Map(TileBuilder.buildTiles(map), corridors,entities, positionPlayerAtStart, stairsPosition);
	}
}
