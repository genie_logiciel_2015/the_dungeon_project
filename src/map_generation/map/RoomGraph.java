package map_generation.map;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author Lucas Delcros
 *
 * This class represent the map as a graph of rooms
 */
public class RoomGraph {
	private RoomNode start;
	private  HashMap<RoomBuilder, RoomNode> rooms;
	private boolean isBuilded = false;

	/**
	 * 
	 * @param start starting room
	 * @param rooms all the rooms
	 */
	public RoomGraph(RoomBuilder start, ArrayList<RoomBuilder> rooms) {
		this.rooms = new HashMap<RoomBuilder, RoomGraph.RoomNode>();
		for (RoomBuilder room : rooms) {
			this.rooms.put(room, new RoomNode(null, room));
		}
		this.start = findRoom(start);
		if (this.start == null) throw new IllegalArgumentException("Start is not is room list !");
	}
	
	/**
	 *  Finds a room Node according to a RoomBuilder
	 * @param room
	 * @return
	 */
	private RoomNode findRoom(RoomBuilder room){
		return rooms.get(room);
	}
	
	/**
	 * Add room a son of room father
	 * @param room
	 * @param fatherNode
	 */
	private void addRoomTo(RoomNode room, RoomNode fatherNode){
		if(fatherNode.voisins == null) fatherNode.voisins = new ArrayList<RoomGraph.RoomNode>();
		if (!fatherNode.voisins.contains(room)) fatherNode.voisins.add(room);
	}
	
	/**
	 * Links room1 and room2 in graph
	 * @param room1
	 * @param room2
	 */
	public void linkRooms(RoomBuilder room1, RoomBuilder room2){
		RoomNode fatherNode = findRoom(room1);
		RoomNode roomNode = findRoom(room2);
		if (fatherNode == null || roomNode == null) throw new IllegalArgumentException("Wrong args : these rooms are not defined");
		if(isBuilded) isBuilded = false;
		addRoomTo(roomNode, fatherNode);
		addRoomTo(fatherNode, roomNode);
	}
	
	/**
	 * Checks if room1 and room2 are linked
	 * @param room1
	 * @param room2
	 * @return
	 */
	public boolean areLinked(RoomBuilder room1, RoomBuilder room2){
		RoomNode n = findRoom(room1), n2 = findRoom(room2);
		if(n.voisins == null) return false;
		return n.voisins.contains(n2);
	}
	
	/**
	 * 
	 * @return a list containing all the rooms that are not reachable from the starting room
	 */
	public ArrayList<RoomBuilder> getNonReachableRooms(){
		ArrayList<RoomBuilder> res = new ArrayList<RoomBuilder>();
		for(RoomBuilder room : rooms.keySet()){
			if(!roomIsReachable(room)) res.add(room);
		}
		return res;
	}
	
	/**
	 * Inner function for buildLeft function
	 * @param top
	 * @param l
	 */
	private void buildLength_inner(RoomNode top, int l){
		if (top.ltostart > -1 && top.ltostart < l) return;
		top.ltostart = l;
		if(top.voisins == null ) return;
		for (RoomNode voisin : top.voisins) {
			if(voisin.ltostart == -1) buildLength_inner(voisin, l+1);
		}
	}
	
	/**
	 * Builds the length of all the rooms in the graph
	 * The length of a room is it's distance (in rooms) from the starting room
	 */
	private void buildLength(){
		buildLength_inner(start, 0);
		isBuilded = true;
	}
	
	/**
	 * 
	 * @return The farthest room in the graph for the starting room
	 */
	public RoomBuilder getMostFarNode(){
		if(!isBuilded)buildLength();
		RoomNode max = start;
		for (RoomBuilder room : getDeadEnds()) {
			RoomNode temp = rooms.get(room);
			if (temp.ltostart > max.ltostart) max = temp;
		}
		return max.room;
	}
	
	/**
	 * Checks that a room is reachable
	 * @param room
	 * @return
	 */
	public boolean roomIsReachable(RoomBuilder room){
		if(!isBuilded) buildLength();
		return rooms.get(room).ltostart != -1;
	}
	
	/**
	 * Checks that all the rooms are reachable
	 * @return
	 */
	public boolean everyRoomIsReachable(){
		for(RoomBuilder room : rooms.keySet()){
			if(!roomIsReachable(room)) return false;
		}
		return true;
	}
	
	/**
	 * Check whether a room is a dead-end in the graph
	 * @param room
	 * @return
	 */
	private boolean isDeadEnd(RoomBuilder room){
		RoomNode node = rooms.get(room);
		if (node.voisins == null) return true;
		return node.voisins.size() == 1;
	}
	
	/**
	 * 
	 * @return All list of all the dead-ends in the graph
	 */
	public ArrayList<RoomBuilder> getDeadEnds(){
		ArrayList<RoomBuilder> deadEnds = new ArrayList<RoomBuilder>();
		for(RoomBuilder room : rooms.keySet()){
			if (isDeadEnd(room)) deadEnds.add(room);
		}
		return deadEnds;
	}
	
	class RoomNode {
		/**
		 * Inner class of a Node in the Graph of Rooms
		 */
		private ArrayList<RoomNode> voisins;
		private RoomBuilder room;
		private int ltostart = -1;

		public RoomNode(ArrayList<RoomNode> voisins, RoomBuilder room) {
			this.voisins = voisins;
			this.room = room;
		}
	}
}
