package map_generation.map;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import logging.Logging;
import assets.UsedForLoadingSprites;

/**
 * 
 * @author Clement Beauseigneur
 *
 */
public class MapPatterns {

	public static MapPattern[] patterns;

	public static void read() {
		URL url=UsedForLoadingSprites.getInstance().getClass().getResource("./mapGeneration/mapPatterns.txt");
		if(url==null)
			throw new IllegalStateException("MapPatterns loading file assets/mapGeneration/mapPatterns.txt not found");
		try(BufferedReader br= new BufferedReader(
		        new InputStreamReader(url.openStream()))){
			String line;

			// Reading the number of patterns
			line=br.readLine();
			int nbPatterns=Integer.parseInt(line.substring(line.length()-1));
			patterns=new MapPattern[nbPatterns];

			// Reading the monsters id
			line=br.readLine();
			int nbMonsters=Integer.parseInt(line.substring(line.length()-1));
			MapPattern.idMonster=new String[nbMonsters];
			for(int i=0;i<nbMonsters;i++) {
				line=br.readLine();
				MapPattern.idMonster[i]=line;
			}

			// Reading the patterns
			for(int i=0;i<nbPatterns;i++) {
				line=br.readLine();
				line=br.readLine();
				int id=Integer.parseInt(line.substring(line.length()-1));
				String name=br.readLine().substring(6);
				int[] monsters=new int[nbMonsters];
				for(int j=0;j<nbMonsters;j++) {
					line=br.readLine();
					monsters[j]=Integer.parseInt(line);
				}
				patterns[i]=new MapPattern(id,name,monsters);
			}
		}
		catch (Exception e) {
			Logging.getInstance().getLogger().warning(e.toString());
		}
	}

}
