package map_generation.map;

import core.zone.Point;

import java.io.Serializable;

/**
 * 
 * @author Clement Beauseigneur
 *
 */
public class MapPoint implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1900793148607795923L;
	private int i;
	private int j;
	
	public MapPoint(int i,int j) {
		this.i=i;
		this.j=j;
	}
	
	public Point getPoint() {
		return Point.constructFromMap(i,j);
	}
	
	public int getI() {
		return(i);
	}
	
	public int getJ() {
		return(j);
	}
	@Override
	public String toString() {
		return i + " " + j;
	}
}
