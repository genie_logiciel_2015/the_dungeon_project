package map_generation.map;

/**
 * 
 * @author Clement Beauseigneur
 *
 */
public class MapPattern {
	
	private int id;
	private String name;
	private int[] monster;
	private int total;
	
	public static String[] idMonster;
	
	public MapPattern(int id,String name,int[] monster) {
		this.id=id;
		this.name=name;
		this.monster=monster;
		total=0;
		for(int i=0;i<monster.length;i++) {
			total+=monster[i];
		}
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int[] getMonster() {
		return monster;
	}
	
	public int getMonster(int i) {
		if(i>=0 && i<monster.length) return(monster[i]);
		return(0);
	}
	
	public int getTotal() {
		return(total);
	}
	
	public int getNbMonster() {
		return(monster.length);
	}
	
}
