package map_generation.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import map_generation.tiles.Tile;
import map_generation.tiles.TileType;
import core.gamestate.Entity;



/**
 *@author mapgen team
 * Represents a map of tiles
 *
 */
public class Map implements Serializable {

	private static final long serialVersionUID = -7578251078403583045L;
	private Tile[][] map;
	private MapPoint positionPlayerAtStart;
	private MapPoint stairsPosition;
	private Entity[] entities;
	private ArrayList<Surface> corridors;
	private int height = 0, width = 0;

	public Map() {}
	
	public Map(Tile[][] m, ArrayList<Surface> corridors, Entity[] entities , MapPoint positionPlayerAtStart,MapPoint stairsPosition) {
		map = m;
		this.entities = entities;
		this.positionPlayerAtStart=positionPlayerAtStart;
		this.stairsPosition=stairsPosition;
		height = m.length;
		width = m[0].length;
		this.corridors = corridors;
	}

	private void checkBounds(int x, int y){
		if(x < 0 || x > height || y < 0 || y > width) {
			throw new ArrayIndexOutOfBoundsException("Trying to get tile at "+x+" , "+y+" but map has dimension "+height+" , "+width);
		}
	} 
	
	/**
	 *
	 * @return the height of the map in tiles
	 */
	public int getHeight(){return height; }

	/**
	 *
	 * @return the width of the map in tiles
	 */
	public int getWidth(){
		return width;
	}

	public MapPoint getPositionPlayerStart() {
		return(positionPlayerAtStart);
	}

	public MapPoint getPositionStairs() {
		return(stairsPosition);
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return Id of tile at position x, y in the map[][]
	 */
	public TileType getIdAt(int x, int y){
		checkBounds(x, y);
		if(map[x][y] == null){
			throw new NoSuchElementException("Trying to get id at "+x+" , "+y+" but there is no element there ! ");
		}
		return map[x][y].getType();
	}
	
	/**
	 *
	 * @param x
	 * @param y
	 * @return tile at position x, y in map[][]
	 */
	public Tile getTileAt(int x, int y){
		checkBounds(x, y);
		return map[x][y];
	}
	
	/**
	 * This functions checks if every tile is reachable in the map by BFS
	 * @return true if every tile in the map is reachable 
	 */
	public boolean check() {
		// Application of a BFS to find the number of different surfaces in the map. If the map is correct it should be 1.
		int nb=0;
		boolean bb=true;
		boolean[][] mask=new boolean[height][width];
		for(int i=0;i<height;i++) {
			for(int j=0;j<width;j++) {
				if(map[i][j].getType()==TileType.GROUND || map[i][j].getType()==TileType.OBSIDIAN || map[i][j].getType()==TileType.WATER || map[i][j].getType()==TileType.STAIRS) mask[i][j]=true;
				else mask[i][j]=false;
			}
		}
		for(int i=0;i<height;i++) {
			for(int j=0;j<width;j++) {
				if(mask[i][j]) {
					nb++;
					List<MapPoint> l=new ArrayList<>();
					MapPoint element=new MapPoint(i,j);
					l.add(element);
					mask[i][j]=false;
					while(l.size()>0) {
						List<MapPoint> l1=new ArrayList<>();
						for(int k=0;k<l.size();k++) {
							int ii=l.get(k).getI();
							int jj=l.get(k).getJ();
							if(ii>0 && mask[ii-1][jj]) {l1.add(new MapPoint(ii-1,jj));mask[ii-1][jj]=false;}
							if(ii<height-1 && mask[ii+1][jj]) {l1.add(new MapPoint(ii+1,jj));mask[ii+1][jj]=false;}
							if(jj>0 && mask[ii][jj-1]) {l1.add(new MapPoint(ii,jj-1));mask[ii][jj-1]=false;}
							if(jj<width-1 && mask[ii][jj+1]) {l1.add(new MapPoint(ii,jj+1));mask[ii][jj+1]=false;}
						}
						l.clear();
						for(int k=0;k<l1.size();k++) l.add(l1.get(k));
					}
				}
			}
		}
		if(nb!=1) bb=false;
		for(int i=0;i<corridors.size();i++) {
			for(int j=0;j<corridors.size();j++) {
				if(i!=j) {
					if(SurfacesMapGeneration.distanceBetweenTwoSurface(corridors.get(i),corridors.get(j))<1) {
						bb=false;
					}
				}
			}
		}
		return(bb);
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				sb.append(map[i][j]);
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

	public Tile getTileAt(MapPoint mapPoint) {
		return getTileAt(mapPoint.getI(),mapPoint.getJ());
	}

	public Entity[] getEntities() {
		return(entities);
	}
}
