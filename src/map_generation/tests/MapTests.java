package map_generation.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import map_generation.map.Map;
import map_generation.map.MapGeneration;
import map_generation.tiles.TileType;

import org.junit.Test;

import core.gamestate.Entity;
import core.gamestate.SpeciesArray;
import core.zone.Point;
/**
 * 
 * @author Lucas Delcros
 * Class of JUnit tests for Map Generation
 */
public class MapTests {
	
	/**
	 * Test if there is entities in a Map
	 */
	@Test 
	public void testThereIsEntities(){
		for (int i = 0; i < 100; i++) {
			Map map = MapGeneration.mapGeneration(20, 20);
			assertTrue("Error only "+map.getEntities().length, map.getEntities().length > 3);
		}
	}
	
	/**
	 * Test if every room is reachable in the map by checking that every tile is reachable
	 */
	@Test
	public void testHardEveryRoomAreReachable(){
		Map map = MapGeneration.mapGeneration(20, 1000);
		assertTrue(map.check());
	}
	
	/**
	 * Test that there is no Ronflex that spawnw on water
	 */
	@Test
	public void testNoRonflexOnWater(){
		Map map = MapGeneration.mapGeneration(20, 100);
		for(Entity entity : map.getEntities()){
			Point pt = Point.construct(entity.getX(), entity.getY());
			if(entity.getName().equals("Ronflex"))assertFalse("An entity has spawn on water !",map.getTileAt(pt.toMapPoint()).getType() == TileType.WATER);
		}
	}
	
	/**
	 * Tests that a map has a starting and an ending room
	 */
	@Test
	public void testMapHasStartEnd(){
		Map map = MapGeneration.mapGeneration(20, 100);
		assertFalse("The Map doesn't cointains any stairs !", map.getPositionStairs() == null || map.getPositionPlayerStart() == null);
	}
	
	/**
	 * Test that the player doesn't spawn on water 
	 */
	@Test
	public void testPlayerNotOnWater(){
		Map map = MapGeneration.mapGeneration(20, 100);
		assertFalse("The player has spawn on water !", map.getTileAt(map.getPositionPlayerStart()).getType() == TileType.WATER);
	}
	
	/**
	 * Tests that Ponyta entities can spawn on water
	 */
	@Test
	public void ponytaCanSpawnOnLava(){
		assertTrue("Ponyta is supossed to be capable to spawn on lava !", SpeciesArray.canSpawn(TileType.WATER, "Ponyta"));
	}

}
