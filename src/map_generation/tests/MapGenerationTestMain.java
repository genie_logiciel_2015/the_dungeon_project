package map_generation.tests;

import logging.Logging;
import map_generation.map.Map;
import map_generation.map.MapGeneration;

/**
 * @author Lucas Delcros
 * Shows a Map in console
 *
 */
public class MapGenerationTestMain {
	public static void main(String[] args) {
		long a = System.currentTimeMillis();
		Map map = MapGeneration.mapGeneration(10, 5);
		Logging.getInstance().getLogger().info("map was generated in "+(System.currentTimeMillis()-a)+" ms");
//		Map map = (new MapBuilder(0)).buildTwoRoomsMap();
		for (int i = 0; i < map.getHeight(); i++) {
			for (int j = 0; j < map.getWidth(); j++) {
				String s;
				switch(map.getTileAt(i, j).getType()){
					case EMPTY : s = " ";
						break;
					case WALL : s = "W";
						break;
					case GROUND: s = "G";
						break;
					case WATER: s = "L";
						break;
					case TORCH: s = "T";
						break;
					case OBSIDIAN: s = "O";
						break;
					case CHEST: s = "C";
						break;
					default: s = "#";
						break; 
				}
				System.out.print(s);
			}
		}
		
	}
}
