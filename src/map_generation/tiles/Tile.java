package map_generation.tiles;

import java.io.Serializable;


/**
 * @author Lucas Delcros
 * Represents a single tile of the map
 *
 */

public class Tile implements Serializable{ // No need for abstract implementation

	/**
	 *
	 */
	private static final long serialVersionUID = -857708415876341217L;

	public TilePropertyVector tpv;

	private final TileType type;

	/**
	 *
	 * @param ttype : id of the tile
	 * @param tpv : An ArrayList of the TileProperty that the Tile must have
	 */
	public Tile(TileType ttype,TilePropertyVector tpv) {
		this.type=ttype;
		this.tpv=tpv;
	}


	public TileType getType(){
		return type;
	}

	public TilePropertyVector getTilePropertyVector() {
		return tpv;
	}

	@Override
	public String toString() {
		return type.getShortName() + tpv.shortString();
	}

	public char term_return(){
		switch( type) {
			case EMPTY:
				return 'E';
			case GROUND:
				return ' ';
			case WALL:
				return '|';
			case STAIRS:
				return 'S';
			case WATER:
				return 'W';
			default:
				return 'D';

		}
	}
}
