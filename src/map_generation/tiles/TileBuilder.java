package map_generation.tiles;

/**
 * @author Lucas Delcros
 * Builder for class Tile
 * Used by MapBuilder to build maps
 *
 */
public class TileBuilder {
	private TileType type = TileType.EMPTY;
	
	public static Tile[][] buildTiles(TileBuilder[][] tilebs){
		Tile[][] tiles = new Tile[tilebs.length][tilebs[0].length];
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[i].length; j++) {
				tiles[i][j] = tilebs[i][j].build();
			}
		}
		return tiles;
	}
	public TileType getType() {
		return type;
	}
	public void setType(TileType type) {
		this.type = type;
	}
	public Tile build(){
		return new Tile(type, type.getTilePropertyVector());
	}
}
