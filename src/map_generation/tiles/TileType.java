package map_generation.tiles;

import map_generation.tiles.TilePropertyVector.TileProperty;

/**
 * This enum is used to define all type of tile.
 * @author Guerquin Arnaud
 *
 */
public enum TileType{
	EMPTY, GROUND, WALL, STAIRS, WATER, TORCH, CHEST, OBSIDIAN;
	
	private TilePropertyVector tpv;
	private String shortName;
	
	static{
		EMPTY.tpv=new TilePropertyVector().addProperty(TileProperty.HOLE);
		EMPTY.shortName=" ";
		GROUND.tpv=new TilePropertyVector().addProperty(TileProperty.SOLID);
		GROUND.shortName="G";
		WALL.tpv=new TilePropertyVector().addProperty(TileProperty.IMPASSABLE);
		WALL.shortName="X";
		STAIRS.tpv=new TilePropertyVector().addProperty(TileProperty.SOLID);
		STAIRS.shortName="S";
		WATER.tpv=new TilePropertyVector().addProperty(TileProperty.LIQUID);
		WATER.shortName="W";
		TORCH.tpv=new TilePropertyVector().addProperty(TileProperty.IMPASSABLE);
		TORCH.shortName="T";
		CHEST.tpv=new TilePropertyVector().addProperty(TileProperty.IMPASSABLE);
		CHEST.shortName="C";
		OBSIDIAN.tpv=new TilePropertyVector().addProperty(TileProperty.SOLID);
		OBSIDIAN.shortName="O";
	}
	
	public TilePropertyVector getTilePropertyVector(){
		return tpv.clone();
	}
	
	public String getShortName(){
		return shortName;
	}
	
}