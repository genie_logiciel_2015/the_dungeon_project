package map_generation.tiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import core.gamestate.SpeciesArray;
import logging.Logging;

/**
 * This class is used to define all the property of a tile. Used for collision with entities.
 * @author Guerquin Arnaud
 *
 */
public class TilePropertyVector implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4018038601817748913L;

	public enum TileProperty{
		IMPASSABLE,SOLID,LIQUID,HOLE
	}
	
	private static int arraySize=TileProperty.values().length/32+1;
	
	private int propertyArray[];
	
	/**
	 * Constructs a new TilePropertyVector with its properties initialized to those in list.
	 * @param list the list of properties to add.
	 */
	public TilePropertyVector(List<TileProperty> list){
		propertyArray=new int[arraySize];
		addProperty(list);
	}
	
	/**
	 * Construct a new {@link TilePropertyVector} with no property.
	 */
	public TilePropertyVector(){
		propertyArray=new int[arraySize];
	}
	
	/* Used by the clone function */
	private TilePropertyVector(int[] propertyArray) {
		this.propertyArray=propertyArray;
	}

	/**
	 * Adds a new TileProperty to this.
	 * @param property the property to add.
	 * @return this.
	 */
	public TilePropertyVector addProperty(TileProperty property){
		int tmp=property.ordinal()>>>5; /* /32 */
		propertyArray[tmp]|=1<<(property.ordinal()&31);
		return this;
	}
	
	/**
	 * Adds a list of TileProperty to this.
	 * @param list the list of properties to add.
	 * @return this.
	 */
	public TilePropertyVector addProperty(List<TileProperty> list){
		for(TileProperty e : list){
			addProperty(e);
		}
		return this;
	}
	
	/**
	 * Removes the property to this if it possesses it.
	 * @param property the property to remove.
	 * @return this.
	 */
	public TilePropertyVector removeProperty(TileProperty property){
		int tmp=property.ordinal()>>>5; /* /32 */
		propertyArray[tmp]&=(~(1<<(property.ordinal()&31)));
		return this;
	}
	
	/**
	 * Removes some TileProperty to this if it possesses them.
	 * @param list the list of properties to remove.
	 * @return this.
	 */
	public TilePropertyVector removeProperty(List<TileProperty> list){
		for(TileProperty e : list){
			removeProperty(e);
		}
		return this;
	}
	
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<arraySize;i++){
			for(int j=0;j<32;j++){
				if(0!=((1<<j)&propertyArray[i])){
					sb.append(TileProperty.values()[j+(i<<5)]+" ");
				}
			}
		}
		return sb.toString();
	}

	public String shortString(){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<arraySize;i++){
			for(int j=0;j<32;j++){
				if(0!=((1<<j)&propertyArray[i])){
					String tmp;
					switch(TileProperty.values()[j+(i<<5)]){
					case HOLE:
						tmp="H";
						break;
					case IMPASSABLE:
						tmp="I";
						break;
					case LIQUID:
						tmp="L";
						break;
					case SOLID:
						tmp="S";
						break;
					default:
						throw new IllegalStateException("Unknow TileProperty.");
					}
					sb.append(tmp + " ");
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Return true if the called vector is included in the parameter vector.
	 * @param vector the vector to test inclusion in.
	 * @return true if this is included in vector
	 */
	public boolean isIncluded(TilePropertyVector vector){
		for(int i=0;i<arraySize;i++){
			if((vector.propertyArray[i]&propertyArray[i])!=propertyArray[i])
				return false;
		}
		return true;
	}

	/**
	 * Return true if the two vector have at least one common property.
	 * @param vector the vector to compare this to.
	 * @return true if the two vector have at least one common property.
	 */
	public boolean intersect(TilePropertyVector vector){
		for(int i=0;i<arraySize;i++){
			if((vector.propertyArray[i]&propertyArray[i])!=0){
				return true;
			}
		}
		return false;
	}

	@Override
	public TilePropertyVector clone() {
		return new TilePropertyVector(Arrays.copyOf(propertyArray, arraySize));
	}

	public static void main(String[] args) {
		Logger LOGGER = Logging.getInstance().getLogger();
		TilePropertyVector tpv=new TilePropertyVector();
		TilePropertyVector tpv2=new TilePropertyVector();
		tpv.addProperty(TileProperty.SOLID);
		tpv.addProperty(TileProperty.IMPASSABLE);
		LOGGER.info(tpv.toString());
		tpv.removeProperty(TileProperty.SOLID);
		LOGGER.info(tpv.toString());
		tpv.addProperty(TileProperty.SOLID);
		tpv2.addProperty(TileProperty.SOLID);
		LOGGER.info(tpv2.isIncluded(tpv)+"");
		LOGGER.info(tpv.isIncluded(tpv2)+"");
	}

	public static TilePropertyVector readFromFile(BufferedReader br) throws IOException {
		TilePropertyVector tpv=new TilePropertyVector();
		int propertyCount=Integer.parseInt(br.readLine());
		SpeciesArray.lineError++;
		for(int i=0;i<propertyCount;i++)
			tpv.addProperty(TileProperty.valueOf(br.readLine()));
		return tpv;
	}

}
